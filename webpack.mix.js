const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/backend/app.js', 'public/js/backend');
// mix.js('resources/js/front/app.js', 'public/js/front');

mix.js('resources/js/front/app.js','public/js/front/app.js')
    .js('resources/js/backend/app.js','public/js/backend/app.js');
