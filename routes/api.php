<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/Config/{name}', 'BackendController@getConfig');
Route::get('/Setting/{group}/{name}', 'BackendController@getSetting');

Route::name('Backend.')->middleware('jwt.user')->group(function (){
    Route::get('/Room', 'RoomController@get');
    Route::get('/Room/Extra', 'RoomController@getExtra');
    Route::post('/Room/Extra', 'RoomController@UpdateExtra');
    Route::post('/Room/Create', 'RoomController@Create');
    Route::post('/Room/Update', 'RoomController@Update');
    Route::post('/Room/UpdateRooms', 'RoomController@UpdateRooms');
    Route::delete('/Room/Delete', 'RoomController@Delete');


    Route::get('/DailyRoom','DailyRoomController@get');
    Route::post('/DailyRoom/UpdateOrCreate','DailyRoomController@UpdateOrCreate');
    Route::post('/DailyRoom/UpdateOrCreateScope','DailyRoomController@UpdateOrCreateScope');


    Route::get('/Promotion', 'PromotionController@get');
    Route::get('/Promotion/filters', 'PromotionController@getfilters');

    Route::post('/Promotion/Update', 'PromotionController@Update');
    Route::post('/Promotion/UpdatePromotions', 'PromotionController@UpdatePromotions');
    Route::post('/Promotion/Create', 'PromotionController@Create');
    Route::delete('/Promotion/Delete', 'PromotionController@Delete');


    Route::get('/Tag', 'TagController@get');
    Route::post('/Tag/Create', 'TagController@Create');
    Route::delete('/Tag/Delete', 'TagController@Delete');
    Route::post('/Tag/UpdateTags', 'TagController@UpdateTags');


    Route::get('/DailyPromotion','DailyPromotionController@get');
    Route::post('/DailyPromotion/UpdateOrCreate','DailyPromotionController@UpdateOrCreate');
    Route::post('/DailyPromotion/UpdateOrCreateScope','DailyPromotionController@UpdateOrCreateScope');


    Route::get('/Remain','RemainController@get');

    Route::get('/Rule','RuleController@get');
    Route::post('/Rule','RuleController@Update');

    Route::get('/ExtraShop','ExtraShopController@get');
    Route::post('/ExtraShop/UpdateExtraShops','ExtraShopController@UpdateExtraShops');
    Route::post('/ExtraShop/Update','ExtraShopController@Update');
    Route::post('/ExtraShop/Create','ExtraShopController@Create');
    Route::delete('/ExtraShop/Delete', 'ExtraShopController@Delete');

    Route::get('/ExtraCar','ExtraCarController@get');
    Route::post('/ExtraCar/UpdateExtraCars','ExtraCarController@UpdateExtraCars');
    Route::post('/ExtraCar/Update','ExtraCarController@Update');
    Route::post('/ExtraCar/Create','ExtraCarController@Create');
    Route::delete('/ExtraCar/Delete', 'ExtraCarController@Delete');



    Route::get('/DiscountCode', 'DiscountCodeController@get');
    Route::post('/DiscountCode/Update', 'DiscountCodeController@Update');
    Route::post('/DiscountCode/UpdateDiscountCodes', 'DiscountCodeController@UpdateDiscountCodes');
    Route::post('/DiscountCode/Create', 'DiscountCodeController@Create');
    Route::delete('/DiscountCode/Delete', 'DiscountCodeController@Delete');


    Route::post('/Members','MemberController@get');
    Route::post('/Members/updateMembers','MemberController@updateMembers');
    Route::post('/Members/update','MemberController@update');
    Route::post('/Members/{id}','MemberController@getMember');
    Route::delete('/Members/Delete','MemberController@Delete');


    Route::post('/Orders','OrderController@get');
    Route::post('/Orders/update','OrderController@update');
    Route::post('/Orders/{Num}','OrderController@getOrder');

    Route::get('/MailSetting','MailSettingController@get');
    Route::post('/MailSetting/update','MailSettingController@update');
    Route::post('/MailSetting/valid','MailSettingController@valid');

    Route::get('/System','SystemController@get');
    Route::post('/System/update','SystemController@update');


    Route::get('/Role','RoleController@get');

    Route::post('/User/create','UserController@create');
    Route::post('/User/updateState','UserController@updateState');
    Route::delete('/User/delete','UserController@deleteUser');
    Route::get('/User/{account}','UserController@getUser');
    Route::post('/User/update','UserController@update');

    Route::get('/Permission','PermissionController@get');
    Route::get('/Permission/{name}','PermissionController@getHasPermissions');
    Route::post('/Permission/update','PermissionController@updateHasPermissions');

});
Route::post('/MemberChecker', 'BackendController@MemberChecker')->name('MemberChecker');






Route::get('/Licenses','LicenseController@get');
Route::get('/init', 'TestController@init');
Route::post('/calc', 'TestController@calc');
Route::post('/order/store', 'TestController@store');

//後台登入
Route::name('Jwt.')->prefix('Jwt')->group(function () {
    Route::post('/login', 'JwtController@login')->name('login');
    Route::post('/updateProfile', 'JwtController@updateProfile')->name('updateProfile');

});


Route::name('Front.')->prefix('Front')->group(function () {
    Route::post('/Searching', 'FrontController@Searching')->name('Searching');
    Route::post('/SearchingByRoom', 'FrontController@SearchingByRoom')->name('SearchingByRoom');
    Route::post('/Remain', 'FrontController@Remain')->name('Remain');
    Route::post('/Rule', 'FrontController@Rule')->name('Rule');
    Route::post('/ExtraShopFromRoom', 'FrontController@ExtraShopFromRoom')->name('ExtraShopFromRoom');
    Route::post('/ExtraCarFromRoom', 'FrontController@ExtraCarFromRoom')->name('ExtraCarFromRoom');
    Route::post('/CheckingDiscountCode', 'FrontController@CheckingDiscountCode')->name('CheckingDiscountCode');
    Route::post('/CheckingOrder', 'FrontController@CheckingOrder')->name('CheckingOrder');

    // Group
    Route::post('/PaymentSelect', 'FrontController@PaymentSelect')->name('PaymentSelect');
    Route::post('/UpdatePayment', 'FrontController@UpdatePayment')->name('UpdatePayment');

    Route::post('/ATMInfo', 'FrontController@ATMInfo')->name('ATMInfo');

    Route::post('/Login', 'FrontController@Login')->name('login');
    Route::post('/Register', 'FrontController@Register')->name('register');
    Route::post('/Forgot', 'FrontController@Forgot')->name('forgot');
    Route::post('/UpdateMemberBasic', 'FrontController@UpdateMemberBasic')->name('UpdateMemberBasic');
    Route::post('/UpdateMemberPassword', 'FrontController@UpdateMemberPassword')->name('UpdateMemberPassword');
    Route::post('/UpdateMemberEmail', 'FrontController@UpdateMemberEmail')->name('UpdateMemberEmail');
    Route::post('/MemberOrderRecord', 'FrontController@MemberOrderRecord')->name('MemberOrderRecord');
    Route::post('/OrderRecord', 'FrontController@OrderRecord')->name('OrderRecord');



    //檢查登入用
    Route::post('/MemberChecker', 'FrontController@MemberChecker')->name('MemberChecker');

});
