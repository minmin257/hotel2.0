<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomHasExtraCar extends Model
{
    protected $guarded = [];
}
