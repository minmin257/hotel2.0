<?php

namespace App\Http\Middleware;

use App\Permission;
use Closure;
use Illuminate\Support\Facades\Auth;

class HasPermission
{
    public function handle($request, Closure $next,$PermissionName,String $PermissionTypes = 'C,R,U,D')
    {
        $PermissionTypes = explode(',', $PermissionTypes);
        $PermissionIds = Permission::whereName($PermissionName)->whereIn('type',$PermissionTypes)->pluck('id')->toArray();
        count($PermissionIds) == count($PermissionTypes) ?? abort(403, "無此權限");

        if(in_array($PermissionIds, $this->user()->Permissions()->get(['id'])->pluck('id')->toArray()))
        {
            return $next($request);
        }
        abort(403,"無權限");
    }

    private function user()
    {
        return Auth::guard('web')->user() ?? abort(403, "尚未登入");
    }
}
