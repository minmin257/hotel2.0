<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtUserMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->checkForToken($request);

        try {
            if(auth('api')->user()){
                return $next($request);
            }
            throw new UnauthorizedHttpException('jwt-user', '未登入');
        }
        catch (TokenExpiredException  $e) {
            try {
                $token = auth('api')->refresh();
                return $this->setAuthenticationHeader($next($request), $token);
            }catch (JWTException $exception) {
                // 如果有異常，代表 refresh 也過期了，需要重新登入。
                throw new UnauthorizedHttpException('jwt-user', $exception->getMessage());
            }
        }
    }
}
