<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RemainRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'months' => ['required','date_format:Y-m'],
            'room' => ['required','exists:rooms,id'],
        ];
    }

    public function messages()
    {
        return [
            'months.required' => '查詢月份 必填',
            'months.date_format' => '查詢月份 格式錯誤',
            'room.required' => '房型 必填',
            'room.exists' => '房型 不存在',
        ];
    }
}
