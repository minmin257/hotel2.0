<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeleteDiscountCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'DiscountCode_id' => ['required','exists:discount_codes,id']
        ];
    }
    public function messages()
    {
        return [
            'DiscountCode_id.exists' =>  '折扣碼不存在'
        ];
    }

}
