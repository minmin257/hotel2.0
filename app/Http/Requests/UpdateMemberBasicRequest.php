<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMemberBasicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'Name' => ['required'],
            'Phone' => ['required'],
            'Sex' => ['nullable','boolean'],
        ];
    }

    public function messages()
    {
        return [
            'Name.required' => '姓名必填',
            'Phone.phone' => '電話格式錯誤',
            'Sex.boolean' => '性別格式錯誤',
        ];
    }
}
