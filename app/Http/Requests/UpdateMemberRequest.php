<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => ['required','exists:members,id'],
            'email' => ['required','email','unique:members,email,'.$this->id],
            'password' => ['nullable','regex:/[a-zA-Z0-9]$/u','max:20','min:6'],
            'Name' => ['required'],
            'IdNumber' => ['required','unique:members,IdNumber,'.$this->id],
            'Phone' => ['required'],
            'Sex' => ['nullable','boolean'],
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'E-mail 必填',
            'email.email' => 'E-mail 格式錯誤',
            'email.unique' => 'E-mail 不得重複',
            'password.required' => '密碼 必填',
            'password.regex' => '密碼 格式錯誤',
            'password.max' => '密碼 長度上限20',
            'password.min' => '密碼 長度需大於6位',
            'Name.required' => '姓名必填',
            'IdNumber.required' => '身分證必填',
            'IdNumber.unique' => '身分證重複',
            'Phone.phone' => '電話格式錯誤',
            'Sex.boolean' => '性別格式錯誤',
        ];
    }
}
