<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => ['required'],
            'password' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'E-mail 必填',
            'password.required' => '密碼 必填'
        ];
    }
}
