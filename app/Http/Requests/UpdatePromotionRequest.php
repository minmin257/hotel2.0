<?php

namespace App\Http\Requests;

use App\Repositories\LicenseRepository;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePromotionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param LicenseRepository $license
     * @return array
     */
    public function rules(LicenseRepository $license)
    {
        $rules =  [
            'id' => ['required','exists:promotions,id'],
            'room_id' => ['required','exists:rooms,id'],
            'Name' => ['required'],
            'State' => ['required','boolean'],
            'DefaultPrice' => ['required','numeric','min:0'],
        ];
        if($EarlyBirdLicnese = $license->check('早鳥折扣'))
        {
            $rules['DefaultAllowEarlyBird'] = ['required','boolean'];
            $rules['DefaultEarlyBirdPrice'] = ['required','numeric'];
            $rules['DefaultEarlyBirdDays'] = ['required','numeric'];
        }
        return $rules;
    }
    public function messages()
    {
        return [
            'id.required' => '活動必填',
            'id.exists' => '活動不存在',
            'room_id.required' => '適用房型必選',
            'room_id.exists' => '適用房型不存在',
            'State.required' => '啟用必填',
            'State.boolean' => '啟用格式不正確',
            'Name.required' => '名稱必填',
            'DefaultPrice.required' => '預設基本價必填',
            'DefaultPrice.numeric' => '預設基本價輸入格式錯誤',
            'DefaultPrice.min' => '預設基本價最小值為 0',

            'DefaultAllowEarlyBird.required' => '預設早鳥必填',
            'DefaultEarlyBirdPrice.required' => '預設早鳥價格必填',
            'DefaultEarlyBirdPrice.numeric' => '預設早鳥價格輸入格式錯誤',
            'DefaultEarlyBirdDays.required' => '預設早鳥天數必填',
            'DefaultEarlyBirdDays.numeric' => '預設早鳥天數輸入格式錯誤',
        ];
    }
}
