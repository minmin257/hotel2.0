<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDiscountCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'Name' => ['required'],
            'Discount' => ['required','numeric','min:0'],
            'Remains' => ['required','numeric','min:0'],
            'Code' => ['required'],
            'id' => ['required','exists:discount_codes,id'],
            'State' => ['required','boolean'],
            'Sort' => ['required','numeric','min:0'],
            'Allow_Start' => ['required','date_format:Y-m-d\TH:i'],
            'Allow_End' => ['required','date_format:Y-m-d\TH:i','after:Allow_Start'],
            'Use_Start' => ['required','date_format:Y-m-d\TH:i'],
            'Use_End' => ['required','date_format:Y-m-d\TH:i','after:Use_Start'],
        ];
        return  $rules;
    }

    public function messages()
    {
        return [
            'Name.required' => '名稱必填',
            'Discount.required' => '折價金必填',
            'Discount.numeric' => '折價金格式不正確',
            'Discount.min' => '折價金最小值為 0',

            'Remains.required' => '剩餘數量必填',
            'Remains.numeric' => '剩餘數量格式不正確',
            'Remains.min' => '剩餘數量最小值為 0',

            'Code.required' => '優惠碼必填',

            'id.required' => '折扣代碼必填',
            'id.exists' => '折扣代碼不存在',

            'State.required' => '啟用必填',
            'State.boolean' => '啟用輸入格式錯誤',
            'Sort.required' => '優先度必填',
            'Sort.numeric' => '優先度輸入格式錯誤',
            'Sort.min' => '優先度最小值為 0',

            'Allow_Start.required' => '開放時段(起) 必填',
            'Allow_Start.date_format' => '開放時段(起) 格式不正確',
            'Allow_End.required' => '開放時段(訖) 必填',
            'Allow_End.date_format' => '開放時段(訖) 格式不正確',
            'Allow_End.after' => '開放時段(訖) 需大於 (起)',

            'Use_Start.required' => '使用時段(起) 必填',
            'Use_Start.date_format' => '使用時段(起) 格式不正確',
            'Use_End.required' => '使用時段(訖) 必填',
            'Use_End.date_format' => '使用時段(訖) 格式不正確',
            'Use_End.after' => '使用時段(訖) 需大於 (起)',
        ];
    }
}
