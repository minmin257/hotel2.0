<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'StartDate' => ['required','date_format:Y/m/d'],
            'EndDate' => ['required','date_format:Y/m/d','after:'.$this->StartDate],
            'Adults' => ['numeric','min:1','max:4'],
            'Childrens' => ['numeric','min:0','max:4'],
            'Babys' => ['numeric','min:0','max:4'],
        ];
    }

    public function messages()
    {
        return [
            'StartDate.required' => 'Check in 必填',
            'StartDate.date_format' => 'Check in 格式錯誤',
            'EndDate.required' => 'Check out 必填',
            'EndDate.date_format' => 'Check out 格式錯誤',
            'EndDate.after' => 'Check out 需大於 Check in',
            'Adults.min' => '成人數量 不可小於1',
            'Adults.max' => '成人數量 不可大於4',
            'Childrens.min' => '小孩數量 不可小於0',
            'Childrens.max' => '小孩數量 不可大於4',
            'Babys.min' => '嬰兒數量 不可小於0',
            'Babys.max' => '嬰兒數量 不可大於4',
        ];
    }
}
