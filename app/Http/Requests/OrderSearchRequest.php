<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'Num' => ['required','exists:orders,Num'],
        ];
    }

    public function messages()
    {
        return [
            'Num.required' => '訂單流水號 必填',
            'Num.exists' => '找不到該訂單流水號',
        ];
    }
}
