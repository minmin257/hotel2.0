<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateExtraShopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'Name' => ['required'],
            'Price' => ['required','numeric','min:0'],
            'Sort' => ['required','numeric','min:0'],
            'State' => ['required','boolean'],
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'Name.required' => '名稱必填',
            'Price.required' => '價格必填',
            'Price.numeric' => '價格格式不正確',
            'Price.min' => '價格最小值為 0',
            'State.required' => '啟用必填',
            'State.boolean' => '啟用格式不正確',
            'Sort.required' => '優先度必填',
            'Sort.numeric' => '優先度輸入格式錯誤',
            'Sort.min'=> '優先度最小值為 0',
        ];
    }

}
