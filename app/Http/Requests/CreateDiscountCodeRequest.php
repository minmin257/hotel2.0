<?php

namespace App\Http\Requests;

use App\Repositories\LicenseRepository;
use Illuminate\Foundation\Http\FormRequest;

class CreateDiscountCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param LicenseRepository $license
     * @return array
     */
    public function rules(LicenseRepository $license)
    {
        $rules =  [
            'Name' => ['required'],
            'Code' => ['required','unique:discount_codes,Code'],
            'Discount' => ['required','numeric','min:0'],
            'Remains' => ['required','numeric','min:0'],
            'Allow_Start' => ['required','date_format:Y-m-d\TH:i'],
            'Allow_End' => ['required','date_format:Y-m-d\TH:i','after:Allow_Start'],
            'Use_Start' => ['required','date_format:Y-m-d\TH:i'],
            'Use_End' => ['required','date_format:Y-m-d\TH:i','after:Use_Start'],
            'State' => ['required','boolean'],
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'Name.required' => '名稱必填',
            'Code.required'  => '優惠碼必填',
            'Code.unique'  => '優惠碼不得重複',
            'Discount.required' => '折扣金必填',
            'Discount.numeric' => '折扣金格式不正確',
            'Discount.min' => '折扣金最小值為 0',
            'Remains.required' => '發放數量必填',
            'Remains.numeric' => '發放數量格式不正確',
            'Remains.min' => '發放數量最小值為 0',

            'Allow_Start.required' => '開放時段(起) 必填',
            'Allow_Start.date_format' => '開放時段(起) 格式不正確',
            'Allow_End.required' => '開放時段(訖) 必填',
            'Allow_End.date_format' => '開放時段(訖) 格式不正確',
            'Allow_End.after' => '開放時段(訖) 需大於 (起)',

            'Use_Start.required' => '使用時段(起) 必填',
            'Use_Start.date_format' => '使用時段(起) 格式不正確',
            'Use_End.required' => '使用時段(訖) 必填',
            'Use_End.date_format' => '使用時段(訖) 格式不正確',
            'Use_End.after' => '使用時段(訖) 需大於 (起)',

            'State.required' => '啟用必填',
            'State.boolean' => '啟用格式不正確',
        ];
    }
}
