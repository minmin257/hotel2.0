<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return  [
            'state' => ['required','boolean'],
            'account' => ['required','unique:users,account'],
            'password' => ['required','between:6,20','regex:/(^[A-Za-z0-9 ]+$)+/'],
            'name' => ['required'],
            'role_id' => ['required','exists:roles,id']
        ];
    }

    public function messages()
    {
        return [
            'state.required' => ['狀態必填'],
            'account.required' => ['帳號必填'],
            'account.unique' => ['帳號不得重複'],
            'password.required' => ['密碼必填'],
            'password.regex' => ['密碼只能輸入 a-z , 0-9'],
            'password.between' => '密碼長度為 6 - 20碼 ',
            'name.required' => ['名稱必填'],
            'role_id.required' => ['等級必選'],
            'role_id.exists' => ['等級不存在'],
        ];
    }
}
