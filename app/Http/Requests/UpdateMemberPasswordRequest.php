<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMemberPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'password' => ['required','regex:/[a-zA-Z0-9]$/u','max:20','min:6'],
        ];
    }

    public function messages()
    {
        return [
            'password.required' => '密碼 必填',
            'password.regex' => '密碼 格式錯誤',
            'password.max' => '密碼 長度上限20',
            'password.min' => '密碼 長度需大於6位',
        ];
    }
}
