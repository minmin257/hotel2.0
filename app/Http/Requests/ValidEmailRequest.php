<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account' => ['required','email'],
            'password' => ['required','min:6']
        ];
    }

    public function messages()
    {
        return [
            'account.required' => '請輸入帳號',
            'account.email' => '帳號格式錯誤',
            'password.required' => '請輸入密碼',
            'password.min' => '密碼最少須6位數',
        ];
    }
}
