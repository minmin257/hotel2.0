<?php

namespace App\Http\Requests;

use App\Repositories\LicenseRepository;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'id' => ['required','exists:rules,id'],
            'DefaultExtraPeoplePrice' => ['required','numeric','min:0'],
            'DefaultExtraBedPrice' => ['required','numeric','min:0'],
        ];
        return $rules;
    }
    public function messages()
    {
        return [

            'DefaultExtraPeoplePrice.required' => '加人費用必填',
            'DefaultExtraPeoplePrice.numeric' => '加人費用輸入格式錯誤',
            'DefaultExtraPeoplePrice.min' => '加人費用最小值為 0',

            'DefaultExtraBedPrice.required' => '加床費用必填',
            'DefaultExtraBedPrice.numeric' => '加床費用輸入格式錯誤',
            'DefaultExtraBedPrice.min' => '加床費用最小值為 0',
        ];
    }
}
