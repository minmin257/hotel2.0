<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Num' => ['required','exists:orders,Num'],
            'Booking_Email' => ['required','email'],
            'Booking_Name' => ['required'],
            'Booking_IdNumber' => ['required'],
            'Booking_Phone' => ['required'],
            'Guest_Email' =>  ['required','email'],
            'Guest_Name' => ['required'],
            'Guest_IdNumber' => ['required'],
            'Guest_Phone' => ['required'],
            'payment_id' => ['nullable','exists:payment_methods,id'],
            'TaxNumber' => ['nullable','numeric'],
        ];
    }

    public function messages()
    {
        return [
            'Num.required' => '訂單編號必填',
            'Num.exists' => '訂單編號不存在',
            'Booking_Email.required' => '訂購人信箱必填',
            'Booking_Email.email' => '訂購人信箱格式不正確',
            'Booking_Name.required' => '訂購人姓名必填',
            'Booking_IdNumber.required' => '訂購人身分證必填',
            'Booking_Phone.required' => '訂購人電話必填',
            'Guest_Email.required' => '入住人信箱必填',
            'Guest_Email.email' => '入住人信箱格式不正確',
            'Guest_Name.required' => '入住人姓名必填',
            'Guest_IdNumber.required' => '入住人身分證必填',
            'Guest_Phone.required' => '入住人電話必填',
        ];
    }
}
