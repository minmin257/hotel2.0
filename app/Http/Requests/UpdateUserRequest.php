<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'account' => ['required','exists:users,account'],
            'name' => ['required'],
            'password' => ['nullable','regex:/(^[A-Za-z0-9 ]+$)+/','between:6,20'],
            'State' => ['sometimes','required'],
            'role_id' => ['sometimes','required','exists:roles,id'],
        ];
    }

    public function messages()
    {
        return [
            'account.required' => ['帳號不存在'],
            'account.exists' => ['帳號不存在'],
            'name.required' => ['名稱'],
            'password.regex' => ['密碼只能輸入 a-z , 0-9'],
            'password.between' => '密碼長度為 6 - 20碼 ',
            'State.required' => ['啟用狀態必填'],
            'role_id.required' => ['等級必填'],
            'role_id.exists' => ['等級不存在']
        ];
    }
}
