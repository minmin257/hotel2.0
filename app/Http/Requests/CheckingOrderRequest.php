<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckingOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'form.Booking.Email' => ['required','email'],
           'form.Booking.Name' => ['required'],
           'form.Booking.IdNumber' => ['required'],
           'form.Booking.Phone' => ['required'],
            'form.Guest.Email' => ['required_if:form.BookingSameGuest,false','nullable','email'],
            'form.Guest.Name' => ['required_if:form.BookingSameGuest,false'],
            'form.Guest.IdNumber' => ['required_if:form.BookingSameGuest,false'],
            'form.Guest.Phone' => ['required_if:form.BookingSameGuest,false'],
            'BookingData' => ['required','array','min:1','max:99']
        ];
    }

    public function messages()
    {
        return [
            'form.Booking.Email.required' => '訂購人 E-mail 必填',
            'form.Booking.Email.email' => '訂購人 E-mail 格式錯誤',
            'form.Booking.Name.required' => '訂購人 姓名 必填',
            'form.Booking.IdNumber.required' => '訂購人 身份證字號 必填',
            'form.Booking.Phone.required' => '訂購人 電話 必填',
            'form.Guest.Email.required_if' => '住房人 E-mail 必填',
            'form.Guest.Email.email' => '住房人 E-mail 格式錯誤',
            'form.Guest.Name.required_if' => '住房人 姓名 必填',
            'form.Guest.IdNumber.required_if' => '住房人 身份證字號 必填',
            'form.Guest.Phone.required_if' => '住房人 電話 必填',
            'BookingData.required' => '請先加入房型優惠',
            'BookingData.array' => '請先加入房型優惠',
            'BookingData.min' => '請先加入房型優惠',
            'BookingData.max' => '超過購物車上限',
        ];
    }

}
