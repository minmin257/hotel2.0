<?php

namespace App\Http\Controllers;

use App\Eloquents\Receiver;
use App\Formatter\FormatContract;
use App\Http\Requests\CheckingOrderRequest;
use App\Http\Requests\MemberForgetRequest;
use App\Http\Requests\MemberLoginRequest;
use App\Http\Requests\MemberRegisterRequest;
use App\Http\Requests\OrderSearchRequest;
use App\Http\Requests\RemainRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\UpdateMemberBasicRequest;
use App\Http\Requests\UpdateMemberEmailRequest;
use App\Http\Requests\UpdateMemberPasswordRequest;
use App\Mail\MemberForgot;
use App\Member;
use App\Repositories\ATMInfoRepository;
use App\Repositories\DiscountCodeRepository;
use App\Repositories\EmailSettingRepository;
use App\Repositories\LicenseRepository;
use App\Repositories\RoomRepository;
use App\Repositories\RuleRepository;
use App\Repositories\SettingRepository;
use App\Service\DiscountCode;
use App\Service\Email;
use App\Service\Order;
use App\Service\OrderData;
use App\Service\Remain;
use App\Service\RoomSearching;
use App\Service\RoomSearchingByRoom;
use App\Service\TransForm;
use http\Env\Response;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    private $FormatContract;
    public function __construct(FormatContract $FormatContract)
    {
        $this->FormatContract = $FormatContract;
        $this->middleware('jwt.member')->only('UpdateMemberBasic');
    }

    public function index()
    {
        return view('front.home.index');
    }

    public function about()
    {
        return view('front.about.index');
    }

    public function booking()
    {
        return view('front.booking');
    }



    //Front api Response
    public function getTags()
    {
        $Tags = \App\Tag::get(['Name','IconSrc']);
        return response()->json($Tags);
    }

    public function Searching(SearchRequest $request,LicenseRepository $license)
    {
       $RoomSearching = new RoomSearching($request->StartDate,$request->EndDate,$request->Adults,$request->Childrens,$request->Babys);
       $data = $RoomSearching->ResponseData($license);
       return response()->json($data,200);
    }

    public function SearchingByRoom(Request $request,LicenseRepository $license)
    {
        $RoomSearchingByRoom = new RoomSearchingByRoom($request->Room,$request->StartDate,$request->EndDate);
        try {
            $data = $RoomSearchingByRoom->ResponseData($license);
            return response()->json($data,200);
        } catch (\Exception $e) {
            return $this->FormatContract->customError('錯誤', ['區間無空房'], 400);
        }
    }

    public function Remain(RemainRequest $request,RoomRepository $roomRepository)
    {
        $Remains = new Remain($roomRepository);
        $data = $Remains->Get($request->room,\Carbon\Carbon::parse($request->months)->startOfMonth(),\Carbon\Carbon::parse($request->months)->endOfMonth());
        $res = [];
        collect($data)->map(function ($date) use (&$res){
            $res[\Carbon\Carbon::parse($date['date'])->format('Y/m/d')] = $date['customData'];
        });
        return response()->json($res,200);
    }

    public function Rule(RuleRepository $Rule)
    {
        return response()->json($Rule->first()->only(['DefaultExtraBedPrice','DefaultExtraPeoplePrice']),200);
    }

    public function ExtraShopFromRoom(Request $request,RoomRepository $roomRepository)
    {
        $res = $roomRepository->getquery()->where('id',$request->Room)->first()->ExtraShops()->where('State',1)->orderBy('Sort','desc')->get([
            'extra_shops.id','Name','Src','Html','Price'
        ]);
        return response()->json($res,200);
    }

    public function ExtraCarFromRoom(Request $request,RoomRepository $roomRepository)
    {
        $res = $roomRepository->getquery()->where('id',$request->Room)->first()->ExtraCars()->where('State',1)->orderBy('Sort','desc')->get([
            'extra_cars.id','Name','Src','Html','Price','Price2','Max'
        ]);
        return response()->json($res,200);
    }

    public function CheckingDiscountCode(Request $request,DiscountCodeRepository $discountCodeRepository,LicenseRepository $licenseRepository)
    {
        $DiscountCode = new DiscountCode($discountCodeRepository,$licenseRepository);
        try {
            $res = $DiscountCode->CodeCheck($request->DiscountCode);
            return response()->json($res,200);
        }
        catch (\Exception $exception)
        {
            return  $this->FormatContract->customError('修改',[
                $exception->getMessage()
            ],400);
        }
    }

    /*
     * @todo 未完成 會員登入狀態
     * */
    public function CheckingOrder(CheckingOrderRequest $request)
    {
        //通過 基本 訂購人 入住人資料確認後
        //接者 確認訂單合法
        $OrderService = new Order(collect($request->BookingData),collect($request->form['DiscountCode']));
        \DB::beginTransaction();
        try {
            if(auth('member')->user()){
                $member = auth('member')->user()->id;
                $request->form['BookingSameGuest'] ? $Guest = collect($request->form['Booking']) :  $Guest = collect($request->form['Guest']);
                $Order = $OrderService->Create($member,collect($request->form['Booking']),$Guest,$request->form['TaxNumber'],$request->form['Note']);
                \DB::commit();
                if($member){
                    $MemberService = new \App\Service\Member();
                    $user = $MemberService->LoginById($member);

                    return response()->json([
                        'Member' => $user,
                        'Order' => $Order->Num
                    ],200);
                }
                else
                {
                    return response()->json([
                        'Order' => $Order->Num
                    ],200);
                }
            }
            else{
                $member = null;
                $request->form['BookingSameGuest'] ? $Guest = collect($request->form['Booking']) :  $Guest = collect($request->form['Guest']);

                if($request->form['BookingSameAuth'])
                {
                    if(Member::where('Email',$request->form['Booking']['Email'])->first()){
                        return  $this->FormatContract->customError('錯誤',[
                            '此信箱已申請過會員'
                        ],400);
                    }
                    else if(Member::where('IdNumber',$request->form['Booking']['IdNumber'])->first()){
                        return  $this->FormatContract->customError('錯誤',[
                            '此身份證已申請過會員'
                        ],400);
                    }

                    $NewMember= Member::Create([
                        'email' => $request->form['Booking']['Email'],
                        'password' => bcrypt($request->form['Booking']['IdNumber']),
                        'Name' => $request->form['Booking']['Name'],
                        'IdNumber' => $request->form['Booking']['IdNumber'],
                        'Phone' => $request->form['Booking']['Phone'],
                        'Area' => $request->form['Booking']['Area'],
                        'Sex' => $request->form['Booking']['Sex'],
                    ]);
                    $member = $NewMember->id;
                }

                $Order = $OrderService->Create($member,collect($request->form['Booking']),$Guest,$request->form['TaxNumber'],$request->form['Note']);
                \DB::commit();
                if($member){
                    $MemberService = new \App\Service\Member();
                    $user = $MemberService->LoginById($member);

                    return response()->json([
                        'Member' => $user,
                        'Order' => $Order->Num
                    ],200);
                }
                else
                {
                    return response()->json([
                        'Order' => $Order->Num
                    ],200);
                }
            }
        }
        catch (\Exception $exception)
        {
            \DB::rollback();
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }

    }


    //都是找到該order 然後去取值
    public function PaymentSelect(OrderSearchRequest $request){
        try {
            $Order = new OrderData($request->only('Num'));
            $ResponseData = $Order->GetOrderDetail();
            return response()->json([
                'Order' => $ResponseData
            ],200);
        }
        catch (\Exception $exception){
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }
    }

    public function UpdatePayment(Request $request){
        try {
            $Order = new OrderData($request->only('Num'));
            $Order->UpdatePayment($request->Payment);
            \Log::info('Update Success');
            return response()->json([],200);
        }
        catch (\Exception $exception){
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }
    }

    public function ATMInfo()
    {
        $data = \App\ATMInfo::first()->only([
           'Html'
        ]);
        return response()->json($data);
    }

    public function Login(MemberLoginRequest $request){
        try{
            $MemberService = new \App\Service\Member();
            $user = $MemberService->Login($request);
            return response()->json($user);
        }
        catch (\Exception $exception)
        {
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }
    }

    public function Register(MemberRegisterRequest $request){
        try{
            $MemberService = new \App\Service\Member();
            $NewMember = $MemberService->Create($request);
            $user = $MemberService->LoginById($NewMember->id);
            return response()->json($user);
        }
        catch (\Exception $exception)
        {
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }
    }

    public function Forgot(MemberForgetRequest $request,EmailSettingRepository $emailSetting)
    {
        $EmailService = new Email($emailSetting);
        try{
            $MemberService = new \App\Service\Member();
            if($member = Member::where('email',$request->email)->where('IdNumber',$request->IdNumber)->first())
            {
                $password = $MemberService->UpdateResetPassword($member->id);
                $Mail =  new MemberForgot($password);
                $EmailService->Sent(new Receiver($request->email),$Mail);
                return response()->json(['data' => '已寄出密碼重置信件至該信箱。'],200);
            }
            else
            {
                return  $this->FormatContract->customError('錯誤',[
                    '會員資料錯誤'
                ],400);
            }
        }
        catch (\Exception $exception)
        {
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }
    }

    public function UpdateMemberBasic(UpdateMemberBasicRequest $request)
    {
        try{
            $MemberService = new \App\Service\Member();
            $user = $MemberService->UpdateBasic($request);
            return response()->json($user);
        }
        catch (\Exception $exception)
        {
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }
    }

    public function UpdateMemberPassword(UpdateMemberPasswordRequest $request)
    {
        $MemberService = new \App\Service\Member();
        try{
            $user = $MemberService->UpdatePassword($request);
            if(get_class($user) == 'Exception'){
                throw new \Exception($user->getMessage());
            }
            return response()->json($user);
        }
        catch (\Exception $exception)
        {
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }
    }

    public function UpdateMemberEmail(UpdateMemberEmailRequest $request)
    {
        $MemberService = new \App\Service\Member();
        try{
            $user = $MemberService->UpdateEmail($request);
            if(get_class($user) == 'Exception'){
                throw new \Exception($user->getMessage());
            }
            return response()->json($user);
        }
        catch (\Exception $exception)
        {
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }
    }

    public function MemberOrderRecord(){
        try{
            $MemberService = new \App\Service\Member();
            $OrderRecords = $MemberService->OrderRecords();

            $Trans = new \App\Service\TransForm();
            $OrderRecords = $Trans->OrderRecords_to_FrontMember_Profile($OrderRecords);
            return response()->json($OrderRecords);
        }
        catch (\Exception $exception)
        {
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }
    }

    public function OrderRecord(Request $request)
    {
        try {
            $Order = new OrderData($request->only('Num'));
            $ResponseData = $Order->GetOrderDetail();
            return response()->json([
                'Order' => $ResponseData
            ],200);
        }
        catch (\Exception $exception){
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }
    }


    public function MemberChecker()
    {
        try {
            if(!auth('member')->user()){
                throw new \Exception('尚未登入');
            }
            else{
                return response()->json([
                    'Checker' => true
                ],200);
            }
        }
        catch (\Exception $exception){
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }
    }
}
