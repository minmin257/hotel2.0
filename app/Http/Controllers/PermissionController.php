<?php

namespace App\Http\Controllers;

use App\Formatter\FormatContract;
use App\Http\Requests\UpdatePermissionsRequest;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\RoleHasPermission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    private $FormatContract;
    public function __construct(FormatContract $FormatContract)
    {
        $this->FormatContract = $FormatContract;
    }

    public function get(PermissionRepository $permission)
    {
        return response()->json($permission->getquery()->get());
    }

    public function getHasPermissions($name,RoleRepository $role,RoleHasPermission $RoleHasPermission)
    {
        $role = $role->getquery()->where('name','=',$name)->first();
        if($role)
        {
            $ModelHasRole = $RoleHasPermission->where('role_id',$role->id)->pluck('permission_id')->toArray();
            return response()->json($ModelHasRole);
        }
        else
        {
            return  $this->FormatContract->customError('錯誤',['查無此等級'],409);
        }
    }


    public function updateHasPermissions(UpdatePermissionsRequest $request,RoleRepository $role,RoleHasPermission $roleHasPermission)
    {
        try {
            $role = $role->getquery()->where('name','=',$request->name)->first();
            $roleHasPermission->where('role_id',$role->id)->delete();
            foreach ($request->HasPermissions as $item)
            {
                $roleHasPermission->create([
                    'role_id' => $role->id,
                    'permission_id' => $item
                ]);
            }
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            return  $this->FormatContract->customError('錯誤',['修改失敗'],409);
        }
    }
}
