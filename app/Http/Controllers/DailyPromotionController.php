<?php

namespace App\Http\Controllers;

use App\Repositories\DailyPromotionRepository;
use App\Repositories\DailyRoomRepository;
use App\Repositories\LicenseRepository;
use App\Repositories\PromotionRepository;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Container\Container as App;
class DailyPromotionController extends Controller
{
    private $dailyPromotion;

    public function __construct(DailyPromotionRepository $dailyPromotion)
    {
        $this->dailyPromotion = $dailyPromotion;
    }


    public function get(Request $request,PromotionRepository $promotion)
    {
        $promotion = $promotion->readById($request->promotion_id);
        if($promotion)
        {
            $Events = [];
            $period = CarbonPeriod::create($request->start, $request->end);
            foreach ($period as $date) {
                $dailyRoom = new DailyRoomRepository(new App());
                if($DailyRoom = $dailyRoom->where('Date','=',$date->format('Y-m-d'))->where('room_id','=',$promotion->room_id)->first())
                {
                    $dailyPromotion = new DailyPromotionRepository(new App());
                    if($DailyPromotion = $dailyPromotion->where('daily_room_id','=',$DailyRoom->id)->where('promotion_id','=',$promotion->id)->first())
                    {
                        $Events[] = $this->DailyPromotionToEvents($DailyPromotion);
                    }
                    else
                    {
                        $Events[] = [
                            'title' => '尚未設定',
                            'date' => $date->format('Y-m-d'),
                            'cssClass' => 'unset',
                            'customData' => [
                                'state' => 1,
                                'price' => $promotion->DefaultPrice,
                                'AllowEarlyBird' => $promotion->DefaultAllowEarlyBird,
                                'EarlyBirdPrice' => $promotion->DefaultEarlyBirdPrice,
                                'EarlyBirdDays' => $promotion->DefaultEarlyBirdDays,
                            ]
                        ];
                    }
                }
                else
                {
                    $Events[] = [
                        'title' => '無法設定',
                        'content' => '請先完成基本房務',
                        'cssClass' => 'invalid',
                        'date' => $date->format('Y-m-d'),
                        'customData' => [
                            'state' => 0,
                        ]
                    ];
                }
            }
            return response()->json($Events,200);
        }
    }

    public function UpdateOrCreate(Request $request,PromotionRepository $promotion,LicenseRepository $license){
        \Log::info($request);
        $promotion = $promotion->readById($request->promotion_id);
        if($promotion)
        {
            $dailyRoom = new DailyRoomRepository(new App());
            //基本檔設了沒
            if($DailyRoom = $dailyRoom->where('Date','=',\Carbon\Carbon::parse($request->date)->format('Y-m-d'))->where('room_id','=',$promotion->room_id)->first()) {
                $this->dailyPromotion->updateOrCreate([
                    'daily_room_id' => $DailyRoom->id,
                    'promotion_id' => $promotion->id,
                ],[
                    'Price' => $request->price,
                ]);

                //License : 早鳥折扣
                if($license->check('早鳥折扣'))
                {
                    $this->dailyPromotion->updateOrCreate([
                        'daily_room_id' => $DailyRoom->id,
                        'promotion_id' => $promotion->id,
                    ],[
                        'AllowEarlyBird' => $request->AllowEarlyBird,
                        'EarlyBirdPrice' => $request->EarlyBirdPrice,
                        'EarlyBirdDays' => $request->EarlyBirdDays,
                    ]);
                }

            }
            else
            {
                //失敗
            }
        }
    }

    public function UpdateOrCreateScope(Request $request,PromotionRepository $promotion,LicenseRepository $license)
    {
        $promotion = $promotion->readById($request->promotion_id);
        $period = CarbonPeriod::create($request->start, $request->end);
        $weeks = $request->weeks;
        // week day are 0-6
        if(in_array(7,$weeks))
        {
            $index = array_search(7,$weeks);
            $weeks[$index] = 0;
        }

        foreach ($period as $date) {
            if(in_array ($date->dayOfWeek,$weeks))
            {
                $dailyRoom = new DailyRoomRepository(new App());
                //基本檔設了沒
                if($DailyRoom = $dailyRoom->where('Date','=',$date->format('Y-m-d'))->where('room_id','=',$promotion->room_id)->first()) {
                    $this->dailyPromotion->updateOrCreate([
                        'daily_room_id' => $DailyRoom->id,
                        'promotion_id' => $promotion->id,
                    ],[
                        'Price' => $request->price,
                    ]);

                    //License : 早鳥折扣
                    if($license->check('早鳥折扣'))
                    {
                        $this->dailyPromotion->updateOrCreate([
                            'daily_room_id' => $DailyRoom->id,
                            'promotion_id' => $promotion->id,
                        ],[
                            'AllowEarlyBird' => $request->AllowEarlyBird,
                            'EarlyBirdPrice' => $request->EarlyBirdPrice,
                            'EarlyBirdDays' => $request->EarlyBirdDays,
                        ]);
                    }

                }
            }

        }
    }


    private function DailyPromotionToEvents($DailyPromotion):array
    {
        return [
            'title' => $DailyPromotion->Promotion->Name,
            'date' => $DailyPromotion->DailyRoom->Date,
            'cssClass' => 'set',
            'customData' => [
                'state' => 1,
                'price' => $DailyPromotion->Price,
                'AllowEarlyBird' => $DailyPromotion->AllowEarlyBird,
                'EarlyBirdPrice' => $DailyPromotion->EarlyBirdPrice,
                'EarlyBirdDays' => $DailyPromotion->EarlyBirdDays,
            ]
        ];
    }
}
