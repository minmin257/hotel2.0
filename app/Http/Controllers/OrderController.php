<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateOrderRequest;
use App\Service\OrderMange;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class OrderController extends Controller
{
    private $order,$FormatContract;
    public function __construct(OrderMange $order)
    {
        $this->order = $order;
        $this->FormatContract = resolve('FormatContract');
    }

    public function get(Request $request)
    {
        if($request->has('Paginate'))
        {
            Paginator::currentPageResolver(function() use ($request) {
                return $request->CurrentPage;
            });
            $request->has('Keyword') ? $keyword = $request->Keyword : $keyword = null;
            $request->has('PromotionFilter') ? $PromotionFilter = ( $request->PromotionFilter ? $request->PromotionFilter  : null ) : $PromotionFilter = null;
            $request->has('StateFilter') ? $StateFilter = ( $request->StateFilter ? $request->StateFilter  : null ) : $StateFilter = null;

            return response()->json($this->order->paginate($request->Paginate,$keyword,$PromotionFilter,$StateFilter,$request->SortTime));
        }
        else
        {
            return response()->json($this->order->GetAll());
        }
    }

    public function getOrder($Num)
    {
        try {
            $Order = $this->order->GetOrderByNum($Num);
            return response()->json($Order);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('查詢',[
                '找不到該訂單紀錄'
            ],409);
        }
    }

    public function update(UpdateOrderRequest $request)
    {
        try {
            $Order = $this->order->UpdateOrder($request);
            return response()->json($Order);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('更新',[
                '更新失敗'
            ],409);
        }
    }
}
