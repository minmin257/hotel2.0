<?php

namespace App\Http\Controllers;

use App\Service\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    private $role;
    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    public function get()
    {
        return response()->json($this->role->get());
    }
}
