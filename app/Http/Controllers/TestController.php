<?php

namespace App\Http\Controllers;

use App\DailyPromotion;
use App\DailyRoom;
use App\Promotion;
use App\Repositories\LicenseRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderDetailRepository;
use App\Room;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Formatter\FormatContract;

class TestController extends Controller
{
    private $Order, $OrderDetail, $FormatContract;

    public function __construct(OrderRepository $Order, OrderDetailRepository $OrderDetail, FormatContract $FormatContract)
    {
        $this->order = $Order;
        $this->orderDetail = $OrderDetail;
        $this->FormatContract = $FormatContract;
    }

    public function init()
    {
        $rules = \App\Rule::first();
        $rooms = \App\Room::all();
        return response()->json([
            'rules' => $rules,
//            'rooms' => $rooms
        ]);
    }

    public function calc(Request $request, LicenseRepository $license)
    {

        $rules = \App\Rule::first();

        //第一步 計算人數
        $total = $request->aduits + ($request->childs * $rules->DefaultChildrenTransRate) + ($request->babys * $rules->DefaultBabyTransRate);

        //找出推薦房型
        $rooms = Room::whereRaw('PeopleNumber + MaxExtraPeopleNumber >=' . $total)->get();

        //排除 未設定基本檔 已訂間數大於基本間數
        //如果有設好 就會去看優惠有沒可用
        $periods = CarbonPeriod::create($request->start, \Carbon\Carbon::parse($request->end)->add(-1, 'days')->format('Y-m-d'));

        $RoomData = [];
        foreach ($rooms as $room) {
            $ck_daily_rooms = true;
            foreach ($periods as $date) {
                if (!$DailyRoom = DailyRoom::where('room_id', $room->id)->where('Date', $date->format('Y-m-d'))->first()) {
                    $ck_daily_rooms = false;
                } else {
                    $count = $this->orderDetail->where('daily_room_id', $DailyRoom->id)->get()->count();
                    if (!($DailyRoom->Number > $count)) {
                        $ck_daily_rooms = false;
                    }
                }
            }

            if ($ck_daily_rooms) {
                //有設定每日
                $Promotions = Promotion::where('room_id', $room->id)->where('State', 1)->get();
                $PromotionsData = [];
                foreach ($Promotions as $Promotion) {
                    //每個活動進去推算 總共多少錢
                    $Total = 0;
                    $Errors = [];
                    foreach ($periods as $date) {
                        $DailyRoom = DailyRoom::where('room_id', $room->id)->where('Date', $date->format('Y-m-d'))->first();
                        if ($DailyPromotion = DailyPromotion::where('daily_room_id', $DailyRoom->id)->where('promotion_id', $Promotion->id)->first()) {
                            if ($license->check('早鳥折扣')) {
                                $Total += $this->CalcEarlyBirdPrice($date, $DailyPromotion);
                            } else {
                                $Total += $DailyPromotion->Price;
                            }
                        } //沒有就原價
                        else {
                            if ($license->check('早鳥折扣')) {
                                $Total += $this->CalcEarlyBirdPrice($date, $DailyRoom);
                            } else {
                                $Total += $DailyRoom->Price;
                            }


                            $Errors[] = $DailyRoom->Date;
                        }
                    }
                    $PromotionsData[] = [
                        'id' => $Promotion->id,
                        'Name' => $Promotion->Name,
                        'Total' => $Total,
                        'Errors' => $Errors
                    ];
                }

                $Basic_Total = 0;
                foreach ($periods as $date) {
                    $DailyRoom = DailyRoom::where('room_id', $room->id)->where('Date', $date->format('Y-m-d'))->first();

                    if ($license->check('早鳥折扣')) {
                        $Basic_Total += $this->CalcEarlyBirdPrice($date, $DailyRoom);
                    } else {
                        $Basic_Total += $DailyRoom->Price;
                    }
                }

                //基本價格
                $PromotionsData[] = [
                    'id' => '',
                    'Name' => '基本方案',
                    'Total' => $Basic_Total,
                    'Errors' => []
                ];


                $RoomData[] = [
                    'room' => $room,
                    'Promotions' => $PromotionsData,
                    'Tags' => $room->Tags()->get()->pluck('Name')->toArray()
                ];
            }

        }

        $Tags = \App\Tag::all()->pluck('Name')->toArray();
        $ResponseData = [
            'Tags' => $Tags,
            'Rooms' => $RoomData
        ];

        return $ResponseData;
    }

    private function CalcEarlyBirdPrice($date, $collect): float
    {
        if ($collect->AllowEarlyBird) {
            if (\Carbon\Carbon::parse($date)->diffInDays(\Carbon\Carbon::today()->startOfDay()) >= $collect->EarlyBirdDays) {
                return $collect->EarlyBirdPrice;
            }
            return $collect->Price;
        }
        return $collect->Price;
    }

    public function store(Request $request)
    {
        $check_out = \Carbon\Carbon::parse($request->end)->subDays();
        // 退房日期減一天區間
        $dailyRooms = DailyRoom::where('room_id', $request->room_id)->whereBetween('Date', [$request->start, $check_out])->get();
        $checker = 0;
        if (count($dailyRooms)) {
            foreach ($dailyRooms as $dailyRoom) {
                $count = $this->orderDetail->where('daily_room_id', $dailyRoom->id)->get()->count();
                if ($dailyRoom->Number > $count) {
                    $checker = 1;
                } else {
                    return $this->FormatContract->customError('錯誤', ['當日查無空房，請更換日期區間或房型，謝謝'], 402);
                }
            }
        } else {
            return $this->FormatContract->customError('錯誤', ['當日無基本檔設定，請更換日期區間或房型，謝謝'], 400);
        }

        $Num = strtotime('now');
        if ($checker) {
            $order = $this->order->firstOrCreate([
                'Num' => $Num,
            ], [
                'Name' => $request->name,
                'Phone' => $request->phone,
                'Adult' => $request->adult,
                'Child' => $request->child,
                'CheckIn' => $request->start,
                'CheckOut' => $request->end,
                'promotion_id' => $request->promotion_id,
                'payment_id' => 1,
                'Price' => $request->price,
            ]);

            foreach ($dailyRooms as $dailyRoom) {
                $this->orderDetail->create([
                    'order_id' => $order->id,
                    'daily_room_id' => $dailyRoom->id
                ]);
            }
        }
    }

}
