<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDiscountCodeRequest;
use App\Http\Requests\DeleteDiscountCodeRequest;
use App\Http\Requests\UpdateDiscountCodeRequest;
use App\Http\Requests\UpdateDiscountCodesRequest;
use App\Service\DiscountCode;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class DiscountCodeController extends Controller
{
    private $FormatContract;
    private DiscountCode $DiscountCode;

    public function __construct(DiscountCode $DiscountCode)
    {
        $this->DiscountCode = $DiscountCode;
        $this->FormatContract = resolve('FormatContract');
    }

    public function get(Request $request)
    {
        if($request->has('DiscountCode_id'))
        {
            return response()->json($this->DiscountCode->readById($request->DiscountCode_id)->toArray());
        }
        else if($request->has('Paginate'))
        {
            Paginator::currentPageResolver(function() use ($request) {
                return $request->CurrentPage;
            });
            return response()->json($this->DiscountCode->paginate($request->Paginate));
        }
        else
        {
            return response()->json($this->DiscountCode->GetAll());
        }
    }

    public function Update(UpdateDiscountCodeRequest $request)
    {
        try {
            $this->DiscountCode->update($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('更新',[
                $e->getMessage()
            ],409);
        }
    }

    private function no_dupes($input_array) {
        return count($input_array) === count(array_flip($input_array));
    }
    public function UpdateDiscountCodes(UpdateDiscountCodesRequest $request)
    {
        if($this->no_dupes(collect($request)->pluck('Code')->toArray())) {
            try {
                $this->DiscountCode->updateDiscountCodes($request);
                return response()->json([],200);
            }
            catch (\Exception $e)
            {
                \Log::info($e);
                return  $this->FormatContract->customError('更新',[
                    $e->getMessage()
                ],409);
            }
        }
        else
        {
            return  $this->FormatContract->customError('更新',[
                '優惠碼不得重複'
            ],409);
        }
    }

    public function Delete(DeleteDiscountCodeRequest $request)
    {
        try {
            $this->DiscountCode->delete($request->DiscountCode_id);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('刪除',[
                $e->getMessage()
            ],409);
        }
    }

    public function Create(CreateDiscountCodeRequest $request)
    {
        try {
            $this->DiscountCode->create($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('新增',[
                $e->getMessage()
            ],409);
        }
    }

}
