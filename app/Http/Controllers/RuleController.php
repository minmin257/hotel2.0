<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateRuleRequest;
use App\Service\Rule;
use Illuminate\Http\Request;

class RuleController extends Controller
{
    private $rule,$FormatContract;
    public function __construct(Rule $rule)
    {
        $this->rule = $rule;
        $this->FormatContract = resolve('FormatContract');
    }
    public function get()
    {
        return response()->json($this->rule->Get());
    }

    public function Update(UpdateRuleRequest $request)
    {
        try {
            $this->rule->update($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('更新',[
                $e->getMessage()
            ],409);
        }

    }
}
