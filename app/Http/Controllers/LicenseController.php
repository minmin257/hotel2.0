<?php

namespace App\Http\Controllers;

use App\Repositories\LicenseRepository;
use Illuminate\Http\Request;

class LicenseController extends Controller
{
    private $license;
    public function __construct(LicenseRepository $license)
    {
        $this->license = $license;
    }

    public function get(Request $request){
        return collect($this->license->where('state','=',1)->get())->pluck('Name');
    }
}
