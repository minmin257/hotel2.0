<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePromotionRequest;
use App\Http\Requests\DeletePromotionRequest;
use App\Http\Requests\UpdatePromotionRequest;
use App\Http\Requests\UpdatePromotionsRequest;
use App\Service\Promotion;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class PromotionController extends Controller
{
    private $promotion,$FormatContract;
    public function __construct(Promotion $promotion)
    {
        $this->promotion = $promotion;
        $this->FormatContract = resolve('FormatContract');
    }

    public function get(Request $request)
    {
        if($request->has('promotion_id'))
        {
            return response()->json($this->promotion->readById($request->promotion_id)->toArray());
        }
        else if($request->has('Paginate'))
        {
            Paginator::currentPageResolver(function() use ($request) {
                return $request->CurrentPage;
            });
            return response()->json($this->promotion->paginate($request->Paginate));
        }
        else
        {
            return response()->json($this->promotion->GetAll());
        }
    }

    public function getfilters(Request $request)
    {
        //用於 後台OrderList 的 table filter selecting中
        return response()->json($this->promotion->Getfilters());
    }

    public function Update(UpdatePromotionRequest $request)
    {
        try {
            $this->promotion->update($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('更新',[
                $e->getMessage()
            ],409);
        }
    }

    public function UpdatePromotions(UpdatePromotionsRequest $request)
    {
        try {
            $this->promotion->updatePromotions($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('更新',[
                $e->getMessage()
            ],409);
        }
    }

    public function Delete(DeletePromotionRequest $request)
    {
        try {
            $this->promotion->delete($request->promotion_id);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('刪除',[
                $e->getMessage()
            ],409);
        }
    }

    public function Create(CreatePromotionRequest $request)
    {
        try {
            $this->promotion->create($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('新增',[
                $e->getMessage()
            ],409);
        }
    }

}
