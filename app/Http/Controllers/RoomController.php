<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoomRequest;
use App\Http\Requests\DeleteRoomRequest;
use App\Http\Requests\UpdateExtraRequest;
use App\Http\Requests\UpdateRoomRequest;
use App\Http\Requests\UpdateRoomsRequest;
use App\Service\Room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    private $room,$FormatContract;
    public function __construct(Room $room)
    {
        $this->room = $room;
        $this->FormatContract = resolve('FormatContract');
    }

    public function get(Request $request)
    {
        if($request->has('room_id'))
        {
            return response()->json($this->room->readById($request->room_id)->toArray());
        }
        return response()->json($this->room->GetAll());
    }

    public function Create(CreateRoomRequest $request)
    {
        try {
            $this->room->create($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('新增',[
                $e->getMessage()
            ],409);
        }
    }


    public function UpdateRooms(UpdateRoomsRequest $request)
    {
        try {
            $this->room->updateRooms($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('更新',[
                $e->getMessage()
            ],409);
        }
    }

    public function Update(UpdateRoomRequest $request)
    {
        try {
            $this->room->update($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('更新',[
                $e->getMessage()
            ],409);
        }
    }

    public function Delete(DeleteRoomRequest $request)
    {
        try {
            $this->room->delete($request->room_id);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('刪除',[
                $e->getMessage()
            ],409);
        }
    }

    //擁有加價購
    public function getExtra(Request $request)
    {
        if($request->has('room_id'))
        {
            return response()->json($this->room->getExtra($request->room_id)->toArray());
        }
        return response()->json($this->room->GetAll());
    }

    public function UpdateExtra(UpdateExtraRequest $request)
    {
        try {
            $this->room->updateExtra($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('更新加購',[
                $e->getMessage()
            ],409);
        }
    }
}
