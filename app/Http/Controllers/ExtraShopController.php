<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateExtraShopRequest;
use App\Http\Requests\DeleteExtraShopRequest;
use App\Http\Requests\UpdateExtraShopRequest;
use App\Http\Requests\UpdateExtraShopsRequest;
use App\Repositories\LicenseRepository;
use App\Service\ExtraShop;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class ExtraShopController extends Controller
{
    private  $extraShop,$FormatContract;
    public function __construct(ExtraShop $extraShop, LicenseRepository $license)
    {
        $this->extraShop = $extraShop;
        $this->FormatContract = resolve('FormatContract');
        $this->license = $license;
        if(!$this->license->check('加購模組')){
            abort(403);
        }
    }

    public function get(Request $request){
        if($request->has('extra_shop_id'))
        {
            return response()->json($this->extraShop->readById($request->extra_shop_id)->toArray());
        }
        else if($request->has('Paginate'))
        {
            Paginator::currentPageResolver(function() use ($request) {
                return $request->CurrentPage;
            });
            return response()->json($this->extraShop->paginate($request->Paginate));
        }
        else
        {
            return response()->json($this->extraShop->GetAll());
        }
    }

    public function Create(CreateExtraShopRequest $request)
    {
        try {
            $this->extraShop->create($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('新增',[
                $e->getMessage()
            ],409);
        }
    }

    public function Update(UpdateExtraShopRequest $request)
    {
        try {
            $this->extraShop->update($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('修改',[
                $e->getMessage()
            ],409);
        }
    }

    public function UpdateExtraShops(UpdateExtraShopsRequest $request)
    {
        try {
            $this->extraShop->updateExtraShops($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('修改',[
                $e->getMessage()
            ],409);
        }
    }

    public function Delete(DeleteExtraShopRequest $request)
    {
        try {
            $this->extraShop->delete($request->extra_shop_id);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('刪除',[
                $e->getMessage()
            ],409);
        }
    }
}
