<?php

namespace App\Http\Controllers;

use App\Repositories\DailyRoomRepository;
use App\Repositories\LicenseRepository;
use Carbon\CarbonPeriod;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DailyRoomController extends Controller
{
    private $dailyRoom;
    public function __construct(DailyRoomRepository $dailyRoom)
    {
        $this->dailyRoom = $dailyRoom;
    }

    public function get(Request $request)
    {
        return $this->DailyRoomToEvents($this->dailyRoom->where('Date','>=',$request->start)
            ->where('Date','<=',$request->end)
            ->where('room_id',$request->room_id)
            ->get()->toArray());
    }

    public function UpdateOrCreate(Request $request,LicenseRepository $license)
    {
        $this->dailyRoom->updateOrCreate(
                [
                    'room_id' => $request->room_id,
                    'Date' => $request->date
                ],[
                    'Price' => $request->price,
                    'Number' => $request->number
                ]);

        //License : 早鳥折扣
        if($license->check('早鳥折扣'))
        {
            $this->dailyRoom->updateOrCreate(
                [
                    'room_id' => $request->room_id,
                    'Date' => $request->date
                ],[
                'AllowEarlyBird' => $request->AllowEarlyBird,
                'EarlyBirdPrice' => $request->EarlyBirdPrice,
                'EarlyBirdDays' => $request->EarlyBirdDays,
            ]);
        }
    }

    public function UpdateOrCreateScope(Request $request,LicenseRepository $license)
    {
        $period = CarbonPeriod::create($request->start, $request->end);
        $weeks = $request->weeks;
        // week day are 0-6
        if(in_array(7,$weeks))
        {
            $index = array_search(7,$weeks);
            $weeks[$index] = 0;
        }

        foreach ($period as $date) {
            if(in_array ($date->dayOfWeek,$weeks))
            {
                $this->dailyRoom->updateOrCreate([
                    'room_id' => $request->room_id,
                    'Date' => $date->format('Y-m-d'),
                ],[
                    'Price' => $request->price,
                    'Number' => $request->number
                ]);

                //License : 早鳥折扣
                if($license->check('早鳥折扣'))
                {
                    $this->dailyRoom->updateOrCreate([
                        'room_id' => $request->room_id,
                        'Date' => $date->format('Y-m-d'),
                    ],[
                        'AllowEarlyBird' => $request->AllowEarlyBird,
                        'EarlyBirdPrice' => $request->EarlyBirdPrice,
                        'EarlyBirdDays' => $request->EarlyBirdDays,
                    ]);
                }
            }

        }
    }

    private function DailyRoomToEvents(array $collection):JsonResponse
    {
        $events = [];
        collect($collection)->each(function($value) use (&$events) {
            $events[] = [
                'title' => '預設間數: '.$value['Number'],
                'content' => '底價: '.$value['Price'],
                'date' => $value['Date'],
                'cssClass' => 'set',
                'customData' => [
                    'number' => $value['Number'],
                    'price' => $value['Price'],
                    'AllowEarlyBird' => $value['AllowEarlyBird'],
                    'EarlyBirdPrice' => $value['EarlyBirdPrice'],
                    'EarlyBirdDays' => $value['EarlyBirdDays'],
                ]
            ];
        });
        return response()->json($events,200);
    }

}
