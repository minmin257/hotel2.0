<?php

namespace App\Http\Controllers;

use App\Http\Requests\JwtLoginRequest;
use App\Http\Requests\JwtUpdateProfileRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
class JwtController extends Controller
{
    private $user;
    private $FormatContract;
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
        $this->FormatContract = resolve('FormatContract');
    }

    private function guard()
    {
        return Auth::guard('api');
    }

    public function login(JwtLoginRequest $request)
    {
        $credentials = $request->only(['account','password']);
        if ($token = $this->guard()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }
        else
        {
            return  $this->FormatContract->customError('登錄',['帳號密碼錯誤'],401);
        }
    }
    public function updateProfile(JwtUpdateProfileRequest $request){
        try {
            $user = $this->user->where('account',$request->account)->first();
            $user->update([
                'name' => $request->name,
            ]);
            if($request->password)
            {
                $user->update([
                    'password' => bcrypt($request->password)
                ]);
            }
            $token = $this->guard()->login($user);
            return $this->respondWithToken($token);
        }
        catch(\Exception $exception)
        {
            return $this->FormatContract->customError('更新',['修改時發生錯誤'],400);
        }
    }
    protected function respondWithToken($token)
    {
        $user = $this->guard()->user();
        $Collection = new Collection();
        $Collection->put('name',$user->name);
        $Collection->put('account',$user->account);

        return response()->json([
            'access_token' => $token,
            'user'=> $Collection,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }
}
