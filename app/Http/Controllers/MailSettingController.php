<?php

namespace App\Http\Controllers;

use App\Eloquents\Receiver;
use App\Eloquents\Senter;
use App\Formatter\FormatContract;
use App\Http\Requests\ValidEmailRequest;
use App\Mail\ValidEmail;
use App\Repositories\EmailSettingRepository;
use App\Service\Email;
use Illuminate\Http\Request;

class MailSettingController extends Controller
{
    private $emailSetting,$FormatContract;
    public function __construct(EmailSettingRepository $emailSetting,FormatContract $FormatContract)
    {
        $this->FormatContract = $FormatContract;
        $this->emailSetting = $emailSetting;
    }

    public function get()
    {
        return response()->json($this->emailSetting->first()->only(['account','Name','Cc']));
    }

    public function update(Request $request)
    {
        try {
            $this->emailSetting->update('1',$request->only(['Name','Cc']));
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('錯誤',[
                $e->getMessage()
            ],400);
        }
    }

    public function valid(ValidEmailRequest $request)
    {
        try {
            $EmailService = new Email($this->emailSetting,new Senter($request->account,$request->password,'信箱驗證'));
            $EmailService->Sent(new Receiver($request->account),new ValidEmail);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('錯誤',[
                $e->getMessage()
            ],400);
        }

        $this->emailSetting->update('1',[
           'account' => $request->account,
           'password' => encrypt($request->password)
        ]);
        return response()->json([],200);
    }

}
