<?php

namespace App\Http\Controllers;

use App\Formatter\FormatContract;
use App\Http\Requests\UpdateSystemRequest;
use App\Service\System;
use Illuminate\Http\Request;

class SystemController extends Controller
{
    private $system,$FormatContract;

    public function __construct(System $system,FormatContract $FormatContract)
    {
        $this->FormatContract = $FormatContract;
        $this->system = $system;
    }

    public function get()
    {
        return response()->json($this->system->get());
    }

    public function update(UpdateSystemRequest $request)
    {
        try {
            $data = $this->system->update($request);
            return response()->json($data);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('錯誤',[
                $e->getMessage()
            ],400);
        }
    }
}
