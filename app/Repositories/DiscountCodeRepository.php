<?php


namespace App\Repositories;


use App\DiscountCode;

class DiscountCodeRepository extends BaseRepository
{

    protected function model()
    {
        return DiscountCode::class;
    }

    protected function query()
    {
        return DiscountCode::query();
    }
}
