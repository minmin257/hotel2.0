<?php
namespace App\Repositories;

use App\Member;

class MemberRepository extends BaseRepository
{

    protected function model()
    {
        return Member::class;
    }

    protected function query()
    {
        return Member::query();
    }
}
