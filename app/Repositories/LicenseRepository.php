<?php


namespace App\Repositories;


use App\License;

class LicenseRepository extends BaseRepository
{

    protected function model()
    {
        return License::class;
    }

    protected function query()
    {
        return License::query();
    }

    public function check($Name):bool
    {
        return $this->where('Name','=',$Name)->where('state','=', 1)->first() ? true : false;
    }
}
