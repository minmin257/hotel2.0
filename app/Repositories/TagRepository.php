<?php


namespace App\Repositories;


use App\Tag;

class TagRepository extends BaseRepository
{

    protected function model()
    {
        return Tag::class;
    }

    protected function query()
    {
        return Tag::query();
    }
}
