<?php
namespace App\Repositories;

interface IRepository
{
    public function create(array $data);

    public function update($id, array $data);

    public function delete($id);

    public function readById($id);

    public function firstOrCreate(array $attributes, array $values);

    public function updateOrCreate(array $attributes, array $values);

    public function where($key, $operator = null,$value = null);

    public function first();

    public function get();

    public function all();

    public function getquery();
}
