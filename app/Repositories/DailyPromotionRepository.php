<?php


namespace App\Repositories;


use App\DailyPromotion;

class DailyPromotionRepository extends BaseRepository
{

    protected function model()
    {
        return DailyPromotion::class;
    }

    protected function query()
    {
        return DailyPromotion::query();
    }
}
