<?php


namespace App\Repositories;


use App\Setting;

class SettingRepository extends BaseRepository
{

    protected function model()
    {
        return Setting::class;
    }

    protected function query()
    {
        return Setting::query();
    }
}
