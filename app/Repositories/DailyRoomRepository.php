<?php


namespace App\Repositories;


use App\DailyRoom;

class DailyRoomRepository extends BaseRepository
{

    protected function model()
    {
        return DailyRoom::class;
    }

    protected function query()
    {
        return DailyRoom::query();
    }
}
