<?php


namespace App\Repositories;


use App\Rule;

class RuleRepository extends BaseRepository
{

    protected function model()
    {
        return Rule::class;
    }

    protected function query()
    {
        return Rule::query();
    }
}
