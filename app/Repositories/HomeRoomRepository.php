<?php
    namespace App\Repositories;

    use App\HomeRoom;

    class HomeRoomRepository extends BaseRepository
    {

        protected function model()
        {
            return HomeRoom::class;
        }

        protected function query()
        {
            return HomeRoom::query();
        }
    }
