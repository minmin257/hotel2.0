<?php
    namespace App\Repositories;

    use App\OrderDetail;

    class OrderDetailRepository extends BaseRepository
    {

        protected function model()
        {
            return OrderDetail::class;
        }

        protected function query()
        {
            return OrderDetail::query();
        }
    }
