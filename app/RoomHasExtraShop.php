<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomHasExtraShop extends Model
{
    protected $guarded = [];
}
