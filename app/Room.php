<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function Promotions()
    {
        return $this->hasMany(\App\Promotion::class);
    }

    public function ExtraShops()
    {
        return $this->hasManyThrough(ExtraShop::class,RoomHasExtraShop::class,'room_id','id','id','extra_shop_id');
    }

    public function ExtraCars()
    {
        return $this->hasManyThrough(ExtraCar::class,RoomHasExtraCar::class,'room_id','id','id','extra_car_id');
    }

    public function Tags()
    {
        return $this->hasManyThrough(Tag::class,RoomHasTag::class,'room_id','id','id','tag_id');
    }


    public function HasExtraShops()
    {
        return $this->hasMany(RoomHasExtraShop::class);
    }
    public function HasExtraCars()
    {
        return $this->hasMany(RoomHasExtraCar::class);
    }
}
