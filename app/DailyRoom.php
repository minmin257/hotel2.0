<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyRoom extends Model
{
    protected $guarded = [];

    public function OrderDetailBookings()
    {
        return $this->hasMany(OrderDetailBooking::class,'daily_room_id');
    }
}
