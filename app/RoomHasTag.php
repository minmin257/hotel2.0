<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomHasTag extends Model
{
    protected $guarded = [];
}
