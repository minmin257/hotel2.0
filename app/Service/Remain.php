<?php


namespace App\Service;


use App\Repositories\DailyRoomRepository;
use App\Repositories\RoomRepository;
use Carbon\CarbonPeriod;
use Illuminate\Container\Container as App;

class Remain
{
    private $room;
    public function __construct(RoomRepository $room)
    {
        $this->room = $room;
    }

    public function Get($id,$start,$end):array
    {
        $Events = [];
        $period = CarbonPeriod::create($start,$end);
        foreach ($period as $date) {
            $dailyRoom = new DailyRoomRepository(new App());
            if($DailyRoom = $dailyRoom->where('Date','=',$date->format('Y-m-d'))->where('room_id','=',$id)->first())
            {
                //當日已訂
                $Events[] = $this->DailyRoomToRemainEvents($DailyRoom);
            }
            else
            {
                $Events[] = [
                    'title' => '尚未設定',
                    'cssClass' => 'unset',
                    'date' => $date->format('Y-m-d'),
                    'customData' => [
                        'state' => 0,
                    ]
                ];
            }
        }
        return $Events;
    }

    private function DailyRoomToRemainEvents($DailyRoom):array
    {
        //總數量
        $Total = $DailyRoom->Number ?? 0;
        $Cancel = 0;
        $Done = 0;
        $UnCheck = 0;

        //全部當日訂購名單
        $OrderDetailBookings = $DailyRoom->OrderDetailBookings;
        foreach ($OrderDetailBookings as $OrderDetailBooking)
        {
            $Order = $OrderDetailBooking->Order;
            if($Order->Cancel)
            {
                $Cancel += 1;
            }
            else if($Order->Paid)
            {
                $Done += 1;
            }
            else
            {
                $UnCheck += 1;
            }
        }

        if($Total >= ($Done+$UnCheck))
        {
            $title = "剩餘數量: ". ($Total - ($Done+$UnCheck));
            $state = 1;
        }
        else
        {
            $title = "超賣";
            $state = 0;
        }



        return [
            'title' => $title,
            'content' => '已完成:'.$Done . '</br>未付款:'.$UnCheck,
            'date' => $DailyRoom->Date,
            'cssClass' => 'set',
            'customData' => [
                'state' => $state,
                'Done' => $Done,
                'UnCheck' => $UnCheck,
                'Total' => $Total
            ]
        ];
    }
}
