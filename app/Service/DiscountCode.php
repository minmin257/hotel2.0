<?php


namespace App\Service;


use App\Repositories\LicenseRepository;
use App\Repositories\DiscountCodeRepository;

class DiscountCode
{
    private $Discount,$license;
    public function __construct(DiscountCodeRepository $Discount,LicenseRepository $license)
    {
        $this->Discount = $Discount;
        $this->license = $license;
    }

    public function GetAll()
    {
        return $this->Discount->getquery()->get();
    }

    public function readById($id)
    {
        return $this->Discount->getquery()->find($id);
    }

    public function paginate($num)
    {

        return $this->Discount->getquery()->paginate($num);
    }

    public function update($data){
        $keys = [
            'id','Code','Name','Html','Discount','Remains','Allow_Start','Allow_End','Use_Start','Use_End','Sort','State'
        ];
        $data = $data->only($keys);
        $this->Discount->update($data['id'],collect($data)->except(['id'])->toArray());
    }

    public function updateDiscountCodes($dataArray)
    {
        $keys = [
            'id','Code','Name','Discount','Remains','Sort','State'
        ];
        $dataArray = collect($dataArray)->map(function ($data) use ($keys){
            return collect($data)->only($keys);
        });
        foreach ($dataArray as $data)
        {
            $this->Discount->update($data['id'],collect($data)->except(['id'])->toArray());
        }
    }

    public function create($data)
    {
        $keys = [
            'Code','Name','Html','Discount','Remains','Allow_Start','Allow_End','Use_Start','Use_End','Sort','State'
        ];
        $data = $data->only($keys);
        return $this->Discount->create($data);
    }

    public function delete($id){
        $this->Discount->delete($id);
    }

    public function CodeCheck($code)
    {
        $Discount = $this->Discount->getquery()
            ->where('Code',$code)
            ->where('State',1)->first();
        if($Discount){
            if($Discount->Remains < 0 )
            {
                throw new \Exception('優惠代碼已售完');
            }
            else if(\Carbon\Carbon::parse($Discount->Allow_Start) <= \Carbon\Carbon::now() && \Carbon\Carbon::parse($Discount->Allow_End) >= \Carbon\Carbon::now())
            {
                return $Discount->only(['Code','Name','Html','Discount','Use_Start','Use_End']);
            }
            else
            {
                throw new \Exception('優惠代碼過期');
            }
        }
        else{
            throw new \Exception('優惠代碼無效');
        }
    }

}
