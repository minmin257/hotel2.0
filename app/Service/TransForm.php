<?php


namespace App\Service;

use Illuminate\Support\Collection;


class TransForm
{
    public function OrderRecords_to_FrontMember_Profile(Collection $OrderRecords):array{
        $unPaid = [];
        $Paid = [];
        $Cancel = [];
        foreach ($OrderRecords as $OrderRecord){
            $Details = [];
            foreach(\App\OrderDetail::where('order_id',$OrderRecord->id)->with(['Room','Promotion'])->get() as $Detail){
                $Details[] = [
                    'StartDate' => $Detail->StartDate,
                    'EndDate' => $Detail->EndDate,
                    'RoomName' => $Detail->Room->Name,
                    'Promotion' => $Detail->Promotion->Name
                ];
            }

            if(!$OrderRecord->Paid && !$OrderRecord->Cancel)
            {
                $unPaid[] = [
                    'Num' => $OrderRecord->Num,
                    'TotalPrice' => $OrderRecord->TotalPrice,
                    'Details' =>  $Details
                ];
            }
            else if($OrderRecord->Paid && !$OrderRecord->Cancel)
            {
                $Paid[] = [
                    'Num' => $OrderRecord->Num,
                    'TotalPrice' => $OrderRecord->TotalPrice,
                    'Details' =>  $Details
                ];
            }
            else if($OrderRecord->Cancel)
            {
                $Cancel[] = [
                    'Num' => $OrderRecord->Num,
                    'TotalPrice' => $OrderRecord->TotalPrice,
                    'Details' =>  $Details
                ];
            }
        }
        return [
          'unPaid' => $unPaid,
          'Paid' => $Paid,
          'Cancel' => $Cancel,
        ];
    }
}
