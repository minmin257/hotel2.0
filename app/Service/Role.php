<?php


namespace App\Service;


use App\Repositories\RoleRepository;

class Role
{
    private $role;
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
    }

    public function get()
    {
        return $this->role->getquery()->with('User')->withCount('ModelHasRole')->get();
    }
}
