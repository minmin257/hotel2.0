<?php


namespace App\Service;


use App\Repositories\LicenseRepository;
use App\Repositories\ExtraCarRepository;
use http\Client\Response;

class ExtraCar
{
    private $extraCar, $license;

    public function __construct(ExtraCarRepository $extraCar, LicenseRepository $license)
    {
        $this->extraCar = $extraCar;
        $this->license = $license;
    }

    public function GetAll()
    {
        return $this->extraCar->getquery()->orderBy('Sort','desc')->get();
    }

    public function readById($id)
    {
        return $this->extraCar->getquery()->find($id);
    }

    public function paginate($num)
    {
        return $this->extraCar->getquery()->orderBy('Sort','desc')->paginate($num);
    }

    public function create($data)
    {
        $keys = [
            'Name','Src','Max','Html','Price','Price2','Sort','State'
        ];
        $data = $data->only($keys);
        return $this->extraCar->create($data);
    }
    public function updateExtraCars($dataArray)
    {
        $keys = [
            'id','Name','Max','Src','Price','Price2','Sort','State'
        ];
        $dataArray = collect($dataArray)->map(function ($data) use ($keys){
            return collect($data)->only($keys);
        });
        foreach ($dataArray as $data)
        {
            $this->extraCar->update($data['id'],collect($data)->except(['id'])->toArray());
        }
    }

    public function update($data)
    {
        $keys = [
            'id','Name','Max','Src','Html','Price','Price2','Sort','State'
        ];
        $data = $data->only($keys);
        $this->extraCar->update($data['id'],collect($data)->except(['id'])->toArray());
    }


    public function delete($id){
        $this->extraCar->delete($id);
    }

}
