<?php


namespace App\Service;


use App\DailyPromotion;
use App\DailyRoom;
use App\OrderDetail;
use App\OrderDetailBooking;
use App\Promotion;
use App\Room;
use App\Repositories\LicenseRepository;
use Carbon\CarbonPeriod;

class RoomSearchingByRoom
{
    private $room,$CheckIn,$Checkout;
    public function __construct($room,$CheckIn,$Checkout)
    {
        $this->room = Room::where('id',$room)->first();
        $this->CheckIn = $CheckIn;
        $this->Checkout = $Checkout;
    }

    public function ResponseData(LicenseRepository $license):array
    {
        $periods = CarbonPeriod::create(\Carbon\Carbon::parse($this->CheckIn), \Carbon\Carbon::parse($this->Checkout)->add(-1, 'days')->format('Y-m-d'));

        $ck_daily_rooms = true;
        $numbers = $this->room->DefaultNumber ?? 0;

        foreach ($periods as $date) {
            if (!$DailyRoom = DailyRoom::where('room_id', $this->room->id)->where('Date', $date->format('Y-m-d'))->first()) {
                $ck_daily_rooms = false;
            } else {
                $count = OrderDetailBooking::where('daily_room_id', $DailyRoom->id)->get()->count();
                $numbers = $numbers - $count;
                if (!($DailyRoom->Number > $count)) {
                    $ck_daily_rooms = false;
                }
            }
        }

        if ($ck_daily_rooms) {
            $Promotions = Promotion::where('room_id', $this->room->id)->where('State', 1)->get();
            $PromotionsData = [];
            foreach ($Promotions as $Promotion) {
                $Total = 0;
                $Errors = [];
                foreach ($periods as $date) {
                    $DailyRoom = DailyRoom::where('room_id', $this->room->id)->where('Date', $date->format('Y-m-d'))->first();
                    if ($DailyPromotion = DailyPromotion::where('daily_room_id', $DailyRoom->id)->where('promotion_id', $Promotion->id)->first()) {
                        if ($license->check('早鳥折扣')) {
                            $Total += $this->CalcEarlyBirdPrice($date, $DailyPromotion);
                        } else {
                            $Total += $DailyPromotion->Price;
                        }
                    } //沒有就原價
                    else {
                        if ($license->check('早鳥折扣')) {
                            $Total += $this->CalcEarlyBirdPrice($date, $DailyRoom);
                        } else {
                            $Total += $DailyRoom->Price;
                        }
                        $Errors[] = $DailyRoom->Date;
                    }
                }
                $PromotionsData[] = [
                    'id' => $Promotion->id,
                    'Name' => $Promotion->Name,
                    'Note' => $Promotion->Note,
                    'Total' => $Total,
                    'Errors' => $Errors
                ];
            }

            $Basic_Total = 0;
            foreach ($periods as $date) {
                $DailyRoom = DailyRoom::where('room_id', $this->room->id)->where('Date', $date->format('Y-m-d'))->first();

                if ($license->check('早鳥折扣')) {
                    $Basic_Total += $this->CalcEarlyBirdPrice($date, $DailyRoom);
                } else {
                    $Basic_Total += $DailyRoom->Price;
                }
            }
            //基本價格
            $PromotionsData[] = [
                'id' => '',
                'Name' => '基本方案',
                'Note' => null,
                'Total' => $Basic_Total,
                'Errors' => []
            ];

             return [
                'numbers' => $numbers,
                'Promotions' => $PromotionsData,
             ];
        }
        else
        {
            throw new \Exception('Not Valid');
        }
    }
    private function CalcEarlyBirdPrice($date, $collect): float
    {
        if ($collect->AllowEarlyBird) {
            if (\Carbon\Carbon::parse($date)->diffInDays(\Carbon\Carbon::today()->startOfDay()) >= $collect->EarlyBirdDays) {
                return $collect->EarlyBirdPrice;
            }
            return $collect->Price;
        }
        return $collect->Price;
    }
}
