<?php


namespace App\Service;


use App\Repositories\LicenseRepository;
use App\Repositories\TagRepository;
use Illuminate\Container\Container as App;

class Tag
{
    private $tag,$license;
    public function __construct(TagRepository $tag,LicenseRepository $license)
    {
        $this->tag = $tag;
        $this->license = $license;
    }

    public function GetAll()
    {
        return $this->tag->getquery()->with('RoomHasTag')->get();
    }

    public function readById($id)
    {
        return $this->tag->getquery()->with('RoomHasTag')->find($id);
    }

    public function paginate($num)
    {
        return $this->tag->getquery()->with('RoomHasTag')->paginate($num);
    }

    public function update($data){
        $keys = [
            'id','Name','State','DefaultPrice','room_id'
        ];
        $keys = $this->MacroEarlyBird($keys);
        $data = $data->only($keys);
        $this->tag->update($data['id'],collect($data)->except(['id'])->toArray());
    }

    public function updateTags($dataArray)
    {

        //檢查是否與DB內重複
        collect($dataArray)->map(function ($data){
            $tag  = new TagRepository(new App());
            if($tag->where('id','<>',$data['id'])->where('Name','=',$data['Name'])->first())
            {
                throw new \Exception('標籤名稱不得重複');
            }
        });

        $keys = [
            'id','Name','IconSrc','Rooms'
        ];
        $dataArray = collect($dataArray)->map(function ($data) use ($keys){
            return collect($data)->only($keys);
        });
        foreach ($dataArray as $data)
        {
            tap($this->tag->update($data['id'],collect($data)->except(['id','Rooms'])->toArray()),function(\App\Tag $tag) use ($data){
                $tag->RoomHasTag()->delete();
                foreach ($data['Rooms'] as $room){
                    $tag->RoomHasTag()->create([
                        'room_id' => $room
                    ]);
                }
            });

        }
    }


    public function delete($id){
        $this->tag->delete($id);
    }


    public function create($data)
    {
        $keys = [
            'Name','IconSrc'
        ];
        $Rooms = $data->Rooms;
        $data = $data->only($keys);
        return tap($this->tag->create($data),function(\App\Tag $tag) use ($Rooms){
            foreach ($Rooms as $Room) {
                $tag->RoomHasTag()->Create([
                    'room_id' => $Room
                ]);
            }
        });
    }
}
