<?php


namespace App\Service;


use App\Repositories\LicenseRepository;
use App\Repositories\ExtraShopRepository;
use http\Client\Response;

class ExtraShop
{
    private $extraShop, $license;

    public function __construct(ExtraShopRepository $extraShop, LicenseRepository $license)
    {
        $this->extraShop = $extraShop;
        $this->license = $license;
    }

    public function GetAll()
    {
        return $this->extraShop->getquery()->orderBy('Sort','desc')->get();
    }

    public function readById($id)
    {
        return $this->extraShop->getquery()->find($id);
    }

    public function paginate($num)
    {
        return $this->extraShop->getquery()->orderBy('Sort','desc')->paginate($num);
    }

    public function create($data)
    {
        $keys = [
            'Name','Src','Html','Price','Sort','State'
        ];
        $data = $data->only($keys);
        return $this->extraShop->create($data);
    }
    public function updateExtraShops($dataArray)
    {
        $keys = [
            'id','Name','Src','Price','Sort','State'
        ];
        $dataArray = collect($dataArray)->map(function ($data) use ($keys){
            return collect($data)->only($keys);
        });
        foreach ($dataArray as $data)
        {
            $this->extraShop->update($data['id'],collect($data)->except(['id'])->toArray());
        }
    }

    public function update($data)
    {
        $keys = [
            'id','Name','Src','Html','Price','Sort','State'
        ];
        $data = $data->only($keys);
        $this->extraShop->update($data['id'],collect($data)->except(['id'])->toArray());
    }


    public function delete($id){
        $this->extraShop->delete($id);
    }

}
