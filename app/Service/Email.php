<?php


namespace App\Service;


use App\Eloquents\Receiver;
use App\Eloquents\Senter;
use App\Repositories\EmailSettingRepository;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class Email
{
    private $setting,$senter;
    public function __construct(EmailSettingRepository $emailSettingRepository,Senter $senter = null)
    {
        $this->setting = $emailSettingRepository;
        $setting = $this->setting->first();
        $senter ? $this->senter = $senter :  $this->senter = new Senter($setting->account,decrypt($setting->password),$setting->Name);
    }
    public function Sent(Receiver $receiver,Mailable $mail){
        \Config::set('mail.from.address',$this->senter->email);
        \Config::set('mail.from.name',$this->senter->name);
        \Config::set('mail.mailers.smtp.username',$this->senter->email);
        \Config::set('mail.mailers.smtp.password',$this->senter->password);
        Mail::to($receiver->email)->queue($mail);
    }

}
