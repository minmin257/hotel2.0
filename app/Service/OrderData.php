<?php


namespace App\Service;


use Exception;
use Illuminate\Support\Facades\Auth;

class OrderData
{
    private $Order;
    public function __construct($Num)
    {
        if($Order = \App\Order::where('Num',$Num)->first()){
            if($Order->member_id){
                if($this->guard()->user() != null)
                {
                    if($this->guard()->user()->id === $Order->member_id ){
                        $this->Order = $Order;
                    }
                    else
                    {
                        throw new Exception('請登入後重試');
                    }
                }
                else
                {
                    throw new Exception('請登入後重試');
                }
            }
            else
            {
                $this->Order = $Order;
            }
        }
        else
        {
            throw new Exception('找不到該定單資料');
        }
    }

    private function guard()
    {
        return Auth::guard('member');
    }


    public function GetOrder()
    {
        return $this->Order;
    }

    public function UpdatePayment($Payment)
    {
        $this->Order->Update([
           'payment_id' => $Payment
        ]);
    }

    //Step3 訂單成立 選擇付款方式 &&　前台定單明細
    public function GetOrderDetail():array
    {
        $Price = $this->Order->TotalPrice;
        $Note = $this->Order->Note;
        $TaxNumber = $this->Order->TaxNumber;
        $Payment = $this->Order->payment_id ? $this->Order->payment_id : null;
        $Paid = $this->Order->Paid;
        $Refund = $this->Order->Refund;
        $Cancel = $this->Order->Cancel;


        $DiscountCode = [];
        if($this->Order->Code != Null)
        {
            $DiscountCode['Code'] = $this->Order->Code;
            $DiscountCode['Name'] = $this->Order->CodeName;
            $DiscountCode['Discount'] = $this->Order->Discount;
        }

        $BookPeople = [];
        $BookPeople['Email'] = $this->Order->Booking_Email;
        $BookPeople['Name'] = $this->Order->Booking_Name;
        $BookPeople['Sex'] = $this->Order->Booking_Sex;
        $BookPeople['IdNumber'] = $this->Order->Booking_IdNumber;
        $BookPeople['Phone'] = $this->Order->Booking_Phone;
        $BookPeople['Area'] = $this->Order->Booking_Area;

        $GuestPeople = [];
        $GuestPeople['Email'] = $this->Order->Guest_Email;
        $GuestPeople['Name'] = $this->Order->Guest_Name;
        $GuestPeople['Sex'] = $this->Order->Guest_Sex;
        $GuestPeople['IdNumber'] = $this->Order->Guest_IdNumber;
        $GuestPeople['Phone'] = $this->Order->Guest_Phone;
        $GuestPeople['Area'] = $this->Order->Guest_Area;

        $BookingData = [];
        foreach ($this->Order->OrderDetail as $OrderDetail){
            $temp = [];
            $temp['startDate'] = $OrderDetail->StartDate;
            $temp['endDate'] = $OrderDetail->EndDate;
            $temp['adults'] = $OrderDetail->Adult;
            $temp['childrens'] = $OrderDetail->Child;
            $temp['babys'] = $OrderDetail->Baby;
            $temp['Room'] = \App\Room::find($OrderDetail->room_id)->only(['id','Name','MainPicture','Html']);
            $temp['Promotion'] = $OrderDetail->promotion_id ? \App\Promotion::find($OrderDetail->promotion_id)->only(['id','Name','Note']) : ['Name' => '基本方案'];
            $temp['Extra'] = $OrderDetail->Extra;
            $temp['ExtraBed'] = $OrderDetail->ExtraBed;
            $temp['ExtraPeople'] = $OrderDetail->ExtraPeople;
            $temp['ExtraBedPrice'] = $OrderDetail->ExtraBedPrice;
            $temp['ExtraPeoplePrice'] = $OrderDetail->ExtraPeoplePrice;

            $TempShops = [];
            foreach ($OrderDetail->Shop as $Shop)
            {
                $TempShop = [];
                $TempShop['Name'] = $Shop->Name;
                $TempShop['Numbers'] = $Shop->Numbers;
                $TempShop['Nights'] = $Shop->Nights;
                $TempShop['Price'] = $Shop->Price;
                $TempShops[] = $TempShop;
            }
            $temp['ExtraShop'] = $TempShops;

            $TempCars = [];
            foreach ($OrderDetail->Car as $Car)
            {
                $TempCar = [];
                $TempCar['Name'] = $Car->Name;
                $TempCar['Numbers'] = $Car->Numbers;
                $TempCar['type'] = $Car->type;
                $TempCar['Price'] = $Car->Price;
                $TempCar['arrvdt'] = $Car->arrvdt;
                $TempCar['pickdt'] = $Car->pickdt;
                $TempCars[] = $TempCar;
            }
            $temp['ExtraCar'] = $TempCars;

            $BookingData[] = $temp;
        }

        $Data = [
            'BookPeople' =>$BookPeople,
            'GuestPeople' =>$GuestPeople,
            'Price' => $Price,
            'Note' => $Note,
            'TaxNumber' => $TaxNumber,
            'Payment' => $Payment,
            'DiscountCode' => $DiscountCode,
            'BookingData' => $BookingData,
            'Paid' => $Paid,
            'Refund' => $Refund,
            'Cancel' => $Cancel
        ];

        return $Data;
    }


}
