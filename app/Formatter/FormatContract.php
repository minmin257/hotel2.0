<?php


namespace App\Formatter;


class FormatContract
{
    public function customError($key = '', $messages = [], $status = 400)
    {
        return response()->json([
            'errors' => [
                $key => $messages
            ]
        ], $status);
    }
}
