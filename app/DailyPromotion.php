<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyPromotion extends Model
{
    protected $guarded = [];

    public function DailyRoom()
    {
        return $this->belongsTo(DailyRoom::class);
    }

    public function Promotion()
    {
        return $this->belongsTo(Promotion::class);
    }
}
