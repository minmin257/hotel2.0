<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $guarded = [];

    public function Type()
    {
        return $this->belongsTo(NewsType::class);
    }
}
