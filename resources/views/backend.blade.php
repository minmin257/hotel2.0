@inject('Setting','\App\Repositories\SettingRepository')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/d17564c281.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/css/backend/bootstrap.min.css">
    <link rel="stylesheet" href="/css/backend/calendar.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"/>
    <title>{{ $Setting->where('group','後台共用')->where('name','SiteName')->first()->value ?? 'Document' }}</title>
</head>
<body>
    <div id="app" style="width: 100%;height: 100%;margin: 0; padding: 0;">
        <main-app></main-app>
    </div>
{{--    <script type="text/javascript" src="/js/backend/vendor.js"></script>--}}
    <script type="text/javascript" src="/js/backend/app.js"></script>

{{--    <script type="text/javascript" src="/vendors~front.js"></script>--}}
{{--    <script type="text/javascript" src="/js/front/app.js"></script>--}}
{{--    <script type="text/javascript" src="/front.js"></script>--}}


    <script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
{{--    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>--}}
    <script src="/js/backend/bootstrap.min.js"></script>
    <script src="/js/backend/main.js"></script>
    <script src="https://cdn3.devexpress.com/jslib/20.1.4/js/dx.all.js"></script>
</body>
</html>
