@extends('front.about.default')
    @section('content')
        <main class="no-padding">
            <div class="AboutPageArea">
                <div class="row">
                    <div class="col-xl-4 col-lg-5 col-md-6 p-lg-0">
                        <div class="AboutPage_box">
                            <div>
                                <div class="logo mb-5">
                                    <div class=" d-flex justify-content-center mb-5 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;" data-wow-delay=".6s"><img src="/images/shape/logo.png" alt=""></div>
                                    <div class=" d-flex justify-content-center mb-4 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;" data-wow-delay=".9s"><img src="/images/shape/logo_2.png" alt=""></div>
                                    <div class=" d-flex justify-content-center logo_btm_line"><img src="/images/shape/logo_btm_line.png" alt=""></div>
                                </div>
                                <div class="AboutTextBox text-center mt-5">
                                    <h5 class="wow fadeIn" style="visibility: visible; animation-name: fadeIn;" data-wow-delay="1.2s">品牌 VS 價值</h5>
                                    <img class="mt-3 mb-5 wow fadeIn" style="visibility: visible; animation-name: fadeIn;" data-wow-delay="1.2s" src="/images/shape/brand_value.png" alt="">
                                    <p class="wow fadeIn" style="visibility: visible; animation-name: fadeIn;" data-wow-delay="1.8s">旅行何嘗不是人生經典闡述？有收穫、有咀嚼、滋味、有富足行旅、歡樂甘苦，也有人生體悟，這就是鴻騰酒店旅行價值觀的真義。
                                        旅行的出發是肇始，歷經洗禮、滋潤，終至成長，豐富人生旅程、富足自我心靈；如與「旅行會員」鏈接，旅宿旅程和賞遊建議提供，使得旅程Easy自在，亦能深度探索在地文化內涵，玩一個行樂開心的休閒目的，何樂不為？
                                        我們認為旅行是主張、由您自在定義，旅行意義在創造人生無限價值，旅宿服務在成就每個旅人/旅行者目的&心願；串接旅人與旅行風景、在地文化、在地事蹟的歷史鏈接~始終傳播著：在地精神、在地旅行的文化價值。</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-7 col-md-6 p-lg-0">
                        <div class="AboutPage_banner" >
                            <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item">
                                        <img src="/images/banner/banner1.jpg" alt="" class="" width="3000px" height="100%">

                                    </div>
                                    <div class="item active">
                                        <img src="/images/banner/banner2.jpg" alt="" class="" width="3000px" height="100%">

                                    </div>
                                    <div class="item">
                                        <img src="/images/banner/banner3.jpg" alt="" class="" width="3000px" height="100%">
                                    </div>
                                </div>
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    @endsection
