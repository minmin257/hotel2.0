@inject('Header', 'App\Presenters\HomePresenter')
<div class="top_menu">
    <div class="container">
        <ul class="top_menu_right">
            <li class="">
                <img src="https://img.icons8.com/ios/50/333333/ringer-volume.png" />
                <a href="tel:{{ $Header->getSystem()->Tel }}"> {{ $Header->getSystem()->Tel }}</a>
            </li>
            <li class="email ">
                <img src="https://img.icons8.com/ios/50/333333/open-envelope.png" />
                <a href="#">
                    <span class="email"> {{ $Header->getSystem()->Email }}</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<header class="fixed">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle mobile_menu_btn" data-toggle="collapse" data-target=".mobile_menu" aria-expanded="false">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="mobile_member_btn" href="{{ route('front.login') }}">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px" viewBox="0 0 30 30" version="1.1">
                    <g id="Page-2" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="Iconscout-Header-New" transform="translate(-1293.000000, -108.000000)">
                            <g id="Group-18" transform="translate(70.000000, 83.000000)">
                                <g id="Group-17" transform="translate(20.000000, 18.000000)">
                                    <g id="Group-78-Copy" transform="translate(1196.000000, 0.000000)">
                                        <g id="Group-77">
                                            <g id="user" transform="translate(7.000000, 7.000000)">
                                                <g id="Layer_1">
                                                    <circle id="Oval" fill="transparent" cx="15" cy="15" r="14.2857143"/>
                                                    <path class="icon-body" d="M15,20.2380952 C10.8809524,20.2380952 7.19047619,21.9761905 4.57142857,24.7619048 C7.16666667,27.547619 10.8809524,29.2857143 15,29.2857143 C19.1190476,29.2857143 22.8095238,27.547619 25.4285714,24.7619048 C22.8095238,21.9761905 19.1190476,20.2380952 15,20.2380952 Z" id="Path" fill="#DEB667"/>
                                                    <path class="icon-border" d="M15,30 C10.8571429,30 6.88095238,28.2619048 4.04761905,25.2619048 C3.80952381,25 3.78571429,24.6190476 4,24.3333333 C4.02380952,24.3095238 4.02380952,24.2857143 4.04761905,24.2857143 C6.88095238,21.2619048 10.8809524,19.5238095 15,19.5238095 C17.9047619,19.5238095 20.7142857,20.3571429 23.1428571,21.9285714 C23.4761905,22.1428571 23.5714286,22.5952381 23.3571429,22.9285714 C23.1428571,23.2619048 22.6904762,23.3571429 22.3571429,23.1428571 C20.1904762,21.7142857 17.6190476,20.952381 15,20.952381 C11.4285714,20.952381 8.11904762,22.3095238 5.57142857,24.7619048 C8.11904762,27.2142857 11.4285714,28.5714286 15,28.5714286 C22.4761905,28.5714286 28.5714286,22.4761905 28.5714286,15 C28.5714286,7.52380952 22.4761905,1.42857143 15,1.42857143 C7.52380952,1.42857143 1.42857143,7.52380952 1.42857143,15 C1.42857143,17.1904762 1.95238095,19.3809524 2.97619048,21.3095238 C3.16666667,21.6666667 3.02380952,22.0952381 2.66666667,22.2857143 C2.30952381,22.4761905 1.88095238,22.3333333 1.69047619,21.9761905 C0.595238095,19.8333333 0,17.4285714 0,15 C0,6.73809524 6.73809524,0 15,0 C23.2619048,0 30,6.73809524 30,15 C30,23.2619048 23.2619048,30 15,30 Z" id="Path" fill="#DEB667"/>
                                                    <path d="M15,5.71428571 C11.8571429,5.71428571 9.28571429,8.28571429 9.28571429,11.4285714 C9.28571429,14.5714286 11.8571429,17.1428571 15,17.1428571 C18.1428571,17.1428571 20.7142857,14.5714286 20.7142857,11.4285714 C20.7142857,8.28571429 18.1428571,5.71428571 15,5.71428571 Z" id="Path" fill="#DEB667"/>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </a>

            <a class="navbar-brand light" href="{{ route('front.index') }}">
                <img src="{{ $Header->getSystem()->Src }}" height="45">
            </a>
            <a class="navbar-brand dark nodisplay" href="{{ route('front.index') }}">
                <img src="{{ $Header->getSystem()->Src }}" height="45">
            </a>
        </div>
        <nav id="main_menu" class="mobile_menu navbar-collapse">
            <ul id="menu-main-menu" class="nav navbar-nav">
                <li class="mobile_menu_title" style="display:none;">Menu</li>
                @foreach($Header->getSubject() as $subject)
                    @switch($subject->Tag)
                        @case('about')
                        @case('news')
                        @case('traffic')
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ route('front.'.$subject->Tag) }}">{{ $subject->Name }}</a></li>
                            @break
                        @case('rooms')
                        <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="rooms.html">房型介紹</a></li>
                            @break
                        @default
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="rooms.html">{{ $subject->Name }}</a></li>
                    @endswitch
                @endforeach

                <li class="menu-item menu-item-type-post_type_archive menu-item-object-hotel_rooms menu-item-has-children dropdown language d-lg-block d-md-none d-none">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle" >中 <span class="arrow mobile-dropdown-toggle"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        @foreach($Header->getLocale() as $locale)
                            <li class="menu-item menu-item-type-post_type_archive menu-item-object-hotel_rooms"><a href="#">{{ $locale->Name }}</a></li>
                        @endforeach
                    </ul>
                </li>

                <li class="menu_button"><a href="{{route('front.booking')}}" class="btn eb-btn" target="_self"><img src="https://img.icons8.com/ios/25/ffffff/today.png" />立即訂房</a></li>
                <li class="member-btn d-lg-block d-md-none d-none">
                    <a href="{{route('front.login')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px" viewBox="0 0 30 30" version="1.1">
                            <g id="Page-2" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="Iconscout-Header-New" transform="translate(-1293.000000, -108.000000)">
                                    <g id="Group-18" transform="translate(70.000000, 83.000000)">
                                        <g id="Group-17" transform="translate(20.000000, 18.000000)">
                                            <g id="Group-78-Copy" transform="translate(1196.000000, 0.000000)">
                                                <g id="Group-77">
                                                    <g id="user" transform="translate(7.000000, 7.000000)">
                                                        <g id="Layer_1">
                                                            <circle id="Oval" fill="transparent" cx="15" cy="15" r="14.2857143"/>
                                                            <path class="icon-body" d="M15,20.2380952 C10.8809524,20.2380952 7.19047619,21.9761905 4.57142857,24.7619048 C7.16666667,27.547619 10.8809524,29.2857143 15,29.2857143 C19.1190476,29.2857143 22.8095238,27.547619 25.4285714,24.7619048 C22.8095238,21.9761905 19.1190476,20.2380952 15,20.2380952 Z" id="Path"/>
                                                            <path class="icon-border" d="M15,30 C10.8571429,30 6.88095238,28.2619048 4.04761905,25.2619048 C3.80952381,25 3.78571429,24.6190476 4,24.3333333 C4.02380952,24.3095238 4.02380952,24.2857143 4.04761905,24.2857143 C6.88095238,21.2619048 10.8809524,19.5238095 15,19.5238095 C17.9047619,19.5238095 20.7142857,20.3571429 23.1428571,21.9285714 C23.4761905,22.1428571 23.5714286,22.5952381 23.3571429,22.9285714 C23.1428571,23.2619048 22.6904762,23.3571429 22.3571429,23.1428571 C20.1904762,21.7142857 17.6190476,20.952381 15,20.952381 C11.4285714,20.952381 8.11904762,22.3095238 5.57142857,24.7619048 C8.11904762,27.2142857 11.4285714,28.5714286 15,28.5714286 C22.4761905,28.5714286 28.5714286,22.4761905 28.5714286,15 C28.5714286,7.52380952 22.4761905,1.42857143 15,1.42857143 C7.52380952,1.42857143 1.42857143,7.52380952 1.42857143,15 C1.42857143,17.1904762 1.95238095,19.3809524 2.97619048,21.3095238 C3.16666667,21.6666667 3.02380952,22.0952381 2.66666667,22.2857143 C2.30952381,22.4761905 1.88095238,22.3333333 1.69047619,21.9761905 C0.595238095,19.8333333 0,17.4285714 0,15 C0,6.73809524 6.73809524,0 15,0 C23.2619048,0 30,6.73809524 30,15 C30,23.2619048 23.2619048,30 15,30 Z" id="Path"/>
                                                            <path class="icon-head" d="M15,5.71428571 C11.8571429,5.71428571 9.28571429,8.28571429 9.28571429,11.4285714 C9.28571429,14.5714286 11.8571429,17.1428571 15,17.1428571 C18.1428571,17.1428571 20.7142857,14.5714286 20.7142857,11.4285714 C20.7142857,8.28571429 18.1428571,5.71428571 15,5.71428571 Z" id="Path"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>
                </li>
                <li class="mobile_menu_language pt-5 mt-2">
                    <div class="d-flex justify-content-center font-13">
                        @foreach($Header->getLocale() as $locale)
                            <a href="">{{$locale->Name}}</a>
                        @endforeach
                    </div>
                </li>
            </ul>
        </nav>
    </div>
</header>
