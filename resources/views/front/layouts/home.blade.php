@inject('Home', 'App\Presenters\HomePresenter')
<main class="no-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 entry">
                <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="wpb_revslider_element wpb_content_element color-overlay-slider overlay-opacity-01">
                                    <p class="rs-p-wp-fix"></p>
                                    <rs-module-wrap id="hero_wrapper" data-source="gallery" style="background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
                                        <rs-module id="hero" data-version="6.2.17">
                                            <rs-slides>
                                                @foreach($Home->getHomeBanner() as $homeBanner)
                                                    <rs-slide data-key="rs-{{ $loop->iteration }}" data-title="Slide" data-anim="ei:d;eo:d;s:d;r:0;t:crossfade;sl:d;">
                                                        <img src="{{ $homeBanner->Src }}" title="slider-3" width="1920" height="863" data-parallax="5" class="rev-slidebg" data-no-retina>
                                                        <rs-layer data-type="text" data-rsp_ch="on" data-xy="x:c;yo:310px,310px,320px,320px;"
                                                                  data-text="s:80,80,40,30;l:80,80,60,60;fw:900,900,700,700;a:inherit;" data-frame_0="y:-100%;o:1;tp:600;" data-frame_0_mask="u:t;" data-frame_1="tp:600;st:500;sp:1500;sR:500;" data-frame_1_mask="u:t;" data-frame_999="o:0;tp:600;st:w;sR:7000;" style="z-index:5;font-family:Roboto;">{!! $homeBanner->Html !!}</rs-layer>
                                                    </rs-slide>
                                                @endforeach
                                            </rs-slides>
                                        </rs-module>
                                    </rs-module-wrap>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="weatherBox text-white position-absolute">
                    <div class="box d-flex">
                        <div class="mr-3">
                            <span id="loc"></span><br>
                            <span class="date"></span>
                        </div>
                        <h1 class="display-3 mr-2">
                            <span id="temp"></span>
                            <span id="unit"></span>
                        </h1>
                        <i class="wi display-4"></i>
                    </div>
                </div>
                <div class="hbf half-over-slider">
                    <div class="inner">
                        <form id="search-form" class="search-form" action="#" method="get" target="">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="eagle_booking_datepicker">Check in/Check out</label>
                                    <input id="eagle_booking_datepicker" type="text" class="form-control eagle-booking-datepicker" placeholder="Check in &nbsp;&nbsp;→&nbsp;&nbsp; Check out" value="" autocomplete="off" readonly>
                                    <input id="eagle_booking_checkin" type="hidden" name="eagle_booking_checkin">
                                    <input id="eagle_booking_checkout" type="hidden" name="eagle_booking_checkout">
                                </div>
                                <div class="col-md-6">
                                    <label for="eagle_booking_guests">Guests</label>
                                    <div class="eb-guestspicker">
                                        <div class="form-control guestspicker">
                                            Guests
                                            <span class="gueststotal"></span>
                                        </div>
                                        <div class="eb-guestspicker-content">
                                            <div class="guests-buttons">
                                                <div class="description">
                                                    <label>成人</label>
                                                    <div class="ages">Ages 12+</div>
                                                </div>
                                                <div class="guests-button">
                                                    <div class="minus"></div>
                                                    <input type="text" id="eagle_booking_adults" name="eagle_booking_adults" class="booking-guests" value="1" min="1" max="4">
                                                    <div class="plus"></div>
                                                </div>
                                            </div>
                                            <div class="guests-buttons">
                                                <div class="description">
                                                    <label>小孩</label>
                                                    <div class="ages">Ages 4 - 12</div>
                                                </div>
                                                <div class="guests-button">
                                                    <div class="minus"></div>
                                                    <input type="text" id="eagle_booking_children" name="eagle_booking_children" class="booking-guests" value="0" min="0" max="4">
                                                    <div class="plus"></div>
                                                </div>
                                            </div>
                                            <div class="guests-buttons">
                                                <div class="description">
                                                    <label>嬰兒</label>
                                                    <div class="ages">Ages 0 - 3</div>
                                                </div>
                                                <div class="guests-button">
                                                    <div class="minus"></div>
                                                    <input type="text" id="eagle_booking_baby" name="eagle_booking_baby" class="booking-guests" value="0" min="0" max="4">
                                                    <div class="plus"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button id="eb_search_form" class="button eb-btn" type="button" onclick="SearchRoom()">
                                        <span class="eb-btn-text">CHECK ONLINE<br>線上訂房</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<div class="container-fluid">
    <div class="row AboutArea">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 AboutArea_L" data-aos="fade-up" data-aos-duration="1200">
            <h1 class="section-title">{{ $Home->getHomeAbout()->Title }}</h1>
            <h2 class="mb-5">{{ $Home->getHomeAbout()->Sub }}</h2>
            {!! $Home->getHomeAbout()->Html !!}
            <a href="" class="moreBtn">了解更多</a>
        </div>
        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 AboutArea_R" data-aos="fade-up" data-aos-duration="2500">
            <div class="AboutArea_img">
                <img src="{{ $Home->getHomeAbout()->Src }}" alt="">
            </div>
            <img class="position-absolute img-left-shadow" src="images/shadow.png" alt="">
            <div class="position-absolute img-logo">
                <img src="/images/logo.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 NewsArea_top d-flex wrap_2">
            <img class="section-title-img position-absolute" src="images/shape/logo_shape.png" alt="">
            <div>
                <h1 class="section-title">{{ $Home->getHomeNews()->Title }}</h1>
                <div class="d-flex justify-content-between">
                    <h2 class=" mb-5 text-right">{{ $Home->getHomeNews()->Sub }}</h2>
                </div>
            </div>
            <div class="section-title_Text d-flex flex-column" data-aos="fade-up" data-aos-duration="1200">
                {!! $Home->getHomeNews()->Html !!}
                <a href="" class="moreBtn mt-4 mr-5">了解更多</a>
            </div>
        </div>
    </div>
</div>

@section('js')
    <script type="text/javascript">
        // 訂房日期
        var eb_js_settings = {
            "eb_booking_type": "builtin",
            "eagle_booking_date_format": "yyyy\/mm\/dd",
            "eb_custom_date_format": "",
            "eb_terms_conditions": "1",
            "eb_calendar_availability_period": "24",
            "eb_room_slider_nav": "1",
            "eagle_booking_price_range_min": "20",
            "eagle_booking_price_range_max": "189",
            "eb_decimal_numbers": "0",
            "eagle_booking_price_range_default_min": "1",
            "eagle_booking_price_range_default_max": "700",
            "eb_discount_text": "Discount",
            "eb_price_range_currency": "\u20ac",
            "eb_booking_nights": "晚",
            "eb_calendar_sunday": "Su",
            "eb_calendar_monday": "Mo",
            "eb_calendar_tuesday": "Tu",
            "eb_calendar_wednesday": "We",
            "eb_calendar_thursday": "Th",
            "eb_calendar_friday": "Fr",
            "eb_calendar_saturday": "Sa",
            "eb_calendar_january": "January",
            "eb_calendar_february": "February",
            "eb_calendar_march": "March",
            "eb_calendar_april": "April",
            "eb_calendar_may": "May",
            "eb_calendar_june": "June",
            "eb_calendar_july": "July",
            "eb_calendar_august": "August",
            "eb_calendar_september": "September",
            "eb_calendar_october": "October",
            "eb_calendar_november": "November",
            "eb_calendar_december": "December"
        };

        // owl輪播
        setREVStartSize({
            c: 'hero',
            rl: [1240, 1024, 778, 480],
            el: [800, 768, 960, 720],
            gw: [1170, 1024, 778, 480],
            gh: [800, 768, 960, 720],
            type: 'standard',
            justify: '',
            layout: 'fullwidth',
            mh: "0"
        });
        var revapi2, tpj;
        jQuery(function() {
            tpj = jQuery;
            revapi2 = tpj("#hero");
            if (revapi2 == undefined || revapi2.revolution == undefined) {
                revslider_showDoubleJqueryError("hero");
            } else {
                revapi2.revolution({
                    sliderLayout: "fullwidth",
                    visibilityLevels: "1240,1024,778,480",
                    gridwidth: "1170,1024,778,480",
                    gridheight: "800,768,960,720",
                    lazyType: "smart",
                    perspectiveType: "local",
                    editorheight: "800,768,960,720",
                    responsiveLevels: "1240,1024,778,480",
                    progressBar: {
                        disableProgressBar: true
                    },
                    navigation: {
                        mouseScrollNavigation: false,
                        onHoverStop: false,
                        touch: {
                            touchenabled: true,
                            swipe_min_touches: 50
                        },
                        arrows: {
                            enable: true,
                            style: "zeus",
                            hide_onmobile: true,
                            hide_under: 600,
                            hide_onleave: true,
                            left: {
                                h_offset: 30
                            },
                            right: {
                                h_offset: 30
                            }
                        }
                    },

                    fallbacks: {
                        allowHTML5AutoPlayOnAndroid: true
                    },
                });
            }

        });
    </script>
    <script type="text/javascript" src='/js/front/weather.js'></script>
    <script type="text/javascript" src='/js/front/room.js'></script>
    <script type="text/javascript">
        function SearchRoom()
        {
            let form =  {
                StartDate: $("#eagle_booking_checkin").val(),
                EndDate:  $("#eagle_booking_checkout").val(),
                Adults: parseInt($("#eagle_booking_adults").val()),
                Childrens: parseInt($("#eagle_booking_children").val()),
                Babys: parseInt($("#eagle_booking_baby").val())
            }
            sessionStorage.setItem("SearchingForm",JSON.stringify(form));
            window.location = "{{route('front.booking')}}";
        }
    </script>
@endsection
