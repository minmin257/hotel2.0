@inject('default', 'App\Presenters\HomePresenter')
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $default->getSystem()->SiteName }}</title>
    <link rel="shortcut icon" href="{{ $default->getSystem()->Src }}" type="image/x-icon">
    <script src="https://kit.fontawesome.com/d17564c281.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.9/css/weather-icons.min.css">
    <link rel="stylesheet" href="https://unpkg.com/aos@2.3.0/dist/aos.css">
    <link rel="stylesheet" href="/css/front/fonts.css">
    <link rel='stylesheet' href='/css/front/daterangepicker.css'>
    <link rel='stylesheet' href='/css/front/eb.css'>
    <link rel='stylesheet' href='/css/front/eb-responsive.css'>
    <link rel='stylesheet' href='/css/front/rs6.css'>
    <link rel='stylesheet' href='/css/front/bootstrap.min.css'>
    <link rel='stylesheet' href='/css/front/animate.min.css'>
    <link rel="stylesheet" href="/css/front/owl.carousel.min.css">
    <link rel='stylesheet' href='/css/front/main.css'>
    <link rel='stylesheet' href='/css/front/responsive.css'>
    <script type="text/javascript" src='/js/front/jquery.js'></script>
    <script type="text/javascript" src='/js/front/jquery-migrate.min.js'></script>
</head>

<body data-rsssl=1 class="home page-template-default page page-id-60 wide wpb-js-composer js-comp-ver-6.2.0 vc_responsive">
<div id="loading">
    <div class="inner">
        <div class="loading_effect d-flex flex-column justify-content-center">
            <img src="{{ $default->getSystem()->Src }}" alt="">
            <div class="d-flex justify-content-center">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
            </div>
        </div>
    </div>
</div>
<div id="app">

<div class="wrapper">
    @yield('header')


    @yield('content')


    @yield('EventsArea')


    <div class="container-fluid">
        @yield('NewsArea')

        @yield('RoomsArea')
    </div>
</div>

@yield('footer')
</div>


</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/aos@2.3.0/dist/aos.js"></script>
<script type="text/javascript" src='/js/front/eb.js'></script>
<script type="text/javascript" src='/js/front/rbtools.min.js'></script>
<script type="text/javascript" src='/js/front/rs6.min.js'></script>
<script type="text/javascript" src='/js/front/bootstrap.min.js'></script>
<script type="text/javascript" src='/js/front/isotope.pkgd.min.js'></script>
<script type="text/javascript" src='/js/front/jPushMenu.js'></script>
<script type="text/javascript" src='/js/front/jquery.countdown.min.js'></script>
<script type="text/javascript" src='/js/front/jquery.magnific-popup.min.js'></script>
<script type="text/javascript" src='/js/front/moment.js'></script>
<script type="text/javascript" src='/js/front/daterangepicker.js'></script>
<script type="text/javascript" src='/js/front/ion.rangeslider.min.js'></script>
<script type="text/javascript" src='/js/front/wow.min.js'></script>
<script type="text/javascript" src='/js/front/main.js'></script>
<script type="text/javascript" src='/js/front/owl.carousel.min.js'></script>
<script type="text/javascript" src='/js/front/owl.carousel.thumbs.min.js'></script>
<script>
    AOS.init({})
</script>
@yield('js')

</html>
