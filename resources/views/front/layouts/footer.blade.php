@inject('Footer', 'App\Presenters\HomePresenter')
<footer>
    <div class="inner">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12 d-flex justify-content-center">
                    <div class="clearfix widget f-widget logo_footer">
                        <a class="d-flex flex-column justify-content-center">
                            <img class="logo" src="{{ $Footer->getSystem()->Src }}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-xs-12 d-flex justify-content-center">
                    <div class="clearfix widget f-widget contact_footer">
                        <h3>CONTACT INFO</h3>
                        <address>
                            <ul class="address_details">
                                <li>
                                    <img class="pr-3" src="https://img.icons8.com/ios/18/858a99/address.png"/> {{ $Footer->getSystem()->Addr }}
                                </li>
                                <li>
                                    <img class="pr-3" src="https://img.icons8.com/ios/18/858a99/ringer-volume.png" alt=""> Tel: {{ $Footer->getSystem()->Tel }}
                                </li>
                                <li>
                                    <img class="pr-3" src="https://img.icons8.com/ios/18/858a99/open-envelope.png" alt=""> Email: {{ $Footer->getSystem()->Email }}
                                </li>
                            </ul>
                        </address>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-xs-12 d-flex justify-content-center">
                    <div class="clearfix widget f-widget widget_nav_menu">
                        <h3>CONTACT US</h3>
                        <div>
                            <a class="btn eb-btn">
                                <img class="pr-2" src="https://img.icons8.com/material-outlined/20/FFFFFF/filled-sent.png"/>Send Message
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-xs-12 d-flex justify-content-center">
                    <div class="clearfix widget f-widget widget_nav_menu">
                        <h3>SOCIAL</h3>
                        <div class="social-media">
                            @foreach($Footer->getSocialLink() as $item)
                                <a href="{{ preg_match('/http(s?)\:/', $item->href) ? $item->href : ($item->href ? url($item->href) : '#') }}" target="_self" class="{{ $item->Title }}">
                                    @if($item->Title === 'line')
                                        <i class="fab fa-{{ $item->Title }}"></i>
                                    @else
                                        <i class="fa fa-{{ $item->Title }}"></i>
                                    @endif
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="downfooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 tetx-center">
                    <div class="copyrights"> &copy; Copyright © HOMETEL HOTEL. All rights reserved<span class="d-lg-inline d-sm-none"> | </span><span class="d-lg-inline d-sm-block">Designed by <a href="https://jhong-creativity.com.tw/" target="_blank">JHONG</a></span></div>
                </div>
            </div>
        </div>
    </div>
</footer>
@yield('ShoppingCart')
<div id="back_to_top" class="right"> <i class="fa fa-angle-up" aria-hidden="true"></i></div>
