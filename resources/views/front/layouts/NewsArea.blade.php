@inject('NewsArea', 'App\Presenters\HomePresenter')
<div class="row NewsArea">
    <div class="col-lg-12 col-md-12 col-sm-12 NewsAreaBody" style="background: #f7f7f7;">
        <div data-aos="fade" data-aos-duration="2000">
            @foreach($NewsArea->getNews() as $news)
                <div class="col-lg-6 col-md-12 col-sm-12 pr-0 pl-0">
                    <div class="news_item">
                        <div class="NewsImg">
                            <a href="#">
                                <img src="{{ $news->Src }}" alt="">
                            </a>
                        </div>
                        <div class="NewsDate position-absolute text-center">
                            <div class="start">
                                <div class="month">
                                    {{ \Carbon\Carbon::parse($news->Start)->translatedFormat('M')}}
                                </div>
                                <div class="day">
                                    {{ \Carbon\Carbon::parse($news->Start)->translatedFormat('j')}}
                                </div>
                            </div>
                            @if($news->End)
                                <div class="end">
                                    <div class="month">
                                        {{ \Carbon\Carbon::parse($news->End)->translatedFormat('M')}}
                                    </div>
                                    <div class="day">
                                        {{ \Carbon\Carbon::parse($news->End)->translatedFormat('j')}}
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="NewsText">
                            <div class="NewsSort">
                                {{ $news->Type->Title }}
                            </div>
                            <h3>
                                <a href="">
                                    {{ $news->Title }}
                                </a>
                            </h3>
                            {!! $news->Html !!}
                            <div class="NewsBtn">
                                <a href="" class="">More +</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
