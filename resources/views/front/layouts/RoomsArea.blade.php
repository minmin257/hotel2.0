@inject('RoomsArea', 'App\Presenters\HomePresenter')
<div class="row RoomsArea">
    <div class="col-md-12 p-0">
        <div class="RoomsTopImg">
            <div class="topimg">
                <img class="w-100" src="{{ $RoomsArea->getHomeRoom()->Src }}" alt="">
            </div>
        </div>
        <div class="sloganBox text-center" data-aos="fade-up" data-aos-duration="2000">
            <h1>{{ $RoomsArea->getHomeRoom()->Title }}</h1>
            <h2>{{ $RoomsArea->getHomeRoom()->Sub }}</h2>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-12 col-lg-offset-2 col-md-offset-0">
                <div class="owl-carousel RoomsBoxBody">
                    @foreach($RoomsArea->getRooms() as $room)
                        <div class="item">
                            <a href="#">
                                <div class="RoomsBox">
                                    <div class="Rooms_des">
                                        <a href="">
                                            <div class="Rooms_img">
                                                <img src="{{ $room->MainPicture }}" alt="">
                                            </div>
                                        </a>
                                        <div class="Rooms_text text-center">
                                            <a href="#">
                                                <h4>{{ $room->Name }}</h4>
                                            </a>
                                            <p class="RoomsBed mb-0">{{ $room->PeopleNumber }}人房</p>
                                            <p class="RoomsSize mb-0">約{{ $room->Ping }}坪</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
</div>
