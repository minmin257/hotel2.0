import VueRouter from 'vue-router'
const Main = () => import(/* webpackChunkName: "Main" */ './components/children/Main')
const login = () => import(/* webpackChunkName: "login" */ './components/children/login')
const LoginedMain = () => import(/* webpackChunkName: "LoginedMain" */ './components/children/logined/Main')
const LoginedIndex = () => import(/* webpackChunkName: "LoginedIndex" */ './components/children/logined/index')
const LoginedBlade = () => import(/* webpackChunkName: "LoginedBlade" */ './components/children/logined/Blade')
const Profile = () => import(/* webpackChunkName: "Profile" */ './components/children/logined/children/Profile')
const Setting = () => import(/* webpackChunkName: "Setting" */ './components/children/logined/children/Setting')
const DailyRoom = () => import(/* webpackChunkName: "DailyRoom" */ './components/children/logined/children/DailyRoom')
const DailyPromotion = () => import(/* webpackChunkName: "DailyPromotion" */ './components/children/logined/children/DailyPromotion')
const RemainRoom = () => import(/* webpackChunkName: "RemainRoom" */ './components/children/logined/children/RemainRoom')
const Rooms = () => import(/* webpackChunkName: "Rooms" */ './components/children/logined/children/Rooms')
const Room = () => import(/* webpackChunkName: "Room" */ './components/children/logined/children/Room')
const Promotions = () => import(/* webpackChunkName: "Promotions" */ './components/children/logined/children/Promotions')
const Promotion = () => import(/* webpackChunkName: "Promotion" */ './components/children/logined/children/Promotion')
const Tags = () => import(/* webpackChunkName: "Tags" */ './components/children/logined/children/Tags')
const Rule = () => import(/* webpackChunkName: "Rule" */ './components/children/logined/children/Rule')
const ExtraShops = () => import(/* webpackChunkName: "ExtraShops" */ './components/children/logined/children/ExtraShops')
const ExtraShop = () => import(/* webpackChunkName: "ExtraShop" */ './components/children/logined/children/ExtraShop')
const ExtraCars = () => import(/* webpackChunkName: "ExtraCars" */ './components/children/logined/children/ExtraCars')
const ExtraCar = () => import(/* webpackChunkName: "ExtraCar" */ './components/children/logined/children/ExtraCar')
const Extra = () => import(/* webpackChunkName: "Extra" */ './components/children/logined/children/Extra')
const DiscountCodes = () => import(/* webpackChunkName: "DiscountCodes" */ './components/children/logined/children/DiscountCodes')
const DiscountCode = () => import(/* webpackChunkName: "DiscountCode" */ './components/children/logined/children/DiscountCode')
const MemberList = () => import(/* webpackChunkName: "MemberList" */ './components/children/logined/children/MemberList')
const Member = () => import(/* webpackChunkName: "Member" */ './components/children/logined/children/Member')
const OrderList = () => import(/* webpackChunkName: "OrderList" */ './components/children/logined/children/OrderList')
const Order = () => import(/* webpackChunkName: "Order" */ './components/children/logined/children/Order')
const EmailSetting = () => import(/* webpackChunkName: "EmailSetting" */ './components/children/logined/children/EmailSetting')
const FrontSetting = () => import(/* webpackChunkName: "FrontSetting" */ './components/children/logined/children/FrontSetting')
const RoleList = () => import(/* webpackChunkName: "RoleList" */ './components/children/logined/children/RoleList')
const Role = () => import(/* webpackChunkName: "Role" */ './components/children/logined/children/Role')
const Auth = () => import(/* webpackChunkName: "Auth" */ './components/children/logined/children/Auth')

const TestCalc = () => import(/* webpackChunkName: "TestCalc" */ './components/children/logined/children/TestCalc')




export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/webmin',
            component: Main,
            children:[
                {
                    meta: {
                        GuestOnly : true
                    },
                    path: 'login',
                    name: 'login',
                    component: login
                },
                {
                    meta: {
                        requiresAuth : true
                    },
                    path: '/',
                    component:LoginedMain,
                    children:[
                        {
                            path: 'index',
                            name: 'index',
                            component: LoginedIndex,
                        },
                        {
                            path: '/',
                            component: LoginedBlade,
                            children:[
                                {
                                    path: 'Profile',
                                    name: 'Profile',
                                    component: Profile,
                                },
                                {
                                    path: 'Setting',
                                    name: 'Setting',
                                    component: Setting,
                                },
                                {
                                    path: 'DailyRoom/:id',
                                    name: 'DailyRoom',
                                    component: DailyRoom,
                                },
                                {
                                    path: 'DailyPromotion/:id',
                                    name: 'DailyPromotion',
                                    component: DailyPromotion,
                                },
                                {
                                    path: 'RemainRoom/:id',
                                    name: 'RemainRoom',
                                    component: RemainRoom,
                                },
                                {
                                    path: 'Rooms',
                                    name: 'Rooms',
                                    component: Rooms,
                                },
                                {
                                    path: 'Room/:id',
                                    name: 'Room',
                                    component: Room,
                                },
                                {
                                    path: 'Promotions',
                                    name: 'Promotions',
                                    component: Promotions,
                                },
                                {
                                    path: 'Promotion/:id',
                                    name: 'Promotion',
                                    component: Promotion,
                                },
                                {
                                    path: 'Tags',
                                    name: 'Tags',
                                    component: Tags,
                                },
                                {
                                    path: 'Rule',
                                    name: 'Rule',
                                    component: Rule,
                                },
                                {
                                    path: 'ExtraShops',
                                    name: 'ExtraShops',
                                    component: ExtraShops,
                                },
                                {
                                    path: 'ExtraShop/:id',
                                    name: 'ExtraShop',
                                    component: ExtraShop,
                                },
                                {
                                    path: 'ExtraCars',
                                    name: 'ExtraCars',
                                    component: ExtraCars,
                                },
                                {
                                    path: 'ExtraCar/:id',
                                    name: 'ExtraCar',
                                    component: ExtraCar,
                                },
                                {
                                    path: 'Extra/:id',
                                    name: 'Extra',
                                    component: Extra,
                                },
                                {
                                    path: 'DiscountCodes',
                                    name: 'DiscountCodes',
                                    component: DiscountCodes,
                                },
                                {
                                    path: 'DiscountCode/:id',
                                    name: 'DiscountCode',
                                    component: DiscountCode,
                                },
                                {
                                    path: 'MemberList',
                                    name: 'MemberList',
                                    component: MemberList,
                                },
                                {
                                    path: 'MemberList/:id',
                                    name: 'Member',
                                    component: Member,
                                },
                                {
                                    path: 'OrderList',
                                    name: 'OrderList',
                                    component: OrderList,
                                },
                                {
                                    path: 'OrderList/:Num',
                                    name: 'Order',
                                    component: Order,
                                },

                                //試算用
                                {
                                    path: 'TestCalc',
                                    name: 'TestCalc',
                                    component: TestCalc,
                                },

                                {
                                    path: 'EmailSetting',
                                    name: 'EmailSetting',
                                    component: EmailSetting,
                                },
                                {
                                    path: 'FrontSetting',
                                    name: 'FrontSetting',
                                    component: FrontSetting,
                                },
                                {
                                    path: 'RoleList',
                                    name: 'RoleList',
                                    component: RoleList,
                                },
                                {
                                    path: 'RoleList/:name',
                                    name: 'Role',
                                    component: Role,
                                },
                                {
                                    path: 'Auth/:account',
                                    name: 'Auth',
                                    component: Auth,
                                },



                            ]
                        },
                    ]
                },
            ]
        }
    ]

})
