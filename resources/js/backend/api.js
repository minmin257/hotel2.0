import store from './storage.js';
import router from './router.js';
window.axios = require('axios');

axios.interceptors.response.use((response) => {
    //如果過期 會refresh token
    var token = response.headers.authorization
    if (token) {
        // 如果 header 中存在 token，那么触发 refreshToken 方法，替换本地的 token
        this.$store.dispatch('Refresh', token)
    }
    return response
},(error)=>{
    if(error.response.status == 401){
        // router.push('/login');
        `Swal.fire({
            title:'提示',
            icon:'error',
            text:'時效過期，請重新登入',
            showCloseButton: true,
            confirmButtonColor: '#ba0000',
            confirmButtonText: '重新登入',
        }).then((result) => {
            store.commit('Logout');
            router.push({name: 'login'});
        })`
    }
    else if(error.response.status == 403)
    {
        error.response.data.errors = {錯誤:['權限不足，請洽管理員']}
    }

    return Promise.reject(error);
});

if (store.getters.currentUser) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${store.getters.currentUser.token}`
}

// Config相關的 api
const ConfigRequest = '/api/Config';

// Setting相關的 api
const SettingRequest = '/api/Setting';

// Jwt 相關的 api
const JwtRequest = '/api/Jwt';

// Room 相關的 api
const RoomRequest = '/api/Room';

// DailyRoom 相關的 api
const DailyRoomRequest = '/api/DailyRoom';

// Promotion 相關的 api
const PromotionRequest = '/api/Promotion';

// Tag 相關的 api
const TagRequest = '/api/Tag';


//DailyPromotion 相關的 api
const DailyPromotionRequest = '/api/DailyPromotion';

//Remain 相關的 api
const RemainRequest = '/api/Remain';

//Rulte 相關的 api
const RuleRequest = '/api/Rule';

//ExtraShop 相關的 api
const ExtraShopRequest = '/api/ExtraShop';

//ExtraCar 相關的 api
const ExtraCarRequest = '/api/ExtraCar';


//DiscountCodeRequest 相關的 api
const DiscountCodeRequest = '/api/DiscountCode';


//Member 管理
const MembersRequest = '/api/Members';

//Order 管理
const OrdersRequest = '/api/Orders';

//Mail
const MailSettingRequest = '/api/MailSetting';

//System
const SystemRequest = '/api/System';


//Role
const RoleRequest = '/api/Role';

const UserRequest = '/api/User';

const PermissionRequest = '/api/Permission';

// 傳入 name 取得 Config
export function GetConfig(name){
    return new Promise((res,rej) =>{
        axios.get(ConfigRequest+"/"+name)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

// 傳入 group 取得 Setting
export function GetSetting(group,name = null) {
    return new Promise((res,rej) =>{
        axios.get(SettingRequest+"/"+group+"/"+name)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

// 傳入 name 取得 Config
export function LoginJwt(credential){
    return new Promise((res,rej) =>{
        axios.post(JwtRequest+"/login",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export function GetRooms(){
    return new Promise((res,rej) =>{
        axios.get(RoomRequest)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function GetRoom(credential){
    return new Promise((res,rej) =>{
        axios.get(RoomRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function GetRoomExtra(credential){
    return new Promise((res,rej) =>{
        axios.get(RoomRequest+"/Extra",{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateRoomExtra(credential){
    return new Promise((res,rej) =>{
        axios.post(RoomRequest+"/Extra",credential )
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function CreateRoom(credential){
    return new Promise((res,rej) =>{
        axios.post(RoomRequest+'/Create',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function UpdateRooms(credential){
    return new Promise((res,rej) =>{
        axios.post(RoomRequest+'/UpdateRooms',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateRoom(credential){
    return new Promise((res,rej) =>{
        axios.post(RoomRequest+'/Update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function DeleteRoom(credential){
    return new Promise((res,rej) =>{
        axios.delete(RoomRequest+'/Delete', {data:credential})
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function GetDailyRoom(credential){
    return new Promise((res,rej) =>{
        axios.get(DailyRoomRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function UpdateOrCreateDailyRoom(credential){
    return new Promise((res,rej) =>{
        axios.post(DailyRoomRequest+'/UpdateOrCreate',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateOrCreateScopeDailyRoom(credential){
    return new Promise((res,rej) =>{
        axios.post(DailyRoomRequest+'/UpdateOrCreateScope',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function GetPromotions(credential){
    return new Promise((res,rej) =>{
        axios.get(PromotionRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function GetPromotionFilters(){
    return new Promise((res,rej) =>{
        axios.get(PromotionRequest+'/filters')
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function GetPromotion(credential){
    return new Promise((res,rej) =>{
        axios.get(PromotionRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdatePromotions(credential){
    return new Promise((res,rej) =>{
        axios.post(PromotionRequest+'/UpdatePromotions',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdatePromotion(credential){
    return new Promise((res,rej) =>{
        axios.post(PromotionRequest+'/Update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function DeletePromotion(credential){
    return new Promise((res,rej) =>{
        axios.delete(PromotionRequest+'/Delete', {data:credential})
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function CreatePromotion(credential){
    return new Promise((res,rej) =>{
        axios.post(PromotionRequest+'/Create',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}



export async function GetTags(credential){
    return new Promise((res,rej) =>{
        axios.get(TagRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function CreateTag(credential){
    return new Promise((res,rej) =>{
        axios.post(TagRequest+'/Create',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function DeleteTag(credential){
    return new Promise((res,rej) =>{
        axios.delete(TagRequest+'/Delete', {data:credential})
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateTags(credential){
    return new Promise((res,rej) =>{
        axios.post(TagRequest+'/UpdateTags',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}



export async function GetDailyPromotion(credential){
    return new Promise((res,rej) =>{
        axios.get(DailyPromotionRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function UpdateOrCreateDailyPromotion(credential){
    return new Promise((res,rej) =>{
        axios.post(DailyPromotionRequest+'/UpdateOrCreate',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateOrCreateScopeDailyPromotion(credential){
    return new Promise((res,rej) =>{
        axios.post(DailyPromotionRequest+'/UpdateOrCreateScope',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}



export async function GetRemainRoom(credential){
    return new Promise((res,rej) =>{
        axios.get(RemainRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}




export async function Init() {
    return new Promise((res,rej) =>{
        axios.get('/api/init')
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function Calc(credential) {
    return new Promise((res,rej) =>{
        axios.post('/api/calc',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export function StoreOrder(credential){
    return new Promise((res,rej) =>{
        axios.post('/api/order/store', credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}



export async function GetRules(){
    return new Promise((res,rej) =>{
        axios.get(RuleRequest)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateRules(credential){
    return new Promise((res,rej) =>{
        axios.post(RuleRequest,credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function GetExtraShop(credential){
    return new Promise((res,rej) =>{
        axios.get(ExtraShopRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function GetExtraShops(credential){
    return new Promise((res,rej) =>{
        axios.get(ExtraShopRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateExtraShops(credential){
    return new Promise((res,rej) =>{
        axios.post(ExtraShopRequest+'/UpdateExtraShops',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function UpdateExtraShop(credential){
    return new Promise((res,rej) =>{
        axios.post(ExtraShopRequest+'/Update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function CreateExtraShop(credential){
    return new Promise((res,rej) =>{
        axios.post(ExtraShopRequest+'/Create',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function DeleteExtraShop(credential){
    return new Promise((res,rej) =>{
        axios.delete(ExtraShopRequest+'/Delete', {data:credential})
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}



export async function GetExtraCar(credential){
    return new Promise((res,rej) =>{
        axios.get(ExtraCarRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function GetExtraCars(credential){
    return new Promise((res,rej) =>{
        axios.get(ExtraCarRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateExtraCars(credential){
    return new Promise((res,rej) =>{
        axios.post(ExtraCarRequest+'/UpdateExtraCars',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function UpdateExtraCar(credential){
    return new Promise((res,rej) =>{
        axios.post(ExtraCarRequest+'/Update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function CreateExtraCar(credential){
    return new Promise((res,rej) =>{
        axios.post(ExtraCarRequest+'/Create',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function DeleteExtraCar(credential){
    return new Promise((res,rej) =>{
        axios.delete(ExtraCarRequest+'/Delete', {data:credential})
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function GetLicenses() {
    return new Promise((res,rej) =>{
        axios.get('/api/Licenses')
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}









export async function GetDiscountCodes(credential){
    return new Promise((res,rej) =>{
        axios.get(DiscountCodeRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function GetDiscountCode(credential){
    return new Promise((res,rej) =>{
        axios.get(DiscountCodeRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateDiscountCodes(credential){
    return new Promise((res,rej) =>{
        axios.post(DiscountCodeRequest+'/UpdateDiscountCodes',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateDiscountCode(credential){
    return new Promise((res,rej) =>{
        axios.post(DiscountCodeRequest+'/Update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function DeleteDiscountCode(credential){
    return new Promise((res,rej) =>{
        axios.delete(DiscountCodeRequest+'/Delete', {data:credential})
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function CreateDiscountCode(credential){
    return new Promise((res,rej) =>{
        axios.post(DiscountCodeRequest+'/Create',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function GetMembers(credential){
    return new Promise((res,rej) =>{
        axios.post(MembersRequest,credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function UpdateMembers(credential){
    return new Promise((res,rej) =>{
        axios.post(MembersRequest+'/updateMembers',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function GetMember(id){
    return new Promise((res,rej) =>{
        axios.post(MembersRequest+'/'+id)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function UpdateMember(credential){
    return new Promise((res,rej) =>{
        axios.post(MembersRequest+'/update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function DeleteMember(credential){
    return new Promise((res,rej) =>{
        axios.delete(MembersRequest+'/Delete', {data:credential})
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function GetOrders(credential){
    return new Promise((res,rej) =>{
        axios.post(OrdersRequest,credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function GetOrder(Num){
    return new Promise((res,rej) =>{
        axios.post(OrdersRequest+'/'+Num)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function UpdateOrder(credential){
    return new Promise((res,rej) =>{
        axios.post(OrdersRequest+'/update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function GetMailSetting(credential){
    return new Promise((res,rej) =>{
        axios.get(MailSettingRequest)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function UpdateMailSetting(credential){
    return new Promise((res,rej) =>{
        axios.post(MailSettingRequest+'/update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function ValidMailSetting(credential){
    return new Promise((res,rej) =>{
        axios.post(MailSettingRequest+'/valid',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function GetSystem(){
    return new Promise((res,rej) =>{
        axios.get(SystemRequest)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateSystem(credential){
    return new Promise((res,rej) =>{
        axios.post(SystemRequest+'/update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export function UpdateProfile(credential)
{
    return new Promise((res,rej) =>{
        axios.post(JwtRequest+'/updateProfile',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export function GetRoles()
{
    return new Promise((res,rej) =>{
        axios.get(RoleRequest)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export function CreateUser(credential)
{
    return new Promise((res,rej) =>{
        axios.post(UserRequest+"/create",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export function UpdateUserState(credential)
{
    return new Promise((res,rej) =>{
        axios.post(UserRequest+"/updateState",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export function DeleteUser(id)
{
    return new Promise((res,rej) =>{
        axios.delete(UserRequest+'/delete', { params : { 'id' : id } })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export function GetUser(account)
{
    return new Promise((res,rej) =>{
        axios.get(UserRequest+'/'+account)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export function UpdateUser(credential)
{
    return new Promise((res,rej) =>{
        axios.post(UserRequest+"/update",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}



export function GetPermissions()
{
    return new Promise((res,rej) =>{
        axios.get(PermissionRequest)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export function GetHasPermissions(name)
{
    return new Promise((res,rej) =>{
        axios.get(PermissionRequest+"/"+name)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export function UpdateHasPermissions(credential)
{
    return new Promise((res,rej) =>{
        axios.post(PermissionRequest+"/update",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
