import storage from './storage.js'

export function SwalAlertErrorMessage(response) {
    if(response.status != 401)
    {
        var errors = response.data.errors;
        var html = '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">';

        Object.keys(errors).forEach(function(item){
            html+= '<div><i class="fas fa-times" style="color:#f27474"></i>  '+errors[item].join(',')+"</div>";
        })
        Swal.fire({
            icon:'error',
            html: html,
            showCloseButton: false,
            showConfirmButton: false,
            width: '300px',
            heightAuto: false
        })
    }

}

export function SwalAlertSuccessRedirect(response,redirect = null)
{
    var messages = response.data ?? '成功';
    if(redirect != null)
    {
        return new Promise((res,rej) => {
            Swal.fire({
                title: messages,
                icon: 'success',
                showConfirmButton: false,
                width: '300px',
                timer: 970,
                heightAuto: false
            }).then((result) => {
                res(true);
            });
        });
    }
    else
    {
        Swal.fire({
            title: messages,
            icon: 'success',
            showConfirmButton: false,
            width: '300px',
            timer: 970,
            heightAuto: false
        })
    }
}

export function SwalAlertAsk(title = null,message = null){
    title = title ?? '您確定要刪除嗎？';
    message = message ?? '此動作將不可回復';
    return new Promise((res,rej) =>{
        Swal.fire({
            title:  '<h2 style="color: #f8bb86">'+ title +'</h2>',
            html: '<span style="color: #707070;font-size: 14px;">'+ message +'<span>',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: '確定',
            cancelButtonText: '取消',
            reverseButtons: true,
            heightAuto: false,
            width: '300px',
            confirmButtonColor: '#43d39e',
            cancelButtonColor: '#ff5c75',
        }).then((result) => {
            if (result.value) {
                res(true);
            }
            else
            {
                rej(false);
            }
        })
    });
}



export function Submit(method,url,data,message = null,redirect = null){
    return new Promise((res,rej) =>{
        axios({
            method: method,
            url: url,
            data: data,
        })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export function getLocalUser(){
    const userStr = localStorage.getItem("user");
    if(!userStr){
        return null;
    }
    return JSON.parse(userStr);
}

export function logout(){
    return storage.commit('Logout');
}

//目前無用到
export function ButtonOpenFileManger(){
    $.fn.filemanager = function (type, options) {
        type = type || 'file';
        const userStr = localStorage.getItem("user");

        this.on('click', function (e) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/filemanager';
            var target_input = $('#' + $(this).data('input'));
            var target_preview = $('#' + $(this).data('preview'));
            if (userStr) {
                var token = 'Bearer ' + JSON.parse(userStr)['token'];
                window.open(route_prefix + '?type=' + type + '&token=' + token, 'FileManager', 'width=900,height=600');
            } else {
                window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
            }
            window.SetUrl = function (items) {
                var file_path = items.map(function (item) {
                    return item.url;
                }).join(',');

                // set the value of the desired input to image url
                target_input.val('').val(file_path).trigger('change');
                target_input[0].dispatchEvent(new CustomEvent('change'));

                // clear previous preview
                target_preview.html('');

                // set or change the preview image src
                items.forEach(function (item) {
                    target_preview.append(
                        $('<img>').css('height', '5rem').attr('src', item.thumb_url)
                    );
                });

                // trigger change event
                target_preview.trigger('change');
            };
            return false;
        });
    }
}
