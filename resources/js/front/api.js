import store from './storage.js';
import router from './router.js';
import Axios from "axios";
window.axios = require('axios');
const Front = '/api/Front';

axios.interceptors.response.use((response) => {
    //如果過期 會refresh token
    var token = response.headers.authorization
    if (token) {
        // 如果 header 中存在 token，那么触发 refreshToken 方法，替换本地的 token
        this.$store.dispatch('Refresh', token)
    }
    return response
}, (error)=>{
    if(error.response.status == 401){
        store.commit('Logout');
        Swal.fire({
            title:'提示',
            icon:'error',
            text:'時效過期，請重新登入',
            showCloseButton: true,
            confirmButtonColor: '#ba0000',
            confirmButtonText: '重新登入',
        }).then((result) => {
            router.push({
                name:'FrontLogin'
            });
        })
    }

    else if(error.response.status == 403)
    {
        error.response.data.errors = {錯誤:['權限不足']}
    }

    return Promise.reject(error);
});

if (store.getters.Member) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${store.getters.Member.token}`
}

// 左邊calendar search推薦
export function  Searching(credential) {
    return new Promise((res,rej) =>{
        axios.post(Front+"/Searching",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

//中間calendar 當前月份抓取剩餘
export function  Remain(credential) {
    return new Promise((res,rej) =>{
        axios.post(Front+"/Remain",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

//中間calendar 重新計算
export function  SearchingByRoom(credential) {
    return new Promise((res,rej) =>{
        axios.post(Front+"/SearchingByRoom",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

//抓取全域參數(加床 加備品費用)
export function Rule() {
    return new Promise((res,rej) =>{
        axios.post(Front+"/Rule")
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

//抓取Room 的 ExtraShops
export function ExtraShopFromRoom(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/ExtraShopFromRoom",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export function ExtraCarFromRoom(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/ExtraCarFromRoom",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export function CheckingDiscountCode(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/CheckingDiscountCode",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export function CheckingOrder(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/CheckingOrder",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export function PaymentSelect(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/PaymentSelect",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export function UpdatePayment(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/UpdatePayment",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export function GetATMInfo(){
    return new Promise((res,rej) =>{
        axios.post(Front+"/ATMInfo")
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}



export function Login(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/Login",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export function Register(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/Register",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export function Forgot(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/Forgot",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export function UpdateMemberBasic(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/UpdateMemberBasic",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export function UpdateMemberPassword(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/UpdateMemberPassword",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export function UpdateMemberEmail(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/UpdateMemberEmail",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export function GetMemberOrderRecord(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/MemberOrderRecord",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export function GetOrder(credential){
    return new Promise((res,rej) =>{
        axios.post(Front+"/OrderRecord",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
