import VueRouter from 'vue-router'
const FrontMain = () => import(/* webpackChunkName: "FrontMain" */ './components/layouts/Main')
const Search = () => import(/* webpackChunkName: "Search" */ './components/layouts/Search')
const PromotionPicking = () => import(/* webpackChunkName: "PromotionPicking" */ './components/layouts/PromotionPicking')
const Initialise = () => import(/* webpackChunkName: "Initialise" */ './components/layouts/Initialise')
const PaymentSelect = () => import(/* webpackChunkName: "PaymentSelect" */ './components/layouts/PaymentSelect')
const ATM = () => import(/* webpackChunkName: "ATM" */ './components/layouts/ATM')
const FrontLogin = () => import(/* webpackChunkName: "FrontLogin" */ './components/Logined/Login')
const Member = () => import(/* webpackChunkName: "Member" */ './components/Logined/Member')
const FrontProfile = () => import(/* webpackChunkName: "FrontProfile" */ './components/Logined/Profile')
const FrontOrder = () => import(/* webpackChunkName: "FrontOrder" */ './components/Logined/Order')
export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/booking',
            component: FrontMain,
            children:[
                {
                    path: '/',
                    name: 'Search',
                    component: Search
                },
                {
                    path: 'Selecting',
                    name: 'Selecting',
                    component: PromotionPicking,
                    props: (route) => ({
                        ...route.params
                    })
                },
                {
                    path: 'Initialise',
                    name: 'Initialise',
                    component: Initialise,
                },
                {
                    path: 'PaymentSelect/:Num',
                    name: 'PaymentSelect',
                    component: PaymentSelect,
                    // props: (route) => ({
                    //     ...route.params
                    // })
                },
                {
                    path: 'ATM/:Num',
                    name: 'ATM',
                    component: ATM,
                },
            ]
        },
        {
            meta: {
                FrontGuestOnly : true
            },
            path: '/login',
            component: FrontLogin,
            name: 'FrontLogin',
        },
        {
            path: '/member',
            component: Member,
            children: [
                {
                    meta: {
                        MemberOnly : true
                    },
                    path: 'Profile',
                    name: 'FrontProfile',
                    component: FrontProfile
                },
                {
                    meta: {
                        MemberOnly : true
                    },
                    path: 'Order',
                    name: 'FrontOrder',
                    component: FrontOrder,
                    props: (route) => ({
                        ...route.params
                    })
                },
            ]
        }

    ]

})
