<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::name('front.')->group(function () {
    Route::get('/', 'FrontController@index')->name('index');
    Route::get('/about', 'FrontController@about')->name('about');
    Route::get('/rooms', 'FrontController@rooms')->name('rooms');
    Route::get('/service', 'FrontController@service')->name('service');
    Route::get('/news', 'FrontController@news')->name('news');
    Route::get('/traffic', 'FrontController@traffic')->name('traffic');

    Route::get('/login', 'FrontController@booking')->name('login');
    Route::prefix('booking')->group(function () {
        Route::get('/', 'FrontController@booking')->name('booking');
        Route::get('/{any}', 'FrontController@booking')->where('any', '.*');
    });
    Route::prefix('member')->group(function () {
        Route::get('/', 'FrontController@booking');
        Route::get('/{any}', 'FrontController@booking')->where('any', '.*');
    });

});

Route::name('backend.')->prefix('webmin')->group(function () {
    Route::get('/', 'BackendController@index')->where('any', '.*');
    Route::get('/{any}', 'BackendController@index')->where('any', '.*')->name('index');
});


Route::group(['prefix' => 'filemanager', 'middleware' => ['api']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::get('/TEST', function(){
    \Artisan::call('storage:link');
});
