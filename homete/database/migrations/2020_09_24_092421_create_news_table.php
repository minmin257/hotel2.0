<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->foreignId('type_id')->nullable()->constrained('news_types')->onDelete('cascade');
            $table->string('Src')->nullable()->comment('封面圖');
            $table->string('Title');
            $table->longText('Html');
            $table->date('Start')->comment('活動開始');
            $table->date('End')->nullable()->comment('活動結束');
            $table->integer('Sort')->default(0)->comment('順序');
            $table->boolean('State')->default(1)->comment('啟用狀態(0關閉/1顯示)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
