<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rules', function (Blueprint $table) {
            $table->id();
            // 用於推薦房型
            $table->unsignedDecimal('DefaultChildrenTransRate', 6, 2)->default(0.6)->comment('預設孩童轉換基準');
            $table->unsignedDecimal('DefaultBabyTransRate', 6, 2)->default(0)->comment('預設嬰兒轉換基準');

            $table->unsignedDecimal('DefaultExtraPeoplePrice', 6, 2)->default(0)->comment('預設全域加人費用');
            $table->unsignedDecimal('DefaultExtraBedPrice', 6, 2)->default(0)->comment('預設全域加床費用');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rules');
    }
}
