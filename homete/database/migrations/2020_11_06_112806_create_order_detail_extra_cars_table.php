<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailExtraCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail_extra_cars', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_detail_id')->constrained('order_details')->onDelete('cascade');
            $table->foreignId('extra_car_id')->constrained('extra_cars')->onDelete('cascade');
            $table->string('Name');
            $table->integer('Numbers');
            $table->unsignedDecimal('Price', 10, 2)->default(0)->comment('當時單價');
            $table->integer('type')->comment('1單程 2來回');
            $table->dateTime('arrvdt')->nullable()->comment('抵達日期時間');
            $table->dateTime('pickdt')->nullable()->comment('出發日期時間');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_detail_extra_cars');
    }
}
