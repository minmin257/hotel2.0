<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_codes', function (Blueprint $table) {
            $table->id();
            $table->string('Code')->unique();
            $table->string('Name');
            $table->longText('Html')->nullable();
            $table->unsignedInteger('Discount')->default(0)->comment('折價金');
            $table->unsignedInteger('Remains')->default(0)->comment('剩餘數量');
            $table->dateTime('Allow_Start')->comment('有效訂單時間 起');
            $table->dateTime('Allow_End')->comment('有效訂單時間 訖');
            $table->dateTime('Use_Start')->comment('有效使用 起');
            $table->dateTime('Use_End')->comment('有效使用 訖');

            $table->integer('Sort')->default(0)->comment('順序');
            $table->boolean('State')->default(1)->comment('啟用狀態(0關閉/1顯示)');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_codes');
    }
}
