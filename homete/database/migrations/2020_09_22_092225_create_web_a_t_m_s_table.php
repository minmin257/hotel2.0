<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebATMSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 暫無，預留ATM虛擬帳戶確認匯款
        Schema::create('web_a_t_m_s', function (Blueprint $table) {
            $table->id();
            $table->string('OrderNum');
            $table->string('AMOUNT');
            $table->string('AUTHSTATUS');
            $table->string('AUTHCODE');
            $table->datetime('AUTHTIME');
            $table->string('AUTHMSG');
            $table->string('CARDNO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('web_a_t_m_s');
    }
}
