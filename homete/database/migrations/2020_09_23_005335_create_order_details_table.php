<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->constrained('orders')->onDelete('cascade');
            $table->date('StartDate')->comment('入住日期');
            $table->date('EndDate')->comment('退房日期');
            $table->foreignId('room_id')->comment('房型')->constrained('rooms')->onDelete('cascade');
            $table->foreignId('promotion_id')->nullable()->comment('活動方案')->constrained('promotions')->onDelete('cascade');
            $table->integer('Price')->default(0)->comment('基本床價(含夜) 不含其餘加購部分');

            $table->integer('Adult')->default(1);
            $table->integer('Child')->default(0);
            $table->integer('Baby')->default(0);

            $table->boolean('Extra')->nullable()->comment('0 只加人 / 1 加人加床');
            $table->integer('ExtraBed')->default(0)->comment('加床數');
            $table->integer('ExtraPeople')->default(0)->comment('加人數');
            $table->integer('ExtraBedPrice')->default(0)->comment('當時加床價格');
            $table->integer('ExtraPeoplePrice')->default(0)->comment('當時加人價格');

//            $table->foreignId('daily_room_id')->constrained('daily_rooms')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
