<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('systems', function (Blueprint $table) {
            $table->id();
            $table->string('SiteName')->comment('網站名稱');
            $table->string('Src')->comment('logo圖片');
            $table->string('MetaDescription');
            $table->string('MetaKeyword');
            $table->string('Addr');
            $table->string('Tel');
            $table->string('Email');
            $table->boolean('Maintain')->default(0)->comment('0關閉/1維護');
            $table->string('Message')->comment('維護訊息');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('systems');
    }
}
