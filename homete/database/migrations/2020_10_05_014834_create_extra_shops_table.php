<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExtraShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_shops', function (Blueprint $table) {
            $table->id();
            $table->string('Name');
            $table->longText('Src')->nullable();
            $table->longText('Html')->nullable();
            $table->unsignedDecimal('Price', 10, 2)->default(0)->comment('商品加價');
            $table->integer('Sort')->default(0)->comment('順序');
            $table->boolean('State')->default(1)->comment('啟用狀態(0關閉/1顯示)');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_shops');
    }
}
