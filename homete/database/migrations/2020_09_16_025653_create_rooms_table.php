<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->string('Name')->comment('房型名稱');
            $table->integer('PeopleNumber')->comment('幾人房');
            // 台灣單位後續其他語言自動換算平方公尺(m²) AND 平方英尺(ft²)
            $table->integer('Ping')->default(0)->comment('房型坪數');

            // 目前預設 最多開放加一人(單人床) 最少0，但是預設未來擴充用，已保留整數。
            $table->integer('MaxExtraPeopleNumber')->default(0)->comment('最大加人數');
            $table->unsignedDecimal('DefaultPrice', 6, 2)->comment('預設基本價');
            $table->integer('DefaultNumber')->comment('預設間數');

            // 圖文相關
            $table->longText('MainPicture')->nullable()->comment('主縮圖');
            $table->longText('Html')->nullable()->comment('圖文編輯器');

            // 早鳥相關
            $table->boolean('DefaultAllowEarlyBird')->default(0)->comment('預設允許使用早鳥');
            $table->unsignedDecimal('DefaultEarlyBirdPrice', 6, 2)->default(0)->comment('預設早鳥價');
            $table->integer('DefaultEarlyBirdDays')->default(0)->comment('預設早鳥天數');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
