<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailyRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_rooms', function (Blueprint $table) {
            $table->id();

            $table->date('Date')->comment('每天日期');
            $table->unsignedBigInteger('room_id');
            $table->foreign('room_id')->references('id')->on('rooms')->cascadeOnDelete();
            $table->unsignedDecimal('Price', 6, 2)->comment('基本價');
            $table->integer('Number')->comment('間數');

            $table->boolean('AllowEarlyBird')->default(0)->comment('允許使用早鳥');
            $table->unsignedDecimal('EarlyBirdPrice', 6, 2)->default(0)->comment('早鳥價');
            $table->integer('EarlyBirdDays')->default(0)->comment('早鳥天數');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_rooms');
    }
}
