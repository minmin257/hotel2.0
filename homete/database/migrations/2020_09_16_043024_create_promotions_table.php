<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 加購
        Schema::create('promotions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('room_id');
            $table->foreign('room_id')->references('id')->on('rooms')->cascadeOnDelete();
            $table->string('Name')->comment('活動名稱');
            $table->longtext('Note')->comment('活動說明')->nullable();
            $table->boolean('State')->default(1)->comment('開啟/關閉');
            $table->unsignedDecimal('DefaultPrice', 6, 2)->comment('預設活動價');

            $table->boolean('DefaultAllowEarlyBird')->default(0)->comment('預設允許使用早鳥');
            $table->unsignedDecimal('DefaultEarlyBirdPrice', 6, 2)->default(0)->comment('預設早鳥價');
            $table->integer('DefaultEarlyBirdDays')->default(0)->comment('預設早鳥天數');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
