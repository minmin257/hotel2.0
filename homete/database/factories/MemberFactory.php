<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Member;
use Faker\Generator as Faker;

$factory->define(Member::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'password' => bcrypt('123456'),
        'Name' => $faker->userName,
        'idNumber' => $faker->unique()->password,
        'Phone' => $faker->unique()->phoneNumber
    ];
});
