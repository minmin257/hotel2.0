<?php

use Illuminate\Database\Seeder;

class Front extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\HomeBanner::create([
            'Src'=>'/images/banner/banner1.jpg',
            'Html'=>'Welcome to hotel',
        ]);
        \App\HomeBanner::create([
            'Src'=>'/images/banner/banner2.jpg',
            'Html'=>'Welcome to hotel',
        ]);
        \App\HomeBanner::create([
            'Src'=>'/images/banner/banner3.jpg',
            'Html'=>'Welcome to hotel',
        ]);

        \App\HomeAbout::create([
            'Title'=>'OUR HOTEL',
            'Sub'=>'關於騰鴻',
            'Html'=>'<p class="">旅行何嘗不是人生經典闡述？有收穫、有咀嚼、滋味、有富足行旅、歡樂甘苦，也有人生體悟，這就是鴻騰酒店旅行價值觀的真義。
                        旅行的出發是肇始，歷經洗禮、滋潤，終至成長，豐富人生旅程、富足自我心靈；如與「旅行會員」鏈接，旅宿旅程和賞遊建議提供，使得旅程Easy自在，亦能深度探索在地文化內涵，玩一個行樂開心的休閒目的，何樂不為？
                        我們認為旅行是主張、由您自在定義，旅行意義在創造人生無限價值，旅宿服務在成就每個旅人/旅行者目的&心願；串接旅人與旅行風景、在地文化、在地事蹟的歷史鏈接~始終傳播著：在地精神、在地旅行的文化價值。
                    </p>',
            'Src'=>'/images/banner/banner1.jpg'
        ]);

        \App\HomeNews::create([
            'Title'=>'News & Events',
            'Sub'=>'最新消息',
            'Html'=>'<p>隨時掌握騰鴻最新住宿優惠、活動不管<br>是好吃的好玩的、保證都不會錯過 !</p>',
        ]);

        \App\NewsType::create([
            'Title'=>'住宿專案'
        ]);
        \App\NewsType::create([
            'Title'=>'餐飲專案'
        ]);
        \App\NewsType::create([
            'Title'=>'一般公告'
        ]);

        \App\News::create([
            'type_id'=>1,
            'Src'=>'/images/news/news1.jpg',
            'Title'=>'2020最新優惠。一泊二食秋季住房專案2020金秋芳華。一泊二食秋季住房專案2020金秋芳華。一泊二食秋季住房專案',
            'Html'=>'<p>來花季放大您的優惠泡在花季。3倍fun鬆專案</p>',
            'Start'=>'2020-07-05',
            'End'=>'2020-09-20'
        ]);
        \App\News::create([
            'type_id'=>2,
            'Src'=>'/images/news/news2.jpg',
            'Title'=>'2020最新優惠。一泊二食秋季住房專案',
            'Html'=>'<p>來花季放大您的優惠泡在花季。3倍fun鬆專案</p>',
            'Start'=>'2020-07-05',
            'End'=>'2020-09-20'
        ]);
        \App\News::create([
            'type_id'=>3,
            'Src'=>'/images/news/news2.jpg',
            'Title'=>'2020最新優惠。一泊二食秋季住房專案',
            'Html'=>'<p>來花季放大您的優惠泡在花季。3倍fun鬆專案</p>',
            'Start'=>'2020-07-05',
            'End'=>'2020-09-20'
        ]);
        \App\News::create([
            'type_id'=>3,
            'Src'=>'/images/news/news1.jpg',
            'Title'=>'2020最新優惠。一泊二食秋季住房專案',
            'Html'=>'<p>來花季放大您的優惠泡在花季。3倍fun鬆專案</p>',
            'Start'=>'2020-07-05',
        ]);

        \App\System::create([
            'SiteName'=>'高雄鴻騰酒店',
            'Src'=>'/images/logo.png',
            'MetaDescription'=>'高雄鴻騰酒店',
            'MetaKeyword'=>'高雄鴻騰酒店',
            'Addr'=>'高雄市三民區大昌二路266號',
            'Tel'=>'07-3898899',
            'Email'=>'Hotel@gmail.com.tw',
            'Message'=>'本系統於進行設備維護，暫時無法提供服務。造成不便，敬請見諒。'
        ]);

        \App\SocialLink::create([
            'Title'=>'facebook',
        ]);
        \App\SocialLink::create([
            'Title'=>'instagram',
        ]);
        \App\SocialLink::create([
            'Title'=>'line',
        ]);
        \App\SocialLink::create([
            'Title'=>'weixin',
        ]);

        \App\Localization::create([
            'Name'=>'中',
            'Short'=>'zh-TW'
        ]);
        \App\Localization::create([
            'Name'=>'EN',
            'Short'=>'en'
        ]);
        \App\Localization::create([
            'Name'=>'日',
            'Short'=>'jp'
        ]);
        \App\Localization::create([
            'Name'=>'簡',
            'Short'=>'cn'
        ]);
        \App\Localization::create([
            'Name'=>'한',
            'Short'=>'ko'
        ]);

        \App\Subject::create([
            'Name'=>'關於我們',
            'Tag'=>'about'
        ]);
        \App\Subject::create([
            'Name'=>'房型介紹',
            'Tag'=>'rooms'
        ]);
        \App\Subject::create([
            'Name'=>'設施服務',
            'Tag'=>'service'
        ]);
        \App\Subject::create([
            'Name'=>'最新消息',
            'Tag'=>'news'
        ]);
        \App\Subject::create([
            'Name'=>'交通資訊',
            'Tag'=>'traffic'
        ]);

        \App\HomeRoom::create([
            'Title'=>'ROOM TYPES',
            'Sub'=>'房型介紹',
            'Src'=>'/images/banner/banner2.jpg'
        ]);
    }
}
