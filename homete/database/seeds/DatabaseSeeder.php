<?php

use App\User;
use App\Setting;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        \App\Role::Create([
            'name' =>  '系統管理者'
        ]);
        \App\Role::Create([
            'name' =>  '網站管理者'
        ]);

        $permissions = [
            '房務管理-基本設置','房務管理-房型設置','房務管理-活動設置','房務管理-標籤設置','房務管理-加購商品設置','房務管理-加購接送設置','房務管理-折扣代碼','房務管理-訂房情況','訂單管理','會員管理','帳號管理',
            '網站設定','信箱設定'
        ];
        foreach ($permissions as $permission)
        {
            \App\Permission::Create([
                'name' => $permission
            ]);
        }

        foreach (\App\Role::all() as $role)
        {
            foreach (\App\Permission::all() as $permission)
            {
                \App\RoleHasPermission::Create([
                    'permission_id' => $permission->id,
                    'role_id' => $role->id
                ]);
            }
        }

        User::Create([
            'name' => 'elliot',
            'account' => 'admin',
            'password' => bcrypt('123456')
        ]);

        \App\ModelHasRole::Create([
            'model_type' => 'user',
            'model_id' => 1,
            'role_id' => 1
        ]);

        $settings = [
          '後台共用' => [
              [
                'name' => '登入icon',
                'value' => '/images/shape/logo.png'
              ],
              [
                  'name' => 'SiteName',
                  'value' => '後台'
              ],
              [
                  'name' => 'Headericon',
                  'value' => '/images/logo03.png'
              ],
              [
                  'name' => 'Header標題',
                  'value' => '高雄鴻騰酒店'
              ],
          ]
        ];

        foreach ($settings as $GroupKey => $Group)
        {
            foreach ($Group as $item)
            {
                Setting::Create([
                    'group' => $GroupKey,
                    'name' => $item['name'],
                    'value' => $item['value']
                ]);
            }
        }


        \App\Rule::Create([
            'DefaultChildrenTransRate' => 0.6,
            'DefaultBabyTransRate' => 0,
            'DefaultExtraPeoplePrice' => 100,
            'DefaultExtraBedPrice' => 600
        ]);


        \App\License::Create([
            'Name' => '篩選過濾',
            'State' => true
        ]);

        \App\License::Create([
           'Name' => '活動模組',
            'State' => true
        ]);

        \App\License::Create([
            'Name' => '加購模組',
            'State' => true
        ]);

        \App\License::Create([
            'Name' => '加購接送',
            'State' => true
        ]);

        \App\License::Create([
            'Name' => '加購高鐵',
            'State' => true
        ]);

        \App\License::Create([
            'Name' => '早鳥折扣',
            'State' => true
        ]);

        \App\License::Create([
            'Name' => '折扣代碼',
            'State' => true
        ]);

        \App\PaymentMethod::Create([
            'Name' => '線上刷卡',
        ]);

        \App\PaymentMethod::Create([
            'Name' => 'ATM轉帳',
        ]);



        \App\Room::Create([
            'Name' => '觀日雙人房(兩小床)',
            'PeopleNumber' => 2,
            'MaxExtraPeopleNumber' => 1,
            'DefaultPrice' => 2000,
            'DefaultNumber' => 5,
            'MainPicture'=>'/images/room/rooms.jpg',
            'Html' => "<strong style='color: #ff0000;'>搭配安心旅遊專案，須提供一位尚未使用補助的國名身分證辦理入住，若無法提供證件須補足差價1000元</strong><br>

                坪數：60坪<br>

                房內設備：戶外採光恆溫戲水池、滑水道／迎賓水幕流瀑、休憩區吧檯、多功能豪華（大理石／湧泉氣泡）幻彩按摩浴缸、法式維琪浴（黃金雨）、浴缸管路潔淨逆洗設備、乾濕分離淋浴間、紓壓蒸氣室、KTV影音歡唱觸控式設備、藍光ZOD影片隨選設備、WIFI／光纖網路上網、客房55吋LED電視／KTV影音歡唱室65吋LED電視／浴室32吋LED電視。<br>



                重金打造超奢華戶外吧檯派對聚會空間、游泳池滑水道、高規格影音設備，不論家庭聚會、生日趴踢、單身派對，<br>

                絕對是狂歡派對的最佳首選！ ＊因政府噪音管制下，晚上十點過後請降低音量!<br>



                進房時間：平日(週日至週四)約18:00~21:00左右；旺日(週五)約21:00~23:00左右；假日(週六/國定假日前夕/連續假期/特殊假日)約21:00~23:00左右。<br>

                實際進房時間【依訂房順序＆旅館回覆為準】，若在意進房時間，記得可事先來電詢問入住當天的訂房狀況或選擇提前入住專案訂房哦！<br>

                連續住宿隔日中午退房，當天晚上(平日18:00後 假日21:00後)再次入住。若不退房需收休息費用(平日2次，假日3次)依房型不同，休息費用亦不同。<br>



                1.每房含【套餐式早餐。中西式各乙客】。<br>

                2.客房內附餅乾點心/礦泉水/汽水/咖啡包/茶包。<br>

                3.此房型為一房一車庫。<br>

                4.房型基本入住人數為兩位，每增加一位酌收清潔費$500元（不附備品、加床墊、早餐）；如須加床及附備品、早餐每加一單人床酌收加床費$800元（含清潔費），以此類推,清潔費及加床費用於旅館現場支付；小朋友120公分以下不佔床(不附備品)均不加價，如需附備品，每位酌收成本費$200元。特殊節日、連續假期、春節期間以及前夕 – 清潔費及加床費依現場公告為主。<br>

                5.以上費用已含稅及服務費。<br>

                6.目前全館皆已設置wifi免費提供使用.<br>

                7.本館接受攜帶寵物入住，須酌收寵物費用$500元不另退還，如退房檢查時客房如有軟硬體設備物品損壞，亦另須依市價賠償。<br><br>

                旅客住宿日當日取消訂房扣預付訂金金額100%<br>
                旅客於住宿日前1日內取消訂房扣房價預付訂金金額80%<br>
                旅客於住宿日前2-3日內取消訂房扣房價預付訂金金額70%<br>
                旅客於住宿日前4-6日內取消訂房扣房價預付訂金金額60%<br>
                旅客於住宿日前7-9日內取消訂房扣房價預付訂金金額50%<br>
                旅客於住宿日前10-13日內取消訂房扣房價預付訂金金額30%<br>
                旅客於住宿日前14日前(含14日)取消訂房扣房價預付訂金金額0%"
        ]);

        \App\Room::Create([
            'Name' => '觀日人房(一大床)',
            'PeopleNumber' => 2,
            'MaxExtraPeopleNumber' => 1,
            'DefaultPrice' => 2000,
            'DefaultNumber' => 3,
            'MainPicture'=>'/images/room/rooms2.jpg',
            'Html' => "<strong style='color: #ff0000;'>搭配安心旅遊專案，須提供一位尚未使用補助的國名身分證辦理入住，若無法提供證件須補足差價1000元</strong><br>

                坪數：60坪<br>

                房內設備：戶外採光恆溫戲水池、滑水道／迎賓水幕流瀑、休憩區吧檯、多功能豪華（大理石／湧泉氣泡）幻彩按摩浴缸、法式維琪浴（黃金雨）、浴缸管路潔淨逆洗設備、乾濕分離淋浴間、紓壓蒸氣室、KTV影音歡唱觸控式設備、藍光ZOD影片隨選設備、WIFI／光纖網路上網、客房55吋LED電視／KTV影音歡唱室65吋LED電視／浴室32吋LED電視。<br>



                重金打造超奢華戶外吧檯派對聚會空間、游泳池滑水道、高規格影音設備，不論家庭聚會、生日趴踢、單身派對，<br>

                絕對是狂歡派對的最佳首選！ ＊因政府噪音管制下，晚上十點過後請降低音量!<br>



                進房時間：平日(週日至週四)約18:00~21:00左右；旺日(週五)約21:00~23:00左右；假日(週六/國定假日前夕/連續假期/特殊假日)約21:00~23:00左右。<br>

                實際進房時間【依訂房順序＆旅館回覆為準】，若在意進房時間，記得可事先來電詢問入住當天的訂房狀況或選擇提前入住專案訂房哦！<br>

                連續住宿隔日中午退房，當天晚上(平日18:00後 假日21:00後)再次入住。若不退房需收休息費用(平日2次，假日3次)依房型不同，休息費用亦不同。<br>



                1.每房含【套餐式早餐。中西式各乙客】。<br>

                2.客房內附餅乾點心/礦泉水/汽水/咖啡包/茶包。<br>

                3.此房型為一房一車庫。<br>

                4.房型基本入住人數為兩位，每增加一位酌收清潔費$500元（不附備品、加床墊、早餐）；如須加床及附備品、早餐每加一單人床酌收加床費$800元（含清潔費），以此類推,清潔費及加床費用於旅館現場支付；小朋友120公分以下不佔床(不附備品)均不加價，如需附備品，每位酌收成本費$200元。特殊節日、連續假期、春節期間以及前夕 – 清潔費及加床費依現場公告為主。<br>

                5.以上費用已含稅及服務費。<br>

                6.目前全館皆已設置wifi免費提供使用.<br>

                7.本館接受攜帶寵物入住，須酌收寵物費用$500元不另退還，如退房檢查時客房如有軟硬體設備物品損壞，亦另須依市價賠償。<br><br>

                旅客住宿日當日取消訂房扣預付訂金金額100%<br>
                旅客於住宿日前1日內取消訂房扣房價預付訂金金額80%<br>
                旅客於住宿日前2-3日內取消訂房扣房價預付訂金金額70%<br>
                旅客於住宿日前4-6日內取消訂房扣房價預付訂金金額60%<br>
                旅客於住宿日前7-9日內取消訂房扣房價預付訂金金額50%<br>
                旅客於住宿日前10-13日內取消訂房扣房價預付訂金金額30%<br>
                旅客於住宿日前14日前(含14日)取消訂房扣房價預付訂金金額0%"
        ]);

        \App\Room::Create([
            'Name' => '家庭四人房',
            'PeopleNumber' => 4,
            'MaxExtraPeopleNumber' => 1,
            'DefaultPrice' => 6000,
            'DefaultNumber' => 3,
            'MainPicture'=>'/images/room/rooms.jpg',
            'Html' => "<strong style='color: #ff0000;'>搭配安心旅遊專案，須提供一位尚未使用補助的國名身分證辦理入住，若無法提供證件須補足差價1000元</strong><br>

                坪數：60坪<br>

                房內設備：戶外採光恆溫戲水池、滑水道／迎賓水幕流瀑、休憩區吧檯、多功能豪華（大理石／湧泉氣泡）幻彩按摩浴缸、法式維琪浴（黃金雨）、浴缸管路潔淨逆洗設備、乾濕分離淋浴間、紓壓蒸氣室、KTV影音歡唱觸控式設備、藍光ZOD影片隨選設備、WIFI／光纖網路上網、客房55吋LED電視／KTV影音歡唱室65吋LED電視／浴室32吋LED電視。<br>



                重金打造超奢華戶外吧檯派對聚會空間、游泳池滑水道、高規格影音設備，不論家庭聚會、生日趴踢、單身派對，<br>

                絕對是狂歡派對的最佳首選！ ＊因政府噪音管制下，晚上十點過後請降低音量!<br>



                進房時間：平日(週日至週四)約18:00~21:00左右；旺日(週五)約21:00~23:00左右；假日(週六/國定假日前夕/連續假期/特殊假日)約21:00~23:00左右。<br>

                實際進房時間【依訂房順序＆旅館回覆為準】，若在意進房時間，記得可事先來電詢問入住當天的訂房狀況或選擇提前入住專案訂房哦！<br>

                連續住宿隔日中午退房，當天晚上(平日18:00後 假日21:00後)再次入住。若不退房需收休息費用(平日2次，假日3次)依房型不同，休息費用亦不同。<br>



                1.每房含【套餐式早餐。中西式各乙客】。<br>

                2.客房內附餅乾點心/礦泉水/汽水/咖啡包/茶包。<br>

                3.此房型為一房一車庫。<br>

                4.房型基本入住人數為兩位，每增加一位酌收清潔費$500元（不附備品、加床墊、早餐）；如須加床及附備品、早餐每加一單人床酌收加床費$800元（含清潔費），以此類推,清潔費及加床費用於旅館現場支付；小朋友120公分以下不佔床(不附備品)均不加價，如需附備品，每位酌收成本費$200元。特殊節日、連續假期、春節期間以及前夕 – 清潔費及加床費依現場公告為主。<br>

                5.以上費用已含稅及服務費。<br>

                6.目前全館皆已設置wifi免費提供使用.<br>

                7.本館接受攜帶寵物入住，須酌收寵物費用$500元不另退還，如退房檢查時客房如有軟硬體設備物品損壞，亦另須依市價賠償。<br><br>

                旅客住宿日當日取消訂房扣預付訂金金額100%<br>
                旅客於住宿日前1日內取消訂房扣房價預付訂金金額80%<br>
                旅客於住宿日前2-3日內取消訂房扣房價預付訂金金額70%<br>
                旅客於住宿日前4-6日內取消訂房扣房價預付訂金金額60%<br>
                旅客於住宿日前7-9日內取消訂房扣房價預付訂金金額50%<br>
                旅客於住宿日前10-13日內取消訂房扣房價預付訂金金額30%<br>
                旅客於住宿日前14日前(含14日)取消訂房扣房價預付訂金金額0%"
        ]);

        \App\Promotion::Create([
            'room_id' => 1,
            'Name' => '一泊一食',
            'State' => 1,
            'DefaultPrice' => 1800
        ]);
        \App\Promotion::Create([
            'room_id' => 2,
            'Name' => '一泊一食',
            'State' => 1,
            'DefaultPrice' => 1800
        ]);
        \App\Promotion::Create([
            'room_id' => 3,
            'Name' => '一泊一食',
            'State' => 1,
            'DefaultPrice' => 5800
        ]);

        \App\Promotion::Create([
            'room_id' => 1,
            'Name' => '一泊二食',
            'State' => 1,
            'DefaultPrice' => 1900
        ]);
        \App\Promotion::Create([
            'room_id' => 2,
            'Name' => '一泊二食',
            'State' => 1,
            'DefaultPrice' => 1900
        ]);
        \App\Promotion::Create([
            'room_id' => 3,
            'Name' => '一泊二食',
            'State' => 1,
            'DefaultPrice' => 5900
        ]);


        \App\Promotion::Create([
            'room_id' => 1,
            'Name' => '平日優惠',
            'State' => 1,
            'DefaultPrice' => 1600
        ]);
        \App\Promotion::Create([
            'room_id' => 2,
            'Name' => '假日優惠',
            'State' => 1,
            'DefaultPrice' => 1700
        ]);

        tap(\App\Tag::Create([
            'Name' => '雙人床',
            'IconSrc' => 'https://img.icons8.com/ios/35/858a99/bed.png'
        ]),function(\App\Tag $tag){
            $tag->RoomHasTag()->Create([
                'room_id' => 1
            ]);
            $tag->RoomHasTag()->Create([
                'room_id' => 2
            ]);
        });

        tap(\App\Tag::Create([
            'Name' => 'TV',
            'IconSrc' => 'https://img.icons8.com/ios/35/858a99/retro-tv.png'
        ]),function(\App\Tag $tag){
            $tag->RoomHasTag()->Create([
                'room_id' => 3
            ]);
        });

        tap(\App\Tag::Create([
            'Name' => '浴缸',
            'IconSrc' => 'https://img.icons8.com/ios/35/858a99/spa.png'
        ]),function(\App\Tag $tag){
            $tag->RoomHasTag()->Create([
                'room_id' => 1
            ]);
            $tag->RoomHasTag()->Create([
                'room_id' => 2
            ]);
            $tag->RoomHasTag()->Create([
                'room_id' => 3
            ]);
        });

        tap(\App\Tag::Create([
            'Name' => 'WIFI',
            'IconSrc' => 'https://img.icons8.com/ios/35/858a99/online.png'
        ]),function(\App\Tag $tag){
            $tag->RoomHasTag()->Create([
                'room_id' => 1
            ]);
            $tag->RoomHasTag()->Create([
                'room_id' => 2
            ]);
            $tag->RoomHasTag()->Create([
                'room_id' => 3
            ]);
        });

        tap(\App\Tag::Create([
            'Name' => '吹風機',
            'IconSrc' => 'https://img.icons8.com/ios/35/858a99/hair-dryer.png'
        ]),function(\App\Tag $tag){
            $tag->RoomHasTag()->Create([
                'room_id' => 1
            ]);
            $tag->RoomHasTag()->Create([
                'room_id' => 2
            ]);
            $tag->RoomHasTag()->Create([
                'room_id' => 3
            ]);
        });

        \App\ATMInfo::Create([
           'Html' => '<h3>匯款帳號:12345678</h3> <h2>匯款後，請來電通知!</h2>'
        ]);

    }
}
