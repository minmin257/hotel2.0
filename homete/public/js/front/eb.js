(function($) {
    "use strict";
    $(document).ready(function() {
        var checkout_tabs = $(".checkout-payment-tabs");
        if (checkout_tabs.length) {
            checkout_tabs.tabs()
        }
        var adminbar = $('#wpadminbar');
        var header = $('header');
        var stickysidebar = $('.sticky-sidebar');
        if (adminbar.length && adminbar.is(':visible')) {
            var adminsidebarfixed = adminbar.height()
        } else {
            var adminsidebarfixed = 0
        }
        if (header.hasClass("fixed")) {
            var headersidebarfixed = header.height()
        } else {
            var headersidebarfixed = 10
        }
        var sidebarfixed = adminsidebarfixed + headersidebarfixed;
        if (stickysidebar.length) {
            var sidebar = new StickySidebar('.sticky-sidebar', {
                topSpacing: sidebarfixed + 20,
                bottomSpacing: 0,
                containerSelector: '.row',
                minWidth: 991
            })
        }
        $(".room-item .room-image").on({
            mouseenter: function() {
                $(this).parent().find('.room-services').addClass('active')
            },
        });
        $(".room-item").on({
            mouseleave: function() {
                $(this).parent().find('.room-services').removeClass('active')
            }
        });
        var sync1 = $("#slider-larg"),
            sync2 = $("#thumbs"),
            duration = 300;
        if (eb_js_settings.eb_room_slider_nav == !0) {
            var eb_room_slider_nav = !0
        } else {
            var eb_room_slider_nav = !1
        }
        sync1.owlCarousel({
            items: 1,
            dots: !1,
            autoplay: !0,
            nav: eb_room_slider_nav,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        }).on('changed.owl.carousel', function(e) {
            var syncedPosition = syncPosition(e.item.index);
            if (syncedPosition != "stayStill") {
                $sync2.trigger('to.owl.carousel', [syncedPosition, duration, !0])
            }
        });
        sync2
            .on('initialized.owl.carousel', function() {
                addClassCurrent(0)
            }).owlCarousel({
                dots: !1,
                margin: 5,
                responsive: {
                    0: {
                        items: 4
                    },
                    600: {
                        items: 4
                    },
                    960: {
                        items: 5
                    },
                    1200: {
                        items: 6
                    }
                }
            }).on('click', '.owl-item', function() {
                sync1.trigger('to.owl.carousel', [$(this).index(), duration, !0])
            });

        function addClassCurrent(index) {
            sync2
                .find(".owl-item").removeClass("active-item").eq(index).addClass("active-item")
        }

        function syncPosition(index) {
            addClassCurrent(index);
            var itemsNo = sync2.find(".owl-item").length;
            var visibleItemsNo = sync2.find(".owl-item.active").length;
            if (itemsNo === visibleItemsNo) {
                return "stayStill"
            }
            var visibleCurrentIndex = sync2.find(".owl-item.active").index(sync2.find(".owl-item.active-item"));
            if (visibleCurrentIndex == 0 && index != 0) {
                return index - 1
            }
            if (visibleCurrentIndex == (visibleItemsNo - 1) && index != (itemsNo - 1)) {
                return index - visibleItemsNo + 2
            }
            return "stayStill"
        }
        $("#room-booking-form, #search-form").on('submit', function(e) {
            var eagle_booking_dates = $("#eagle_booking_datepicker");
            if (eagle_booking_dates.val() === '') {
                e.preventDefault();
                eagle_booking_dates.click()
            } else {
                eb_button_loading('eb_search_form');
                var eb_booking_type = eb_js_settings.eb_booking_type;
                var eb_custom_date_format = eb_js_settings.eb_custom_date_format;
                var eb_date_format = eb_js_settings.eagle_booking_date_format.toUpperCase();
                var eb_output_checkin = $('#eagle_booking_checkin').val();
                var eb_output_checkout = $('#eagle_booking_checkout').val();
                if (eb_booking_type === 'builtin') {
                    if ($('form').hasClass('room-booking-form')) {
                        var eb_output_format = 'MM/DD/YYYY'
                    } else {
                        var eb_output_format = 'MM-DD-YYYY'
                    }
                } else if (eb_booking_type === 'booking') {
                    var eb_output_format = 'YYYY-MM-DD'
                } else if (eb_booking_type === 'airbnb') {
                    var eb_output_format = 'MM-DD-YYYY'
                } else if (eb_booking_type === 'tripadvisor') {
                    var eb_output_format = 'MM-DD-YYYY'
                } else if (eb_booking_type === 'custom') {
                    var eb_output_format = eb_custom_date_format
                }
                var eb_output_checkin_formated = moment(eb_output_checkin, eb_date_format).format(eb_output_format);
                var eb_output_checkout_formated = moment(eb_output_checkout, eb_date_format).format(eb_output_format);
                $('#eagle_booking_checkin').val(eb_output_checkin_formated);
                $('#eagle_booking_checkout').val(eb_output_checkout_formated)
            }
        })
        var eb_calendar_min_date = new Date();
        var eb_calendar_max_date = moment(eb_calendar_min_date).add(eb_js_settings.eb_calendar_availability_period, 'M').endOf('month');
        var eagle_booking_date_format = eb_js_settings.eagle_booking_date_format.toUpperCase();
        var eagle_booking_datepicker = $("#eagle_booking_datepicker");
        $(eagle_booking_datepicker).each(function() {
            $(eagle_booking_datepicker).daterangepicker({
                autoUpdateInput: !1,
                autoApply: !0,
                alwaysShowCalendars: !0,
                linkedCalendars: !0,
                minDate: eb_calendar_min_date,
                maxDate: eb_calendar_max_date,
                locale: {
                    format: eagle_booking_date_format,
                    separator: " → ",
                    "daysOfWeek": [eb_js_settings.eb_calendar_sunday, eb_js_settings.eb_calendar_monday, eb_js_settings.eb_calendar_tuesday, eb_js_settings.eb_calendar_wednesday, eb_js_settings.eb_calendar_thursday, eb_js_settings.eb_calendar_friday, eb_js_settings.eb_calendar_saturday, ],
                    "monthNames": [eb_js_settings.eb_calendar_january, eb_js_settings.eb_calendar_february, eb_js_settings.eb_calendar_march, eb_js_settings.eb_calendar_april, eb_js_settings.eb_calendar_may, eb_js_settings.eb_calendar_june, eb_js_settings.eb_calendar_july, eb_js_settings.eb_calendar_august, eb_js_settings.eb_calendar_september, eb_js_settings.eb_calendar_october, eb_js_settings.eb_calendar_november, eb_js_settings.eb_calendar_december, ],
                    "firstDay": 1
                }
            }), $(eagle_booking_datepicker).on("apply.daterangepicker", function() {
                var checkin = $(eagle_booking_datepicker).data('daterangepicker').startDate.format(eagle_booking_date_format);
                var checkout = $(eagle_booking_datepicker).data('daterangepicker').endDate.format(eagle_booking_date_format);
                $(this).val(checkin + " " + " " + " → " + " " + " " + checkout);
                $('#eagle_booking_checkin').val(checkin);
                $('#eagle_booking_checkout').val(checkout);
                if ($("div").hasClass("search-filters")) {
                    eagle_booking_filters()
                }
                if ($("div").hasClass("search-filters") || $("div").hasClass("calendar")) {
                    eagle_booking_get_nights()
                }
            }), $(eagle_booking_datepicker).on("show.daterangepicker", function() {
                var live_checkin = $('#eagle_booking_checkin').val();
                var live_checkout = $('#eagle_booking_checkout').val();
                if (live_checkin != '' && live_checkout != '') {
                    var eagle_booking_nights_div = $('<div class="booking-nights">' + live_checkin + '&nbsp;' + ' → ' + '&nbsp' + live_checkout + ' (' + eagle_booking_get_nights() + ' ' + eb_js_settings.eb_booking_nights + ')</div>');
                    $(".booking-nights").remove();
                    $(".daterangepicker").append(eagle_booking_nights_div)
                }
                $(document).on('mouseenter', '.start-date', function() {
                    live_checkin = $(this).attr('data-date');
                    live_checkin = moment(live_checkin, 'MM/DD/YYYY').format(eb_js_settings.eagle_booking_date_format.toUpperCase());
                    $('#eagle_booking_checkin').val(live_checkin)
                })
                $(document).on('mouseenter', '.in-range', function() {
                    live_checkout = $(this).attr('data-date');
                    live_checkout = moment(live_checkout, 'MM/DD/YYYY').format(eb_js_settings.eagle_booking_date_format.toUpperCase());
                    $('#eagle_booking_checkout').val(live_checkout)
                })
                $(document).on('mouseenter', '.start-date, .in-range', function() {
                    var eagle_booking_nights_div = $('<div class="booking-nights">' + live_checkin + '&nbsp;' + ' → ' + '&nbsp' + live_checkout + ' (' + eagle_booking_get_nights() + ' ' + eb_js_settings.eb_booking_nights + ')</div>');
                    $(".booking-nights").remove();
                    $(".daterangepicker").append(eagle_booking_nights_div)
                })
            })
        })

        function eagle_booking_get_nights() {
            var eagle_booking_checkin = $('#eagle_booking_checkin').val();
            var eagle_booking_checkout = $('#eagle_booking_checkout').val();
            var eagle_booking_start_date = moment(eagle_booking_checkin, eb_js_settings.eagle_booking_date_format.toUpperCase()).format('YYYY-MM-DD');
            var eagle_booking_end_date = moment(eagle_booking_checkout, eb_js_settings.eagle_booking_date_format.toUpperCase()).format('YYYY-MM-DD');
            var booking_nights = (new Date(eagle_booking_end_date)) - (new Date(eagle_booking_start_date));
            var eagle_booking_nights_number = booking_nights / (1000 * 60 * 60 * 24);
            if (eagle_booking_nights_number < 0) {
                var eagle_booking_nights_number = '0'
            }
            return eagle_booking_nights_number
        }
        $('.eb-guestspicker .guestspicker').on('click', function(event) {
            $('.eb-guestspicker').toggleClass('active');
            event.preventDefault()
        });
        $(window).click(function() {
            $('.eb-guestspicker').removeClass('active')
        });
        $('.eb-guestspicker').on('click', function(event) {
            event.stopPropagation()
        });

        function guestsSum() {
            var guests_button = $('.guests-button');
            var arr = $('.booking-guests');
            var guests = 0;
            for (var i = 0; i < arr.length; i++) {
                if (parseInt(arr[i].value, 10))
                    guests += parseInt(arr[i].value, 10)
            }
            if (guests > 0) {
                var cardQty = document.querySelector(".gueststotal");
                cardQty.innerHTML = guests
            }
            $("#eagle_booking_guests").val(guests)
        }
        guestsSum();

        function guestsPicker() {
            $(".plus, .minus").on("click", function() {
                var button = $(this);
                var oldValue = button.parent().find("input").val();
                var max_value = parseFloat(button.parent().find("input").attr('max'));
                var min_value = parseFloat(button.parent().find("input").attr("min"));
                if (button.hasClass('plus') && max_value > 0) {
                    if (oldValue < max_value) {
                        var newVal = parseFloat(oldValue) + 1
                    } else {
                        newVal = oldValue
                    }
                } else {
                    if (oldValue > min_value) {
                        var newVal = parseFloat(oldValue) - 1
                    } else {
                        newVal = min_value
                    }
                }
                button.parent().find("input").val(newVal);
                guestsSum();
                if ($('form').hasClass('booking-search-form')) {
                    eagle_booking_filters()
                }
            })
        }
        guestsPicker();
        var eagle_booking_price_range = $("#eagle_booking_slider_range");
        eagle_booking_price_range.ionRangeSlider({
            type: "double",
            skin: "round",
            grid: !0,
            min: eb_js_settings.eagle_booking_price_range_min,
            max: eb_js_settings.eagle_booking_price_range_max,
            from: eb_js_settings.eagle_booking_price_range_default_min,
            to: eb_js_settings.eagle_booking_price_range_default_max,
            prefix: eb_js_settings.eb_price_range_currency,
            onFinish: function(data) {
                $('#eagle_booking_min_price').val(data.from);
                $('#eagle_booking_max_price').val(data.to);
                eagle_booking_filters()
            },
            onUpdate: function(data) {
                disable: !0
            }
        });
        var eb_ajax_final_price = null;

        function eb_services_price() {
            if (eb_ajax_final_price != null) {
                eb_ajax_final_price.abort();
                eb_ajax_final_price = null
            }
            var eb_checkbox_services = $("#eb_additional_services").val();
            var eb_room_price = $("#eb_room_price").val();
            var eb_coupon_code_value = $("#eb_coupon_value").val();
            var eb_final_price_nonce_val = $('#eb_security').val();
            eb_button_loading('submit_booking_form');
            eb_ajax_final_price = $.ajax({
                url: eb_frontend_ajax.eb_final_price_ajax,
                method: 'GET',
                dataType: 'json',
                data: {
                    action: 'eb_final_price_action',
                    eb_additional_services: eb_checkbox_services,
                    eb_room_price: eb_room_price,
                    eb_coupon_code_value: eb_coupon_code_value,
                    eb_final_price_nonce: eb_final_price_nonce_val
                },
                success: function(eb_final_price_data) {
                    var eb_decimal_numbers = eb_js_settings.eb_decimal_numbers;
                    var eb_final_trip_price = parseFloat(eb_final_price_data.trip_price).toFixed(eb_decimal_numbers);
                    console.log(eb_final_price_data.message);
                    $("#eb_trip_price_text").empty();
                    $("#eb_trip_price_text").text(eb_final_trip_price);
                    $("#eb_trip_price").val(eb_final_trip_price)
                },
                complete: function() {
                    eb_button_loading('submit_booking_form', 'hide')
                }
            })
        }
        $('.eb-tabs-content .eb-tab-content').hide();
        $('.eb-tab:first').addClass('active');
        $('.eb-tab.active .tab-radio').prop('checked', !0);
        $('.eb-tabs-content .eb-tab-content:first').show();
        $(".eb-tab").click(function() {
            $(this).addClass("active").siblings().removeClass("active");
            $(".eb-tabs-content > div").hide();
            $('.' + $(this).data("tab")).show()
        });
        $('.eb-panel-dropdown-toggle').on('click', function(event) {
            $('.eb-panel-dropdown .eb-panel-dropdown-inner').toggleClass('active')
        });
        $(window).click(function() {
            $('.eb-panel-dropdown .eb-panel-dropdown-inner').removeClass('active')
        });
        $('.eb-panel-dropdown-toggle').on('click', function(event) {
            event.stopPropagation()
        });
        $('.time-slot').on('click', function(event) {
            var eb_timepicker_time = $('input[name=eb_user_arrival]:checked').val();
            $('#eb_timepicker_text').text(': ' + eb_timepicker_time);
            $('.eb-panel-dropdown-toggle').removeClass('unset')
        });
        $("#submit_booking_form").on('click', function(event) {
            var eb_checked_tab = $('input[name=eb_billing_tab]:checked').val();
            var eb_user_first_name = $('#eb_user_first_name');
            var eb_user_last_name = $('#eb_user_last_name');
            var eb_user_email = $('#eb_user_email');
            var eb_user_phone = $('#eb_user_phone');
            var eb_user_country = $('#eb_user_country');
            var eb_user_city = $('#eb_user_city');
            var eb_user_address = $('#eb_user_address');
            var eb_user_zip = $('#eb_user_zip');
            var eb_form_terms = $("#eb_terms");
            var eb_form_has_error = !1;
            if ($('#eb_billing_tabs_content').is(':visible')) {
                if (eb_checked_tab === 'signin') {
                    console.log('Login Form Selected')
                    eb_form_has_error = !0
                } else if (eb_checked_tab === 'signup') {
                    console.log('Signup Form Selected');
                    var eb_sign_up_phone_field = document.querySelector('#eb_user_sign_up_phone');
                    var eb_sign_up_phone_iti = window.intlTelInputGlobals.getInstance(eb_sign_up_phone_field);
                    var eb_sign_up_phone_number = eb_sign_up_phone_iti.getNumber();
                    var eb_sign_up_first_name = $("#eb_user_sign_up_first_name");
                    var eb_sign_up_last_name = $("#eb_user_sign_up_last_name");
                    var eb_sign_up_email = $("#eb_user_sign_up_email");
                    var eb_sign_up_password = $("#eb_user_sign_up_password");
                    var eb_sign_up_country = $("#eb_user_sign_up_country");
                    var eb_sign_up_city = $("#eb_user_sign_up_city");
                    var eb_sign_up_address = $("#eb_user_sign_up_address");
                    var eb_sign_up_zip = $("#eb_user_sign_up_zip");
                    eb_sign_up_first_name.add(eb_sign_up_last_name).add(eb_sign_up_email).add(eb_sign_up_phone_field).add(eb_sign_up_password).each(function() {
                        if (!this.value) {
                            eb_form_has_error = !0;
                            $(this).addClass("empty")
                        }
                    });
                    eb_user_first_name.val(eb_sign_up_first_name.val());
                    eb_user_last_name.val(eb_sign_up_last_name.val());
                    eb_user_email.val(eb_sign_up_email.val());
                    eb_user_phone.val(eb_sign_up_phone_number);
                    eb_user_country.val(eb_sign_up_country.val());
                    eb_user_city.val(eb_sign_up_city.val());
                    eb_user_address.val(eb_sign_up_address.val());
                    eb_user_zip.val(eb_sign_up_zip.val())
                } else {
                    var eb_guest_phone_field = document.querySelector('#eb_guest_phone');
                    var eb_guest_phone_iti = window.intlTelInputGlobals.getInstance(eb_guest_phone_field);
                    var eb_guest_phone_number = eb_guest_phone_iti.getNumber();
                    var eb_guest_first_name = $("#eb_guest_first_name");
                    var eb_guest_last_name = $("#eb_guest_last_name");
                    var eb_guest_email = $("#eb_guest_email");
                    var eb_guest_country = $("#eb_guest_country");
                    var eb_guest_city = $("#eb_guest_city");
                    var eb_guest_address = $("#eb_guest_address");
                    var eb_guest_zip = $("#eb_guest_zip");
                    eb_guest_first_name.add(eb_guest_last_name).add(eb_guest_email).add(eb_guest_phone_field).each(function() {
                        if (!this.value) {
                            eb_form_has_error = !0;
                            $(this).addClass("empty")
                        }
                    });
                    eb_user_first_name.val(eb_guest_first_name.val());
                    eb_user_last_name.val(eb_guest_last_name.val());
                    eb_user_email.val(eb_guest_email.val());
                    eb_user_phone.val(eb_guest_phone_number);
                    eb_user_country.val(eb_guest_country.val());
                    eb_user_city.val(eb_guest_city.val());
                    eb_user_address.val(eb_guest_address.val());
                    eb_user_zip.val(eb_guest_zip.val());
                    console.log('Guest Form Selected')
                }
            } else {
                var eb_signed_in_user_phone_field = document.querySelector('#eb_signed_in_user_phone');
                var eb_signed_in_user_phone_iti = window.intlTelInputGlobals.getInstance(eb_signed_in_user_phone_field);
                var eb_signed_in_user_phone_number = eb_signed_in_user_phone_iti.getNumber();
                var eb_signed_in_user_first_name = $('#eb_signed_in_user_first_name');
                var eb_signed_in_user_last_name = $('#eb_signed_in_user_last_name');
                var eb_signed_in_user_email = $('#eb_signed_in_user_email');
                var eb_signed_in_user_country = $('#eb_signed_in_user_country');
                var eb_signed_in_user_city = $('#eb_signed_in_user_city');
                var eb_signed_in_user_address = $('#eb_signed_in_user_address');
                var eb_signed_in_user_zip = $('#eb_signed_in_user_zip');
                eb_signed_in_user_first_name.add(eb_signed_in_user_last_name).add(eb_signed_in_user_email).add(eb_signed_in_user_phone_field).each(function() {
                    if (!this.value) {
                        eb_form_has_error = !0;
                        $(this).addClass("empty")
                    }
                });
                eb_user_first_name.val(eb_signed_in_user_first_name.val());
                eb_user_last_name.val(eb_signed_in_user_last_name.val());
                eb_user_email.val(eb_signed_in_user_email.val());
                eb_user_phone.val(eb_signed_in_user_phone_number);
                eb_user_country.val(eb_signed_in_user_country.val());
                eb_user_city.val(eb_signed_in_user_city.val());
                eb_user_address.val(eb_signed_in_user_address.val());
                eb_user_zip.val(eb_signed_in_user_zip.val())
            }
            if (!eb_form_terms.is(':checked') && eb_js_settings.eb_terms_conditions == !0) {
                eb_form_terms.addClass("empty");
                if (eb_form_has_error == !1) {
                    $('html').animate({
                        scrollTop: $(eb_form_terms).offset().top
                    }, 500)
                }
                eb_form_has_error = !0
            }
            if (eb_form_has_error == !1) {
                if (eb_checked_tab === 'signup') {
                    $("#eb_user_signup_booking_form").submit()
                } else {
                    $('#eb_booking_form').submit()
                }
                eb_button_loading('submit_booking_form')
            } else {
                event.preventDefault();
                console.log('Error')
            }
        })
        $('input').on("keydown", function() {
            $(this).removeClass("empty")
        })

        function eb_button_loading(eb_button_id, eb_button_action) {
            var eb_button = $('#' + eb_button_id);
            var eb_loader_dom = '<span class="eb-btn-loader"><span class="spinner spinner1"></span><span class="spinner spinner2"></span><span class="spinner spinner3"></span><span class="spinner spinner4"></span><span class="spinner spinner5"></span></span>';
            if (eb_button_action === 'hide') {
                eb_button.find('.eb-btn-loader').remove();
                eb_button.find('.eb-btn-text').show();
                eb_button.css('pointer-events', '');
                eb_button.blur()
            } else {
                eb_button.append(eb_loader_dom);
                eb_button.find('.eb-btn-text').hide();
                eb_button.css('pointer-events', 'none')
            }
        }

        function eb_user_sign_in() {
            var eb_user_sign_in_xhr = null;
            $('#eb_user_sign_in').on('click', function(event) {
                var eb_user_sign_in_username = $('#eb_user_sign_in_username');
                var eb_user_sign_in_password = $('#eb_user_sign_in_password')
                var eb_user_sign_in_username_val = eb_user_sign_in_username.val();
                var eb_user_sign_in_password_val = eb_user_sign_in_password.val();
                var eb_user_sign_response = $('#eb_user_sign_in_response');
                var eb_user_sign_response_text = $('#eb_user_sign_in_response_text');
                var eb_user_sign_in_nonce_val = $('#eb_security').val();
                var eb_user_sign_in_has_error = !1;
                eb_user_sign_in_username.add(eb_user_sign_in_password).each(function() {
                    if (!this.value) {
                        eb_user_sign_in_has_error = !0;
                        $(this).addClass("empty")
                    }
                });
                if (eb_user_sign_in_has_error == !1) {
                    eb_button_loading('eb_user_sign_in');
                    if (eb_user_sign_in_xhr != null) {
                        eb_user_sign_in_xhr.abort();
                        eb_user_sign_in_xhr = null
                    }
                    eb_user_sign_in_xhr = $.ajax({
                        url: eb_frontend_ajax.eb_user_sign_in_ajax,
                        method: 'GET',
                        dataType: 'json',
                        data: {
                            action: 'eb_user_sign_in_action',
                            eb_user_sign_in_username: eb_user_sign_in_username_val,
                            eb_user_sign_in_password: eb_user_sign_in_password_val,
                            eb_user_sign_in_nonce: eb_user_sign_in_nonce_val,
                        },
                        success: function(eb_user_sign_in_data) {
                            if (eb_user_sign_in_data.status === 'failed') {
                                eb_user_sign_response.show();
                                eb_user_sign_response_text.text(eb_user_sign_in_data.message);
                                eb_user_sign_response.addClass('eb-alert-error eb-alert-icon');
                                eb_button_loading('eb_user_sign_in', 'hide')
                            } else {
                                $('#eb_security').val(eb_user_sign_in_data.new_nonce);
                                if ($('#eb_user_dashboard_signin_form').length) {
                                    eb_user_sign_response.show();
                                    eb_user_sign_response.removeClass('eb-alert-error eb-alert-icon');
                                    eb_user_sign_response.addClass('eb-alert-success eb-alert-icon');
                                    eb_user_sign_response_text.text(eb_user_sign_in_data.redirect_mssg);
                                    document.location.href = eb_user_sign_in_data.redirect_url
                                } else {
                                    $('#eb_billing_tabs').hide();
                                    eb_user_sign_response.hide();
                                    $('#eb_signed_in_user').show();
                                    $('#eb_signed_in_user_text').text(eb_user_sign_in_data.message);
                                    $('#eb_signed_in_user_first_name').val(eb_user_sign_in_data.firstname);
                                    $('#eb_signed_in_user_last_name').val(eb_user_sign_in_data.lastname);
                                    $('#eb_signed_in_user_email').val(eb_user_sign_in_data.email);
                                    $('#eb_signed_in_user_phone').val(eb_user_sign_in_data.phone);
                                    $('#eb_signed_in_user_country').val(eb_user_sign_in_data.country);
                                    $('#eb_signed_in_user_city').val(eb_user_sign_in_data.city);
                                    $('#eb_signed_in_user_address').val(eb_user_sign_in_data.address);
                                    $('#eb_signed_in_user_zip').val(eb_user_sign_in_data.zip)
                                }
                                console.log(eb_user_sign_in_data.message)
                            }
                            console.log(eb_user_sign_in_data.message)
                        },
                        error: function(eb_user_sign_in_xhr, textStatus, errorThrown) {
                            console.log(errorThrown)
                        },
                        complete: function() {},
                    })
                }
                event.preventDefault()
            })
        }
        eb_user_sign_in();

        function eb_user_sign_out() {
            var eb_user_sign_out_xhr = null;
            $('body').on('click', '#eb_user_sign_out', function(event) {
                var eb_logged_in_user_nonce = $('#eb_security').val();
                eb_user_sign_out_xhr = $.ajax({
                    url: eb_frontend_ajax.eb_user_sign_out_ajax,
                    method: 'GET',
                    dataType: 'json',
                    data: {
                        action: 'eb_user_sign_out_action',
                        eb_sign_out_nonce: eb_logged_in_user_nonce,
                    },
                    success: function(eb_user_sign_out_data) {
                        if (eb_user_sign_out_data.status === 'success') {
                            $('#eb_security').val(eb_user_sign_out_data.new_nonce);
                            $('#eb_billing_tabs').show();
                            $('#eb_signed_in_user').hide();
                            eb_button_loading('eb_user_sign_in', 'hide');
                            console.log(eb_user_sign_out_data.message)
                        } else {
                            console.log(eb_user_sign_out_data.message)
                        }
                    },
                    error: function(eb_user_sign_out_xhr, textStatus, errorThrown) {
                        console.log(errorThrown)
                    },
                    complete: function() {},
                });
                event.preventDefault()
            })
        }
        eb_user_sign_out();

        function eb_user_sign_up() {
            var eb_user_sign_up_xhr = null;
            $('#eb_user_dashboard_signup_form, #eb_user_signup_booking_form').on('submit', function(event) {
                var eb_user_sign_up_username = $('#eb_user_sign_up_username');
                var eb_user_sign_up_password = $('#eb_user_sign_up_password');
                var eb_user_sign_up_email = $('#eb_user_sign_up_email');
                var eb_user_sign_up_terms = $('#eb_user_sign_up_terms');
                var eb_user_sign_up_username_val = eb_user_sign_up_username.val();
                var eb_user_sign_up_password_val = eb_user_sign_up_password.val();
                var eb_user_sign_up_email_val = eb_user_sign_up_email.val();
                var eb_user_sign_up_response = $('#eb_user_sign_up_response');
                var eb_user_sign_up_response_text = $('#eb_user_sign_up_response_text');
                var eb_user_sign_up_has_error = !1;
                eb_user_sign_up_username.add(eb_user_sign_up_password).add(eb_user_sign_up_email).each(function() {
                    if (!this.value) {
                        eb_user_sign_up_has_error = !0;
                        $(this).addClass("empty")
                    }
                });
                if (!eb_user_sign_up_terms.is(':checked') && eb_js_settings.eb_terms_conditions == !0 && $('#eb_user_dashboard_signup_form').length) {
                    eb_user_sign_up_terms.addClass("empty");
                    eb_user_sign_up_has_error = !0
                }
                if (eb_user_sign_up_has_error == !1) {
                    eb_button_loading('eb_user_sign_up');
                    if (eb_user_sign_up_xhr != null) {
                        eb_user_sign_up_xhr.abort();
                        eb_user_sign_up_xhr = null
                    }
                    eb_user_sign_up_xhr = $.ajax({
                        url: eb_frontend_ajax.eb_user_sign_up_ajax,
                        method: 'GET',
                        dataType: 'json',
                        data: {
                            action: 'eb_user_sign_up_action',
                            eb_user_sign_up_username: eb_user_sign_up_username_val,
                            eb_user_sign_up_password: eb_user_sign_up_password_val,
                            eb_user_sign_up_email: eb_user_sign_up_email_val,
                        },
                        success: function(eb_user_sign_up_data) {
                            if (eb_user_sign_up_data.status === 'failed') {
                                eb_button_loading('submit_booking_form', 'hide');
                                eb_button_loading('eb_user_sign_up', 'hide');
                                eb_user_sign_up_response.css("display", "block");
                                eb_user_sign_up_response_text.text(eb_user_sign_up_data.mssg);
                                eb_user_sign_up_response.addClass('eb-alert-error eb-alert-icon')
                            } else {
                                if ($('#eb_user_dashboard_signup_form').length) {
                                    eb_user_sign_up_response.css("display", "block");
                                    eb_user_sign_up_response.removeClass('eb-alert-error');
                                    eb_user_sign_up_response.addClass('eb-alert-success eb-alert-icon');
                                    eb_user_sign_up_response_text.text(eb_user_sign_up_data.mssg);
                                    document.location.href = eb_user_sign_up_data.redirect_url
                                } else {
                                    $('#eb_booking_form').submit()
                                }
                            }
                            console.log(eb_user_sign_up_data)
                        },
                        error: function(eb_user_sign_up_xhr, textStatus, errorThrown) {
                            console.log(errorThrown)
                        },
                        complete: function() {},
                    })
                }
                event.preventDefault()
            })
        }
        eb_user_sign_up();

        function eb_validate_coupon_code() {
            var eb_ajax_coupon_code_xhr = null;
            $('#eb_validate_coupon').on('click', function(event) {
                var eb_coupon_code = $('#eb_coupon');
                var eb_coupon_code_value = eb_coupon_code.val();
                var eb_coupon_code_response = $('#eb_coupon_code_response');
                var eb_coupon_code_response_text = $('#eb_coupon_code_response_text');
                var eb_room_price = $('#eb_room_price').val();
                var eb_trip_price = $('#eb_trip_price').val();
                var eb_booking_form_button = $("#submit_booking_form");
                var eb_decimal_numbers = eb_js_settings.eb_decimal_numbers;
                var eb_coupon_nonce_val = $('#eb_security').val();
                if (eb_coupon_code_value != '') {
                    eb_button_loading('eb_validate_coupon');
                    eb_booking_form_button.css('pointer-events', 'none');
                    if (eb_ajax_coupon_code_xhr != null) {
                        eb_ajax_coupon_code_xhr.abort();
                        eb_ajax_coupon_code_xhr = null
                    }
                    eb_ajax_coupon_code_xhr = $.ajax({
                        url: eb_frontend_ajax.eb_coupon_code_ajax,
                        method: 'GET',
                        dataType: 'json',
                        data: {
                            action: 'eb_coupon_code_action',
                            eb_coupon_code: eb_coupon_code_value,
                            eb_coupon_nonce: eb_coupon_nonce_val,
                        },
                        success: function(eb_coupon_code_data) {
                            eb_coupon_code_response.show();
                            if (eb_coupon_code_data.status === 'failed') {
                                eb_coupon_code_response_text.text(eb_coupon_code_data.message);
                                eb_coupon_code_response.addClass('eb-alert-error mb20')
                            } else {
                                eb_button_loading('submit_booking_form');
                                eb_coupon_code_response_text.text(eb_coupon_code_data.message);
                                eb_coupon_code_response.addClass('eb-alert-success');
                                eb_coupon_code_response.removeClass('eb-alert-error mb20');
                                $('#eb_coupon_code_group').css("display", "none");
                                $('#eb_total_price_text').before('<div class="booking-details-item">' + eb_js_settings.eb_discount_text + ' (' + eb_coupon_code_data.coupon_code + ') <span class="value">  ' + eb_coupon_code_data.coupon_percent + '% </span></div>');
                                var eb_final_room_price = eb_room_price - eb_room_price * eb_coupon_code_data.coupon_percent / 100;
                                var eb_final_trip_price = eb_trip_price - eb_trip_price * eb_coupon_code_data.coupon_percent / 100;
                                var eb_final_trip_price = parseFloat(eb_final_trip_price).toFixed(eb_decimal_numbers);
                                $("#eb_room_price").val(eb_final_room_price);
                                $("#eb_trip_price_text").text(eb_final_trip_price);
                                $("#eb_trip_price").val(eb_final_trip_price);
                                $('#eb_coupon_value').val(eb_coupon_code_data.coupon_percent);
                                $('#eb_coupon_code').val(eb_coupon_code_data.coupon_code)
                            }
                            console.log(eb_coupon_code_data.status)
                        },
                        error: function(eb_ajax_coupon_code_xhr, textStatus, errorThrown) {
                            console.log(errorThrown)
                        },
                        complete: function() {
                            eb_button_loading('submit_booking_form', 'hide');
                            eb_button_loading('eb_validate_coupon', 'hide')
                        },
                    })
                } else {
                    eb_coupon_code.addClass("empty")
                }
                event.preventDefault()
            })
        }
        eb_validate_coupon_code();
        $('.additional-service-item').on('click', function(event) {
            if (!$(event.target).is('.toggle-service-full-details')) {
                $(this).toggleClass('selected');
                var checkbox = $(this).children('input[type="checkbox"]');
                checkbox.prop('checked', !checkbox.prop('checked'));
                var eb_service_value = checkbox.val();
                if (checkbox.is(":checked")) {
                    var eb_service_previous_value = $("#eb_additional_services").val();
                    $("#eb_additional_services").val(eb_service_value + eb_service_previous_value);
                    var eb_service_id = checkbox.attr("data-id");
                    var eb_service_previous_value_id = $("#eb_additional_services_id").val();
                    $("#eb_additional_services_id").val(eb_service_id + eb_service_previous_value_id)
                } else {
                    var eb_service_previous_value = $("#eb_additional_services").val();
                    var eb_checkbox_services = eb_service_previous_value.replace(eb_service_value, "");
                    $("#eb_additional_services").val(eb_checkbox_services);
                    var eb_service_id = checkbox.attr("data-id");
                    var eb_service_previous_value_id = $("#eb_additional_services_id").val();
                    var eb_checkbox_services_id = eb_service_previous_value_id.replace(eb_service_id, "");
                    $("#eb_additional_services_id").val(eb_checkbox_services_id)
                }
                eb_services_price()
            }
        });
        $('.toggle-service-full-details').on('click', function() {
            $(this).parent().parent().parent().toggleClass('open')
        });
        $(".eb_normal_service").change(function() {
            if ($(this).is(":checked")) {
                var eb_normal_service_value = $(this).val();
                var eb_normal_service_previous_value = $("#eb_normal_services").val();
                $("#eb_normal_services").val(eb_normal_service_value + eb_normal_service_previous_value);
                eagle_booking_filters()
            } else {
                var eb_normal_service_value = $(this).val();
                var eb_normal_service_previous_value = $("#eb_normal_services").val();
                var eb_normal_services = eb_normal_service_previous_value.replace(eb_normal_service_value, "");
                $("#eb_normal_services").val(eb_normal_services);
                eagle_booking_filters()
            }
        });
        $(".eb_checkbox_additional_service").change(function() {
            if ($(this).is(":checked")) {
                var eb_additional_service_value = $(this).val();
                var eb_additional_service_previous_value = $("#eb_additional_services").val();
                $("#eb_additional_services").val(eb_additional_service_value + eb_additional_service_previous_value);
                eagle_booking_filters()
            } else {
                var eb_additional_service_value = $(this).val();
                var eb_additional_service_previous_value = $("#eb_additional_services").val();
                var eb_additional_services = eb_additional_service_previous_value.replace(eb_additional_service_value, "");
                $("#eb_additional_services").val(eb_additional_services);
                eagle_booking_filters()
            }
        });
        $("#eagle_booking_search_sorting li").on("click", function() {
            $('#eagle_booking_search_sorting li').removeClass("selected");
            $(this).addClass("selected");
            $('#eagle_booking_active_sorting').text($(this).text());
            eagle_booking_filters()
        });
        var eb_ajax_filters_xhr = null;

        function eagle_booking_filters(paged) {
            if (eb_ajax_filters_xhr != null) {
                eb_ajax_filters_xhr.abort();
                eb_ajax_filters_xhr = null
            }
            var eagle_booking_checkin = $(eagle_booking_datepicker).data('daterangepicker').startDate.format(eagle_booking_date_format);
            var eagle_booking_checkout = $(eagle_booking_datepicker).data('daterangepicker').endDate.format(eagle_booking_date_format);
            var eb_search_results_alert = $('#eb-no-search-results');
            var eb_search_rooms_rooms_list = $("#eagle_booking_rooms_list");
            if (eagle_booking_checkin && eagle_booking_checkout) {
                $("#eagle_booking_search_results").remove();
                eb_search_results_alert.remove();
                var eagle_booking_search_loader = $('<div class="eagle_booking_search_loader"><div class="wrapper-cell"><div class="image-line"></div><div class="text-cell"><div class="text-line title-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div></div><div class="price-cell"><div class="price-line"></div><div class="night-line"></div><div class="button-line"></div></div></div><div class="wrapper-cell"><div class="image-line"></div><div class="text-cell"><div class="text-line title-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div></div><div class="price-cell"><div class="price-line"></div><div class="night-line"></div><div class="button-line"></div></div></div><div class="wrapper-cell"><div class="image-line"></div><div class="text-cell"><div class="text-line title-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div></div><div class="price-cell"><div class="price-line"></div><div class="night-line"></div><div class="button-line"></div></div></div><div class="wrapper-cell"><div class="image-line"></div><div class="text-cell"><div class="text-line title-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div></div><div class="price-cell"><div class="price-line"></div><div class="night-line"></div><div class="button-line"></div></div></div><div class="wrapper-cell"><div class="image-line"></div><div class="text-cell"><div class="text-line title-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div><div class="service-line"></div></div><div class="price-cell"><div class="price-line"></div><div class="night-line"></div><div class="button-line"></div></div></div></div>');
                if (!$('.eagle_booking_search_loader').length) {
                    eb_search_rooms_rooms_list.append(eagle_booking_search_loader)
                }
                var eagle_booking_guests = $("#eagle_booking_guests").val();
                var eagle_booking_adults = $("#eagle_booking_adults").val();
                var eagle_booking_children = $("#eagle_booking_children").val();
                var eagle_booking_min_price = $("#eagle_booking_min_price").val();
                var eagle_booking_max_price = $("#eagle_booking_max_price").val();
                var eb_normal_services = $("#eb_normal_services").val();
                var eb_additional_services = $("#eb_additional_services").val();
                var eagle_booking_search_sorting_filter_meta_key = $("#eagle_booking_search_sorting .selected a").attr('data-meta-key');
                var eagle_booking_search_sorting_filter_order = $("#eagle_booking_search_sorting .selected a").attr('data-order');
                var eagle_booking_paged = paged;
                if (stickysidebar.length) {
                    sidebar.updateSticky()
                }
                eb_ajax_filters_xhr = $.ajax({
                    url: eb_frontend_ajax.eb_search_filters_ajax,
                    method: 'GET',
                    data: {
                        action: 'eb_search_filters_action',
                        eb_search_filters_nonce: eb_frontend_ajax.eb_ajax_nonce,
                        eagle_booking_paged: eagle_booking_paged,
                        eagle_booking_checkin: eagle_booking_checkin,
                        eagle_booking_checkout: eagle_booking_checkout,
                        eagle_booking_guests: eagle_booking_guests,
                        eagle_booking_adults: eagle_booking_adults,
                        eagle_booking_children: eagle_booking_children,
                        eagle_booking_min_price: eagle_booking_min_price,
                        eagle_booking_max_price: eagle_booking_max_price,
                        eb_normal_services: eb_normal_services,
                        eb_additional_services: eb_additional_services,
                        eagle_booking_search_sorting_filter_meta_key: eagle_booking_search_sorting_filter_meta_key,
                        eagle_booking_search_sorting_filter_order: eagle_booking_search_sorting_filter_order,
                    },
                    success: function(eagle_booking_filters_result) {
                        eb_search_results_alert.remove();
                        $("#eagle_booking_rooms_list").append(eagle_booking_filters_result);
                        $('[data-toggle="popover"]').popover({
                            html: !0,
                            offset: '0 10px'
                        });
                        if (stickysidebar.length) {
                            sidebar.updateSticky()
                        }
                        $('.eagle_booking_search_loader').remove();
                        console.log("Successful")
                    },
                    error: function(eb_ajax_filters_xhr, textStatus, errorThrown) {
                        console.log(errorThrown)
                    },
                    complete: function() {
                        var eagle_booking_results_qnt = $('#eagle_booking_results_qnt').val();
                        $("#results-number").text(eagle_booking_results_qnt)
                    }
                })
            } else {}
        }
        $(document).on('click', '#select-booking-dates', function() {
            $('#eagle_booking_datepicker').click()
        })
        $(document).on('click', '.pagination-button', function() {
            var eb_pagination = $(this).attr('data-pagination');
            eagle_booking_filters(eb_pagination)
        })
        $(document).on('click', '.toggle-room-breakpoint', function() {
            $(this).closest('.room-list-item').find('.room-quick-details').toggleClass('open', 200);
            $(this).toggleClass('open');
            $(this).find('i').toggleClass('fa-angle-down fa-angle-up')
        });
        $(document).on('click', '.more-normal-services', function() {
            $(this).closest('.room-list-item').find('.room-quick-details').toggleClass('open', 200);
            $(this).closest('.room-list-item').find('.toggle-room-breakpoint').toggleClass('open');
            $(this).closest('.room-list-item').find('.toggle-room-breakpoint i').toggleClass('fa-angle-down fa-angle-up')
        })
    });
    var pluginName = "simpleCalendar",
        defaults = {
            days: [eb_js_settings.eb_calendar_sunday, eb_js_settings.eb_calendar_monday, eb_js_settings.eb_calendar_tuesday, eb_js_settings.eb_calendar_wednesday, eb_js_settings.eb_calendar_thursday, eb_js_settings.eb_calendar_friday, eb_js_settings.eb_calendar_saturday, ],
            months: [eb_js_settings.eb_calendar_january, eb_js_settings.eb_calendar_february, eb_js_settings.eb_calendar_march, eb_js_settings.eb_calendar_april, eb_js_settings.eb_calendar_may, eb_js_settings.eb_calendar_june, eb_js_settings.eb_calendar_july, eb_js_settings.eb_calendar_august, eb_js_settings.eb_calendar_september, eb_js_settings.eb_calendar_october, eb_js_settings.eb_calendar_november, eb_js_settings.eb_calendar_december, ],
            minDate: "YYYY/MM/DD",
            maxDate: "YYYY/MM/DD",
            insertEvent: !0,
            displayEvent: !0,
            fixedStartDay: !0,
            events: [],
            insertCallback: function() {}
        };

    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.currentDate = new Date();
        this.events = options.events;
        this.init()
    }
    $.extend(Plugin.prototype, {
        init: function() {
            var container = $(this.element);
            var todayDate = this.currentDate;
            var events = this.events;
            var calendar = $('<div class="availability-calendar"></div>');
            var header = $('<div class="availability-calendar-header">' + '<span class="btn-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>' + '<span class="month"></span>' + '<span class="btn-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>' + '</div class="availability-calendar-header">');
            this.updateHeader(todayDate, header);
            calendar.append(header);
            this.buildCalendar(todayDate, calendar);
            container.append(calendar);
            this.bindEvents()
        },
        updateHeader: function(date, header) {
            header.find('.month').html(this.settings.months[date.getMonth()] + ' ' + date.getFullYear())
        },
        buildCalendar: function(fromDate, calendar) {
            var plugin = this;
            calendar.find('table').remove();
            var body = $('<table class="calendar"></table>');
            var thead = $('<thead></thead>');
            var tbody = $('<tbody></tbody>');
            for (var i = 1; i <= this.settings.days.length; i++) {
                thead.append($('<td class="day-name">' + this.settings.days[i % 7].substring(0, 3) + '</td>'))
            }
            var y = fromDate.getFullYear(),
                m = fromDate.getMonth();
            var firstDay = new Date(y, m, 1);
            while (firstDay.getDay() != 1) {
                firstDay.setDate(firstDay.getDate() - 1)
            }
            var lastDay = new Date(y, m + 1, 0);
            while (lastDay.getDay() != 0) {
                lastDay.setDate(lastDay.getDate() + 1)
            }
            for (var day = firstDay; day <= lastDay; day.setDate(day.getDate())) {
                var tr = $('<tr></tr>');
                for (var i = 0; i < 7; i++) {
                    var td = $('<td><span class="day">' + day.getDate() + '</span></td>');
                    var ymd = day.getFullYear() + '-' + day.getMonth() + '-' + day.getDay();
                    var ymd = this.formatToYYYYMMDD(day);
                    if ($.inArray(this.formatToYYYYMMDD(day), plugin.events) !== -1) {
                        td.find(".day").addClass("event")
                    }
                    if (day < (new Date())) {
                        td.find(".day").addClass("wrong-day")
                    }
                    if (day.toDateString() === (new Date).toDateString()) {
                        td.find(".day").addClass("today");
                        td.find(".day").removeClass("wrong-day")
                    }
                    if (day.getMonth() != fromDate.getMonth()) {
                        td.find(".day").addClass("wrong-month")
                    }
                    td.on('click', function(e) {});
                    tr.append(td);
                    day.setDate(day.getDate() + 1)
                }
                tbody.append(tr)
            }
            body.append(thead);
            body.append(tbody);
            var eventContainer = $('<div class="event-container"></div>');
            calendar.append(body);
            calendar.append(eventContainer)
        },
        bindEvents: function() {
            var eb_end_period = eb_js_settings.eb_calendar_availability_period;
            var plugin = this;
            var container = $(this.element);
            var counter = '';
            var startMoth = plugin.currentDate.getMonth();
            var endMonth = startMoth + (eb_end_period - 0);
            var currentMonth = startMoth;
            container.find('.btn-prev').on('click', function() {
                if (currentMonth > startMoth) {
                    plugin.currentDate.setMonth(plugin.currentDate.getMonth() - 1);
                    plugin.buildCalendar(plugin.currentDate, container.find('.availability-calendar'));
                    plugin.updateHeader(plugin.currentDate, container.find('.availability-calendar .availability-calendar-header'));
                    currentMonth--
                }
            });
            container.find('.btn-next').on('click', function() {
                if (currentMonth < endMonth) {
                    plugin.currentDate.setMonth(plugin.currentDate.getMonth() + 1);
                    plugin.buildCalendar(plugin.currentDate, container.find('.availability-calendar'));
                    plugin.updateHeader(plugin.currentDate, container.find('.availability-calendar .availability-calendar-header'));
                    currentMonth++
                }
            })
        },
        formatToYYYYMMDD: function(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return [year, month, day].join('/')
        }
    });
    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options))
            }
        })
    }
})(jQuery)
