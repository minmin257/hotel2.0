$('.owl-carousel').owlCarousel({
    nav: true,
    centerMode: true,
    stagePadding: 200,
    loop: true,
    margin: 10,
    items: 3,
    lazyLoad: true,
    nav: true,
    autoplay: true,
    autoplaySpeed: 1200,
    pauseOnHover: true,
    pauseOnDotsHover: true,
    edgeFriction: 0.05,
    infinite: true,
    navText: [
        "<img src='https://img.icons8.com/fluent-systems-regular/24/858a99/left.png'>",
        "<img src='https://img.icons8.com/fluent-systems-regular/24/858a99/right.png'>"
    ],
    responsive: {
        0: {
            items: 1,
            stagePadding: 0
        },

        600: {
            items: 1,
            stagePadding: 50
        },

        700: {
            items: 2,
            stagePadding: 50
        },
        1000: {
            items: 2,
            stagePadding: 50
        },
        1200: {
            items: 2,
            stagePadding: 100
        },
        1400: {
            items: 2,
            stagePadding: 100
        },
        1600: {
            items: 2,
            stagePadding: 150
        },
        1800: {
            items: 2,
            stagePadding: 200
        }
    }
})
