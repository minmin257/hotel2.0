(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["EmailSetting"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../api */ "./resources/js/backend/api.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../helper */ "./resources/js/backend/helper.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "EmailSetting",
  data: function data() {
    return {
      form: {
        Title: '信箱設定',
        Icon: 'unlock'
      },
      MailSetting: {},
      newPassword: ''
    };
  },
  created: function created() {
    this.GetMailSetting();
    this.$emit('ChildUpdated', this.form);
  },
  methods: {
    GetMailSetting: function GetMailSetting() {
      var _this = this;

      Object(_api__WEBPACK_IMPORTED_MODULE_0__["GetMailSetting"])().then(function (res) {
        _this.MailSetting = res;

        _this.$nextTick(function () {
          feather.replace();
        });
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertErrorMessage"])(response);
      });
    },
    UpdateMailSetting: function UpdateMailSetting() {
      var credential = {
        Name: this.MailSetting.Name,
        Cc: this.MailSetting.Cc
      };

      Object(_api__WEBPACK_IMPORTED_MODULE_0__["UpdateMailSetting"])(credential).then(function (res) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertSuccessRedirect"])(res);
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertErrorMessage"])(response);
      });
    },
    ValidMailSetting: function ValidMailSetting() {
      var credential = {
        account: this.MailSetting.account,
        password: this.newPassword
      };

      Object(_api__WEBPACK_IMPORTED_MODULE_0__["ValidMailSetting"])(credential).then(function (res) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertSuccessRedirect"])(res);
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertErrorMessage"])(response);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=style&index=0&id=b299ef06&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=style&index=0&id=b299ef06&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.vnis[data-v-b299ef06]{\n    display: inline-flex;\n    width: 100%;\n}\n.wh-100[data-v-b299ef06]{\n    width: 100%;\n    height: 100%;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=style&index=0&id=b299ef06&scoped=true&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=style&index=0&id=b299ef06&scoped=true&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmailSetting.vue?vue&type=style&index=0&id=b299ef06&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=style&index=0&id=b299ef06&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=template&id=b299ef06&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=template&id=b299ef06&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "wh-100" }, [
    _c("div", { staticClass: "col-md-12 col-12 mb-3" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12 col-md-12 col-lg-4" }, [
          _c("span", { staticClass: "store-btn" }, [
            _c("input", {
              staticClass: "btn btn-success",
              attrs: { type: "button", value: " 儲存變更" },
              on: { click: _vm.UpdateMailSetting }
            }),
            _vm._v(" "),
            _c("i", { staticClass: "fa fa-star" })
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col-xl-6 col-lg-6 col-md-12 col-12" }, [
      _c("div", { staticClass: "card" }, [
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("form", { staticClass: "profile" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12 col-lg-12 col-12" }, [
                    _vm._m(0),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-xl-2 col-lg-3 col-12 col-form-label"
                        },
                        [_vm._v("寄件者帳號")]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-xl-10 col-lg-9 col-12" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.MailSetting.account,
                              expression: "MailSetting.account"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            "aria-describedby": "",
                            required: ""
                          },
                          domProps: { value: _vm.MailSetting.account },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.MailSetting,
                                "account",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "form-group row border-bottom pb-3" },
                      [
                        _c(
                          "label",
                          {
                            staticClass:
                              "col-xl-2 col-lg-3 col-12 d-block col-form-label"
                          },
                          [_vm._v("寄件者密碼")]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-xl-10 col-lg-9 col-12 d-flex" },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.newPassword,
                                  expression: "newPassword"
                                }
                              ],
                              staticClass:
                                "form-control  font-weight-bold text-info",
                              staticStyle: {
                                "border-top-right-radius": "0px",
                                "border-bottom-right-radius": "0px"
                              },
                              attrs: { type: "password" },
                              domProps: { value: _vm.newPassword },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.newPassword = $event.target.value
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass: "input-group-append",
                                staticStyle: {
                                  "min-width": "75px",
                                  "max-width": "75px"
                                }
                              },
                              [
                                _c(
                                  "button",
                                  {
                                    staticClass: "btn btn-soft-info w-100",
                                    staticStyle: {
                                      "border-bottom-left-radius": "0px",
                                      "border-top-left-radius": "0px"
                                    },
                                    attrs: { type: "button" },
                                    on: { click: _vm.ValidMailSetting }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "uil uil-envelope-shield"
                                    }),
                                    _vm._v(
                                      " 驗證\n                                                "
                                    )
                                  ]
                                )
                              ]
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-xl-2 col-lg-3 col-12 col-form-label"
                        },
                        [_vm._v("寄件者名稱")]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-xl-10 col-lg-9 col-12" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.MailSetting.Name,
                              expression: "MailSetting.Name"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            "aria-describedby": "",
                            required: ""
                          },
                          domProps: { value: _vm.MailSetting.Name },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.MailSetting,
                                "Name",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c(
                        "label",
                        {
                          staticClass: "col-xl-2 col-lg-3 col-12 col-form-label"
                        },
                        [_vm._v("寄件副本")]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-xl-10 col-lg-9 col-12" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.MailSetting.Cc,
                              expression: "MailSetting.Cc"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            "aria-describedby": "",
                            required: "",
                            placeholder:
                              "example1@gamil.com,example2@yahoo.com.tw"
                          },
                          domProps: { value: _vm.MailSetting.Cc },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.MailSetting,
                                "Cc",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "form-group row justify-content-center " },
      [
        _c("i", {
          staticClass: "icon-xxl icon-dual-info mb-2",
          attrs: { "data-feather": "mail" }
        })
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/EmailSetting.vue":
/*!************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/EmailSetting.vue ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EmailSetting_vue_vue_type_template_id_b299ef06_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EmailSetting.vue?vue&type=template&id=b299ef06&scoped=true& */ "./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=template&id=b299ef06&scoped=true&");
/* harmony import */ var _EmailSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EmailSetting.vue?vue&type=script&lang=js& */ "./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _EmailSetting_vue_vue_type_style_index_0_id_b299ef06_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./EmailSetting.vue?vue&type=style&index=0&id=b299ef06&scoped=true&lang=css& */ "./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=style&index=0&id=b299ef06&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _EmailSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EmailSetting_vue_vue_type_template_id_b299ef06_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EmailSetting_vue_vue_type_template_id_b299ef06_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "b299ef06",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/backend/components/children/logined/children/EmailSetting.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmailSetting.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=style&index=0&id=b299ef06&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=style&index=0&id=b299ef06&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSetting_vue_vue_type_style_index_0_id_b299ef06_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmailSetting.vue?vue&type=style&index=0&id=b299ef06&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=style&index=0&id=b299ef06&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSetting_vue_vue_type_style_index_0_id_b299ef06_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSetting_vue_vue_type_style_index_0_id_b299ef06_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSetting_vue_vue_type_style_index_0_id_b299ef06_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSetting_vue_vue_type_style_index_0_id_b299ef06_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSetting_vue_vue_type_style_index_0_id_b299ef06_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=template&id=b299ef06&scoped=true&":
/*!*******************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=template&id=b299ef06&scoped=true& ***!
  \*******************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSetting_vue_vue_type_template_id_b299ef06_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmailSetting.vue?vue&type=template&id=b299ef06&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/EmailSetting.vue?vue&type=template&id=b299ef06&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSetting_vue_vue_type_template_id_b299ef06_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSetting_vue_vue_type_template_id_b299ef06_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);