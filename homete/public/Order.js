(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Order"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Order.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Order.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../api */ "./resources/js/backend/api.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../helper */ "./resources/js/backend/helper.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Order",
  props: ['Licenses'],
  data: function data() {
    return {
      form: {
        Title: '訂單管理',
        Icon: 'settings'
      },
      Order: {},
      ShowBasic: true,
      ShowDetail: true,
      ShowBooking: true,
      ShowGuest: true
    };
  },
  created: function created() {
    this.$emit('ChildUpdated', this.form);
    this.GetOrder();
  },
  methods: {
    GetOrder: function GetOrder() {
      var _this = this;

      Object(_api__WEBPACK_IMPORTED_MODULE_0__["GetOrder"])(this.$route.params.Num).then(function (res) {
        _this.Order = res;

        _this.$nextTick(function () {
          feather.replace();
        });
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertErrorMessage"])(response);
      });
    },
    Days: function Days(date) {
      return moment__WEBPACK_IMPORTED_MODULE_2___default()(date).format('Y/M/D');
    },
    WeekDay: function WeekDay(date) {
      moment__WEBPACK_IMPORTED_MODULE_2___default.a.locale('zh-tw');
      return moment__WEBPACK_IMPORTED_MODULE_2___default()(date).format('dddd');
    },
    nights: function nights(startDate, endDate) {
      var _moment$diff;

      return (_moment$diff = moment__WEBPACK_IMPORTED_MODULE_2___default()(endDate).diff(moment__WEBPACK_IMPORTED_MODULE_2___default()(startDate), 'days')) !== null && _moment$diff !== void 0 ? _moment$diff : 0;
    },
    UpdateOrder: function UpdateOrder() {
      var _this2 = this;

      var credential = {
        Num: this.$route.params.Num,
        Booking_Email: this.Order.Booking_Email,
        Booking_Name: this.Order.Booking_Name,
        Booking_Sex: this.Order.Booking_Sex,
        Booking_IdNumber: this.Order.Booking_IdNumber,
        Booking_Phone: this.Order.Booking_Phone,
        Booking_Area: this.Order.Booking_Area,
        Guest_Email: this.Order.Guest_Email,
        Guest_Name: this.Order.Guest_Name,
        Guest_Sex: this.Order.Guest_Sex,
        Guest_IdNumber: this.Order.Guest_IdNumber,
        Guest_Phone: this.Order.Guest_Phone,
        Guest_Area: this.Order.Guest_Area,
        payment_id: this.Order.payment_id,
        BackendNote: this.Order.BackendNote,
        TaxNumber: this.Order.TaxNumber,
        Cancel: this.Order.Cancel,
        Paid: this.Order.Paid,
        Refund: this.Order.Refund
      };

      Object(_api__WEBPACK_IMPORTED_MODULE_0__["UpdateOrder"])(credential).then(function (res) {
        _this2.GetOrder();
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertErrorMessage"])(response);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Order.vue?vue&type=style&index=0&id=2a298f35&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Order.vue?vue&type=style&index=0&id=2a298f35&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.wh-100[data-v-2a298f35]{\n    width: 100%;\n    height: 100%;\n    display: contents;\n}\n.cursor[data-v-2a298f35]{\n    cursor: pointer;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Order.vue?vue&type=style&index=0&id=2a298f35&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Order.vue?vue&type=style&index=0&id=2a298f35&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Order.vue?vue&type=style&index=0&id=2a298f35&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Order.vue?vue&type=style&index=0&id=2a298f35&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Order.vue?vue&type=template&id=2a298f35&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Order.vue?vue&type=template&id=2a298f35&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "wh-100" }, [
    _c("div", { staticClass: "col-md-12 col-12" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12 col-md-12 col-lg-4 mb-3" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-light mr-2",
              staticStyle: { "background-color": "rgba(216, 217, 219, .5)" },
              on: {
                click: function($event) {
                  return _vm.$router.push({ name: "OrderList" })
                }
              }
            },
            [
              _vm._m(0),
              _vm._v("\n                    返回訂單管理\n                ")
            ]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col-md-12 col-12" }, [
      _c("div", { staticClass: "card" }, [
        _c(
          "div",
          {
            staticClass: "card-header cursor",
            on: {
              click: function($event) {
                _vm.ShowBasic = !_vm.ShowBasic
              }
            }
          },
          [_vm._v("\n                訂單基本資料\n            ")]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.ShowBasic,
                expression: "ShowBasic"
              }
            ],
            staticClass: "card-body"
          },
          [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12 col-lg-12 col-xl-12" }, [
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-1 col-form-label" }, [
                        _vm._v("訂單編號")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-11" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.Num,
                              expression: "Order.Num"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", readonly: "", disabled: "" },
                          domProps: { value: _vm.Order.Num },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.Order, "Num", $event.target.value)
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-1 col-form-label" }, [
                        _vm._v("總金額")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-11" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.TotalPrice,
                              expression: "Order.TotalPrice"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", readonly: "", disabled: "" },
                          domProps: { value: _vm.Order.TotalPrice },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.Order,
                                "TotalPrice",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _vm.Order.Code != null && _vm.Order.Code != undefined
                      ? _c("div", { staticClass: "form-group row" }, [
                          _c(
                            "label",
                            { staticClass: "col-lg-1 col-form-label" },
                            [_vm._v("折扣碼")]
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-lg-11" }, [
                            _c("input", {
                              staticClass: "form-control",
                              attrs: {
                                type: "text",
                                readonly: "",
                                disabled: ""
                              },
                              domProps: {
                                value:
                                  "代碼 " +
                                  _vm.Order.Code +
                                  " / " +
                                  _vm.Order.CodeName +
                                  " / 折價" +
                                  _vm.Order.Discount
                              }
                            })
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-1 col-form-label" }, [
                        _vm._v("備註")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-11" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.Note,
                              expression: "Order.Note"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            cols: "30",
                            rows: "10",
                            disabled: "",
                            readonly: ""
                          },
                          domProps: { value: _vm.Order.Note },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.Order, "Note", $event.target.value)
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-1 col-form-label" }, [
                        _vm._v("統一編號")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-11" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.TaxNumber,
                              expression: "Order.TaxNumber"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "number" },
                          domProps: { value: _vm.Order.TaxNumber },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.Order,
                                "TaxNumber",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-1 col-form-label" }, [
                        _vm._v("付款方式")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-11" }, [
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.Order.payment_id,
                                expression: "Order.payment_id"
                              }
                            ],
                            staticClass: "form-control",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.Order,
                                  "payment_id",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c("option", { attrs: { value: "" } }),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "1" } }, [
                              _vm._v("線上刷卡")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "2" } }, [
                              _vm._v("ATM轉帳")
                            ])
                          ]
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-1 col-form-label" }, [
                        _vm._v("取消否")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-11" }, [
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.Order.Cancel,
                                expression: "Order.Cancel"
                              }
                            ],
                            staticClass: "form-control",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.Order,
                                  "Cancel",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c("option", { attrs: { value: "0" } }, [
                              _vm._v("否")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "1" } }, [
                              _vm._v("是")
                            ])
                          ]
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-1 col-form-label" }, [
                        _vm._v("付款否")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-11" }, [
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.Order.Paid,
                                expression: "Order.Paid"
                              }
                            ],
                            staticClass: "form-control",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.Order,
                                  "Paid",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c("option", { attrs: { value: "1" } }, [
                              _vm._v("是")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "0" } }, [
                              _vm._v("否")
                            ])
                          ]
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _vm.Order.Cancel && _vm.Order.Paid
                      ? _c("div", { staticClass: "form-group row" }, [
                          _c(
                            "label",
                            { staticClass: "col-lg-1 col-form-label" },
                            [_vm._v("退款否")]
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-lg-11" }, [
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.Order.Refund,
                                    expression: "Order.Refund"
                                  }
                                ],
                                staticClass: "form-control",
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.$set(
                                      _vm.Order,
                                      "Refund",
                                      $event.target.multiple
                                        ? $$selectedVal
                                        : $$selectedVal[0]
                                    )
                                  }
                                }
                              },
                              [
                                _c("option", { attrs: { value: "1" } }, [
                                  _vm._v("是")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "0" } }, [
                                  _vm._v("否")
                                ])
                              ]
                            )
                          ])
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-1 col-form-label" }, [
                        _vm._v("管理者備註")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-11" }, [
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.BackendNote,
                              expression: "Order.BackendNote"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { cols: "30", rows: "10" },
                          domProps: { value: _vm.Order.BackendNote },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.Order,
                                "BackendNote",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-12 col-12" }, [
                      _c(
                        "div",
                        { staticClass: "text-right mt-3 position-relative" },
                        [
                          _c("input", {
                            staticClass: "btn btn-success submit-btn",
                            attrs: { type: "button", value: " 確定送出" },
                            on: { click: _vm.UpdateOrder }
                          }),
                          _vm._v(" "),
                          _c("i", { staticClass: "uil uil-message mr-1" })
                        ]
                      )
                    ])
                  ])
                ])
              ])
            ])
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col-md-6 col-6" }, [
      _c("div", { staticClass: "card" }, [
        _c(
          "div",
          {
            staticClass: "card-header cursor",
            on: {
              click: function($event) {
                _vm.ShowBooking = !_vm.ShowBooking
              }
            }
          },
          [_vm._v("\n                訂購人\n            ")]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.ShowBooking,
                expression: "ShowBooking"
              }
            ],
            staticClass: "card-body"
          },
          [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12 col-lg-12 col-xl-12" }, [
                    _vm.Order.member_id != null ||
                    _vm.Order.member_id != undefined
                      ? _c("div", { staticClass: "form-group row" }, [
                          _c(
                            "label",
                            { staticClass: "col-lg-2 col-form-label" },
                            [_vm._v("會員")]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-lg-10" },
                            [
                              _c(
                                "router-link",
                                {
                                  attrs: {
                                    to: {
                                      name: "Member",
                                      params: { id: _vm.Order.member_id }
                                    }
                                  }
                                },
                                [
                                  _c(
                                    "a",
                                    { staticClass: "active btn btn-focus" },
                                    [
                                      _c("i", {
                                        staticClass: "icon-xs",
                                        attrs: { "data-feather": "user" }
                                      })
                                    ]
                                  )
                                ]
                              )
                            ],
                            1
                          )
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-2 col-form-label" }, [
                        _vm._v("姓名")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-10" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.Booking_Name,
                              expression: "Order.Booking_Name"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text" },
                          domProps: { value: _vm.Order.Booking_Name },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.Order,
                                "Booking_Name",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-2 col-form-label" }, [
                        _vm._v("Email")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-10" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.Booking_Email,
                              expression: "Order.Booking_Email"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "email" },
                          domProps: { value: _vm.Order.Booking_Email },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.Order,
                                "Booking_Email",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-2 col-form-label" }, [
                        _vm._v("電話")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-10" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.Booking_Phone,
                              expression: "Order.Booking_Phone"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "tel" },
                          domProps: { value: _vm.Order.Booking_Phone },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.Order,
                                "Booking_Phone",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-2 col-form-label" }, [
                        _vm._v("身分證")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-10" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.Booking_IdNumber,
                              expression: "Order.Booking_IdNumber"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text" },
                          domProps: { value: _vm.Order.Booking_IdNumber },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.Order,
                                "Booking_IdNumber",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-2 col-form-label" }, [
                        _vm._v("性別")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-10" }, [
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.Order.Booking_Sex,
                                expression: "Order.Booking_Sex"
                              }
                            ],
                            staticClass: "form-control m-0",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.Order,
                                  "Booking_Sex",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c("option"),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "1" } }, [
                              _vm._v("先生")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "0" } }, [
                              _vm._v("小姐")
                            ])
                          ]
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-2 col-form-label" }, [
                        _vm._v("地址")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-10" }, [
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.Order.Booking_Area,
                                expression: "Order.Booking_Area"
                              }
                            ],
                            staticClass: "form-control m-0",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.Order,
                                  "Booking_Area",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c("option"),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "台北市" } }, [
                              _vm._v("台北市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "新北市" } }, [
                              _vm._v("新北市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "基隆市" } }, [
                              _vm._v("基隆市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "桃園市" } }, [
                              _vm._v("桃園市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "新竹市" } }, [
                              _vm._v("新竹市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "新竹縣" } }, [
                              _vm._v("新竹縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "宜蘭縣" } }, [
                              _vm._v("宜蘭縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "花蓮縣" } }, [
                              _vm._v("花蓮縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "苗栗縣" } }, [
                              _vm._v("苗栗縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "台中市" } }, [
                              _vm._v("台中市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "彰化縣" } }, [
                              _vm._v("彰化縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "南投縣" } }, [
                              _vm._v("南投縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "雲林縣" } }, [
                              _vm._v("雲林縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "嘉義市" } }, [
                              _vm._v("嘉義市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "嘉義縣" } }, [
                              _vm._v("嘉義縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "台南市" } }, [
                              _vm._v("台南市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "澎湖縣" } }, [
                              _vm._v("澎湖縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "高雄市" } }, [
                              _vm._v("高雄市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "屏東縣" } }, [
                              _vm._v("屏東縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "台東縣" } }, [
                              _vm._v("台東縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "金門縣" } }, [
                              _vm._v("金門縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "連江縣" } }, [
                              _vm._v("連江縣")
                            ])
                          ]
                        )
                      ])
                    ])
                  ])
                ])
              ])
            ])
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col-md-6 col-6" }, [
      _c("div", { staticClass: "card" }, [
        _c(
          "div",
          {
            staticClass: "card-header cursor",
            on: {
              click: function($event) {
                _vm.ShowGuest = !_vm.ShowGuest
              }
            }
          },
          [_vm._v("\n                入住人\n            ")]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.ShowGuest,
                expression: "ShowGuest"
              }
            ],
            staticClass: "card-body"
          },
          [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-12" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12 col-lg-12 col-xl-12" }, [
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-2 col-form-label" }, [
                        _vm._v("姓名")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-10" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.Guest_Name,
                              expression: "Order.Guest_Name"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text" },
                          domProps: { value: _vm.Order.Guest_Name },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.Order,
                                "Guest_Name",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-2 col-form-label" }, [
                        _vm._v("Email")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-10" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.Guest_Email,
                              expression: "Order.Guest_Email"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "email" },
                          domProps: { value: _vm.Order.Guest_Email },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.Order,
                                "Guest_Email",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-2 col-form-label" }, [
                        _vm._v("電話")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-10" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.Guest_Phone,
                              expression: "Order.Guest_Phone"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "tel" },
                          domProps: { value: _vm.Order.Guest_Phone },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.Order,
                                "Guest_Phone",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-2 col-form-label" }, [
                        _vm._v("身分證")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-10" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Order.Guest_IdNumber,
                              expression: "Order.Guest_IdNumber"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text" },
                          domProps: { value: _vm.Order.Guest_IdNumber },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.Order,
                                "Guest_IdNumber",
                                $event.target.value
                              )
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-2 col-form-label" }, [
                        _vm._v("性別")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-10" }, [
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.Order.Guest_Sex,
                                expression: "Order.Guest_Sex"
                              }
                            ],
                            staticClass: "form-control m-0",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.Order,
                                  "Guest_Sex",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c("option"),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "1" } }, [
                              _vm._v("先生")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "0" } }, [
                              _vm._v("小姐")
                            ])
                          ]
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group row" }, [
                      _c("label", { staticClass: "col-lg-2 col-form-label" }, [
                        _vm._v("地址")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-lg-10" }, [
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.Order.Guest_Area,
                                expression: "Order.Guest_Area"
                              }
                            ],
                            staticClass: "form-control m-0",
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.$set(
                                  _vm.Order,
                                  "Guest_Area",
                                  $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                )
                              }
                            }
                          },
                          [
                            _c("option"),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "台北市" } }, [
                              _vm._v("台北市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "新北市" } }, [
                              _vm._v("新北市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "基隆市" } }, [
                              _vm._v("基隆市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "桃園市" } }, [
                              _vm._v("桃園市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "新竹市" } }, [
                              _vm._v("新竹市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "新竹縣" } }, [
                              _vm._v("新竹縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "宜蘭縣" } }, [
                              _vm._v("宜蘭縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "花蓮縣" } }, [
                              _vm._v("花蓮縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "苗栗縣" } }, [
                              _vm._v("苗栗縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "台中市" } }, [
                              _vm._v("台中市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "彰化縣" } }, [
                              _vm._v("彰化縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "南投縣" } }, [
                              _vm._v("南投縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "雲林縣" } }, [
                              _vm._v("雲林縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "嘉義市" } }, [
                              _vm._v("嘉義市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "嘉義縣" } }, [
                              _vm._v("嘉義縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "台南市" } }, [
                              _vm._v("台南市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "澎湖縣" } }, [
                              _vm._v("澎湖縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "高雄市" } }, [
                              _vm._v("高雄市")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "屏東縣" } }, [
                              _vm._v("屏東縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "台東縣" } }, [
                              _vm._v("台東縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "金門縣" } }, [
                              _vm._v("金門縣")
                            ]),
                            _vm._v(" "),
                            _c("option", { attrs: { value: "連江縣" } }, [
                              _vm._v("連江縣")
                            ])
                          ]
                        )
                      ])
                    ])
                  ])
                ])
              ])
            ])
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col-md-12 col-12" }, [
      _c("div", { staticClass: "card" }, [
        _c(
          "div",
          {
            staticClass: "card-header cursor",
            on: {
              click: function($event) {
                _vm.ShowDetail = !_vm.ShowDetail
              }
            }
          },
          [_vm._v("\n                訂單明細\n            ")]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.ShowDetail,
                expression: "ShowDetail"
              }
            ],
            staticClass: "card-body",
            staticStyle: { "padding-bottom": "unset" }
          },
          [
            _c(
              "div",
              { staticClass: "row" },
              _vm._l(_vm.Order.order_detail, function(detail) {
                return _c(
                  "div",
                  {
                    staticClass: "col-12 pt-5",
                    staticStyle: { "border-bottom": "solid 5px #d6d6d6" }
                  },
                  [
                    _c("div", { staticClass: "row" }, [
                      _c(
                        "div",
                        { staticClass: "col-md-12 col-lg-12 col-xl-12" },
                        [
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              { staticClass: "col-lg-1 col-form-label" },
                              [_vm._v("房型 | 方案")]
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-lg-11" }, [
                              _c("h5", [
                                _vm._v(
                                  _vm._s(detail.room ? detail.room.Name : "") +
                                    " | " +
                                    _vm._s(
                                      detail.promotion
                                        ? detail.promotion.Name
                                        : "基本方案"
                                    )
                                )
                              ])
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              { staticClass: "col-lg-1 col-form-label" },
                              [_vm._v("入住日期")]
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-lg-11" }, [
                              _c("h5", [
                                _vm._v(
                                  _vm._s(detail.StartDate) +
                                    " " +
                                    _vm._s(_vm.WeekDay(detail.StartDate)) +
                                    " ~ " +
                                    _vm._s(detail.EndDate) +
                                    " " +
                                    _vm._s(_vm.WeekDay(detail.EndDate)) +
                                    " (" +
                                    _vm._s(
                                      _vm.nights(
                                        detail.StartDate,
                                        detail.EndDate
                                      )
                                    ) +
                                    "晚)"
                                )
                              ])
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group row" }, [
                            _c(
                              "label",
                              { staticClass: "col-lg-1 col-form-label" },
                              [_vm._v("入住人數")]
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-lg-11" }, [
                              _c("h5", [
                                _vm._v(_vm._s(detail.Adult) + " 大人")
                              ]),
                              _vm._v(" "),
                              _c("h5", [
                                _vm._v(_vm._s(detail.Child) + " 小孩")
                              ]),
                              _vm._v(" "),
                              _c("h5", [_vm._v(_vm._s(detail.Baby) + " 嬰兒")])
                            ])
                          ]),
                          _vm._v(" "),
                          detail.ExtraPeople > 0 || detail.ExtraBed > 0
                            ? _c("div", { staticClass: "form-group row" }, [
                                _c(
                                  "label",
                                  { staticClass: "col-lg-1 col-form-label" },
                                  [_vm._v("必要加購")]
                                ),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-lg-11" }, [
                                  detail.ExtraBed > 0
                                    ? _c("h5", [
                                        _vm._v(
                                          "加 " +
                                            _vm._s(detail.ExtraBed) +
                                            " 床"
                                        )
                                      ])
                                    : _vm._e(),
                                  _vm._v(" "),
                                  detail.ExtraPeople > 0
                                    ? _c("h5", [
                                        _vm._v(
                                          "加 " +
                                            _vm._s(detail.ExtraPeople) +
                                            " 備品"
                                        )
                                      ])
                                    : _vm._e()
                                ])
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          detail.shop.length > 0
                            ? _c("div", { staticClass: "form-group row" }, [
                                _c(
                                  "label",
                                  { staticClass: "col-lg-1 col-form-label" },
                                  [_vm._v("加購商品")]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "col-lg-11" },
                                  _vm._l(detail.shop, function(shop) {
                                    return _c("h5", [
                                      _vm._v(
                                        _vm._s(shop.Name) +
                                          " x " +
                                          _vm._s(shop.Numbers) +
                                          " x " +
                                          _vm._s(shop.Nights) +
                                          " 晚"
                                      )
                                    ])
                                  }),
                                  0
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          detail.car.length > 0
                            ? _c("div", { staticClass: "form-group row" }, [
                                _c(
                                  "label",
                                  { staticClass: "col-lg-1 col-form-label" },
                                  [_vm._v("加購接駁")]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "col-lg-11" },
                                  _vm._l(detail.car, function(car) {
                                    return _c(
                                      "h5",
                                      [
                                        _vm._v(
                                          "\n                                            " +
                                            _vm._s(car.Name) +
                                            " ( " +
                                            _vm._s(
                                              car.type === "1" ? "單趟" : "來回"
                                            ) +
                                            " ) x " +
                                            _vm._s(car.Numbers) +
                                            "\n                                            "
                                        ),
                                        car.pickdt != null || car.arrvdt != null
                                          ? [
                                              car.arrvdt != null
                                                ? _c("div", [
                                                    _vm._v(
                                                      " 抵達日期時間: " +
                                                        _vm._s(car.arrvdt) +
                                                        "  "
                                                    )
                                                  ])
                                                : _vm._e(),
                                              _vm._v(" "),
                                              car.pickdt != null
                                                ? _c("div", [
                                                    _vm._v(
                                                      " 出發日期時間: " +
                                                        _vm._s(car.pickdt) +
                                                        "  "
                                                    )
                                                  ])
                                                : _vm._e()
                                            ]
                                          : _vm._e()
                                      ],
                                      2
                                    )
                                  }),
                                  0
                                )
                              ])
                            : _vm._e()
                        ]
                      )
                    ])
                  ]
                )
              }),
              0
            )
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "btn-icon-wrapper pr-2 fw-500" }, [
      _c("i", {
        staticClass: "icon-xs",
        attrs: { "data-feather": "corner-up-left" }
      })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Order.vue":
/*!*****************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Order.vue ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Order_vue_vue_type_template_id_2a298f35_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Order.vue?vue&type=template&id=2a298f35&scoped=true& */ "./resources/js/backend/components/children/logined/children/Order.vue?vue&type=template&id=2a298f35&scoped=true&");
/* harmony import */ var _Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Order.vue?vue&type=script&lang=js& */ "./resources/js/backend/components/children/logined/children/Order.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Order_vue_vue_type_style_index_0_id_2a298f35_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Order.vue?vue&type=style&index=0&id=2a298f35&scoped=true&lang=css& */ "./resources/js/backend/components/children/logined/children/Order.vue?vue&type=style&index=0&id=2a298f35&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Order_vue_vue_type_template_id_2a298f35_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Order_vue_vue_type_template_id_2a298f35_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "2a298f35",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/backend/components/children/logined/children/Order.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Order.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Order.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Order.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Order.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Order.vue?vue&type=style&index=0&id=2a298f35&scoped=true&lang=css&":
/*!**************************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Order.vue?vue&type=style&index=0&id=2a298f35&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_2a298f35_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Order.vue?vue&type=style&index=0&id=2a298f35&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Order.vue?vue&type=style&index=0&id=2a298f35&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_2a298f35_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_2a298f35_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_2a298f35_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_2a298f35_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_2a298f35_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Order.vue?vue&type=template&id=2a298f35&scoped=true&":
/*!************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Order.vue?vue&type=template&id=2a298f35&scoped=true& ***!
  \************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_template_id_2a298f35_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Order.vue?vue&type=template&id=2a298f35&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Order.vue?vue&type=template&id=2a298f35&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_template_id_2a298f35_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_template_id_2a298f35_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);