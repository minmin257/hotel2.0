(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Member"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Member.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Member.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../api */ "./resources/js/backend/api.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../helper */ "./resources/js/backend/helper.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Member",
  props: ['Licenses'],
  data: function data() {
    return {
      form: {
        Title: '會員管理',
        Icon: 'settings'
      },
      Member: {},
      Bookings: [],
      NewPassword: null
    };
  },
  created: function created() {
    this.$emit('ChildUpdated', this.form);
    this.GetMember();
  },
  methods: {
    GetMember: function GetMember() {
      var _this = this;

      Object(_api__WEBPACK_IMPORTED_MODULE_0__["GetMember"])(this.$route.params.id).then(function (res) {
        _this.Member = res.Member;
        _this.Bookings = res.Bookings;

        _this.$nextTick(function () {
          feather.replace();
        });
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertErrorMessage"])(response);
      });
    },
    UpdateMember: function UpdateMember() {
      var _this2 = this;

      var credential = {
        id: this.$route.params.id,
        Name: this.Member.Name,
        email: this.Member.email,
        password: this.NewPassword,
        IdNumber: this.Member.IdNumber,
        Phone: this.Member.Phone,
        Sex: this.Member.Sex,
        Area: this.Member.Area,
        State: this.Member.State,
        Note: this.Member.Note
      };

      Object(_api__WEBPACK_IMPORTED_MODULE_0__["UpdateMember"])(credential).then(function (res) {
        _this2.Member = res;
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertErrorMessage"])(response);
      });
    },
    CopyNum: function CopyNum(id) {
      var CopyNum = document.querySelector('#Num' + id);
      CopyNum.setAttribute('type', 'text');
      CopyNum.select();

      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
      } catch (err) {
        alert('Oops, unable to copy');
      }
      /* unselect the range */


      CopyNum.setAttribute('type', 'hidden');
      window.getSelection().removeAllRanges();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/Logined/Member.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/Logined/Member.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Member"
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Member.vue?vue&type=style&index=0&id=bebb847a&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Member.vue?vue&type=style&index=0&id=bebb847a&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.wh-100[data-v-bebb847a]{\n    width: 100%;\n    height: 100%;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Member.vue?vue&type=style&index=0&id=bebb847a&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Member.vue?vue&type=style&index=0&id=bebb847a&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Member.vue?vue&type=style&index=0&id=bebb847a&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Member.vue?vue&type=style&index=0&id=bebb847a&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Member.vue?vue&type=template&id=bebb847a&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Member.vue?vue&type=template&id=bebb847a&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "wh-100" }, [
    _c("div", { staticClass: "col-md-12 col-12" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12 col-md-12 col-lg-4 mb-3" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-light mr-2",
              staticStyle: { "background-color": "rgba(216, 217, 219, .5)" },
              on: {
                click: function($event) {
                  return _vm.$router.push({ name: "MemberList" })
                }
              }
            },
            [
              _vm._m(0),
              _vm._v("\n                    返回會員管理\n                ")
            ]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col-md-12 col-12" }, [
      _c("div", { staticClass: "card" }, [
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-md-12 col-lg-12 col-xl-12" }, [
                  _c("div", { staticClass: "form-group row" }, [
                    _c(
                      "label",
                      {
                        staticClass: "col-lg-1 col-form-label",
                        attrs: { for: "Name" }
                      },
                      [_vm._v("姓名")]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-lg-11" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.Member.Name,
                            expression: "Member.Name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "text",
                          id: "Name",
                          placeholder: "姓名"
                        },
                        domProps: { value: _vm.Member.Name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.Member, "Name", $event.target.value)
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group row" }, [
                    _c(
                      "label",
                      {
                        staticClass: "col-lg-1 col-form-label",
                        attrs: { for: "email" }
                      },
                      [_vm._v("Email")]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-lg-11" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.Member.email,
                            expression: "Member.email"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "email",
                          id: "email",
                          placeholder: "Email"
                        },
                        domProps: { value: _vm.Member.email },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.Member, "email", $event.target.value)
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group row" }, [
                    _c(
                      "label",
                      {
                        staticClass: "col-lg-1 col-form-label",
                        attrs: { for: "password" }
                      },
                      [_vm._v("密碼")]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-lg-11" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.NewPassword,
                            expression: "NewPassword"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "password",
                          id: "password",
                          placeholder: "*******"
                        },
                        domProps: { value: _vm.NewPassword },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.NewPassword = $event.target.value
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group row" }, [
                    _c(
                      "label",
                      {
                        staticClass: "col-lg-1 col-form-label",
                        attrs: { for: "IdNumber" }
                      },
                      [_vm._v("身份證")]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-lg-11" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.Member.IdNumber,
                            expression: "Member.IdNumber"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "text",
                          id: "IdNumber",
                          placeholder: "身份證"
                        },
                        domProps: { value: _vm.Member.IdNumber },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.Member,
                              "IdNumber",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group row" }, [
                    _c(
                      "label",
                      {
                        staticClass: "col-lg-1 col-form-label",
                        attrs: { for: "Phone" }
                      },
                      [_vm._v("電話")]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-lg-11" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.Member.Phone,
                            expression: "Member.Phone"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "tel",
                          id: "Phone",
                          placeholder: "電話"
                        },
                        domProps: { value: _vm.Member.Phone },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.Member, "Phone", $event.target.value)
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group row" }, [
                    _c("label", { staticClass: "col-lg-1 col-form-label" }, [
                      _vm._v("性別")
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-lg-11" }, [
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Member.Sex,
                              expression: "Member.Sex"
                            }
                          ],
                          staticClass: "form-control mt-2",
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.Member,
                                "Sex",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        [
                          _c("option"),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "1" } }, [
                            _vm._v("先生")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "0" } }, [
                            _vm._v("小姐")
                          ])
                        ]
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group row" }, [
                    _c("label", { staticClass: "col-lg-1 col-form-label" }, [
                      _vm._v("地址")
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-lg-11" }, [
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Member.Area,
                              expression: "Member.Area"
                            }
                          ],
                          staticClass: "form-control mt-2",
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.Member,
                                "Area",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        [
                          _c("option"),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "台北市" } }, [
                            _vm._v("台北市")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "新北市" } }, [
                            _vm._v("新北市")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "基隆市" } }, [
                            _vm._v("基隆市")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "桃園市" } }, [
                            _vm._v("桃園市")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "新竹市" } }, [
                            _vm._v("新竹市")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "新竹縣" } }, [
                            _vm._v("新竹縣")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "宜蘭縣" } }, [
                            _vm._v("宜蘭縣")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "花蓮縣" } }, [
                            _vm._v("花蓮縣")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "苗栗縣" } }, [
                            _vm._v("苗栗縣")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "台中市" } }, [
                            _vm._v("台中市")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "彰化縣" } }, [
                            _vm._v("彰化縣")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "南投縣" } }, [
                            _vm._v("南投縣")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "雲林縣" } }, [
                            _vm._v("雲林縣")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "嘉義市" } }, [
                            _vm._v("嘉義市")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "嘉義縣" } }, [
                            _vm._v("嘉義縣")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "台南市" } }, [
                            _vm._v("台南市")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "澎湖縣" } }, [
                            _vm._v("澎湖縣")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "高雄市" } }, [
                            _vm._v("高雄市")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "屏東縣" } }, [
                            _vm._v("屏東縣")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "台東縣" } }, [
                            _vm._v("台東縣")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "金門縣" } }, [
                            _vm._v("金門縣")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "連江縣" } }, [
                            _vm._v("連江縣")
                          ])
                        ]
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group row" }, [
                    _c("label", { staticClass: "col-lg-1 col-form-label" }, [
                      _vm._v("預設啟用")
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-lg-11" }, [
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.Member.State,
                              expression: "Member.State"
                            }
                          ],
                          staticClass: "form-control",
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.Member,
                                "State",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        [
                          _c("option", { attrs: { value: "1" } }, [
                            _vm._v("啟用")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "0" } }, [
                            _vm._v("關閉")
                          ])
                        ]
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group row" }, [
                    _c("label", { staticClass: "col-lg-1 col-form-label" }, [
                      _vm._v("備註")
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-lg-11" }, [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.Member.Note,
                            expression: "Member.Note"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { cols: "30", rows: "10" },
                        domProps: { value: _vm.Member.Note },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.Member, "Note", $event.target.value)
                          }
                        }
                      })
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-12 col-12" }, [
                  _c(
                    "div",
                    { staticClass: "text-right mt-3 position-relative" },
                    [
                      _c("input", {
                        staticClass: "btn btn-success submit-btn",
                        attrs: { type: "button", value: " 確定送出" },
                        on: { click: _vm.UpdateMember }
                      }),
                      _vm._v(" "),
                      _c("i", { staticClass: "uil uil-message mr-1" })
                    ]
                  )
                ])
              ])
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _vm.Bookings.length > 0
      ? _c("div", { staticClass: "col-md-12 col-12" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "table-responsive" }, [
                _c(
                  "table",
                  {
                    staticClass:
                      "align-middle mb-0 table table-borderless table-striped table-hover font-style"
                  },
                  [
                    _vm._m(1),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      _vm._l(_vm.Bookings, function(Booking) {
                        return _c("tr", [
                          _c(
                            "td",
                            { staticClass: "text-md-center text-sm-left" },
                            [
                              _c("input", {
                                attrs: {
                                  type: "hidden",
                                  id: "Num" + Booking.id
                                },
                                domProps: { value: Booking.Num }
                              }),
                              _vm._v(
                                "\n                                    " +
                                  _vm._s(Booking.Num) +
                                  "\n                                    "
                              ),
                              _c(
                                "button",
                                {
                                  staticClass: "active btn btn-focus",
                                  on: {
                                    click: function($event) {
                                      return _vm.CopyNum(Booking.id)
                                    }
                                  }
                                },
                                [
                                  _c("i", {
                                    staticClass: "icon-xs",
                                    attrs: { "data-feather": "copy" }
                                  })
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            { staticClass: "text-md-center text-sm-left" },
                            [_vm._v(_vm._s(Booking.TotalPrice))]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            { staticClass: "text-md-center text-sm-left" },
                            [
                              _vm._v(
                                "\n                                    " +
                                  _vm._s(
                                    Booking.Refund
                                      ? "以退款"
                                      : Booking.Paid
                                      ? "已付款"
                                      : "尚未付款"
                                  ) +
                                  "\n                                "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticClass: "text-md-center text-sm-left",
                              attrs: { "data-title": "編輯" }
                            },
                            [
                              _c(
                                "div",
                                {
                                  staticClass: "btn-group-sm btn-group",
                                  attrs: { role: "group" }
                                },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        to: {
                                          name: "Order",
                                          params: { Num: Booking.Num }
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "button",
                                        {
                                          staticClass: "active btn btn-focus",
                                          attrs: { type: "button" }
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "icon-xs",
                                            attrs: { "data-feather": "edit" }
                                          })
                                        ]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ]
                          )
                        ])
                      }),
                      0
                    )
                  ]
                )
              ])
            ])
          ])
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "btn-icon-wrapper pr-2 fw-500" }, [
      _c("i", {
        staticClass: "icon-xs",
        attrs: { "data-feather": "corner-up-left" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { staticClass: "text-center" }, [_vm._v("訂單編號")]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center" }, [_vm._v("總金額")]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center" }, [_vm._v("訂單狀態")]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center" }, [_vm._v("編輯")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/Logined/Member.vue?vue&type=template&id=ccf10ab4&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/Logined/Member.vue?vue&type=template&id=ccf10ab4&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("router-view", { key: this.$route.path })
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Member.vue":
/*!******************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Member.vue ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Member_vue_vue_type_template_id_bebb847a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Member.vue?vue&type=template&id=bebb847a&scoped=true& */ "./resources/js/backend/components/children/logined/children/Member.vue?vue&type=template&id=bebb847a&scoped=true&");
/* harmony import */ var _Member_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Member.vue?vue&type=script&lang=js& */ "./resources/js/backend/components/children/logined/children/Member.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Member_vue_vue_type_style_index_0_id_bebb847a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Member.vue?vue&type=style&index=0&id=bebb847a&scoped=true&lang=css& */ "./resources/js/backend/components/children/logined/children/Member.vue?vue&type=style&index=0&id=bebb847a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Member_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Member_vue_vue_type_template_id_bebb847a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Member_vue_vue_type_template_id_bebb847a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "bebb847a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/backend/components/children/logined/children/Member.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Member.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Member.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Member.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Member.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Member.vue?vue&type=style&index=0&id=bebb847a&scoped=true&lang=css&":
/*!***************************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Member.vue?vue&type=style&index=0&id=bebb847a&scoped=true&lang=css& ***!
  \***************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_style_index_0_id_bebb847a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Member.vue?vue&type=style&index=0&id=bebb847a&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Member.vue?vue&type=style&index=0&id=bebb847a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_style_index_0_id_bebb847a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_style_index_0_id_bebb847a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_style_index_0_id_bebb847a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_style_index_0_id_bebb847a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_style_index_0_id_bebb847a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Member.vue?vue&type=template&id=bebb847a&scoped=true&":
/*!*************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Member.vue?vue&type=template&id=bebb847a&scoped=true& ***!
  \*************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_template_id_bebb847a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Member.vue?vue&type=template&id=bebb847a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Member.vue?vue&type=template&id=bebb847a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_template_id_bebb847a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_template_id_bebb847a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/front/components/Logined/Member.vue":
/*!**********************************************************!*\
  !*** ./resources/js/front/components/Logined/Member.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Member_vue_vue_type_template_id_ccf10ab4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Member.vue?vue&type=template&id=ccf10ab4&scoped=true& */ "./resources/js/front/components/Logined/Member.vue?vue&type=template&id=ccf10ab4&scoped=true&");
/* harmony import */ var _Member_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Member.vue?vue&type=script&lang=js& */ "./resources/js/front/components/Logined/Member.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Member_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Member_vue_vue_type_template_id_ccf10ab4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Member_vue_vue_type_template_id_ccf10ab4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "ccf10ab4",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/front/components/Logined/Member.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/front/components/Logined/Member.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/front/components/Logined/Member.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Member.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/Logined/Member.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/front/components/Logined/Member.vue?vue&type=template&id=ccf10ab4&scoped=true&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/front/components/Logined/Member.vue?vue&type=template&id=ccf10ab4&scoped=true& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_template_id_ccf10ab4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Member.vue?vue&type=template&id=ccf10ab4&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/Logined/Member.vue?vue&type=template&id=ccf10ab4&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_template_id_ccf10ab4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Member_vue_vue_type_template_id_ccf10ab4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);