(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Promotions"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Paginate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../Paginate */ "./resources/js/backend/components/Paginate.vue");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../api */ "./resources/js/backend/api.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../helper */ "./resources/js/backend/helper.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var paginate = 10;


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Promotions",
  props: ['Licenses'],
  components: {
    Paginate: _Paginate__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      form: {
        Title: '活動設置',
        Icon: 'settings'
      },
      Promotions: {
        last_page: 1,
        total: 0
      },
      newPromotion: {
        Name: '',
        room_id: '',
        State: 1,
        DefaultPrice: 0,
        DefaultAllowEarlyBird: 0,
        DefaultEarlyBirdPrice: 0,
        DefaultEarlyBirdDays: 0
      },
      currentPage: 1,
      Paginate: paginate,
      showModal: false,
      showModalStyle: {
        display: 'none'
      },
      Rooms: {}
    };
  },
  created: function created() {
    this.$emit('ChildUpdated', this.form);
    this.GetRooms();
    this.GetPromotions();
  },
  watch: {
    showModal: function showModal(newValue) {
      if (newValue === true) {
        this.showModalStyle.display = 'block';
        document.getElementsByTagName("body")[0].className = "modal-open";
      } else {
        this.showModalStyle.display = 'none';
        document.getElementsByTagName("body")[0].className = "";
      }
    },
    currentPage: function currentPage(newValue) {
      this.GetPromotions();
    }
  },
  methods: {
    GetRooms: function GetRooms() {
      var _this = this;

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["GetRooms"])().then(function (res) {
        _this.Rooms = res;

        _this.$nextTick(function () {
          feather.replace();
        });
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
      });
    },
    GetPromotions: function GetPromotions() {
      var _this2 = this;

      var credential = {
        Paginate: this.Paginate,
        CurrentPage: this.currentPage
      };

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["GetPromotions"])(credential).then(function (res) {
        _this2.Promotions = res;

        _this2.$nextTick(function () {
          feather.replace();
        });
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
      });
    },
    onPageChange: function onPageChange(page) {
      this.currentPage = page;
    },
    UpdatePromotions: function UpdatePromotions() {
      var _this3 = this;

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["UpdatePromotions"])(this.Promotions.data).then(function (res) {
        _this3.GetPromotions();
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
      });
    },
    DeletePromotion: function DeletePromotion(Promotion) {
      var _this4 = this;

      var credential = {
        promotion_id: Promotion.id
      };

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["DeletePromotion"])(credential).then(function (res) {
        _this4.GetPromotions();
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
      });
    },
    ClearNewPromotion: function ClearNewPromotion() {
      this.newPromotion = {
        Name: '',
        room_id: '',
        State: 1,
        DefaultPrice: 0,
        DefaultAllowEarlyBird: 0,
        DefaultEarlyBirdPrice: 0,
        DefaultEarlyBirdDays: 0
      };
    },
    CreatePromotion: function CreatePromotion() {
      var _this5 = this;

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["CreatePromotion"])(this.newPromotion).then(function (res) {
        _this5.showModal = !_this5.showModal;

        _this5.GetPromotions();

        _this5.ClearNewPromotion();
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
      });
    },
    EditPromotion: function EditPromotion(Promotion) {
      this.$router.push({
        name: 'Promotion',
        params: {
          id: Promotion.id
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=style&index=0&id=445a5fd9&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=style&index=0&id=445a5fd9&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.vnis[data-v-445a5fd9]{\n    display: inline-flex;\n    width: 100%;\n}\n.wh-100[data-v-445a5fd9]{\n    width: 100%;\n    height: 100%;\n}\n.modal-enter[data-v-445a5fd9] {\n    top: -10px\n}\n.modal-enter-active[data-v-445a5fd9] {\n    transition: all .5s;\n}\n.modal-enter-to[data-v-445a5fd9]{\n    opacity: 1;\n}\n.modal-leave-active[data-v-445a5fd9] {\n    transition: all .5s;\n    opacity: 0;\n    top: -10px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=style&index=0&id=445a5fd9&scoped=true&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=style&index=0&id=445a5fd9&scoped=true&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Promotions.vue?vue&type=style&index=0&id=445a5fd9&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=style&index=0&id=445a5fd9&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=template&id=445a5fd9&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=template&id=445a5fd9&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "wh-100" },
    [
      _c("div", { staticClass: "col-md-12 col-12" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 col-md-12 col-lg-4 mb-3" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-main_color mr-2 openModal",
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    _vm.showModal = true
                  }
                }
              },
              [
                _vm._m(0),
                _vm._v("\n                    新增\n                ")
              ]
            ),
            _vm._v(" "),
            _c("span", { staticClass: "store-btn" }, [
              _c("input", {
                staticClass: "btn btn-success",
                attrs: { type: "button", value: " 儲存變更" },
                on: { click: _vm.UpdatePromotions }
              }),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-star" })
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "col-md-12 col-12" },
        [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "table-responsive" }, [
              _c(
                "table",
                {
                  staticClass:
                    "align-middle mb-0 table table-borderless table-striped table-hover font-style"
                },
                [
                  _c("thead", [
                    _c(
                      "tr",
                      [
                        _c("th", { staticClass: "text-center " }, [
                          _vm._v("名稱")
                        ]),
                        _vm._v(" "),
                        _c("th", { staticClass: "text-center " }, [
                          _vm._v("適用房型")
                        ]),
                        _vm._v(" "),
                        _c("th", { staticClass: "text-center " }, [
                          _vm._v("預設活動價")
                        ]),
                        _vm._v(" "),
                        _c("th", { staticClass: "text-center " }, [
                          _vm._v("啟用")
                        ]),
                        _vm._v(" "),
                        _vm.Licenses.includes("早鳥折扣")
                          ? [
                              _c("th", { staticClass: "text-center " }, [
                                _vm._v("預設早鳥")
                              ]),
                              _vm._v(" "),
                              _c("th", { staticClass: "text-center " }, [
                                _vm._v("預設早鳥價")
                              ]),
                              _vm._v(" "),
                              _c("th", { staticClass: "text-center " }, [
                                _vm._v("預設早鳥天數")
                              ])
                            ]
                          : _vm._e(),
                        _vm._v(" "),
                        _c("th", { staticClass: "text-center " }, [
                          _vm._v("編輯/刪除")
                        ])
                      ],
                      2
                    )
                  ]),
                  _vm._v(" "),
                  _vm.Promotions.data !== undefined &&
                  _vm.Promotions.data.length > 0
                    ? _c(
                        "tbody",
                        _vm._l(_vm.Promotions.data, function(Promotion, index) {
                          return _c(
                            "tr",
                            [
                              _c(
                                "td",
                                {
                                  staticClass: "text-md-center text-sm-left",
                                  attrs: { "data-title": "名稱" }
                                },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: Promotion.Name,
                                        expression: "Promotion.Name"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      type: "text",
                                      "aria-describedby": ""
                                    },
                                    domProps: { value: Promotion.Name },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          Promotion,
                                          "Name",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "td",
                                {
                                  staticClass: "text-md-center text-sm-left",
                                  attrs: { "data-title": "適用房型" }
                                },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      attrs: {
                                        "aria-expanded": "false",
                                        to: {
                                          name: "DailyPromotion",
                                          params: { id: Promotion.id }
                                        }
                                      }
                                    },
                                    [_vm._v(_vm._s(Promotion.room.Name))]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "td",
                                {
                                  staticClass: "text-md-center text-sm-left",
                                  attrs: { "data-title": "預設活動價" }
                                },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: Promotion.DefaultPrice,
                                        expression: "Promotion.DefaultPrice"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      type: "number",
                                      "aria-describedby": ""
                                    },
                                    domProps: { value: Promotion.DefaultPrice },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          Promotion,
                                          "DefaultPrice",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "td",
                                {
                                  staticClass: "text-md-center text-sm-left",
                                  attrs: { "data-title": "啟用" }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "toggle-btn",
                                      attrs: { id: "status-toggle-btn" }
                                    },
                                    [
                                      _c("div", { staticClass: "on" }, [
                                        _vm._v("ON")
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "off" }, [
                                        _vm._v("OFF")
                                      ]),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: Promotion.State,
                                            expression: "Promotion.State"
                                          }
                                        ],
                                        attrs: {
                                          type: "checkbox",
                                          "true-value": 0,
                                          "false-value": 1
                                        },
                                        domProps: {
                                          checked: Array.isArray(
                                            Promotion.State
                                          )
                                            ? _vm._i(Promotion.State, null) > -1
                                            : _vm._q(Promotion.State, 0)
                                        },
                                        on: {
                                          change: function($event) {
                                            var $$a = Promotion.State,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? 0 : 1
                                            if (Array.isArray($$a)) {
                                              var $$v = null,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  _vm.$set(
                                                    Promotion,
                                                    "State",
                                                    $$a.concat([$$v])
                                                  )
                                              } else {
                                                $$i > -1 &&
                                                  _vm.$set(
                                                    Promotion,
                                                    "State",
                                                    $$a
                                                      .slice(0, $$i)
                                                      .concat(
                                                        $$a.slice($$i + 1)
                                                      )
                                                  )
                                              }
                                            } else {
                                              _vm.$set(Promotion, "State", $$c)
                                            }
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span")
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _vm.Licenses.includes("早鳥折扣")
                                ? [
                                    _c(
                                      "td",
                                      {
                                        staticClass:
                                          "text-md-center text-sm-left",
                                        attrs: { "data-title": "預設早鳥" }
                                      },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass: "toggle-btn",
                                            attrs: { id: "status-toggle-btn" }
                                          },
                                          [
                                            _c("div", { staticClass: "on" }, [
                                              _vm._v("ON")
                                            ]),
                                            _vm._v(" "),
                                            _c("div", { staticClass: "off" }, [
                                              _vm._v("OFF")
                                            ]),
                                            _vm._v(" "),
                                            _c("input", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value:
                                                    Promotion.DefaultAllowEarlyBird,
                                                  expression:
                                                    "Promotion.DefaultAllowEarlyBird"
                                                }
                                              ],
                                              attrs: {
                                                type: "checkbox",
                                                "true-value": 0,
                                                "false-value": 1
                                              },
                                              domProps: {
                                                checked: Array.isArray(
                                                  Promotion.DefaultAllowEarlyBird
                                                )
                                                  ? _vm._i(
                                                      Promotion.DefaultAllowEarlyBird,
                                                      null
                                                    ) > -1
                                                  : _vm._q(
                                                      Promotion.DefaultAllowEarlyBird,
                                                      0
                                                    )
                                              },
                                              on: {
                                                change: function($event) {
                                                  var $$a =
                                                      Promotion.DefaultAllowEarlyBird,
                                                    $$el = $event.target,
                                                    $$c = $$el.checked ? 0 : 1
                                                  if (Array.isArray($$a)) {
                                                    var $$v = null,
                                                      $$i = _vm._i($$a, $$v)
                                                    if ($$el.checked) {
                                                      $$i < 0 &&
                                                        _vm.$set(
                                                          Promotion,
                                                          "DefaultAllowEarlyBird",
                                                          $$a.concat([$$v])
                                                        )
                                                    } else {
                                                      $$i > -1 &&
                                                        _vm.$set(
                                                          Promotion,
                                                          "DefaultAllowEarlyBird",
                                                          $$a
                                                            .slice(0, $$i)
                                                            .concat(
                                                              $$a.slice($$i + 1)
                                                            )
                                                        )
                                                    }
                                                  } else {
                                                    _vm.$set(
                                                      Promotion,
                                                      "DefaultAllowEarlyBird",
                                                      $$c
                                                    )
                                                  }
                                                }
                                              }
                                            }),
                                            _vm._v(" "),
                                            _c("span")
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "td",
                                      {
                                        staticClass:
                                          "text-md-center text-sm-left",
                                        attrs: { "data-title": "預設早鳥價" }
                                      },
                                      [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value:
                                                Promotion.DefaultEarlyBirdPrice,
                                              expression:
                                                "Promotion.DefaultEarlyBirdPrice"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: {
                                            type: "number",
                                            "aria-describedby": ""
                                          },
                                          domProps: {
                                            value:
                                              Promotion.DefaultEarlyBirdPrice
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                Promotion,
                                                "DefaultEarlyBirdPrice",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "td",
                                      {
                                        staticClass:
                                          "text-md-center text-sm-left",
                                        attrs: { "data-title": "預設早鳥天數" }
                                      },
                                      [
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value:
                                                Promotion.DefaultEarlyBirdDays,
                                              expression:
                                                "Promotion.DefaultEarlyBirdDays"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: {
                                            type: "number",
                                            "aria-describedby": ""
                                          },
                                          domProps: {
                                            value:
                                              Promotion.DefaultEarlyBirdDays
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                Promotion,
                                                "DefaultEarlyBirdDays",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]
                                    )
                                  ]
                                : _vm._e(),
                              _vm._v(" "),
                              _c(
                                "td",
                                {
                                  staticClass: "text-md-center text-sm-left",
                                  attrs: { "data-title": "編輯/刪除" }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "btn-group-sm btn-group",
                                      attrs: { role: "group" }
                                    },
                                    [
                                      _c(
                                        "button",
                                        {
                                          staticClass: "active btn btn-focus",
                                          on: {
                                            click: function($event) {
                                              return _vm.EditPromotion(
                                                Promotion
                                              )
                                            }
                                          }
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "icon-xs",
                                            attrs: { "data-feather": "edit" }
                                          })
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "button",
                                        {
                                          staticClass: "btn btn-focus",
                                          on: {
                                            click: function($event) {
                                              return _vm.DeletePromotion(
                                                Promotion
                                              )
                                            }
                                          }
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "icon-xs",
                                            attrs: { "data-feather": "trash-2" }
                                          })
                                        ]
                                      )
                                    ]
                                  )
                                ]
                              )
                            ],
                            2
                          )
                        }),
                        0
                      )
                    : _vm._e()
                ]
              )
            ])
          ]),
          _vm._v(" "),
          _vm.Promotions
            ? _c("paginate", {
                attrs: {
                  "total-pages": _vm.Promotions.last_page,
                  total: _vm.Promotions.total,
                  "per-page": _vm.Paginate,
                  "current-page": _vm.currentPage
                },
                on: { pagechanged: _vm.onPageChange }
              })
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c("transition", { attrs: { name: "modal" } }, [
        _vm.showModal
          ? _c(
              "div",
              {
                staticClass: "modal fade",
                class: { show: _vm.showModal },
                style: [_vm.showModalStyle],
                attrs: {
                  id: "addModal",
                  tabindex: "-1",
                  role: "dialog",
                  "aria-labelledby": "addModalLabel",
                  "aria-hidden": "true"
                }
              },
              [
                _c(
                  "div",
                  { staticClass: "modal-dialog", attrs: { role: "document" } },
                  [
                    _c("div", { staticClass: "modal-content" }, [
                      _c("div", { staticClass: "modal-header" }, [
                        _c(
                          "h5",
                          {
                            staticClass: "modal-title",
                            attrs: { id: "addModalLabel" }
                          },
                          [_vm._v("新增活動")]
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "close",
                            attrs: { type: "button", "aria-label": "Close" },
                            on: {
                              click: function($event) {
                                _vm.showModal = false
                              }
                            }
                          },
                          [
                            _c("span", { attrs: { "aria-hidden": "true" } }, [
                              _vm._v("×")
                            ])
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "modal-body" },
                        [
                          _c("div", { staticClass: "row form-group" }, [
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("label", { attrs: { for: "" } }, [
                                _vm._v("名稱")
                              ]),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.newPromotion.Name,
                                    expression: "newPromotion.Name"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { type: "text" },
                                domProps: { value: _vm.newPromotion.Name },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.newPromotion,
                                      "Name",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row form-group" }, [
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("label", { attrs: { for: "" } }, [
                                _vm._v("適用房型")
                              ]),
                              _vm._v(" "),
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.newPromotion.room_id,
                                      expression: "newPromotion.room_id"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  on: {
                                    change: function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.$set(
                                        _vm.newPromotion,
                                        "room_id",
                                        $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      )
                                    }
                                  }
                                },
                                [
                                  _c("option"),
                                  _vm._v(" "),
                                  _vm._l(_vm.Rooms, function(Room) {
                                    return _c(
                                      "option",
                                      { domProps: { value: Room.id } },
                                      [_vm._v(_vm._s(Room.Name))]
                                    )
                                  })
                                ],
                                2
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row form-group" }, [
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("label", { attrs: { for: "" } }, [
                                _vm._v("預設活動價")
                              ]),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.newPromotion.DefaultPrice,
                                    expression: "newPromotion.DefaultPrice"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "number",
                                  "aria-describedby": "",
                                  min: "0"
                                },
                                domProps: {
                                  value: _vm.newPromotion.DefaultPrice
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.newPromotion,
                                      "DefaultPrice",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row form-group" }, [
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("label", { attrs: { for: "" } }, [
                                _vm._v("預設啟用")
                              ]),
                              _vm._v(" "),
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.newPromotion.State,
                                      expression: "newPromotion.State"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  on: {
                                    change: function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.$set(
                                        _vm.newPromotion,
                                        "State",
                                        $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      )
                                    }
                                  }
                                },
                                [
                                  _c("option", { attrs: { value: "1" } }, [
                                    _vm._v("啟用")
                                  ]),
                                  _vm._v(" "),
                                  _c("option", { attrs: { value: "0" } }, [
                                    _vm._v("關閉")
                                  ])
                                ]
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _vm.Licenses.includes("早鳥折扣")
                            ? [
                                _c("div", { staticClass: "row form-group" }, [
                                  _c("div", { staticClass: "col-md-12" }, [
                                    _c("label", { attrs: { for: "" } }, [
                                      _vm._v("預設早鳥")
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "select",
                                      {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value:
                                              _vm.newPromotion
                                                .DefaultAllowEarlyBird,
                                            expression:
                                              "newPromotion.DefaultAllowEarlyBird"
                                          }
                                        ],
                                        staticClass: "form-control",
                                        on: {
                                          change: function($event) {
                                            var $$selectedVal = Array.prototype.filter
                                              .call(
                                                $event.target.options,
                                                function(o) {
                                                  return o.selected
                                                }
                                              )
                                              .map(function(o) {
                                                var val =
                                                  "_value" in o
                                                    ? o._value
                                                    : o.value
                                                return val
                                              })
                                            _vm.$set(
                                              _vm.newPromotion,
                                              "DefaultAllowEarlyBird",
                                              $event.target.multiple
                                                ? $$selectedVal
                                                : $$selectedVal[0]
                                            )
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "option",
                                          { attrs: { value: "0" } },
                                          [_vm._v("關閉")]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "option",
                                          { attrs: { value: "1" } },
                                          [_vm._v("啟用")]
                                        )
                                      ]
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "row form-group" }, [
                                  _c("div", { staticClass: "col-md-12" }, [
                                    _c("label", { attrs: { for: "" } }, [
                                      _vm._v("預設早鳥價格")
                                    ]),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value:
                                            _vm.newPromotion
                                              .DefaultEarlyBirdPrice,
                                          expression:
                                            "newPromotion.DefaultEarlyBirdPrice"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        type: "number",
                                        "aria-describedby": ""
                                      },
                                      domProps: {
                                        value:
                                          _vm.newPromotion.DefaultEarlyBirdPrice
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.newPromotion,
                                            "DefaultEarlyBirdPrice",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "row form-group" }, [
                                  _c("div", { staticClass: "col-md-12" }, [
                                    _c("label", { attrs: { for: "" } }, [
                                      _vm._v("預設早鳥天數")
                                    ]),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value:
                                            _vm.newPromotion
                                              .DefaultEarlyBirdDays,
                                          expression:
                                            "newPromotion.DefaultEarlyBirdDays"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        type: "number",
                                        "aria-describedby": ""
                                      },
                                      domProps: {
                                        value:
                                          _vm.newPromotion.DefaultEarlyBirdDays
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.newPromotion,
                                            "DefaultEarlyBirdDays",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ])
                                ])
                              ]
                            : _vm._e()
                        ],
                        2
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "modal-footer position-relative" },
                        [
                          _c("input", {
                            staticClass: "btn btn-success submit-btn",
                            attrs: { type: "button", value: " 確定送出" },
                            on: { click: _vm.CreatePromotion }
                          }),
                          _vm._v(" "),
                          _c("i", {
                            staticClass: "uil uil-message mr-1",
                            staticStyle: { right: "92px" }
                          })
                        ]
                      )
                    ])
                  ]
                )
              ]
            )
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("div", {
        staticClass: "modal-backdrop fade",
        class: { show: _vm.showModal },
        style: [_vm.showModalStyle]
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "btn-icon-wrapper pr-2 fw-500" }, [
      _c("i", { staticClass: "fas fa-plus" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Promotions.vue":
/*!**********************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Promotions.vue ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Promotions_vue_vue_type_template_id_445a5fd9_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Promotions.vue?vue&type=template&id=445a5fd9&scoped=true& */ "./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=template&id=445a5fd9&scoped=true&");
/* harmony import */ var _Promotions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Promotions.vue?vue&type=script&lang=js& */ "./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Promotions_vue_vue_type_style_index_0_id_445a5fd9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Promotions.vue?vue&type=style&index=0&id=445a5fd9&scoped=true&lang=css& */ "./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=style&index=0&id=445a5fd9&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Promotions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Promotions_vue_vue_type_template_id_445a5fd9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Promotions_vue_vue_type_template_id_445a5fd9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "445a5fd9",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/backend/components/children/logined/children/Promotions.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Promotions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Promotions.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Promotions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=style&index=0&id=445a5fd9&scoped=true&lang=css&":
/*!*******************************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=style&index=0&id=445a5fd9&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Promotions_vue_vue_type_style_index_0_id_445a5fd9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Promotions.vue?vue&type=style&index=0&id=445a5fd9&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=style&index=0&id=445a5fd9&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Promotions_vue_vue_type_style_index_0_id_445a5fd9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Promotions_vue_vue_type_style_index_0_id_445a5fd9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Promotions_vue_vue_type_style_index_0_id_445a5fd9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Promotions_vue_vue_type_style_index_0_id_445a5fd9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Promotions_vue_vue_type_style_index_0_id_445a5fd9_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=template&id=445a5fd9&scoped=true&":
/*!*****************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=template&id=445a5fd9&scoped=true& ***!
  \*****************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Promotions_vue_vue_type_template_id_445a5fd9_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Promotions.vue?vue&type=template&id=445a5fd9&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Promotions.vue?vue&type=template&id=445a5fd9&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Promotions_vue_vue_type_template_id_445a5fd9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Promotions_vue_vue_type_template_id_445a5fd9_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);