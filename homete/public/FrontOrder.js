(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["FrontOrder"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/Logined/Order.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/Logined/Order.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../api */ "./resources/js/front/api.js");
/* harmony import */ var _backend_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../backend/helper */ "./resources/js/backend/helper.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Order",
  props: {
    Num: {
      required: true
    }
  },
  data: function data() {
    return {
      Order: {}
    };
  },
  created: function created() {
    if (this.Num === undefined) {
      this.$router.push({
        name: 'FrontProfile'
      });
    } else {
      this.init();
    }
  },
  methods: {
    init: function init() {
      var _this = this;

      var credential = {
        Num: this.Num
      };
      Object(_api__WEBPACK_IMPORTED_MODULE_0__["GetOrder"])(credential).then(function (res) {
        _this.Order = res.Order;
      })["catch"](function (rep) {
        return Object(_backend_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertErrorMessage"])(rep);
      });
    },
    Price: function Price(Num) {
      return Num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    },
    Days: function Days(date) {
      return moment__WEBPACK_IMPORTED_MODULE_2___default()(date).format('Y/M/D');
    },
    WeekDay: function WeekDay(date) {
      moment__WEBPACK_IMPORTED_MODULE_2___default.a.locale('zh-tw');
      return moment__WEBPACK_IMPORTED_MODULE_2___default()(date).format('dddd');
    },
    nights: function nights(startDate, endDate) {
      var _moment$diff;

      return (_moment$diff = moment__WEBPACK_IMPORTED_MODULE_2___default()(endDate).diff(moment__WEBPACK_IMPORTED_MODULE_2___default()(startDate), 'days')) !== null && _moment$diff !== void 0 ? _moment$diff : 0;
    },
    CancelOrder: function CancelOrder() {
      Object(_backend_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertAsk"])('取消', '確定取消訂單嗎 ? 此動作將無法復原').then(function (res) {
        console.log('OK');
      })["catch"](function (rep) {
        console.log('Cancel');
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/Logined/Order.vue?vue&type=template&id=e5f0ac1c&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/Logined/Order.vue?vue&type=template&id=e5f0ac1c&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "main",
    { staticClass: "no-padding", staticStyle: { overflow: "hidden" } },
    [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "MemberPage warp_p" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12" }, [
              _c(
                "div",
                { staticClass: "d-flex justify-content-end mb-3 mr-3" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "back",
                      attrs: { to: { name: "FrontProfile" } }
                    },
                    [
                      _vm._v(
                        "\n                            BACK\n                        "
                      )
                    ]
                  )
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "box_shadow" }, [
                _c("h6", { staticStyle: { color: "#deb666" } }, [
                  _c("img", {
                    staticClass: "position-absolute",
                    attrs: {
                      src:
                        "https://img.icons8.com/pastel-glyph/30/deb666/regular-file.png"
                    }
                  }),
                  _vm._v(
                    "訂單編號 :\n                            " +
                      _vm._s(_vm.Num) +
                      "\n                            "
                  ),
                  !_vm.Order.Cancel
                    ? _c(
                        "a",
                        {
                          staticClass:
                            "booking-details-btn btn btn-danger position-absolute",
                          attrs: { type: "button" },
                          on: { click: _vm.CancelOrder }
                        },
                        [_vm._v("取消訂單")]
                      )
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "booking-details" }, [
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-md-12 mb-5 pb-5" }, [
                      _vm._m(1),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "row" },
                        [
                          _vm._l(_vm.Order.BookingData, function(BookingData) {
                            return _c(
                              "div",
                              { staticClass: "rooms-detail-item" },
                              [
                                _c("div", { staticClass: "col-md-12 p-0" }, [
                                  _c("div", { staticClass: "col-md-4" }, [
                                    _c("p", [
                                      _vm._v("入住/退房日期 :"),
                                      _c("br"),
                                      _vm._v(
                                        _vm._s(
                                          _vm.Days(BookingData.startDate)
                                        ) +
                                          _vm._s(
                                            _vm.WeekDay(BookingData.startDate)
                                          ) +
                                          " ~ " +
                                          _vm._s(
                                            _vm.Days(BookingData.endDate)
                                          ) +
                                          " " +
                                          _vm._s(
                                            _vm.WeekDay(BookingData.endDate)
                                          ) +
                                          " (" +
                                          _vm._s(
                                            _vm.nights(
                                              BookingData.startDate,
                                              BookingData.endDate
                                            )
                                          ) +
                                          "晚)"
                                      )
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "col-md-4" }, [
                                    _c("p", [
                                      _vm._v("房型 :"),
                                      _c("br"),
                                      _vm._v(
                                        _vm._s(BookingData.Room.Name) +
                                          " | " +
                                          _vm._s(BookingData.Promotion.Name)
                                      )
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "col-md-4" }, [
                                    _c(
                                      "p",
                                      [
                                        _vm._v("加購 :"),
                                        _c("br"),
                                        _vm._v(" "),
                                        BookingData.ExtraPeople > 0 ||
                                        BookingData.ExtraBed > 0
                                          ? [
                                              BookingData.ExtraPeople > 0
                                                ? [
                                                    _vm._v(
                                                      "\n                                                                加備品 x " +
                                                        _vm._s(
                                                          _vm.nights(
                                                            BookingData.startDate,
                                                            BookingData.endDate
                                                          )
                                                        ) +
                                                        " 晚 x " +
                                                        _vm._s(
                                                          BookingData.ExtraPeople
                                                        ) +
                                                        " "
                                                    ),
                                                    BookingData.ExtraBed > 0
                                                      ? [_vm._v(" + ")]
                                                      : _vm._e()
                                                  ]
                                                : _vm._e(),
                                              _vm._v(" "),
                                              BookingData.ExtraBed > 0
                                                ? [
                                                    _vm._v(
                                                      "\n                                                                加床 x " +
                                                        _vm._s(
                                                          _vm.nights(
                                                            BookingData.startDate,
                                                            BookingData.endDate
                                                          )
                                                        ) +
                                                        " 晚 x " +
                                                        _vm._s(
                                                          BookingData.ExtraBed
                                                        ) +
                                                        "\n                                                            "
                                                    )
                                                  ]
                                                : _vm._e(),
                                              _vm._v(" "),
                                              _c("br")
                                            ]
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _vm._l(BookingData.ExtraShop, function(
                                          Shop
                                        ) {
                                          return BookingData.ExtraShop.length >
                                            0
                                            ? [
                                                _vm._v(
                                                  "\n                                                            " +
                                                    _vm._s(Shop.Name) +
                                                    " x " +
                                                    _vm._s(Shop.Nights) +
                                                    " 晚 x " +
                                                    _vm._s(Shop.Numbers) +
                                                    " "
                                                ),
                                                _c("br")
                                              ]
                                            : _vm._e()
                                        }),
                                        _vm._v(" "),
                                        _vm._l(BookingData.ExtraCar, function(
                                          Car
                                        ) {
                                          return BookingData.ExtraCar.length > 0
                                            ? [
                                                _vm._v(
                                                  "\n                                                            " +
                                                    _vm._s(Car.Name) +
                                                    " - " +
                                                    _vm._s(
                                                      Car.type == "1"
                                                        ? "單趟"
                                                        : "來回"
                                                    ) +
                                                    " x " +
                                                    _vm._s(Car.Numbers) +
                                                    "\n                                                            "
                                                ),
                                                Car.arrvdt
                                                  ? [
                                                      _c("br"),
                                                      _vm._v(" "),
                                                      _c(
                                                        "span",
                                                        {
                                                          staticStyle: {
                                                            color: "#a94442"
                                                          }
                                                        },
                                                        [
                                                          _vm._v(
                                                            " 抵達日期時間 : " +
                                                              _vm._s(Car.arrvdt)
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  : _vm._e(),
                                                _vm._v(" "),
                                                Car.pickdt
                                                  ? [
                                                      _c("br"),
                                                      _vm._v(" "),
                                                      _c(
                                                        "span",
                                                        {
                                                          staticStyle: {
                                                            color: "#a94442"
                                                          }
                                                        },
                                                        [
                                                          _vm._v(
                                                            " 出發日期時間 : " +
                                                              _vm._s(Car.pickdt)
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  : _vm._e(),
                                                _vm._v(" "),
                                                _c("br")
                                              ]
                                            : _vm._e()
                                        })
                                      ],
                                      2
                                    )
                                  ])
                                ])
                              ]
                            )
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "clearfix hidden-xs" })
                        ],
                        2
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "clearfix hidden-xs" }),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-6 mb-5 pb-4" }, [
                      _vm._m(2),
                      _vm._v(" "),
                      _vm.Order.BookPeople
                        ? _c("div", { staticClass: "row" }, [
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("姓名 :"),
                                _c("br"),
                                _vm._v(_vm._s(_vm.Order.BookPeople.Name))
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("身分證號碼或護照號碼 :"),
                                _c("br"),
                                _vm._v(_vm._s(_vm.Order.BookPeople.IdNumber))
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("手機 :"),
                                _c("br"),
                                _vm._v(_vm._s(_vm.Order.BookPeople.Phone))
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("E-mail :"),
                                _c("br"),
                                _vm._v(_vm._s(_vm.Order.BookPeople.Email))
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("性別 :"),
                                _c("br"),
                                _vm._v(
                                  _vm._s(
                                    _vm.Order.BookPeople.Sex == "1"
                                      ? "男"
                                      : "女"
                                  )
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("地址 :"),
                                _c("br"),
                                _vm._v(_vm._s(_vm.Order.BookPeople.Area))
                              ])
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("br"),
                      _vm._v(" "),
                      _vm._m(3),
                      _vm._v(" "),
                      _vm.Order.GuestPeople
                        ? _c("div", { staticClass: "row" }, [
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("姓名 :"),
                                _c("br"),
                                _vm._v(_vm._s(_vm.Order.GuestPeople.Name))
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("身分證號碼或護照號碼 :"),
                                _c("br"),
                                _vm._v(_vm._s(_vm.Order.GuestPeople.IdNumber))
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("手機 :"),
                                _c("br"),
                                _vm._v(_vm._s(_vm.Order.GuestPeople.Phone))
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("E-mail :"),
                                _c("br"),
                                _vm._v(_vm._s(_vm.Order.GuestPeople.Email))
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("性別 :"),
                                _c("br"),
                                _vm._v(
                                  _vm._s(
                                    _vm.Order.GuestPeople.Sex == "1"
                                      ? "男"
                                      : "女"
                                  )
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("地址 :"),
                                _c("br"),
                                _vm._v(_vm._s(_vm.Order.GuestPeople.Area))
                              ])
                            ])
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-6 mb-5 pb-4" }, [
                      _vm._m(4),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _vm.Order.Price
                          ? _c("div", { staticClass: "col-md-6" }, [
                              _c("p", [
                                _vm._v("金額 :"),
                                _c("br"),
                                _vm._v(
                                  "NT$" + _vm._s(_vm.Price(_vm.Order.Price))
                                )
                              ])
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-md-6" }, [
                          _c(
                            "p",
                            [
                              _vm._v(
                                "付款方式 :\n                                                "
                              ),
                              _vm.Order.Payment != null
                                ? [
                                    _vm.Order.Payment == "1"
                                      ? [
                                          _c("br"),
                                          _vm._v(
                                            "線上刷卡\n                                                    "
                                          )
                                        ]
                                      : _vm.Order.Payment == "2"
                                      ? [
                                          _c("br"),
                                          _vm._v(
                                            "ATM轉帳\n                                                    "
                                          )
                                        ]
                                      : _vm._e()
                                  ]
                                : [
                                    _c("br"),
                                    _vm._v(" "),
                                    _c(
                                      "router-link",
                                      {
                                        staticClass:
                                          "booking-details-btn btn btn-danger",
                                        attrs: {
                                          to: {
                                            name: "PaymentSelect",
                                            params: { Num: _vm.Num }
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "\n                                                        前往付款\n                                                    "
                                        )
                                      ]
                                    )
                                  ]
                            ],
                            2
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-md-6" }, [
                          _c(
                            "p",
                            [
                              _vm._v(
                                "訂單狀態 :\n                                                "
                              ),
                              _vm.Order.Refund
                                ? [
                                    _c("br"),
                                    _vm._v(
                                      "已退款\n                                                "
                                    )
                                  ]
                                : _vm.Order.Cancel
                                ? [
                                    _c("br"),
                                    _vm._v(
                                      "已取消\n                                                "
                                    )
                                  ]
                                : _vm.Order.Paid
                                ? [
                                    _c("br"),
                                    _vm._v(
                                      "已付款\n                                                "
                                    )
                                  ]
                                : _vm._e(),
                              _vm._v(" "),
                              [
                                _c("br"),
                                _vm._v(
                                  "未付款\n                                                "
                                )
                              ]
                            ],
                            2
                          )
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _vm._m(5)
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "BannerBlock",
        staticStyle: { "background-image": "url('/images/banner/banner6.jpg')" }
      },
      [
        _c("div", { staticClass: "BannerTitle text-center" }, [
          _c(
            "h1",
            {
              staticClass: "wow fadeInUp",
              staticStyle: { visibility: "visible" },
              attrs: { "data-wow-delay": ".2s" }
            },
            [_vm._v("BOOKING")]
          ),
          _vm._v(" "),
          _c(
            "p",
            {
              staticClass: "wow fadeInUp",
              staticStyle: { visibility: "visible" },
              attrs: { "data-wow-delay": ".6s" }
            },
            [_vm._v("/")]
          ),
          _vm._v(" "),
          _c(
            "h2",
            {
              staticClass: "wow fadeInUp",
              staticStyle: { visibility: "visible" },
              attrs: { "data-wow-delay": ".8s" }
            },
            [_vm._v("訂房紀錄")]
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h6", [
      _c("img", {
        staticClass: "position-absolute",
        attrs: { src: "https://img.icons8.com/ios/30/cccccc/bed.png" }
      }),
      _vm._v("房型資料\n                                    ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h6", [
      _c("img", {
        staticClass: "position-absolute",
        attrs: {
          src:
            "https://img.icons8.com/pastel-glyph/30/cccccc/gender-neutral-user.png"
        }
      }),
      _vm._v("訂房資料")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h6", [
      _c("img", {
        staticClass: "position-absolute",
        attrs: {
          src:
            "https://img.icons8.com/pastel-glyph/30/cccccc/gender-neutral-user.png"
        }
      }),
      _vm._v("入住資料")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h6", [
      _c("img", {
        staticClass: "position-absolute",
        attrs: { src: "https://img.icons8.com/ios/30/cccccc/checklist.png" }
      }),
      _vm._v("訂單狀態")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-12" }, [
      _c("h6", [
        _c("img", {
          staticClass: "position-absolute",
          attrs: {
            src: "https://img.icons8.com/ios/30/cccccc/box-important.png"
          }
        }),
        _vm._v("注意事項")
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "取消和預付款政策根據各種住宿類型而有所不同。 請輸入您的入住日期並參閱您所需的客房的條款。"
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v("\n                                        【防詐騙提醒】"),
        _c("br"),
        _vm._v(
          "\n                                        【飯店業者】不會指示旅客以任何理由要求去操作 ATM 自動提款機解除 自動扣款，或誤設成分期付款或通知您變更付款條件,亦不會在電話中要求您提供信用卡帳號 資料、銀行帳號，或主動提供您銀行/信用卡客服電話。 請隨時提高警覺勿上當！！ 如有任何疑問請您直接致電飯店訂房部確認。"
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/front/components/Logined/Order.vue":
/*!*********************************************************!*\
  !*** ./resources/js/front/components/Logined/Order.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Order_vue_vue_type_template_id_e5f0ac1c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Order.vue?vue&type=template&id=e5f0ac1c&scoped=true& */ "./resources/js/front/components/Logined/Order.vue?vue&type=template&id=e5f0ac1c&scoped=true&");
/* harmony import */ var _Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Order.vue?vue&type=script&lang=js& */ "./resources/js/front/components/Logined/Order.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Order_vue_vue_type_template_id_e5f0ac1c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Order_vue_vue_type_template_id_e5f0ac1c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "e5f0ac1c",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/front/components/Logined/Order.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/front/components/Logined/Order.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/front/components/Logined/Order.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Order.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/Logined/Order.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/front/components/Logined/Order.vue?vue&type=template&id=e5f0ac1c&scoped=true&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/front/components/Logined/Order.vue?vue&type=template&id=e5f0ac1c&scoped=true& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_template_id_e5f0ac1c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Order.vue?vue&type=template&id=e5f0ac1c&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/Logined/Order.vue?vue&type=template&id=e5f0ac1c&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_template_id_e5f0ac1c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_template_id_e5f0ac1c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);