(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ATM~FrontLogin~FrontProfile~Initialise~PaymentSelect~PromotionPicking~Search"],{

/***/ "./resources/js/backend/helper.js":
/*!****************************************!*\
  !*** ./resources/js/backend/helper.js ***!
  \****************************************/
/*! exports provided: SwalAlertErrorMessage, SwalAlertSuccessRedirect, SwalAlertAsk, Submit, getLocalUser, logout, ButtonOpenFileManger */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwalAlertErrorMessage", function() { return SwalAlertErrorMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwalAlertSuccessRedirect", function() { return SwalAlertSuccessRedirect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwalAlertAsk", function() { return SwalAlertAsk; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Submit", function() { return Submit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLocalUser", function() { return getLocalUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logout", function() { return logout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonOpenFileManger", function() { return ButtonOpenFileManger; });
/* harmony import */ var _storage_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./storage.js */ "./resources/js/backend/storage.js");

function SwalAlertErrorMessage(response) {
  if (response.status != 401) {
    var errors = response.data.errors;
    var html = '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">';
    Object.keys(errors).forEach(function (item) {
      html += '<div><i class="fas fa-times" style="color:#f27474"></i>  ' + errors[item].join(',') + "</div>";
    });
    Swal.fire({
      icon: 'error',
      html: html,
      showCloseButton: false,
      showConfirmButton: false,
      width: '300px',
      heightAuto: false
    });
  }
}
function SwalAlertSuccessRedirect(response) {
  var _response$data;

  var redirect = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var messages = (_response$data = response.data) !== null && _response$data !== void 0 ? _response$data : '成功';

  if (redirect != null) {
    return new Promise(function (res, rej) {
      Swal.fire({
        title: messages,
        icon: 'success',
        showConfirmButton: false,
        width: '300px',
        timer: 970,
        heightAuto: false
      }).then(function (result) {
        res(true);
      });
    });
  } else {
    Swal.fire({
      title: messages,
      icon: 'success',
      showConfirmButton: false,
      width: '300px',
      timer: 970,
      heightAuto: false
    });
  }
}
function SwalAlertAsk() {
  var _title, _message;

  var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var message = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  title = (_title = title) !== null && _title !== void 0 ? _title : '您確定要刪除嗎？';
  message = (_message = message) !== null && _message !== void 0 ? _message : '此動作將不可回復';
  return new Promise(function (res, rej) {
    Swal.fire({
      title: '<h2 style="color: #f8bb86">' + title + '</h2>',
      html: '<span style="color: #707070;font-size: 14px;">' + message + '<span>',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: '確定',
      cancelButtonText: '取消',
      reverseButtons: true,
      heightAuto: false,
      width: '300px',
      confirmButtonColor: '#43d39e',
      cancelButtonColor: '#ff5c75'
    }).then(function (result) {
      if (result.value) {
        res(true);
      } else {
        rej(false);
      }
    });
  });
}
function Submit(method, url, data) {
  var message = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  var redirect = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
  return new Promise(function (res, rej) {
    axios({
      method: method,
      url: url,
      data: data
    }).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function getLocalUser() {
  var userStr = localStorage.getItem("user");

  if (!userStr) {
    return null;
  }

  return JSON.parse(userStr);
}
function logout() {
  return _storage_js__WEBPACK_IMPORTED_MODULE_0__["default"].commit('Logout');
} //目前無用到

function ButtonOpenFileManger() {
  $.fn.filemanager = function (type, options) {
    type = type || 'file';
    var userStr = localStorage.getItem("user");
    this.on('click', function (e) {
      var route_prefix = options && options.prefix ? options.prefix : '/filemanager';
      var target_input = $('#' + $(this).data('input'));
      var target_preview = $('#' + $(this).data('preview'));

      if (userStr) {
        var token = 'Bearer ' + JSON.parse(userStr)['token'];
        window.open(route_prefix + '?type=' + type + '&token=' + token, 'FileManager', 'width=900,height=600');
      } else {
        window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
      }

      window.SetUrl = function (items) {
        var file_path = items.map(function (item) {
          return item.url;
        }).join(','); // set the value of the desired input to image url

        target_input.val('').val(file_path).trigger('change');
        target_input[0].dispatchEvent(new CustomEvent('change')); // clear previous preview

        target_preview.html(''); // set or change the preview image src

        items.forEach(function (item) {
          target_preview.append($('<img>').css('height', '5rem').attr('src', item.thumb_url));
        }); // trigger change event

        target_preview.trigger('change');
      };

      return false;
    });
  };
}

/***/ }),

/***/ "./resources/js/backend/storage.js":
/*!*****************************************!*\
  !*** ./resources/js/backend/storage.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _router_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./router.js */ "./resources/js/backend/router.js");
/* harmony import */ var _helper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./helper.js */ "./resources/js/backend/helper.js");



vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vuex__WEBPACK_IMPORTED_MODULE_1__["default"]);

var user = Object(_helper_js__WEBPACK_IMPORTED_MODULE_3__["getLocalUser"])();
/* harmony default export */ __webpack_exports__["default"] = (new vuex__WEBPACK_IMPORTED_MODULE_1__["default"].Store({
  state: {
    currentUser: user
  },
  mutations: {
    Login: function Login(state, data) {
      state.currentUser = Object.assign({}, data.user, {
        token: data.access_token
      });
      localStorage.setItem("user", JSON.stringify(state.currentUser));
      _router_js__WEBPACK_IMPORTED_MODULE_2__["default"].go({
        name: 'index'
      });
    },
    Logout: function Logout(state) {
      state.currentUser = null;
      localStorage.removeItem("user");
      _router_js__WEBPACK_IMPORTED_MODULE_2__["default"].go({
        name: 'login'
      });
    }
  },
  getters: {
    currentUser: function currentUser(state) {
      return state.currentUser;
    }
  }
}));

/***/ }),

/***/ "./resources/js/front/api.js":
/*!***********************************!*\
  !*** ./resources/js/front/api.js ***!
  \***********************************/
/*! exports provided: Searching, Remain, SearchingByRoom, Rule, ExtraShopFromRoom, ExtraCarFromRoom, CheckingDiscountCode, CheckingOrder, PaymentSelect, GetATMInfo, Login, Register, Forgot, UpdateMemberBasic, UpdateMemberPassword, UpdateMemberEmail, GetMemberOrderRecord */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Searching", function() { return Searching; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Remain", function() { return Remain; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchingByRoom", function() { return SearchingByRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Rule", function() { return Rule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtraShopFromRoom", function() { return ExtraShopFromRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExtraCarFromRoom", function() { return ExtraCarFromRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckingDiscountCode", function() { return CheckingDiscountCode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckingOrder", function() { return CheckingOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentSelect", function() { return PaymentSelect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetATMInfo", function() { return GetATMInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Login", function() { return Login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Register", function() { return Register; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Forgot", function() { return Forgot; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateMemberBasic", function() { return UpdateMemberBasic; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateMemberPassword", function() { return UpdateMemberPassword; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateMemberEmail", function() { return UpdateMemberEmail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetMemberOrderRecord", function() { return GetMemberOrderRecord; });
/* harmony import */ var _storage_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./storage.js */ "./resources/js/front/storage.js");
/* harmony import */ var _router_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./router.js */ "./resources/js/front/router.js");
var _this = undefined;



window.axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
var Front = '/api/Front';
axios.interceptors.response.use(function (response) {
  //如果過期 會refresh token
  var token = response.headers.authorization;

  if (token) {
    // 如果 header 中存在 token，那么触发 refreshToken 方法，替换本地的 token
    _this.$store.dispatch('Refresh', token);
  }

  return response;
}, function (error) {
  if (error.response.status == 401) {
    // router.push('/login');
    Swal.fire({
      title: '提示',
      icon: 'error',
      text: '時效過期，請重新登入',
      showCloseButton: true,
      confirmButtonColor: '#ba0000',
      confirmButtonText: '重新登入'
    }).then(function (result) {
      _storage_js__WEBPACK_IMPORTED_MODULE_0__["default"].commit('Logout');
      _router_js__WEBPACK_IMPORTED_MODULE_1__["default"].push({
        name: 'login'
      });
    });
  } else if (error.response.status == 403) {
    error.response.data.errors = {
      錯誤: ['權限不足，請洽管理員']
    };
  }

  return Promise.reject(error);
});

if (_storage_js__WEBPACK_IMPORTED_MODULE_0__["default"].getters.Member) {
  axios.defaults.headers.common["Authorization"] = "Bearer ".concat(_storage_js__WEBPACK_IMPORTED_MODULE_0__["default"].getters.Member.token);
} // 左邊calendar search推薦


function Searching(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/Searching", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
} //中間calendar 當前月份抓取剩餘

function Remain(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/Remain", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
} //中間calendar 重新計算

function SearchingByRoom(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/SearchingByRoom", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
} //抓取全域參數(加床 加備品費用)

function Rule() {
  return new Promise(function (res, rej) {
    axios.post(Front + "/Rule").then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
} //抓取Room 的 ExtraShops

function ExtraShopFromRoom(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/ExtraShopFromRoom", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function ExtraCarFromRoom(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/ExtraCarFromRoom", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function CheckingDiscountCode(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/CheckingDiscountCode", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function CheckingOrder(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/CheckingOrder", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function PaymentSelect(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/PaymentSelect", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function GetATMInfo() {
  return new Promise(function (res, rej) {
    axios.post(Front + "/ATMInfo").then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function Login(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/Login", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function Register(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/Register", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function Forgot(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/Forgot", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function UpdateMemberBasic(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/UpdateMemberBasic", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function UpdateMemberPassword(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/UpdateMemberPassword", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function UpdateMemberEmail(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/UpdateMemberEmail", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function GetMemberOrderRecord(credential) {
  return new Promise(function (res, rej) {
    axios.post(Front + "/MemberOrderRecord", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}

/***/ })

}]);