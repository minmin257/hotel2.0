(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["PaymentSelect"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/PaymentSelect.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/PaymentSelect.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../api */ "./resources/js/front/api.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _backend_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../backend/helper */ "./resources/js/backend/helper.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "PaymentSelect",
  // props:{
  //     Num: {
  //         // required: true,
  //         default: 'f7a67b55737d09a32b96c7d51514ff8ab71dcdce'
  //     }
  // },
  data: function data() {
    return {
      Num: this.$route.params.Num,
      AllowPrecautions: false,
      Price: '',
      DiscountCode: '',
      BookingData: '',
      TaxNumber: null,
      Note: null,
      Payment: null,
      Paid: false,
      Refund: false
    };
  },
  computed: {
    Member: function Member() {
      return this.$store.getters.Member;
    }
  },
  created: function created() {
    if (this.Num === undefined) {
      this.$router.push({
        name: 'Search'
      });
    }

    this.init();
  },
  methods: {
    init: function init() {
      var _this = this;

      this.$emit('updateStep', 3);
      var credential = {
        Num: this.Num
      };
      Object(_api__WEBPACK_IMPORTED_MODULE_0__["PaymentSelect"])(credential).then(function (res) {
        _this.Price = res.Order.Price;
        _this.DiscountCode = res.Order.DiscountCode;
        _this.BookingData = res.Order.BookingData;
        _this.TaxNumber = res.Order.TaxNumber;
        _this.Payment = res.Order.Payment;
        _this.Note = res.Order.Note;
        _this.Paid = res.Order.Paid;
        _this.Refund = res.Order.Refund;
      })["catch"](function (rep) {
        Object(_backend_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(rep);

        _this.$router.push({
          name: 'Search'
        });
      });
    },
    Day: function Day(date) {
      moment__WEBPACK_IMPORTED_MODULE_1___default.a.locale('zh-tw');
      return moment__WEBPACK_IMPORTED_MODULE_1___default()(date).format('Y/M/D');
    },
    WeekDay: function WeekDay(date) {
      moment__WEBPACK_IMPORTED_MODULE_1___default.a.locale('zh-tw');
      return moment__WEBPACK_IMPORTED_MODULE_1___default()(date).format('dddd');
    },
    nights: function nights(startDate, endDate) {
      var _moment$diff;

      return (_moment$diff = moment__WEBPACK_IMPORTED_MODULE_1___default()(endDate).diff(moment__WEBPACK_IMPORTED_MODULE_1___default()(startDate), 'days')) !== null && _moment$diff !== void 0 ? _moment$diff : 0;
    },
    CopyNum: function CopyNum() {
      var CopyNum = document.querySelector('#Num');
      CopyNum.setAttribute('type', 'text');
      CopyNum.select();

      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
      } catch (err) {
        alert('Oops, unable to copy');
      }
      /* unselect the range */


      CopyNum.setAttribute('type', 'hidden');
      window.getSelection().removeAllRanges();
    },
    GoPay: function GoPay() {
      var _this2 = this;

      if (!this.AllowPrecautions) {
        var html = '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">';
        html += '<div><i class="fas fa-times" style="color:#f27474"></i>  ' + "請先詳閱並同意訂房注意事項條款" + "</div>";
        Swal.fire({
          icon: 'error',
          html: html,
          showCloseButton: false,
          showConfirmButton: false,
          width: '300px',
          heightAuto: false
        });
      } else if (this.Payment === null) {
        var html = '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">';
        html += '<div><i class="fas fa-times" style="color:#f27474"></i>  ' + "請選擇付款方式" + "</div>";
        Swal.fire({
          icon: 'error',
          html: html,
          showCloseButton: false,
          showConfirmButton: false,
          width: '300px',
          heightAuto: false
        });
      } else {
        var credential = {
          Num: this.Num,
          Payment: this.Payment
        };
        Object(_api__WEBPACK_IMPORTED_MODULE_0__["UpdatePayment"])(credential).then(function (res) {
          if (_this2.Payment === '2') {
            _this2.$router.push({
              name: 'ATM',
              params: {
                Num: _this2.Num
              }
            });
          }
        })["catch"](function (rep) {});
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/PaymentSelect.vue?vue&type=template&id=5995b9e7&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/PaymentSelect.vue?vue&type=template&id=5995b9e7&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row warp_p" }, [
    _c("div", { staticClass: "col-md-12" }, [
      _c("section", { staticClass: "card mb-5 pb-5" }, [
        _c("h5", { staticClass: "card-header text-center" }, [
          _vm._v("\n                訂單內容\n            ")
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "card-body" },
          [
            _vm._l(_vm.BookingData, function(ob, index) {
              return _c(
                "div",
                { key: index, staticClass: "row reservation-item pb-4" },
                [
                  _c("div", { staticClass: "col-md-9 reservation-item-info" }, [
                    _c(
                      "h3",
                      { staticClass: "reservation-item-info-title mb-3" },
                      [
                        _c(
                          "span",
                          {
                            staticClass: "reservation-item-info-title-roomname "
                          },
                          [
                            _vm._v(
                              _vm._s(ob.Room.Name) +
                                " | " +
                                _vm._s(ob.Promotion.Name)
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "row reservation-item-info-body" },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              "col-md-5 col-sm-6 col-12 reservation-item-info-body-photo"
                          },
                          [
                            _c("img", {
                              staticClass: "w-100 mw-100 mb-5",
                              attrs: { src: ob.Room.MainPicture }
                            })
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass:
                              "col-md-7 col-sm-6 col-12 pt-1 reservation-item-info-body-content"
                          },
                          [
                            _c(
                              "span",
                              { staticClass: "d-block font-w-600 b-b-sold-1" },
                              [
                                _vm._v(
                                  "\n                                    人數\n                                "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "span",
                              { staticClass: "d-block" },
                              [
                                ob.adults > 0
                                  ? [
                                      _vm._v(
                                        "\n                                        成人" +
                                          _vm._s(ob.adults) +
                                          "位 |\n                                    "
                                      )
                                    ]
                                  : _vm._e(),
                                _vm._v(" "),
                                ob.childrens > 0
                                  ? [
                                      _vm._v(
                                        "\n                                        小孩" +
                                          _vm._s(ob.childrens) +
                                          "位 |\n                                    "
                                      )
                                    ]
                                  : _vm._e(),
                                _vm._v(" "),
                                ob.babys > 0
                                  ? [
                                      _vm._v(
                                        "\n                                        兒童" +
                                          _vm._s(ob.babys) +
                                          "位\n                                    "
                                      )
                                    ]
                                  : _vm._e()
                              ],
                              2
                            ),
                            _vm._v(" "),
                            _c("br"),
                            _vm._v(" "),
                            _c(
                              "span",
                              { staticClass: "d-block font-w-600 b-b-sold-1" },
                              [
                                _vm._v(
                                  "\n                                    入住日期/退房日期\n                                "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("span", { staticClass: "d-block" }, [
                              _vm._v(
                                "\n                                   " +
                                  _vm._s(_vm.Day(ob.startDate)) +
                                  " " +
                                  _vm._s(_vm.WeekDay(ob.startDate)) +
                                  " ~ " +
                                  _vm._s(_vm.Day(ob.endDate)) +
                                  " " +
                                  _vm._s(_vm.WeekDay(ob.endDate)) +
                                  " (" +
                                  _vm._s(_vm.nights(ob.startDate, ob.endDate)) +
                                  "晚)\n                                "
                              )
                            ]),
                            _vm._v(" "),
                            _c("br"),
                            _vm._v(" "),
                            ob.Extra !== null
                              ? _c(
                                  "span",
                                  {
                                    staticClass: "d-block font-w-600 b-b-sold-1"
                                  },
                                  [
                                    _vm._v(
                                      "\n                                    基本必選項目\n                                "
                                    )
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            ob.Extra !== null
                              ? _c(
                                  "span",
                                  {
                                    staticClass:
                                      "d-block text-danger font-w-600 row"
                                  },
                                  [
                                    _c(
                                      "th",
                                      {
                                        staticClass:
                                          "text-center  col-xs-12 pl-0",
                                        attrs: { scope: "row" }
                                      },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "co-12 pl-0" },
                                          [
                                            _c(
                                              "label",
                                              {
                                                staticClass:
                                                  "radio font-w-500 m-0 pb-2"
                                              },
                                              [
                                                ob.ExtraPeople > 0
                                                  ? [
                                                      _vm._v(
                                                        "\n                                                   加備品 x " +
                                                          _vm._s(
                                                            _vm.nights(
                                                              ob.startDate,
                                                              ob.endDate
                                                            )
                                                          ) +
                                                          " 晚 x " +
                                                          _vm._s(
                                                            ob.ExtraPeople
                                                          ) +
                                                          " "
                                                      ),
                                                      ob.ExtraBed > 0
                                                        ? [_vm._v(" + ")]
                                                        : _vm._e()
                                                    ]
                                                  : _vm._e(),
                                                _vm._v(" "),
                                                ob.ExtraBed > 0
                                                  ? [
                                                      _vm._v(
                                                        "\n                                                   加床 x " +
                                                          _vm._s(
                                                            _vm.nights(
                                                              ob.startDate,
                                                              ob.endDate
                                                            )
                                                          ) +
                                                          " 晚 x " +
                                                          _vm._s(ob.ExtraBed) +
                                                          "\n                                               "
                                                      )
                                                    ]
                                                  : _vm._e()
                                              ],
                                              2
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _c("br"),
                            _vm._v(" "),
                            ob.ExtraShop.length > 0
                              ? _c(
                                  "span",
                                  {
                                    staticClass: "d-block font-w-600 b-b-sold-1"
                                  },
                                  [
                                    _vm._v(
                                      "\n                                    加購項目\n                                "
                                    )
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _vm._l(ob.ExtraShop, function(Shop) {
                              return ob.ExtraShop.length > 0
                                ? _c(
                                    "span",
                                    {
                                      staticClass:
                                        "d-block text-danger font-w-600"
                                    },
                                    [
                                      _vm._v(
                                        "\n                                    " +
                                          _vm._s(Shop.Name) +
                                          " x " +
                                          _vm._s(Shop.Nights) +
                                          " 晚 x " +
                                          _vm._s(Shop.Numbers) +
                                          "\n                                "
                                      )
                                    ]
                                  )
                                : _vm._e()
                            }),
                            _vm._v(" "),
                            _c("br"),
                            _vm._v(" "),
                            ob.ExtraCar.length > 0
                              ? _c(
                                  "span",
                                  {
                                    staticClass: "d-block font-w-600 b-b-sold-1"
                                  },
                                  [
                                    _vm._v(
                                      "\n                                    加購接駁\n                                "
                                    )
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _vm._l(ob.ExtraCar, function(Car) {
                              return ob.ExtraCar.length > 0
                                ? _c(
                                    "span",
                                    {
                                      staticClass:
                                        "d-block text-danger font-w-600"
                                    },
                                    [
                                      _vm._v(
                                        "\n                                    " +
                                          _vm._s(Car.Name) +
                                          " - " +
                                          _vm._s(
                                            Car.type == "1" ? "單趟" : "來回"
                                          ) +
                                          " x " +
                                          _vm._s(Car.Numbers) +
                                          "\n                                    "
                                      ),
                                      Car.arrvdt
                                        ? [
                                            _c("br"),
                                            _vm._v(
                                              "\n                                        抵達日期時間 : " +
                                                _vm._s(Car.arrvdt) +
                                                "\n                                    "
                                            )
                                          ]
                                        : _vm._e(),
                                      _vm._v(" "),
                                      Car.pickdt
                                        ? [
                                            _c("br"),
                                            _vm._v(
                                              "\n                                        出發日期時間 : " +
                                                _vm._s(Car.pickdt) +
                                                "\n                                    "
                                            )
                                          ]
                                        : _vm._e()
                                    ],
                                    2
                                  )
                                : _vm._e()
                            })
                          ],
                          2
                        )
                      ]
                    )
                  ])
                ]
              )
            }),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-12 DiscountBlock" }, [
                _c("div", { staticClass: "reservation-item-discount" }, [
                  _vm.DiscountCode !== undefined && _vm.DiscountCode.length > 0
                    ? _c(
                        "span",
                        {
                          staticClass:
                            "DiscountCodeMessage text-danger d-block mt-2",
                          attrs: { id: "DiscountCodeMessage" }
                        },
                        [
                          _vm._v(
                            _vm._s(_vm.DiscountCode.Name) +
                              " -NT$ " +
                              _vm._s(_vm.DiscountCode.Discount)
                          )
                        ]
                      )
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c(
                  "table",
                  {
                    staticClass:
                      "text-left d-flex flex-column justify-content-center reservation-item-total"
                  },
                  [
                    _c("tbody", [
                      _c("tr", [
                        _c("td", { staticClass: "price-title" }, [
                          _vm._v("結帳狀態")
                        ]),
                        _vm._v(" "),
                        _c("td", { staticClass: "price-content ft-currency" }, [
                          _vm._v(
                            _vm._s(
                              _vm.Refund
                                ? "以退款"
                                : _vm.Paid
                                ? "已付款"
                                : "尚未付款"
                            )
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("tr", [
                        _c("td", { staticClass: "price-title" }, [
                          _vm._v("訂房總額")
                        ]),
                        _vm._v(" "),
                        _c("td", { staticClass: "price-content ft-currency" }, [
                          _vm._v("NT$ " + _vm._s(_vm.Price))
                        ])
                      ]),
                      _vm._v(" "),
                      _c("tr", [
                        _c("td", { staticClass: "price-title" }, [
                          _vm._v("訂房編號")
                        ]),
                        _vm._v(" "),
                        _c("td", { staticClass: "price-content ft-currency" }, [
                          _c("input", {
                            attrs: { type: "hidden", id: "Num" },
                            domProps: { value: _vm.Num }
                          }),
                          _vm._v(
                            "\n                                    " +
                              _vm._s(_vm.Num) +
                              "\n                                    "
                          ),
                          _c(
                            "span",
                            {
                              staticStyle: { cursor: "pointer" },
                              on: { click: _vm.CopyNum }
                            },
                            [
                              _c("img", {
                                staticClass: "pl-2",
                                attrs: {
                                  src:
                                    "https://img.icons8.com/fluent-systems-regular/18/606060/copy.png"
                                }
                              })
                            ]
                          )
                        ])
                      ])
                    ])
                  ]
                )
              ])
            ])
          ],
          2
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "row pb-4",
            staticStyle: { padding: "20px", "background-color": "#fff" }
          },
          [
            _c("div", { staticClass: "col-md-12 col-sm-12 pt-4" }, [
              _c(
                "label",
                { staticClass: "ft-h5 d-block", attrs: { for: "RegisterNo" } },
                [_vm._v("統一編號")]
              ),
              _vm._v(" "),
              _c("input", {
                staticClass: "form-control",
                attrs: {
                  id: "RegisterNo",
                  name: "RegisterNo",
                  type: "text",
                  disabled: ""
                },
                domProps: { value: _vm.TaxNumber }
              }),
              _vm._v(" "),
              _c("small", { staticClass: "d-block pt-2" }, [
                _vm._v(
                  "因應財政部新版電子發票格式，即日起公司戶電子發票證明聯一律無需提供「抬頭」。電子發票上將不顯示抬頭欄位，紙本電子發票印有統編即可作為會計憑證。"
                )
              ]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c(
                "label",
                {
                  staticClass: "ft-h5 d-block",
                  staticStyle: { color: "#858a99" },
                  attrs: { for: "" }
                },
                [_vm._v("其它備註")]
              ),
              _vm._v(" "),
              _c("textarea", {
                staticClass: "form-control w-100 mt-0",
                attrs: { id: "", name: "", rows: "6", disabled: "" },
                domProps: { value: _vm.Note }
              })
            ])
          ]
        )
      ]),
      _vm._v(" "),
      _c("section", { staticClass: "card mt-4" }, [
        _c(
          "h4",
          {
            staticClass: "card-header text-center",
            staticStyle: { background: "#444" }
          },
          [_vm._v("訂房注意事項")]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "card-body pb-0", staticStyle: { border: "0" } },
          [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-12 pt-4 " }, [
                _c("div", { staticClass: "PrivacyTermsContent px-3 pb-5" }, [
                  _c("h5", { staticClass: "pt-3 pb-3" }, [
                    _vm._v("訂房注意事項")
                  ]),
                  _vm._v(" "),
                  _vm._m(0),
                  _vm._v(" "),
                  _c(
                    "h4",
                    {
                      staticClass: "card-header text-center my-5",
                      staticStyle: { background: "#444" }
                    },
                    [_vm._v("選擇付款方式")]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "text-center d-flex flex-wrap" }, [
                    _c(
                      "label",
                      {
                        staticClass: "radio font-w-500",
                        staticStyle: {
                          margin: "auto 0",
                          "border-right": "1px solid #ededed",
                          "padding-right": "20px",
                          "margin-right": "30px"
                        },
                        attrs: { for: "selectpay1" }
                      },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              "d-flex flex-column justify-content-center"
                          },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.Payment,
                                  expression: "Payment"
                                }
                              ],
                              attrs: {
                                id: "selectpay1",
                                value: "1",
                                disabled: "",
                                type: "radio"
                              },
                              domProps: { checked: _vm._q(_vm.Payment, "1") },
                              on: {
                                change: function($event) {
                                  _vm.Payment = "1"
                                }
                              }
                            }),
                            _c("span")
                          ]
                        ),
                        _vm._v(" "),
                        _vm._m(1)
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "radio font-w-500",
                        staticStyle: { margin: "auto 0" },
                        attrs: { for: "selectpay2" }
                      },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              " d-flex flex-column justify-content-center"
                          },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.Payment,
                                  expression: "Payment"
                                }
                              ],
                              attrs: {
                                id: "selectpay2",
                                value: "2",
                                type: "radio"
                              },
                              domProps: { checked: _vm._q(_vm.Payment, "2") },
                              on: {
                                change: function($event) {
                                  _vm.Payment = "2"
                                }
                              }
                            }),
                            _c("span")
                          ]
                        ),
                        _vm._v(" "),
                        _vm._m(2)
                      ]
                    )
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            !_vm.Paid && !_vm.Refund
              ? _c("div", { staticClass: "row" }, [
                  _c(
                    "div",
                    { staticClass: "col-md-12 pt-4 pb-4 text-center" },
                    [
                      _c("label", { staticClass: "AllowPrecautionsItem" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.AllowPrecautions,
                              expression: "AllowPrecautions"
                            }
                          ],
                          staticClass: "AllowPrecautions",
                          attrs: {
                            id: "AllowPrecautions",
                            name: "AllowPrecautions",
                            type: "checkbox"
                          },
                          domProps: {
                            checked: Array.isArray(_vm.AllowPrecautions)
                              ? _vm._i(_vm.AllowPrecautions, null) > -1
                              : _vm.AllowPrecautions
                          },
                          on: {
                            change: function($event) {
                              var $$a = _vm.AllowPrecautions,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = null,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    (_vm.AllowPrecautions = $$a.concat([$$v]))
                                } else {
                                  $$i > -1 &&
                                    (_vm.AllowPrecautions = $$a
                                      .slice(0, $$i)
                                      .concat($$a.slice($$i + 1)))
                                }
                              } else {
                                _vm.AllowPrecautions = $$c
                              }
                            }
                          }
                        }),
                        _vm._v(
                          "\n                            已詳閱並同意訂房注意事項條款\n                        "
                        )
                      ]),
                      _vm._v(" "),
                      _c("br"),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "button eb-btn ReservationSubmit",
                          attrs: { type: "button" },
                          on: { click: _vm.GoPay }
                        },
                        [_vm._v("前往付款")]
                      )
                    ]
                  )
                ])
              : _vm._e()
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-content" }, [
      _c("ul", [
        _c("li", [
          _vm._v(
            "原房價內已含早餐或其它項目，旅客因個人因素未使用時，均視同自動放棄，任何有關此類之退費要求，恕無法提供此項服務。"
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "行館所在地或旅客出發地，入住當日因天災（人力不可抗力因素）影響，如：陸上颱風警報發佈之警戒區域，或有主要交通中斷情形發生，致使無法如期入住時，敬請儘速與本行館連繫，更改或取消您的訂房紀錄。"
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "本行館依照交通部於99年1月13日函頒「觀光旅館業個別旅客直接訂房定型化契約範本」辦理。("
          ),
          _c(
            "span",
            {
              staticStyle: {
                display: "inline !important",
                float: "none",
                "background-color": "rgb(255, 255, 255)",
                color: "rgb(51, 51, 51)",
                "font-family": 'sans-serif,Arial,Verdana,"Trebuchet MS"',
                "font-size": "13px",
                "font-style": "normal",
                "font-variant": "normal",
                "font-weight": "400",
                "letter-spacing": "normal",
                orphans: "2",
                "text-align": "left",
                "text-decoration": "none",
                "text-indent": "0px",
                "text-transform": "none",
                "-webkit-text-stroke-width": "0px",
                "white-space": "normal",
                "word-spacing": "0px"
              }
            },
            [
              _vm._v(
                "詳細內容請至交通部觀光局行政資訊系統\\消保事項專區\\觀光旅館業\\定型化契約專區查詢。)"
              )
            ]
          ),
          _c("br"),
          _vm._v("\n                                        將收取"),
          _c("strong", [_vm._v("30%")]),
          _vm._v("之住宿費用作為訂金，取消訂房與退還訂金的相關規定如下 :"),
          _c("br"),
          _vm._v("\n                                         ")
        ]),
        _vm._v(" "),
        _c("li", [_vm._v("旅客住宿日當日取消訂房扣預付訂金金額100% 。")]),
        _vm._v(" "),
        _c("li", [
          _vm._v("旅客於住宿日前1日內取消訂房扣房價預付訂金金額80%。")
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v("旅客於住宿日前2-3日內取消訂房扣房價預付訂金金額70%。")
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v("旅客於住宿日前4-6日內取消訂房扣房價預付訂金金額60%。")
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v("旅客於住宿日前7-9日內取消訂房扣房價預付訂金金額50%。")
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v("旅客於住宿日前10-13日內取消訂房扣房價預付訂金金額30%。")
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v("旅客於住宿日前14日前(含14日)取消訂房扣房價預付訂金金額0%。")
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "因可歸責於酒店之事由致無法履行訂房契約者\n                                        "
          ),
          _c("ul", [
            _c("li", [
              _vm._v(
                "如酒店已收取訂金，應加倍返還之；其因酒店之故意所致者，旅客並得另外請求以訂金三倍計算之損害賠償。"
              )
            ]),
            _vm._v(" "),
            _c("li", [
              _vm._v(
                "如酒店未收取訂金，甲方得請求以約定房價一倍計算之損害賠償；其因酒店之故意所致者，旅客得請求以約定房價三倍計算之損害賠償。"
              )
            ]),
            _vm._v(" "),
            _c("li", [
              _vm._v("旅客證明受有前項所定以外之其他損害者，得併請求賠償之。")
            ]),
            _vm._v(" "),
            _c("li", [
              _vm._v("酒店與旅客有其他更有利於旅客之協議者，依其協議。")
            ])
          ])
        ]),
        _vm._v(" "),
        _c("li", [
          _vm._v(
            "因不可抗力或其他不可歸責於雙方當事人之事由，致酒店無法履行者，酒店應即無息返還旅客已支付之全部訂金及其他費用。"
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("img", {
        staticClass: "pl-3 pr-3",
        staticStyle: { width: "70px" },
        attrs: { src: "/images/card/visa.png" }
      }),
      _vm._v(" "),
      _c("img", {
        staticClass: "pl-3 pr-3",
        staticStyle: { width: "70px" },
        attrs: { src: "/images/card/master.png" }
      }),
      _vm._v(" "),
      _c("img", {
        staticClass: "pl-3 pr-3",
        staticStyle: { width: "70px" },
        attrs: { src: "/images/card/jcb.png" }
      }),
      _vm._v(" "),
      _c("img", {
        staticClass: "pl-3 pr-3",
        staticStyle: { width: "70px" },
        attrs: { src: "/images/card/union-pay.png" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("img", {
        staticClass: "pl-3 pr-3",
        staticStyle: { width: "70px" },
        attrs: { src: "/images/cash/esun.png" }
      })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/front/components/layouts/PaymentSelect.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/front/components/layouts/PaymentSelect.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PaymentSelect_vue_vue_type_template_id_5995b9e7_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PaymentSelect.vue?vue&type=template&id=5995b9e7&scoped=true& */ "./resources/js/front/components/layouts/PaymentSelect.vue?vue&type=template&id=5995b9e7&scoped=true&");
/* harmony import */ var _PaymentSelect_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PaymentSelect.vue?vue&type=script&lang=js& */ "./resources/js/front/components/layouts/PaymentSelect.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PaymentSelect_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PaymentSelect_vue_vue_type_template_id_5995b9e7_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PaymentSelect_vue_vue_type_template_id_5995b9e7_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "5995b9e7",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/front/components/layouts/PaymentSelect.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/front/components/layouts/PaymentSelect.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/PaymentSelect.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PaymentSelect_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PaymentSelect.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/PaymentSelect.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PaymentSelect_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/front/components/layouts/PaymentSelect.vue?vue&type=template&id=5995b9e7&scoped=true&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/PaymentSelect.vue?vue&type=template&id=5995b9e7&scoped=true& ***!
  \************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PaymentSelect_vue_vue_type_template_id_5995b9e7_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PaymentSelect.vue?vue&type=template&id=5995b9e7&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/PaymentSelect.vue?vue&type=template&id=5995b9e7&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PaymentSelect_vue_vue_type_template_id_5995b9e7_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PaymentSelect_vue_vue_type_template_id_5995b9e7_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);