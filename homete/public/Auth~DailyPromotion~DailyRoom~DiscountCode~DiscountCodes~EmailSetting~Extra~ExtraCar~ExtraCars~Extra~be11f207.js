(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Auth~DailyPromotion~DailyRoom~DiscountCode~DiscountCodes~EmailSetting~Extra~ExtraCar~ExtraCars~Extra~be11f207"],{

/***/ "./resources/js/backend/api.js":
/*!*************************************!*\
  !*** ./resources/js/backend/api.js ***!
  \*************************************/
/*! exports provided: GetConfig, GetSetting, LoginJwt, GetRooms, GetRoom, GetRoomExtra, UpdateRoomExtra, CreateRoom, UpdateRooms, UpdateRoom, DeleteRoom, GetDailyRoom, UpdateOrCreateDailyRoom, UpdateOrCreateScopeDailyRoom, GetPromotions, GetPromotionFilters, GetPromotion, UpdatePromotions, UpdatePromotion, DeletePromotion, CreatePromotion, GetTags, CreateTag, DeleteTag, UpdateTags, GetDailyPromotion, UpdateOrCreateDailyPromotion, UpdateOrCreateScopeDailyPromotion, GetRemainRoom, Init, Calc, StoreOrder, GetRules, UpdateRules, GetExtraShop, GetExtraShops, UpdateExtraShops, UpdateExtraShop, CreateExtraShop, DeleteExtraShop, GetExtraCar, GetExtraCars, UpdateExtraCars, UpdateExtraCar, CreateExtraCar, DeleteExtraCar, GetLicenses, GetDiscountCodes, GetDiscountCode, UpdateDiscountCodes, UpdateDiscountCode, DeleteDiscountCode, CreateDiscountCode, GetMembers, UpdateMembers, GetMember, UpdateMember, DeleteMember, GetOrders, GetOrder, UpdateOrder, GetMailSetting, UpdateMailSetting, ValidMailSetting, GetSystem, UpdateSystem, UpdateProfile, GetRoles, CreateUser, UpdateUserState, DeleteUser, GetUser, UpdateUser, GetPermissions, GetHasPermissions, UpdateHasPermissions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetConfig", function() { return GetConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetSetting", function() { return GetSetting; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginJwt", function() { return LoginJwt; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetRooms", function() { return GetRooms; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetRoom", function() { return GetRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetRoomExtra", function() { return GetRoomExtra; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateRoomExtra", function() { return UpdateRoomExtra; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateRoom", function() { return CreateRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateRooms", function() { return UpdateRooms; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateRoom", function() { return UpdateRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteRoom", function() { return DeleteRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetDailyRoom", function() { return GetDailyRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateOrCreateDailyRoom", function() { return UpdateOrCreateDailyRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateOrCreateScopeDailyRoom", function() { return UpdateOrCreateScopeDailyRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetPromotions", function() { return GetPromotions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetPromotionFilters", function() { return GetPromotionFilters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetPromotion", function() { return GetPromotion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatePromotions", function() { return UpdatePromotions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatePromotion", function() { return UpdatePromotion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeletePromotion", function() { return DeletePromotion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreatePromotion", function() { return CreatePromotion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetTags", function() { return GetTags; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateTag", function() { return CreateTag; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteTag", function() { return DeleteTag; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateTags", function() { return UpdateTags; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetDailyPromotion", function() { return GetDailyPromotion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateOrCreateDailyPromotion", function() { return UpdateOrCreateDailyPromotion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateOrCreateScopeDailyPromotion", function() { return UpdateOrCreateScopeDailyPromotion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetRemainRoom", function() { return GetRemainRoom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Init", function() { return Init; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Calc", function() { return Calc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoreOrder", function() { return StoreOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetRules", function() { return GetRules; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateRules", function() { return UpdateRules; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetExtraShop", function() { return GetExtraShop; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetExtraShops", function() { return GetExtraShops; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateExtraShops", function() { return UpdateExtraShops; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateExtraShop", function() { return UpdateExtraShop; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateExtraShop", function() { return CreateExtraShop; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteExtraShop", function() { return DeleteExtraShop; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetExtraCar", function() { return GetExtraCar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetExtraCars", function() { return GetExtraCars; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateExtraCars", function() { return UpdateExtraCars; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateExtraCar", function() { return UpdateExtraCar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateExtraCar", function() { return CreateExtraCar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteExtraCar", function() { return DeleteExtraCar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetLicenses", function() { return GetLicenses; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetDiscountCodes", function() { return GetDiscountCodes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetDiscountCode", function() { return GetDiscountCode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateDiscountCodes", function() { return UpdateDiscountCodes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateDiscountCode", function() { return UpdateDiscountCode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteDiscountCode", function() { return DeleteDiscountCode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateDiscountCode", function() { return CreateDiscountCode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetMembers", function() { return GetMembers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateMembers", function() { return UpdateMembers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetMember", function() { return GetMember; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateMember", function() { return UpdateMember; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteMember", function() { return DeleteMember; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrders", function() { return GetOrders; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetOrder", function() { return GetOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateOrder", function() { return UpdateOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetMailSetting", function() { return GetMailSetting; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateMailSetting", function() { return UpdateMailSetting; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidMailSetting", function() { return ValidMailSetting; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetSystem", function() { return GetSystem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateSystem", function() { return UpdateSystem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProfile", function() { return UpdateProfile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetRoles", function() { return GetRoles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateUser", function() { return CreateUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateUserState", function() { return UpdateUserState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteUser", function() { return DeleteUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetUser", function() { return GetUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateUser", function() { return UpdateUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetPermissions", function() { return GetPermissions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetHasPermissions", function() { return GetHasPermissions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateHasPermissions", function() { return UpdateHasPermissions; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _storage_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./storage.js */ "./resources/js/backend/storage.js");
/* harmony import */ var _router_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./router.js */ "./resources/js/backend/router.js");


var _this = undefined;

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



window.axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
axios.interceptors.response.use(function (response) {
  //如果過期 會refresh token
  var token = response.headers.authorization;

  if (token) {
    // 如果 header 中存在 token，那么触发 refreshToken 方法，替换本地的 token
    _this.$store.dispatch('Refresh', token);
  }

  return response;
}, function (error) {
  if (error.response.status == 401) {
    // router.push('/login');
    "Swal.fire({\n            title:'\u63D0\u793A',\n            icon:'error',\n            text:'\u6642\u6548\u904E\u671F\uFF0C\u8ACB\u91CD\u65B0\u767B\u5165',\n            showCloseButton: true,\n            confirmButtonColor: '#ba0000',\n            confirmButtonText: '\u91CD\u65B0\u767B\u5165',\n        }).then((result) => {\n            store.commit('Logout');\n            router.push({name: 'login'});\n        })";
  } else if (error.response.status == 403) {
    error.response.data.errors = {
      錯誤: ['權限不足，請洽管理員']
    };
  }

  return Promise.reject(error);
});

if (_storage_js__WEBPACK_IMPORTED_MODULE_1__["default"].getters.currentUser) {
  axios.defaults.headers.common["Authorization"] = "Bearer ".concat(_storage_js__WEBPACK_IMPORTED_MODULE_1__["default"].getters.currentUser.token);
} // Config相關的 api


var ConfigRequest = '/api/Config'; // Setting相關的 api

var SettingRequest = '/api/Setting'; // Jwt 相關的 api

var JwtRequest = '/api/Jwt'; // Room 相關的 api

var RoomRequest = '/api/Room'; // DailyRoom 相關的 api

var DailyRoomRequest = '/api/DailyRoom'; // Promotion 相關的 api

var PromotionRequest = '/api/Promotion'; // Tag 相關的 api

var TagRequest = '/api/Tag'; //DailyPromotion 相關的 api

var DailyPromotionRequest = '/api/DailyPromotion'; //Remain 相關的 api

var RemainRequest = '/api/Remain'; //Rulte 相關的 api

var RuleRequest = '/api/Rule'; //ExtraShop 相關的 api

var ExtraShopRequest = '/api/ExtraShop'; //ExtraCar 相關的 api

var ExtraCarRequest = '/api/ExtraCar'; //DiscountCodeRequest 相關的 api

var DiscountCodeRequest = '/api/DiscountCode'; //Member 管理

var MembersRequest = '/api/Members'; //Order 管理

var OrdersRequest = '/api/Orders'; //Mail

var MailSettingRequest = '/api/MailSetting'; //System

var SystemRequest = '/api/System'; //Role

var RoleRequest = '/api/Role';
var UserRequest = '/api/User';
var PermissionRequest = '/api/Permission'; // 傳入 name 取得 Config

function GetConfig(name) {
  return new Promise(function (res, rej) {
    axios.get(ConfigRequest + "/" + name).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
} // 傳入 group 取得 Setting

function GetSetting(group) {
  var name = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return new Promise(function (res, rej) {
    axios.get(SettingRequest + "/" + group + "/" + name).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
} // 傳入 name 取得 Config

function LoginJwt(credential) {
  return new Promise(function (res, rej) {
    axios.post(JwtRequest + "/login", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function GetRooms() {
  return new Promise(function (res, rej) {
    axios.get(RoomRequest).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function GetRoom(_x) {
  return _GetRoom.apply(this, arguments);
}

function _GetRoom() {
  _GetRoom = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", new Promise(function (res, rej) {
              axios.get(RoomRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _GetRoom.apply(this, arguments);
}

function GetRoomExtra(_x2) {
  return _GetRoomExtra.apply(this, arguments);
}

function _GetRoomExtra() {
  _GetRoomExtra = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            return _context2.abrupt("return", new Promise(function (res, rej) {
              axios.get(RoomRequest + "/Extra", {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _GetRoomExtra.apply(this, arguments);
}

function UpdateRoomExtra(_x3) {
  return _UpdateRoomExtra.apply(this, arguments);
}

function _UpdateRoomExtra() {
  _UpdateRoomExtra = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            return _context3.abrupt("return", new Promise(function (res, rej) {
              axios.post(RoomRequest + "/Extra", credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _UpdateRoomExtra.apply(this, arguments);
}

function CreateRoom(_x4) {
  return _CreateRoom.apply(this, arguments);
}

function _CreateRoom() {
  _CreateRoom = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            return _context4.abrupt("return", new Promise(function (res, rej) {
              axios.post(RoomRequest + '/Create', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _CreateRoom.apply(this, arguments);
}

function UpdateRooms(_x5) {
  return _UpdateRooms.apply(this, arguments);
}

function _UpdateRooms() {
  _UpdateRooms = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            return _context5.abrupt("return", new Promise(function (res, rej) {
              axios.post(RoomRequest + '/UpdateRooms', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));
  return _UpdateRooms.apply(this, arguments);
}

function UpdateRoom(_x6) {
  return _UpdateRoom.apply(this, arguments);
}

function _UpdateRoom() {
  _UpdateRoom = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            return _context6.abrupt("return", new Promise(function (res, rej) {
              axios.post(RoomRequest + '/Update', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));
  return _UpdateRoom.apply(this, arguments);
}

function DeleteRoom(_x7) {
  return _DeleteRoom.apply(this, arguments);
}

function _DeleteRoom() {
  _DeleteRoom = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            return _context7.abrupt("return", new Promise(function (res, rej) {
              axios["delete"](RoomRequest + '/Delete', {
                data: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));
  return _DeleteRoom.apply(this, arguments);
}

function GetDailyRoom(_x8) {
  return _GetDailyRoom.apply(this, arguments);
}

function _GetDailyRoom() {
  _GetDailyRoom = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            return _context8.abrupt("return", new Promise(function (res, rej) {
              axios.get(DailyRoomRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));
  return _GetDailyRoom.apply(this, arguments);
}

function UpdateOrCreateDailyRoom(_x9) {
  return _UpdateOrCreateDailyRoom.apply(this, arguments);
}

function _UpdateOrCreateDailyRoom() {
  _UpdateOrCreateDailyRoom = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            return _context9.abrupt("return", new Promise(function (res, rej) {
              axios.post(DailyRoomRequest + '/UpdateOrCreate', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));
  return _UpdateOrCreateDailyRoom.apply(this, arguments);
}

function UpdateOrCreateScopeDailyRoom(_x10) {
  return _UpdateOrCreateScopeDailyRoom.apply(this, arguments);
}

function _UpdateOrCreateScopeDailyRoom() {
  _UpdateOrCreateScopeDailyRoom = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee10(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            return _context10.abrupt("return", new Promise(function (res, rej) {
              axios.post(DailyRoomRequest + '/UpdateOrCreateScope', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10);
  }));
  return _UpdateOrCreateScopeDailyRoom.apply(this, arguments);
}

function GetPromotions(_x11) {
  return _GetPromotions.apply(this, arguments);
}

function _GetPromotions() {
  _GetPromotions = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee11(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            return _context11.abrupt("return", new Promise(function (res, rej) {
              axios.get(PromotionRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11);
  }));
  return _GetPromotions.apply(this, arguments);
}

function GetPromotionFilters() {
  return _GetPromotionFilters.apply(this, arguments);
}

function _GetPromotionFilters() {
  _GetPromotionFilters = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee12() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            return _context12.abrupt("return", new Promise(function (res, rej) {
              axios.get(PromotionRequest + '/filters').then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12);
  }));
  return _GetPromotionFilters.apply(this, arguments);
}

function GetPromotion(_x12) {
  return _GetPromotion.apply(this, arguments);
}

function _GetPromotion() {
  _GetPromotion = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee13(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            return _context13.abrupt("return", new Promise(function (res, rej) {
              axios.get(PromotionRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee13);
  }));
  return _GetPromotion.apply(this, arguments);
}

function UpdatePromotions(_x13) {
  return _UpdatePromotions.apply(this, arguments);
}

function _UpdatePromotions() {
  _UpdatePromotions = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee14(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee14$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            return _context14.abrupt("return", new Promise(function (res, rej) {
              axios.post(PromotionRequest + '/UpdatePromotions', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context14.stop();
        }
      }
    }, _callee14);
  }));
  return _UpdatePromotions.apply(this, arguments);
}

function UpdatePromotion(_x14) {
  return _UpdatePromotion.apply(this, arguments);
}

function _UpdatePromotion() {
  _UpdatePromotion = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee15(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee15$(_context15) {
      while (1) {
        switch (_context15.prev = _context15.next) {
          case 0:
            return _context15.abrupt("return", new Promise(function (res, rej) {
              axios.post(PromotionRequest + '/Update', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context15.stop();
        }
      }
    }, _callee15);
  }));
  return _UpdatePromotion.apply(this, arguments);
}

function DeletePromotion(_x15) {
  return _DeletePromotion.apply(this, arguments);
}

function _DeletePromotion() {
  _DeletePromotion = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee16(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee16$(_context16) {
      while (1) {
        switch (_context16.prev = _context16.next) {
          case 0:
            return _context16.abrupt("return", new Promise(function (res, rej) {
              axios["delete"](PromotionRequest + '/Delete', {
                data: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context16.stop();
        }
      }
    }, _callee16);
  }));
  return _DeletePromotion.apply(this, arguments);
}

function CreatePromotion(_x16) {
  return _CreatePromotion.apply(this, arguments);
}

function _CreatePromotion() {
  _CreatePromotion = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee17(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee17$(_context17) {
      while (1) {
        switch (_context17.prev = _context17.next) {
          case 0:
            return _context17.abrupt("return", new Promise(function (res, rej) {
              axios.post(PromotionRequest + '/Create', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context17.stop();
        }
      }
    }, _callee17);
  }));
  return _CreatePromotion.apply(this, arguments);
}

function GetTags(_x17) {
  return _GetTags.apply(this, arguments);
}

function _GetTags() {
  _GetTags = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee18(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee18$(_context18) {
      while (1) {
        switch (_context18.prev = _context18.next) {
          case 0:
            return _context18.abrupt("return", new Promise(function (res, rej) {
              axios.get(TagRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context18.stop();
        }
      }
    }, _callee18);
  }));
  return _GetTags.apply(this, arguments);
}

function CreateTag(_x18) {
  return _CreateTag.apply(this, arguments);
}

function _CreateTag() {
  _CreateTag = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee19(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee19$(_context19) {
      while (1) {
        switch (_context19.prev = _context19.next) {
          case 0:
            return _context19.abrupt("return", new Promise(function (res, rej) {
              axios.post(TagRequest + '/Create', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context19.stop();
        }
      }
    }, _callee19);
  }));
  return _CreateTag.apply(this, arguments);
}

function DeleteTag(_x19) {
  return _DeleteTag.apply(this, arguments);
}

function _DeleteTag() {
  _DeleteTag = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee20(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee20$(_context20) {
      while (1) {
        switch (_context20.prev = _context20.next) {
          case 0:
            return _context20.abrupt("return", new Promise(function (res, rej) {
              axios["delete"](TagRequest + '/Delete', {
                data: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context20.stop();
        }
      }
    }, _callee20);
  }));
  return _DeleteTag.apply(this, arguments);
}

function UpdateTags(_x20) {
  return _UpdateTags.apply(this, arguments);
}

function _UpdateTags() {
  _UpdateTags = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee21(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee21$(_context21) {
      while (1) {
        switch (_context21.prev = _context21.next) {
          case 0:
            return _context21.abrupt("return", new Promise(function (res, rej) {
              axios.post(TagRequest + '/UpdateTags', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context21.stop();
        }
      }
    }, _callee21);
  }));
  return _UpdateTags.apply(this, arguments);
}

function GetDailyPromotion(_x21) {
  return _GetDailyPromotion.apply(this, arguments);
}

function _GetDailyPromotion() {
  _GetDailyPromotion = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee22(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee22$(_context22) {
      while (1) {
        switch (_context22.prev = _context22.next) {
          case 0:
            return _context22.abrupt("return", new Promise(function (res, rej) {
              axios.get(DailyPromotionRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context22.stop();
        }
      }
    }, _callee22);
  }));
  return _GetDailyPromotion.apply(this, arguments);
}

function UpdateOrCreateDailyPromotion(_x22) {
  return _UpdateOrCreateDailyPromotion.apply(this, arguments);
}

function _UpdateOrCreateDailyPromotion() {
  _UpdateOrCreateDailyPromotion = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee23(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee23$(_context23) {
      while (1) {
        switch (_context23.prev = _context23.next) {
          case 0:
            return _context23.abrupt("return", new Promise(function (res, rej) {
              axios.post(DailyPromotionRequest + '/UpdateOrCreate', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context23.stop();
        }
      }
    }, _callee23);
  }));
  return _UpdateOrCreateDailyPromotion.apply(this, arguments);
}

function UpdateOrCreateScopeDailyPromotion(_x23) {
  return _UpdateOrCreateScopeDailyPromotion.apply(this, arguments);
}

function _UpdateOrCreateScopeDailyPromotion() {
  _UpdateOrCreateScopeDailyPromotion = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee24(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee24$(_context24) {
      while (1) {
        switch (_context24.prev = _context24.next) {
          case 0:
            return _context24.abrupt("return", new Promise(function (res, rej) {
              axios.post(DailyPromotionRequest + '/UpdateOrCreateScope', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context24.stop();
        }
      }
    }, _callee24);
  }));
  return _UpdateOrCreateScopeDailyPromotion.apply(this, arguments);
}

function GetRemainRoom(_x24) {
  return _GetRemainRoom.apply(this, arguments);
}

function _GetRemainRoom() {
  _GetRemainRoom = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee25(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee25$(_context25) {
      while (1) {
        switch (_context25.prev = _context25.next) {
          case 0:
            return _context25.abrupt("return", new Promise(function (res, rej) {
              axios.get(RemainRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context25.stop();
        }
      }
    }, _callee25);
  }));
  return _GetRemainRoom.apply(this, arguments);
}

function Init() {
  return _Init.apply(this, arguments);
}

function _Init() {
  _Init = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee26() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee26$(_context26) {
      while (1) {
        switch (_context26.prev = _context26.next) {
          case 0:
            return _context26.abrupt("return", new Promise(function (res, rej) {
              axios.get('/api/init').then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context26.stop();
        }
      }
    }, _callee26);
  }));
  return _Init.apply(this, arguments);
}

function Calc(_x25) {
  return _Calc.apply(this, arguments);
}

function _Calc() {
  _Calc = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee27(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee27$(_context27) {
      while (1) {
        switch (_context27.prev = _context27.next) {
          case 0:
            return _context27.abrupt("return", new Promise(function (res, rej) {
              axios.post('/api/calc', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context27.stop();
        }
      }
    }, _callee27);
  }));
  return _Calc.apply(this, arguments);
}

function StoreOrder(credential) {
  return new Promise(function (res, rej) {
    axios.post('/api/order/store', credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function GetRules() {
  return _GetRules.apply(this, arguments);
}

function _GetRules() {
  _GetRules = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee28() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee28$(_context28) {
      while (1) {
        switch (_context28.prev = _context28.next) {
          case 0:
            return _context28.abrupt("return", new Promise(function (res, rej) {
              axios.get(RuleRequest).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context28.stop();
        }
      }
    }, _callee28);
  }));
  return _GetRules.apply(this, arguments);
}

function UpdateRules(_x26) {
  return _UpdateRules.apply(this, arguments);
}

function _UpdateRules() {
  _UpdateRules = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee29(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee29$(_context29) {
      while (1) {
        switch (_context29.prev = _context29.next) {
          case 0:
            return _context29.abrupt("return", new Promise(function (res, rej) {
              axios.post(RuleRequest, credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context29.stop();
        }
      }
    }, _callee29);
  }));
  return _UpdateRules.apply(this, arguments);
}

function GetExtraShop(_x27) {
  return _GetExtraShop.apply(this, arguments);
}

function _GetExtraShop() {
  _GetExtraShop = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee30(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee30$(_context30) {
      while (1) {
        switch (_context30.prev = _context30.next) {
          case 0:
            return _context30.abrupt("return", new Promise(function (res, rej) {
              axios.get(ExtraShopRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context30.stop();
        }
      }
    }, _callee30);
  }));
  return _GetExtraShop.apply(this, arguments);
}

function GetExtraShops(_x28) {
  return _GetExtraShops.apply(this, arguments);
}

function _GetExtraShops() {
  _GetExtraShops = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee31(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee31$(_context31) {
      while (1) {
        switch (_context31.prev = _context31.next) {
          case 0:
            return _context31.abrupt("return", new Promise(function (res, rej) {
              axios.get(ExtraShopRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context31.stop();
        }
      }
    }, _callee31);
  }));
  return _GetExtraShops.apply(this, arguments);
}

function UpdateExtraShops(_x29) {
  return _UpdateExtraShops.apply(this, arguments);
}

function _UpdateExtraShops() {
  _UpdateExtraShops = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee32(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee32$(_context32) {
      while (1) {
        switch (_context32.prev = _context32.next) {
          case 0:
            return _context32.abrupt("return", new Promise(function (res, rej) {
              axios.post(ExtraShopRequest + '/UpdateExtraShops', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context32.stop();
        }
      }
    }, _callee32);
  }));
  return _UpdateExtraShops.apply(this, arguments);
}

function UpdateExtraShop(_x30) {
  return _UpdateExtraShop.apply(this, arguments);
}

function _UpdateExtraShop() {
  _UpdateExtraShop = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee33(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee33$(_context33) {
      while (1) {
        switch (_context33.prev = _context33.next) {
          case 0:
            return _context33.abrupt("return", new Promise(function (res, rej) {
              axios.post(ExtraShopRequest + '/Update', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context33.stop();
        }
      }
    }, _callee33);
  }));
  return _UpdateExtraShop.apply(this, arguments);
}

function CreateExtraShop(_x31) {
  return _CreateExtraShop.apply(this, arguments);
}

function _CreateExtraShop() {
  _CreateExtraShop = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee34(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee34$(_context34) {
      while (1) {
        switch (_context34.prev = _context34.next) {
          case 0:
            return _context34.abrupt("return", new Promise(function (res, rej) {
              axios.post(ExtraShopRequest + '/Create', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context34.stop();
        }
      }
    }, _callee34);
  }));
  return _CreateExtraShop.apply(this, arguments);
}

function DeleteExtraShop(_x32) {
  return _DeleteExtraShop.apply(this, arguments);
}

function _DeleteExtraShop() {
  _DeleteExtraShop = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee35(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee35$(_context35) {
      while (1) {
        switch (_context35.prev = _context35.next) {
          case 0:
            return _context35.abrupt("return", new Promise(function (res, rej) {
              axios["delete"](ExtraShopRequest + '/Delete', {
                data: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context35.stop();
        }
      }
    }, _callee35);
  }));
  return _DeleteExtraShop.apply(this, arguments);
}

function GetExtraCar(_x33) {
  return _GetExtraCar.apply(this, arguments);
}

function _GetExtraCar() {
  _GetExtraCar = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee36(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee36$(_context36) {
      while (1) {
        switch (_context36.prev = _context36.next) {
          case 0:
            return _context36.abrupt("return", new Promise(function (res, rej) {
              axios.get(ExtraCarRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context36.stop();
        }
      }
    }, _callee36);
  }));
  return _GetExtraCar.apply(this, arguments);
}

function GetExtraCars(_x34) {
  return _GetExtraCars.apply(this, arguments);
}

function _GetExtraCars() {
  _GetExtraCars = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee37(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee37$(_context37) {
      while (1) {
        switch (_context37.prev = _context37.next) {
          case 0:
            return _context37.abrupt("return", new Promise(function (res, rej) {
              axios.get(ExtraCarRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context37.stop();
        }
      }
    }, _callee37);
  }));
  return _GetExtraCars.apply(this, arguments);
}

function UpdateExtraCars(_x35) {
  return _UpdateExtraCars.apply(this, arguments);
}

function _UpdateExtraCars() {
  _UpdateExtraCars = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee38(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee38$(_context38) {
      while (1) {
        switch (_context38.prev = _context38.next) {
          case 0:
            return _context38.abrupt("return", new Promise(function (res, rej) {
              axios.post(ExtraCarRequest + '/UpdateExtraCars', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context38.stop();
        }
      }
    }, _callee38);
  }));
  return _UpdateExtraCars.apply(this, arguments);
}

function UpdateExtraCar(_x36) {
  return _UpdateExtraCar.apply(this, arguments);
}

function _UpdateExtraCar() {
  _UpdateExtraCar = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee39(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee39$(_context39) {
      while (1) {
        switch (_context39.prev = _context39.next) {
          case 0:
            return _context39.abrupt("return", new Promise(function (res, rej) {
              axios.post(ExtraCarRequest + '/Update', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context39.stop();
        }
      }
    }, _callee39);
  }));
  return _UpdateExtraCar.apply(this, arguments);
}

function CreateExtraCar(_x37) {
  return _CreateExtraCar.apply(this, arguments);
}

function _CreateExtraCar() {
  _CreateExtraCar = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee40(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee40$(_context40) {
      while (1) {
        switch (_context40.prev = _context40.next) {
          case 0:
            return _context40.abrupt("return", new Promise(function (res, rej) {
              axios.post(ExtraCarRequest + '/Create', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context40.stop();
        }
      }
    }, _callee40);
  }));
  return _CreateExtraCar.apply(this, arguments);
}

function DeleteExtraCar(_x38) {
  return _DeleteExtraCar.apply(this, arguments);
}

function _DeleteExtraCar() {
  _DeleteExtraCar = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee41(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee41$(_context41) {
      while (1) {
        switch (_context41.prev = _context41.next) {
          case 0:
            return _context41.abrupt("return", new Promise(function (res, rej) {
              axios["delete"](ExtraCarRequest + '/Delete', {
                data: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context41.stop();
        }
      }
    }, _callee41);
  }));
  return _DeleteExtraCar.apply(this, arguments);
}

function GetLicenses() {
  return _GetLicenses.apply(this, arguments);
}

function _GetLicenses() {
  _GetLicenses = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee42() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee42$(_context42) {
      while (1) {
        switch (_context42.prev = _context42.next) {
          case 0:
            return _context42.abrupt("return", new Promise(function (res, rej) {
              axios.get('/api/Licenses').then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context42.stop();
        }
      }
    }, _callee42);
  }));
  return _GetLicenses.apply(this, arguments);
}

function GetDiscountCodes(_x39) {
  return _GetDiscountCodes.apply(this, arguments);
}

function _GetDiscountCodes() {
  _GetDiscountCodes = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee43(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee43$(_context43) {
      while (1) {
        switch (_context43.prev = _context43.next) {
          case 0:
            return _context43.abrupt("return", new Promise(function (res, rej) {
              axios.get(DiscountCodeRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context43.stop();
        }
      }
    }, _callee43);
  }));
  return _GetDiscountCodes.apply(this, arguments);
}

function GetDiscountCode(_x40) {
  return _GetDiscountCode.apply(this, arguments);
}

function _GetDiscountCode() {
  _GetDiscountCode = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee44(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee44$(_context44) {
      while (1) {
        switch (_context44.prev = _context44.next) {
          case 0:
            return _context44.abrupt("return", new Promise(function (res, rej) {
              axios.get(DiscountCodeRequest, {
                params: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context44.stop();
        }
      }
    }, _callee44);
  }));
  return _GetDiscountCode.apply(this, arguments);
}

function UpdateDiscountCodes(_x41) {
  return _UpdateDiscountCodes.apply(this, arguments);
}

function _UpdateDiscountCodes() {
  _UpdateDiscountCodes = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee45(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee45$(_context45) {
      while (1) {
        switch (_context45.prev = _context45.next) {
          case 0:
            return _context45.abrupt("return", new Promise(function (res, rej) {
              axios.post(DiscountCodeRequest + '/UpdateDiscountCodes', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context45.stop();
        }
      }
    }, _callee45);
  }));
  return _UpdateDiscountCodes.apply(this, arguments);
}

function UpdateDiscountCode(_x42) {
  return _UpdateDiscountCode.apply(this, arguments);
}

function _UpdateDiscountCode() {
  _UpdateDiscountCode = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee46(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee46$(_context46) {
      while (1) {
        switch (_context46.prev = _context46.next) {
          case 0:
            return _context46.abrupt("return", new Promise(function (res, rej) {
              axios.post(DiscountCodeRequest + '/Update', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context46.stop();
        }
      }
    }, _callee46);
  }));
  return _UpdateDiscountCode.apply(this, arguments);
}

function DeleteDiscountCode(_x43) {
  return _DeleteDiscountCode.apply(this, arguments);
}

function _DeleteDiscountCode() {
  _DeleteDiscountCode = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee47(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee47$(_context47) {
      while (1) {
        switch (_context47.prev = _context47.next) {
          case 0:
            return _context47.abrupt("return", new Promise(function (res, rej) {
              axios["delete"](DiscountCodeRequest + '/Delete', {
                data: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context47.stop();
        }
      }
    }, _callee47);
  }));
  return _DeleteDiscountCode.apply(this, arguments);
}

function CreateDiscountCode(_x44) {
  return _CreateDiscountCode.apply(this, arguments);
}

function _CreateDiscountCode() {
  _CreateDiscountCode = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee48(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee48$(_context48) {
      while (1) {
        switch (_context48.prev = _context48.next) {
          case 0:
            return _context48.abrupt("return", new Promise(function (res, rej) {
              axios.post(DiscountCodeRequest + '/Create', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context48.stop();
        }
      }
    }, _callee48);
  }));
  return _CreateDiscountCode.apply(this, arguments);
}

function GetMembers(_x45) {
  return _GetMembers.apply(this, arguments);
}

function _GetMembers() {
  _GetMembers = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee49(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee49$(_context49) {
      while (1) {
        switch (_context49.prev = _context49.next) {
          case 0:
            return _context49.abrupt("return", new Promise(function (res, rej) {
              axios.post(MembersRequest, credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context49.stop();
        }
      }
    }, _callee49);
  }));
  return _GetMembers.apply(this, arguments);
}

function UpdateMembers(_x46) {
  return _UpdateMembers.apply(this, arguments);
}

function _UpdateMembers() {
  _UpdateMembers = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee50(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee50$(_context50) {
      while (1) {
        switch (_context50.prev = _context50.next) {
          case 0:
            return _context50.abrupt("return", new Promise(function (res, rej) {
              axios.post(MembersRequest + '/updateMembers', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context50.stop();
        }
      }
    }, _callee50);
  }));
  return _UpdateMembers.apply(this, arguments);
}

function GetMember(_x47) {
  return _GetMember.apply(this, arguments);
}

function _GetMember() {
  _GetMember = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee51(id) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee51$(_context51) {
      while (1) {
        switch (_context51.prev = _context51.next) {
          case 0:
            return _context51.abrupt("return", new Promise(function (res, rej) {
              axios.post(MembersRequest + '/' + id).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context51.stop();
        }
      }
    }, _callee51);
  }));
  return _GetMember.apply(this, arguments);
}

function UpdateMember(_x48) {
  return _UpdateMember.apply(this, arguments);
}

function _UpdateMember() {
  _UpdateMember = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee52(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee52$(_context52) {
      while (1) {
        switch (_context52.prev = _context52.next) {
          case 0:
            return _context52.abrupt("return", new Promise(function (res, rej) {
              axios.post(MembersRequest + '/update', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context52.stop();
        }
      }
    }, _callee52);
  }));
  return _UpdateMember.apply(this, arguments);
}

function DeleteMember(_x49) {
  return _DeleteMember.apply(this, arguments);
}

function _DeleteMember() {
  _DeleteMember = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee53(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee53$(_context53) {
      while (1) {
        switch (_context53.prev = _context53.next) {
          case 0:
            return _context53.abrupt("return", new Promise(function (res, rej) {
              axios["delete"](MembersRequest + '/Delete', {
                data: credential
              }).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context53.stop();
        }
      }
    }, _callee53);
  }));
  return _DeleteMember.apply(this, arguments);
}

function GetOrders(_x50) {
  return _GetOrders.apply(this, arguments);
}

function _GetOrders() {
  _GetOrders = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee54(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee54$(_context54) {
      while (1) {
        switch (_context54.prev = _context54.next) {
          case 0:
            return _context54.abrupt("return", new Promise(function (res, rej) {
              axios.post(OrdersRequest, credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context54.stop();
        }
      }
    }, _callee54);
  }));
  return _GetOrders.apply(this, arguments);
}

function GetOrder(_x51) {
  return _GetOrder.apply(this, arguments);
}

function _GetOrder() {
  _GetOrder = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee55(Num) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee55$(_context55) {
      while (1) {
        switch (_context55.prev = _context55.next) {
          case 0:
            return _context55.abrupt("return", new Promise(function (res, rej) {
              axios.post(OrdersRequest + '/' + Num).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context55.stop();
        }
      }
    }, _callee55);
  }));
  return _GetOrder.apply(this, arguments);
}

function UpdateOrder(_x52) {
  return _UpdateOrder.apply(this, arguments);
}

function _UpdateOrder() {
  _UpdateOrder = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee56(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee56$(_context56) {
      while (1) {
        switch (_context56.prev = _context56.next) {
          case 0:
            return _context56.abrupt("return", new Promise(function (res, rej) {
              axios.post(OrdersRequest + '/update', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context56.stop();
        }
      }
    }, _callee56);
  }));
  return _UpdateOrder.apply(this, arguments);
}

function GetMailSetting(_x53) {
  return _GetMailSetting.apply(this, arguments);
}

function _GetMailSetting() {
  _GetMailSetting = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee57(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee57$(_context57) {
      while (1) {
        switch (_context57.prev = _context57.next) {
          case 0:
            return _context57.abrupt("return", new Promise(function (res, rej) {
              axios.get(MailSettingRequest).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context57.stop();
        }
      }
    }, _callee57);
  }));
  return _GetMailSetting.apply(this, arguments);
}

function UpdateMailSetting(_x54) {
  return _UpdateMailSetting.apply(this, arguments);
}

function _UpdateMailSetting() {
  _UpdateMailSetting = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee58(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee58$(_context58) {
      while (1) {
        switch (_context58.prev = _context58.next) {
          case 0:
            return _context58.abrupt("return", new Promise(function (res, rej) {
              axios.post(MailSettingRequest + '/update', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context58.stop();
        }
      }
    }, _callee58);
  }));
  return _UpdateMailSetting.apply(this, arguments);
}

function ValidMailSetting(_x55) {
  return _ValidMailSetting.apply(this, arguments);
}

function _ValidMailSetting() {
  _ValidMailSetting = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee59(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee59$(_context59) {
      while (1) {
        switch (_context59.prev = _context59.next) {
          case 0:
            return _context59.abrupt("return", new Promise(function (res, rej) {
              axios.post(MailSettingRequest + '/valid', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context59.stop();
        }
      }
    }, _callee59);
  }));
  return _ValidMailSetting.apply(this, arguments);
}

function GetSystem() {
  return _GetSystem.apply(this, arguments);
}

function _GetSystem() {
  _GetSystem = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee60() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee60$(_context60) {
      while (1) {
        switch (_context60.prev = _context60.next) {
          case 0:
            return _context60.abrupt("return", new Promise(function (res, rej) {
              axios.get(SystemRequest).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context60.stop();
        }
      }
    }, _callee60);
  }));
  return _GetSystem.apply(this, arguments);
}

function UpdateSystem(_x56) {
  return _UpdateSystem.apply(this, arguments);
}

function _UpdateSystem() {
  _UpdateSystem = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee61(credential) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee61$(_context61) {
      while (1) {
        switch (_context61.prev = _context61.next) {
          case 0:
            return _context61.abrupt("return", new Promise(function (res, rej) {
              axios.post(SystemRequest + '/update', credential).then(function (response) {
                res(response.data);
              })["catch"](function (error) {
                rej(error.response);
              });
            }));

          case 1:
          case "end":
            return _context61.stop();
        }
      }
    }, _callee61);
  }));
  return _UpdateSystem.apply(this, arguments);
}

function UpdateProfile(credential) {
  return new Promise(function (res, rej) {
    axios.post(JwtRequest + '/updateProfile', credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function GetRoles() {
  return new Promise(function (res, rej) {
    axios.get(RoleRequest).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function CreateUser(credential) {
  return new Promise(function (res, rej) {
    axios.post(UserRequest + "/create", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function UpdateUserState(credential) {
  return new Promise(function (res, rej) {
    axios.post(UserRequest + "/updateState", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function DeleteUser(id) {
  return new Promise(function (res, rej) {
    axios["delete"](UserRequest + '/delete', {
      params: {
        'id': id
      }
    }).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function GetUser(account) {
  return new Promise(function (res, rej) {
    axios.get(UserRequest + '/' + account).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function UpdateUser(credential) {
  return new Promise(function (res, rej) {
    axios.post(UserRequest + "/update", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function GetPermissions() {
  return new Promise(function (res, rej) {
    axios.get(PermissionRequest).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function GetHasPermissions(name) {
  return new Promise(function (res, rej) {
    axios.get(PermissionRequest + "/" + name).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function UpdateHasPermissions(credential) {
  return new Promise(function (res, rej) {
    axios.post(PermissionRequest + "/update", credential).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}

/***/ }),

/***/ "./resources/js/backend/helper.js":
/*!****************************************!*\
  !*** ./resources/js/backend/helper.js ***!
  \****************************************/
/*! exports provided: SwalAlertErrorMessage, SwalAlertSuccessRedirect, SwalAlertAsk, Submit, getLocalUser, logout, ButtonOpenFileManger */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwalAlertErrorMessage", function() { return SwalAlertErrorMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwalAlertSuccessRedirect", function() { return SwalAlertSuccessRedirect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwalAlertAsk", function() { return SwalAlertAsk; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Submit", function() { return Submit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLocalUser", function() { return getLocalUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logout", function() { return logout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonOpenFileManger", function() { return ButtonOpenFileManger; });
/* harmony import */ var _storage_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./storage.js */ "./resources/js/backend/storage.js");

function SwalAlertErrorMessage(response) {
  if (response.status != 401) {
    var errors = response.data.errors;
    var html = '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">';
    Object.keys(errors).forEach(function (item) {
      html += '<div><i class="fas fa-times" style="color:#f27474"></i>  ' + errors[item].join(',') + "</div>";
    });
    Swal.fire({
      icon: 'error',
      html: html,
      showCloseButton: false,
      showConfirmButton: false,
      width: '300px',
      heightAuto: false
    });
  }
}
function SwalAlertSuccessRedirect(response) {
  var _response$data;

  var redirect = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var messages = (_response$data = response.data) !== null && _response$data !== void 0 ? _response$data : '成功';

  if (redirect != null) {
    return new Promise(function (res, rej) {
      Swal.fire({
        title: messages,
        icon: 'success',
        showConfirmButton: false,
        width: '300px',
        timer: 970,
        heightAuto: false
      }).then(function (result) {
        res(true);
      });
    });
  } else {
    Swal.fire({
      title: messages,
      icon: 'success',
      showConfirmButton: false,
      width: '300px',
      timer: 970,
      heightAuto: false
    });
  }
}
function SwalAlertAsk() {
  var _title, _message;

  var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var message = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  title = (_title = title) !== null && _title !== void 0 ? _title : '您確定要刪除嗎？';
  message = (_message = message) !== null && _message !== void 0 ? _message : '此動作將不可回復';
  return new Promise(function (res, rej) {
    Swal.fire({
      title: '<h2 style="color: #f8bb86">' + title + '</h2>',
      html: '<span style="color: #707070;font-size: 14px;">' + message + '<span>',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: '確定',
      cancelButtonText: '取消',
      reverseButtons: true,
      heightAuto: false,
      width: '300px',
      confirmButtonColor: '#43d39e',
      cancelButtonColor: '#ff5c75'
    }).then(function (result) {
      if (result.value) {
        res(true);
      } else {
        rej(false);
      }
    });
  });
}
function Submit(method, url, data) {
  var message = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  var redirect = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
  return new Promise(function (res, rej) {
    axios({
      method: method,
      url: url,
      data: data
    }).then(function (response) {
      res(response.data);
    })["catch"](function (error) {
      rej(error.response);
    });
  });
}
function getLocalUser() {
  var userStr = localStorage.getItem("user");

  if (!userStr) {
    return null;
  }

  return JSON.parse(userStr);
}
function logout() {
  return _storage_js__WEBPACK_IMPORTED_MODULE_0__["default"].commit('Logout');
} //目前無用到

function ButtonOpenFileManger() {
  $.fn.filemanager = function (type, options) {
    type = type || 'file';
    var userStr = localStorage.getItem("user");
    this.on('click', function (e) {
      var route_prefix = options && options.prefix ? options.prefix : '/filemanager';
      var target_input = $('#' + $(this).data('input'));
      var target_preview = $('#' + $(this).data('preview'));

      if (userStr) {
        var token = 'Bearer ' + JSON.parse(userStr)['token'];
        window.open(route_prefix + '?type=' + type + '&token=' + token, 'FileManager', 'width=900,height=600');
      } else {
        window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
      }

      window.SetUrl = function (items) {
        var file_path = items.map(function (item) {
          return item.url;
        }).join(','); // set the value of the desired input to image url

        target_input.val('').val(file_path).trigger('change');
        target_input[0].dispatchEvent(new CustomEvent('change')); // clear previous preview

        target_preview.html(''); // set or change the preview image src

        items.forEach(function (item) {
          target_preview.append($('<img>').css('height', '5rem').attr('src', item.thumb_url));
        }); // trigger change event

        target_preview.trigger('change');
      };

      return false;
    });
  };
}

/***/ }),

/***/ "./resources/js/backend/storage.js":
/*!*****************************************!*\
  !*** ./resources/js/backend/storage.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _router_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./router.js */ "./resources/js/backend/router.js");
/* harmony import */ var _helper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./helper.js */ "./resources/js/backend/helper.js");



vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vuex__WEBPACK_IMPORTED_MODULE_1__["default"]);

var user = Object(_helper_js__WEBPACK_IMPORTED_MODULE_3__["getLocalUser"])();
/* harmony default export */ __webpack_exports__["default"] = (new vuex__WEBPACK_IMPORTED_MODULE_1__["default"].Store({
  state: {
    currentUser: user
  },
  actions: {
    Login: function Login(context, val) {
      return new Promise(function (resolve, reject) {
        context.commit('Login', val);
        resolve();
      });
    }
  },
  mutations: {
    Login: function Login(state, data) {
      state.currentUser = Object.assign({}, data.user, {
        token: data.access_token
      });
      localStorage.setItem("user", JSON.stringify(state.currentUser));
      axios.defaults.headers.common["Authorization"] = "Bearer ".concat(data.access_token);
    },
    Logout: function Logout(state) {
      state.currentUser = null;
      localStorage.removeItem("user");
      _router_js__WEBPACK_IMPORTED_MODULE_2__["default"].go({
        name: 'login'
      });
    },
    Refresh: function Refresh(state, token) {
      state.currentUser.token = token;
      var user = JSON.parse(sessionStorage.getItem("user"));
      user.token = token;
      sessionStorage.setItem("user", JSON.stringify(user));
      axios.defaults.headers.common["Authorization"] = "Bearer ".concat(token);
    }
  },
  getters: {
    currentUser: function currentUser(state) {
      return state.currentUser;
    }
  }
}));

/***/ })

}]);