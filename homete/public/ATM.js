(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ATM"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/ATM.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/ATM.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../api */ "./resources/js/front/api.js");
/* harmony import */ var _backend_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../backend/helper */ "./resources/js/backend/helper.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var PaidPercent = 30;
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ATM",
  data: function data() {
    return {
      Num: this.$route.params.Num,
      ATMInfo: "",
      Price: '',
      DiscountCode: '',
      BookingData: '',
      Paid: false
    };
  },
  computed: {
    Member: function Member() {
      return this.$store.getters.Member;
    }
  },
  created: function created() {
    if (this.Num === undefined) {
      this.$router.push({
        name: 'Search'
      });
    }

    this.init();
  },
  methods: {
    init: function init() {
      var _this = this;

      this.$emit('updateStep', 4);
      Object(_api__WEBPACK_IMPORTED_MODULE_0__["GetATMInfo"])().then(function (res) {
        _this.ATMInfo = res.Html;
      })["catch"](function (rep) {
        Object(_backend_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertErrorMessage"])(rep);
      });
      var credential = {
        Num: this.Num
      };
      Object(_api__WEBPACK_IMPORTED_MODULE_0__["PaymentSelect"])(credential).then(function (res) {
        _this.Price = res.Order.Price;
        _this.DiscountCode = res.Order.DiscountCode;
        _this.BookingData = res.Order.BookingData;
        _this.Paid = res.Order.Paid;
      })["catch"](function (rep) {
        Object(_backend_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertErrorMessage"])(rep);

        _this.$router.push({
          name: 'Search'
        });
      });
    },
    nights: function nights(startDate, endDate) {
      var _moment$diff;

      return (_moment$diff = moment__WEBPACK_IMPORTED_MODULE_2___default()(endDate).diff(moment__WEBPACK_IMPORTED_MODULE_2___default()(startDate), 'days')) !== null && _moment$diff !== void 0 ? _moment$diff : 0;
    },
    ShouldPaid: function ShouldPaid(Price) {
      return Math.ceil(parseInt(Price) * PaidPercent / 100);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/ATM.vue?vue&type=template&id=5499fbc2&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/ATM.vue?vue&type=template&id=5499fbc2&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row warp_p" }, [
    _c("div", { staticClass: "col-md-12 text-center booking-success-step" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("h5", { staticClass: "mt-3 pt-5 pb-3" }, [_vm._v("匯款資訊")]),
      _vm._v(" "),
      _c("p", [_vm._v("訂單編號 : " + _vm._s(_vm.Num))]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "col-md-6 col-md-offset-3 booking-success-price-details"
        },
        [_c("div", { domProps: { innerHTML: _vm._s(_vm.ATMInfo) } })]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "col-12" }),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "col-md-6 col-md-offset-3 booking-success-price-details"
        },
        [
          _c("table", { staticClass: "table" }, [
            _vm._m(1),
            _vm._v(" "),
            _c(
              "tbody",
              [
                _vm._l(_vm.BookingData, function(ob, index) {
                  return _c("tr", { key: index }, [
                    _c("td", [
                      _vm._v(
                        _vm._s(ob.Room.Name) +
                          " | " +
                          _vm._s(ob.Promotion.Name) +
                          " (" +
                          _vm._s(_vm.nights(ob.startDate, ob.endDate)) +
                          "晚)"
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "td",
                      [
                        ob.Extra !== null
                          ? _c(
                              "div",
                              [
                                ob.ExtraPeople > 0
                                  ? [
                                      _vm._v(
                                        "\n                                加備品 x " +
                                          _vm._s(
                                            _vm.nights(ob.startDate, ob.endDate)
                                          ) +
                                          " 晚 x " +
                                          _vm._s(ob.ExtraPeople) +
                                          " "
                                      ),
                                      ob.ExtraBed > 0
                                        ? [_vm._v(" + ")]
                                        : _vm._e()
                                    ]
                                  : _vm._e(),
                                _vm._v(" "),
                                ob.ExtraBed > 0
                                  ? [
                                      _vm._v(
                                        "\n                                加床 x " +
                                          _vm._s(
                                            _vm.nights(ob.startDate, ob.endDate)
                                          ) +
                                          " 晚 x " +
                                          _vm._s(ob.ExtraBed) +
                                          "\n                            "
                                      )
                                    ]
                                  : _vm._e()
                              ],
                              2
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        ob.ExtraShop.length > 0
                          ? _vm._l(ob.ExtraShop, function(Shop) {
                              return ob.ExtraShop.length > 0
                                ? _c("div", [
                                    _vm._v(
                                      "\n                                " +
                                        _vm._s(Shop.Name) +
                                        " x " +
                                        _vm._s(Shop.Nights) +
                                        " 晚 x " +
                                        _vm._s(Shop.Numbers) +
                                        "\n                            "
                                    )
                                  ])
                                : _vm._e()
                            })
                          : _vm._e(),
                        _vm._v(" "),
                        ob.ExtraCar.length > 0
                          ? _vm._l(ob.ExtraCar, function(Car) {
                              return ob.ExtraCar.length > 0
                                ? _c("div", [
                                    _vm._v(
                                      "\n                                " +
                                        _vm._s(Car.Name) +
                                        " - " +
                                        _vm._s(
                                          Car.type === "1" ? "單趟" : "來回"
                                        ) +
                                        " x " +
                                        _vm._s(Car.Numbers) +
                                        "\n                            "
                                    )
                                  ])
                                : _vm._e()
                            })
                          : _vm._e()
                      ],
                      2
                    )
                  ])
                }),
                _vm._v(" "),
                _vm.DiscountCode !== undefined && _vm.DiscountCode.length > 0
                  ? _c("tr", [
                      _c(
                        "td",
                        { staticClass: "text-right", attrs: { colspan: "3" } },
                        [
                          _vm._v(
                            _vm._s(_vm.DiscountCode.Name) +
                              " -NT$ " +
                              _vm._s(_vm.DiscountCode.Discount)
                          )
                        ]
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c("tr", [
                  _c(
                    "td",
                    { staticClass: "text-right", attrs: { colspan: "2" } },
                    [
                      _c("span", { staticClass: "d-block" }, [
                        _vm._v("訂房總額 NT$ " + _vm._s(_vm.Price))
                      ]),
                      _vm._v(" "),
                      _c("span", { staticClass: "d-block paid" }, [
                        _c("i", { staticClass: "fas fa-check pr-2" }),
                        _vm._v(
                          "\n                            應付金額 NT$ " +
                            _vm._s(_vm.ShouldPaid(_vm.Price)) +
                            "\n                        "
                        )
                      ])
                    ]
                  )
                ])
              ],
              2
            )
          ])
        ]
      ),
      _vm._v(" "),
      _vm._m(2),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "col-12 col-push-0 col-xs-6 col-xs-push-6 col-sm-4 col-sm-push-4 col-sm-offset-2 col-md-3 col-md-push-3 col-md-offset-3 mb-10"
        },
        [
          _c(
            "a",
            {
              staticClass: "button eb-btn w-100",
              attrs: { href: "mumber.html", title: "" }
            },
            [_vm._v("前往會員中心")]
          ),
          _vm._v(" "),
          _c("router-link", { attrs: { to: { name: "" } } })
        ],
        1
      ),
      _vm._v(" "),
      _vm._m(3)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "booking-success-icon d-flex justify-content-center" },
      [_c("div", { staticClass: "success-icon" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { staticClass: "text-center", attrs: { colspan: "2" } }, [
          _vm._v("付款明細")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-md-offset-3" }, [
      _c("p", { staticClass: "booking-success-message" }, [
        _vm._v("訂房詳情將會寄至您的信箱，再請前往收信"),
        _c("br"),
        _vm._v(
          "\n                如欲查詢訂單，登入會員後點選會員中心訂房紀錄查詢"
        ),
        _c("br"),
        _vm._v(
          "\n                若有其他任何住房需求與問題，請與我們電話或電子郵件聯繫\n            "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "col-12 col-pull-0 col-xs-6 col-xs-pull-6 col-sm-4 col-sm-pull-4 col-md-3 col-md-pull-3 mb-10"
      },
      [
        _c(
          "a",
          {
            staticClass: "button eb-btn w-100",
            attrs: { href: "/", title: "" }
          },
          [_vm._v("返回首頁")]
        )
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/front/components/layouts/ATM.vue":
/*!*******************************************************!*\
  !*** ./resources/js/front/components/layouts/ATM.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ATM_vue_vue_type_template_id_5499fbc2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ATM.vue?vue&type=template&id=5499fbc2&scoped=true& */ "./resources/js/front/components/layouts/ATM.vue?vue&type=template&id=5499fbc2&scoped=true&");
/* harmony import */ var _ATM_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ATM.vue?vue&type=script&lang=js& */ "./resources/js/front/components/layouts/ATM.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ATM_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ATM_vue_vue_type_template_id_5499fbc2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ATM_vue_vue_type_template_id_5499fbc2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "5499fbc2",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/front/components/layouts/ATM.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/front/components/layouts/ATM.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/front/components/layouts/ATM.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ATM_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ATM.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/ATM.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ATM_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/front/components/layouts/ATM.vue?vue&type=template&id=5499fbc2&scoped=true&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/ATM.vue?vue&type=template&id=5499fbc2&scoped=true& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ATM_vue_vue_type_template_id_5499fbc2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ATM.vue?vue&type=template&id=5499fbc2&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/ATM.vue?vue&type=template&id=5499fbc2&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ATM_vue_vue_type_template_id_5499fbc2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ATM_vue_vue_type_template_id_5499fbc2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);