<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = [];

    public function ModelHasRole()
    {
        return $this->belongsTo(ModelHasRole::class,'id','role_id');
    }

    public function User()
    {
        return $this->hasManyThrough(User::class,ModelHasRole::class,'role_id','id','id','model_id');
    }

}
