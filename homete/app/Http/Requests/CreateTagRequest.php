<?php

namespace App\Http\Requests;

use App\Repositories\LicenseRepository;
use Illuminate\Foundation\Http\FormRequest;

class CreateTagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'Rooms.*' => ['nullable','exists:rooms,id'],
            'Name' => ['required','unique:tags,Name'],
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            'Rooms.*.exists' => '房型不存在',
            'Name.required' => '標籤名稱必填',
            'Name.unique' => '標籤不得重複',
        ];
    }
}
