<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMemberEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => ['required','email'],
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'E-mail 必填',
            'email.email' => 'E-mail 格式錯誤',
        ];
    }
}
