<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMembersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            '*.id' => ['required','exists:members,id'],
            '*.State' => ['required','boolean'],
        ];
        return $rules;
    }

    public function messages()
    {
        return [
            '*.id.required' => '會員不存在',
            '*.id.exists' => '會員不存在',
            '*.State.required' => '啟用不存在',
            '*.State.boolean' => '啟用格式不正確',
        ];
    }
}
