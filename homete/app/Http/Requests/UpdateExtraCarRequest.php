<?php

namespace App\Http\Requests;

use App\Repositories\LicenseRepository;
use Illuminate\Foundation\Http\FormRequest;

class UpdateExtraCarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules(LicenseRepository $license)
    {
        $rules =  [
            'Max' => ['required','numeric','min:0'],
            'id' => ['required','exists:extra_cars,id'],
            'Name' => ['required'],
            'Price' => ['required','numeric','min:0'],
            'Price2' => ['required','numeric','min:0'],
            'Sort' => ['required','numeric','min:0'],
            'State' => ['required','boolean'],
        ];
        return $rules;
    }
    public function messages()
    {
        return [
            'Max.required' => '最大選購量必填',
            'Max.numeric' => '最大選購量輸入格式錯誤',
            'Max.min' => '最大選購量最小值為 0',

            'Name.required' => '名稱必填',
            'Price.required' => '單趟價格必填',
            'Price.numeric' => '單趟價格輸入格式錯誤',
            'Price.min' => '單趟價格最小值為 0',

            'Price2.required' => '來回價格必填',
            'Price2.numeric' => '來回價格輸入格式錯誤',
            'Price2.min' => '來回價格最小值為 0',

            'Sort.required' => '優先度必填',
            'Sort.numeric' => '優先度輸入格式錯誤',
            'Sort.min' => '優先度最小值為 0',
            'State.required' => '啟用必填',
            'State.boolean' => '啟用輸入格式錯誤',
        ];
    }

}
