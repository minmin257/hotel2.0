<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JwtLoginRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account' => ['required','exists:users,account'],
            'password' => ['required'],
//            'google_recaptcha_token' => ['required','captcha'],
        ];
    }
}
