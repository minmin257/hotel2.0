<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JwtUpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account' => ['required','exists:users,account'],
            'name' => ['required'],
            'password' => ['nullable','min:6']
        ];
    }

    public function messages()
    {
        return [
            'account.required' => '找不到此帳號',
            'account.exists' => '找不到此帳號',
            'name.required' => '名稱必填',
            'password.min' => '密碼最短需 6碼'
        ];
    }
}
