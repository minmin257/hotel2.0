<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateExtraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'room_id' => ['required','exists:rooms,id'],
            '*.cars' => ['exists:extra_cars,id'],
            '*.shops' => ['exists:extra_shops,id'],
        ];
        return  $rules;
    }

    public function messages()
    {
        return [
            'room_id.exists' => '房間不存在',
            '*.cars.exists' => '加購接送不存在',
            '*.shops.exists' => '加購商品不存在',
        ];
    }
}
