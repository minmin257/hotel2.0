<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePermissionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'name' => ['required','exists:roles,name'],
            'HasPermissions.*' => ['exists:permissions,id'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => ['等級不存在'],
            'exists.required' => ['等級不存在'],
            'HasPermissions.*.exists' => ['權限不存在'],
        ];
    }
}
