<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeleteExtraCarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'extra_car_id' => ['required','exists:extra_cars,id']
        ];
    }
    public function messages()
    {
        return [
            'extra_car_id.exists' =>  '加購接送不存在'
        ];
    }

}
