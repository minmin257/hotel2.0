<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDiscountCodesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            '*.Name' => ['required'],
            '*.Discount' => ['required','numeric','min:0'],
            '*.Remains' => ['required','numeric','min:0'],
            '*.Code' => ['required'],
            '*.id' => ['required','exists:discount_codes,id'],
            '*.State' => ['required','boolean'],
            '*.Sort' => ['required','numeric','min:0'],
        ];
        return  $rules;
    }

    public function messages()
    {
        return [
            '*.Name.required' => '名稱必填',
            '*.Discount.required' => '折價金必填',
            '*.Discount.numeric' => '折價金格式不正確',
            '*.Discount.min' => '折價金最小值為 0',

            '*.Remains.required' => '剩餘數量必填',
            '*.Remains.numeric' => '剩餘數量格式不正確',
            '*.Remains.min' => '剩餘數量最小值為 0',

            '*.Code.required' => '優惠碼必填',

            '*.id.required' => '折扣代碼必填',
            '*.id.exists' => '折扣代碼不存在',

            '*.State.required' => '啟用必填',
            '*.State.boolean' => '啟用輸入格式錯誤',
            '*.Sort.required' => '優先度必填',
            '*.Sort.numeric' => '優先度輸入格式錯誤',
            '*.Sort.min' => '優先度最小值為 0',
        ];
    }
}
