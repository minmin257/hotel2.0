<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberForgetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => ['required'],
            'IdNumber' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'E-mail 必填',
            'IdNumber.required' => '身份證字號 必填'
        ];
    }
}
