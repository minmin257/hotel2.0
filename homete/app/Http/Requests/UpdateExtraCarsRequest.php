<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateExtraCarsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            '*.Max' => ['required','numeric','min:0'],
            '*.Name' => ['required'],
            '*.State' => ['required','boolean'],
            '*.Sort' => ['required','numeric','min:0'],
            '*.Price' => ['required','numeric','min:0'],
            '*.Price2' => ['required','numeric','min:0'],
        ];
        return  $rules;
    }

    public function messages()
    {
        return [

            '*.Max.required' => '最大選購量必填',
            '*.Max.numeric' => '最大選購量格式不正確',
            '*.Max.min' => '最大選購量最小值為 0',
            '*.Name.required' => '名稱必填',
            '*.State.required' => '啟用必填',
            '*.State.boolean' => '啟用輸入格式錯誤',
            '*.Sort.required' => '優先度必填',
            '*.Sort.numeric' => '優先度輸入格式錯誤',
            '*.Sort.min' => '優先度最小值為 0',
            '*.Price.required' => '單趟價格必填',
            '*.Price.numeric' => '單趟價格輸入格式錯誤',
            '*.Price.min' => '單趟價格最小值為 0',

            '*.Price2.required' => '來回價格必填',
            '*.Price2.numeric' => '來回價格輸入格式錯誤',
            '*.Price2.min' => '來回價格最小值為 0',
        ];
    }
}
