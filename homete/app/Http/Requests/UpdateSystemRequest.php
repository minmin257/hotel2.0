<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSystemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'SiteName' => ['required'],
            'Src' => ['required'],
            'MetaDescription' => ['required'],
            'MetaKeyword' => ['required'],
            'Addr' => ['required'],
            'Tel' => ['required'],
            'Email' => ['required','email'],
            'Maintain' =>  ['required','boolean'],
            'Message' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'SiteName.required' => '網站名稱 必填',
            'Src.required' => 'Logo 必填',
            'MetaDescription.required' => '網站描述 必填',
            'MetaKeyword.required' => '網站關鍵字 必填',
            'Addr.required' => '地址 必填',
            'Tel.required' => '聯絡電話 必填',
            'Email.required' => '聯絡信箱 必填',
            'Maintain.required' => '網站維護 必填',
            'Message.required' => '維護訊息 必填',
        ];
    }
}
