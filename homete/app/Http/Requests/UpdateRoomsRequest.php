<?php

namespace App\Http\Requests;

use App\Repositories\LicenseRepository;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRoomsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(LicenseRepository $license)
    {
        $rules =  [
            '*.Name' => ['required'],
            '*.PeopleNumber' => ['required','numeric','min:0'],
            '*.DefaultPrice' => ['required','numeric','min:0'],
            '*.DefaultNumber' => ['required','numeric','min:0'],
        ];
        if($EarlyBirdLicnese = $license->check('早鳥折扣'))
        {
            $rules['*.DefaultAllowEarlyBird'] = ['required','boolean'];
            $rules['*.DefaultEarlyBirdPrice'] = ['required','numeric'];
            $rules['*.DefaultEarlyBirdDays'] = ['required','numeric'];
        }
        return $rules;
    }

    public function messages()
    {
        return [
            '*.Name.required' => '名稱必填',
            '*.PeopleNumber.required' => '人數必填',
            '*.PeopleNumber.numeric' => '人數輸入格式錯誤',
            '*.PeopleNumber.min' => '人數最小值為 0',
            '*.DefaultPrice.required' => '預設基本價必填',
            '*.DefaultPrice.numeric' => '預設基本價輸入格式錯誤',
            '*.DefaultPrice.min' => '預設基本價最小值為 0',
            '*.DefaultNumber.required' => '預設間數必填',
            '*.DefaultNumber.numeric' => '預設間數輸入格式錯誤',
            '*.DefaultNumber.min' => '預設間數最小值為 0',

            '*.DefaultAllowEarlyBird.required' => '預設早鳥必填',
            '*.DefaultEarlyBirdPrice.required' => '預設早鳥價格必填',
            '*.DefaultEarlyBirdPrice.numeric' => '預設早鳥價格輸入格式錯誤',
            '*.DefaultEarlyBirdDays.required' => '預設早鳥天數必填',
            '*.DefaultEarlyBirdDays.numeric' => '預設早鳥天數輸入格式錯誤',
        ];
    }
}
