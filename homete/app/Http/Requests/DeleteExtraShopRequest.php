<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeleteExtraShopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'extra_shop_id' => ['required','exists:extra_shops,id']
        ];
    }
    public function messages()
    {
        return [
            'extra_shop_id.exists' =>  '加購商品不存在'
        ];
    }

}
