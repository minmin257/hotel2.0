<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTagRequest;
use App\Http\Requests\DeleteTagRequest;
use App\Http\Requests\UpdateTagsRequest;
use App\Repositories\LicenseRepository;
use App\Service\Tag;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class TagController extends Controller
{

    private $tag,$FormatContract,$license;
    public function __construct(Tag $tag, LicenseRepository $license)
    {
        $this->tag = $tag;
        $this->FormatContract = resolve('FormatContract');
        $this->license = $license;
        if(!$this->license->check('篩選過濾')){
            abort(403);
        }
    }

    public function get(Request $request)
    {
        if($request->has('tag_id'))
        {
            return response()->json($this->tag->readById($request->tag_id)->toArray());
        }
        else if($request->has('Paginate'))
        {
            Paginator::currentPageResolver(function() use ($request) {
                return $request->CurrentPage;
            });
            return response()->json($this->tag->paginate($request->Paginate));
        }
        else
        {
            return response()->json($this->tag->GetAll());
        }
    }

    public function Create(CreateTagRequest $request)
    {
        try {
            $this->tag->create($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('新增',[
                $e->getMessage()
            ],409);
        }
    }

    public function Delete(DeleteTagRequest $request)
    {
        try {
            $this->tag->delete($request->tag_id);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('刪除',[
                $e->getMessage()
            ],409);
        }
    }
    private function no_dupes($input_array) {
        return count($input_array) === count(array_flip($input_array));
    }
    public function UpdateTags(UpdateTagsRequest $request)
    {
        // 檢查陣列有否重複
        if($this->no_dupes(collect($request)->pluck('Name')->toArray())) {
            try {
                $this->tag->updateTags($request);
                return response()->json([],200);
            }
            catch (\Exception $e)
            {
                \Log::info($e);
                return  $this->FormatContract->customError('更新',[
                    $e->getMessage()
                ],409);
            }
        }
        else
        {
            return  $this->FormatContract->customError('更新',[
               '標籤名稱不得重複'
            ],409);
        }
    }
}
