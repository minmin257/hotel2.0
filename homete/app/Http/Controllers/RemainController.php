<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\Remain;

class RemainController extends Controller
{
    private $remain,$FormatContract;
    public function __construct(Remain $remain)
    {
        $this->remain = $remain;
        $this->FormatContract = resolve('FormatContract');
    }

    public function get(Request $request)
    {
        return response()->json($this->remain->Get($request->room_id,$request->start,$request->end),200);
    }
}
