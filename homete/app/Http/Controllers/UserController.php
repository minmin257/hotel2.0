<?php

namespace App\Http\Controllers;

use App\Formatter\FormatContract;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserProfileRequest;
use App\Http\Requests\UpdateUserRequest;
use App\ModelHasRole;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $user,$FormatContract;
    public function __construct(UserRepository $user,FormatContract $FormatContract)
    {
        $this->user = $user;
        $this->FormatContract = $FormatContract;
    }

    public function create(CreateUserRequest $request)
    {
        try{
            $user = $this->user->create([
                'name' => $request->name,
                'account' => $request->account,
                'password' => bcrypt($request->password),
                'state' => $request->state
            ]);
            $user->Role()->create([
                'model_type' => 'user',
                'model_id' => $user->id,
                'role_id' => $request->role_id
            ]);
            return response()->json(['新增成功'],200);
        }
        catch (\Exception $exception)
        {
            \Log::info($exception);
            return  $this->FormatContract->customError('錯誤',['新增失敗'],409);
        }
    }

    public function updateState(Request $request)
    {
        try{
            $this->user->update($request->id,[
                'State'=>$request->State
            ]);
            return response()->json(['更新成功'],200);
        }
        catch (\Exception $exception)
        {
            return  $this->FormatContract->customError('錯誤',['修改失敗'],409);
        }
    }

    public function deleteUser(Request $request,UserRepository $user,ModelHasRole $modelHasRole)
    {
        try
        {
            if($request->has('id'))
            {
                $modelHasRole->where('model_id',$request['id'])->delete();
                $user->delete($request['id']);
                return response()->json(['更新成功'],200);
            }
        }
        catch (\Exception $e)
        {
            return  $this->FormatContract->customError('錯誤',['刪除失敗'],409);
        }
    }

    public function getUser($account)
    {
        $user = $this->user->getquery()->where('account','=',$account)->first();
        if($user)
        {
            $role = \App\Role::where('id',$user->Role->role_id)->first()->id;
            return response()->json(array_merge($user->toArray(),['role' => $role]));
        }
        else
        {
            return  $this->FormatContract->customError('錯誤',['查無此帳號'],409);
        }
    }

    public function update(UpdateUserRequest $request)
    {
        \Log::info($request);
        try {
            $user = $this->user->where('account', $request->account)->first();
            $user->update([
                'name' => $request->name,
                'State' => $request->State
            ]);
            if ($request->password) {
                $user->update([
                    'password' => bcrypt($request->password)
                ]);
            }
            $user->Role()->update([
                'role_id' => $request->role_id
            ]);
            return response()->json(['更新成功'],200);
        }
        catch (\Exception $e)
        {
            return  $this->FormatContract->customError('錯誤',['修改失敗'],409);
        }
    }
}
