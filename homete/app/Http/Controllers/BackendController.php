<?php

namespace App\Http\Controllers;

use App\Formatter\FormatContract;
use App\Repositories\SettingRepository;
use Illuminate\Http\Request;

class BackendController extends Controller
{
    private $FormatContract;
    public function __construct(FormatContract $FormatContract)
    {
        $this->FormatContract = $FormatContract;
    }

    public function index()
    {
        return view('backend');
    }

    public function getConfig($name)
    {
        return response()->json(Config($name));
    }

    public function getSetting($group,$name = null,SettingRepository $setting)
    {
        if($name){
            return response()->json($setting->where('group',$group)->where('name',$name)->first()->toArray());
        }
        return response()->json($setting->where('group',$group)->get()->toArray());
    }

    public function MemberChecker()
    {
        try {
            if(!auth('api')->user()){
                throw new \Exception('尚未登入');
            }
            else{
                return response()->json([
                    'Checker' => true
                ],200);
            }
        }
        catch (\Exception $exception){
            return  $this->FormatContract->customError('錯誤',[
                $exception->getMessage()
            ],400);
        }
    }
}
