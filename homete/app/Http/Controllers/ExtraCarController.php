<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateExtraCarRequest;
use App\Http\Requests\DeleteExtraCarRequest;
use App\Http\Requests\UpdateExtraCarRequest;
use App\Http\Requests\UpdateExtraCarsRequest;
use App\Repositories\LicenseRepository;
use App\Service\ExtraCar;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class ExtraCarController extends Controller
{
    private  $extraCar,$FormatContract;
    public function __construct(ExtraCar $extraCar, LicenseRepository $license)
    {
        $this->extraCar = $extraCar;
        $this->FormatContract = resolve('FormatContract');
        $this->license = $license;
        if(!$this->license->check('加購接送')){
            abort(403);
        }
    }

    public function get(Request $request){
        if($request->has('extra_car_id'))
        {
            return response()->json($this->extraCar->readById($request->extra_car_id)->toArray());
        }
        else if($request->has('Paginate'))
        {
            Paginator::currentPageResolver(function() use ($request) {
                return $request->CurrentPage;
            });
            return response()->json($this->extraCar->paginate($request->Paginate));
        }
        else
        {
            return response()->json($this->extraCar->GetAll());
        }
    }

    public function Create(CreateExtraCarRequest $request)
    {
        try {
            $this->extraCar->create($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('新增',[
                $e->getMessage()
            ],409);
        }
    }

    public function Update(UpdateExtraCarRequest $request)
    {
        try {
            $this->extraCar->update($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('修改',[
                $e->getMessage()
            ],409);
        }
    }

    public function UpdateExtraCars(UpdateExtraCarsRequest $request)
    {
        try {
            $this->extraCar->updateExtraCars($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('修改',[
                $e->getMessage()
            ],409);
        }
    }

    public function Delete(DeleteExtraCarRequest $request)
    {
        try {
            $this->extraCar->delete($request->extra_car_id);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('刪除',[
                $e->getMessage()
            ],409);
        }
    }
}
