<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateMemberRequest;
use App\Http\Requests\UpdateMembersRequest;
use App\Service\Member;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class MemberController extends Controller
{
    private $member,$FormatContract;
    public function __construct(Member $member)
    {
        $this->member = $member;
        $this->FormatContract = resolve('FormatContract');
    }

    //用於後台 呈現前台會員們 Member
    public function get(Request $request){
        if($request->has('member_id'))
        {
//            return response()->json($this->member->readById($request->member_id)->toArray());
        }
        else if($request->has('Paginate'))
        {
            Paginator::currentPageResolver(function() use ($request) {
                return $request->CurrentPage;
            });
            $request->has('Keyword') ? $keyword = $request->Keyword : $keyword = null;
            return response()->json($this->member->paginate($request->Paginate,$keyword));
        }
        else
        {
            return response()->json($this->member->GetAll());
        }
    }

    public function updateMembers(UpdateMembersRequest $request){
        try {
            $this->member->updateMembers($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('更新',[
                $e->getMessage()
            ],409);
        }
    }

    public function getMember($id)
    {
        try {
            $Member = $this->member->GetMemberById($id);
            $Bookings = $Member->OrderRecords()->get();
            return response()->json([
                'Member' => $Member, 'Bookings' => $Bookings
            ]);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('更新',[
                '找不到該會員紀錄'
            ],409);
        }
    }

    public function update(UpdateMemberRequest $request)
    {
        try {
            $Member = $this->member->updateMember($request);
            return response()->json($Member);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('更新',[
                '更新時發生錯誤'
            ],409);
        }
    }

    public function Delete(Request $request)
    {
        try {
            $Member = $this->member->DeleteMember($request);
            return response()->json([],200);
        }
        catch (\Exception $e)
        {
            \Log::info($e);
            return  $this->FormatContract->customError('更新',[
                '刪除時發生錯誤'
            ],409);
        }
    }
}
