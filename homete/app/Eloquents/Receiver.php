<?php


namespace App\Eloquents;


class Receiver
{
    public $email;
    public $name;

    public function __construct($email,$name = null)
    {
        $this->email = $email;
        $this->name = $name;
    }
}
