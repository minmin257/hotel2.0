<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $guarded = [];

    public function Order()
    {
        return $this->belongsTo(Order::class);
    }

    public function Shop()
    {
        return $this->hasMany(OrderDetailExtraShop::class);
    }
    public function Car()
    {
        return $this->hasMany(OrderDetailExtraCar::class);
    }

    public function Room()
    {
        return $this->belongsTo(Room::class);
    }

    public function Promotion()
    {
        return $this->belongsTo(Promotion::class);
    }
}
