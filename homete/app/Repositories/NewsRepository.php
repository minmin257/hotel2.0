<?php
    namespace App\Repositories;

    use App\News;

    class NewsRepository extends BaseRepository
    {

        protected function model()
        {
            return News::class;
        }

        protected function query()
        {
            return News::query();
        }
    }
