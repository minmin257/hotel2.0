<?php


namespace App\Repositories;


use App\Role;

class RoleRepository extends BaseRepository
{

    protected function model()
    {
        return Role::class;
    }

    protected function query()
    {
        return Role::query();
    }
}
