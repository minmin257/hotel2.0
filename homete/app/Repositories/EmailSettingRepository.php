<?php


namespace App\Repositories;


use App\EmailSetting;

class EmailSettingRepository extends BaseRepository
{

    protected function model()
    {
        return EmailSetting::class;
    }

    protected function query()
    {
        return EmailSetting::query();
    }
}
