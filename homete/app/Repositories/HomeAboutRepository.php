<?php
    namespace App\Repositories;

    use App\HomeAbout;

    class HomeAboutRepository extends BaseRepository
    {

        protected function model()
        {
            return HomeAbout::class;
        }

        protected function query()
        {
            return HomeAbout::query();
        }
    }
