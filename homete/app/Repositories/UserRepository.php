<?php


namespace App\Repositories;


use App\User;

class UserRepository extends BaseRepository
{

    protected function model()
    {
        return User::class;
    }

    protected function query()
    {
        return User::query();
    }

}
