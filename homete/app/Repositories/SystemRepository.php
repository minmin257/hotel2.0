<?php
    namespace App\Repositories;

    use App\System;

    class SystemRepository extends BaseRepository
    {

        protected function model()
        {
            return System::class;
        }

        protected function query()
        {
            return System::query();
        }
    }
