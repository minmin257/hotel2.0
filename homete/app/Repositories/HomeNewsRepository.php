<?php
    namespace App\Repositories;

    use App\HomeNews;

    class HomeNewsRepository extends BaseRepository
    {

        protected function model()
        {
            return HomeNews::class;
        }

        protected function query()
        {
            return HomeNews::query();
        }
    }
