<?php
    namespace App\Repositories;

    use App\Localization;

    class LocalizationRepository extends BaseRepository
    {

        protected function model()
        {
            return Localization::class;
        }

        protected function query()
        {
            return Localization::query();
        }
    }
