<?php
    namespace App\Repositories;

    use App\HomeBanner;

    class HomeBannerRepository extends BaseRepository
    {

        protected function model()
        {
            return HomeBanner::class;
        }

        protected function query()
        {
            return HomeBanner::query();
        }
    }
