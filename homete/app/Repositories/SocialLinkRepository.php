<?php
    namespace App\Repositories;

    use App\SocialLink;

    class SocialLinkRepository extends BaseRepository
    {

        protected function model()
        {
            return SocialLink::class;
        }

        protected function query()
        {
            return SocialLink::query();
        }
    }
