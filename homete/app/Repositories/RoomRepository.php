<?php


namespace App\Repositories;


use App\Room;

class RoomRepository extends BaseRepository
{

    protected function model()
    {
        return Room::class;
    }

    protected function query()
    {
        return Room::query();
    }
}
