<?php


namespace App\Repositories;


use App\Permission;

class PermissionRepository  extends BaseRepository
{
    protected function model()
    {
        return Permission::class;
    }

    protected function query()
    {
        return Permission::query();
    }
}
