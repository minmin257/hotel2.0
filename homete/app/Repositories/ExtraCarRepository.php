<?php


namespace App\Repositories;


use App\ExtraCar;

class ExtraCarRepository extends BaseRepository
{

    protected function model()
    {
        return ExtraCar::class;
    }

    protected function query()
    {
        return ExtraCar::query();
    }
}
