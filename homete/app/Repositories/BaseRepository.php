<?php


namespace App\Repositories;
use Illuminate\Container\Container as App;

abstract class BaseRepository implements IRepository
{
    private $app;
    private $modelClass;
    public $query;
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->modelClass = $this->model();
        $this->query = $this->query();
    }

    protected abstract function model();
    protected abstract function query();

    public function create(array $data)
    {
        $newModelInstance = $this->app->make($this->modelClass);
        return $this->setModelInstance($newModelInstance,$data);
    }

    public function update($id, array $data)
    {
        $ModelInstance = $this->readById($id);
        return $this->setModelInstance($ModelInstance,$data);
    }

    protected function setModelInstance($instance, array $data = [])
    {
        if(isset($instance)) {
            foreach ($data as $key => $value){
                if(isset($value)){
                    $instance[$key] = $value;
                }
            }
            \DB::beginTransaction();
            $saveResponse = $instance->save();
            if($saveResponse){
                \DB::commit();
                return $instance;
            }
            \DB::rollback();
        }
        throw new \Exception("Create/Update failed");
    }

    public function delete($id)
    {
        $this->modelClass::find($id)->delete();
    }

    public function readById($id)
    {
        return $this->modelClass::find($id);
    }

    public function firstOrCreate(array $attributes, array $values = [])
    {
        if(!is_null($ModelInstance = $this->modelClass::where($attributes)->first()))
        {
            return $ModelInstance;
        }
        return $this->setModelInstance($this->app->make($this->modelClass), $values + $attributes);
    }

    public function updateOrCreate(array $attributes, array $values = [])
    {
        if(!is_null($ModelInstance = $this->modelClass::where($attributes)->first()))
        {
            return $this->setModelInstance($ModelInstance, $values);
        }
        return $this->setModelInstance($this->app->make($this->modelClass), $values + $attributes);
    }






    public function where($key, $operator = null,$value = null)
    {
        $this->query->where($key,$operator,$value);
        return $this;
    }




    public function first()
    {
        return $this->query->first();
    }
    public function get()
    {
        return $this->query->get();
    }

    public function all()
    {
        return $this->modelClass::all();
    }

    public function getquery()
    {
        return $this->query;
    }
}
