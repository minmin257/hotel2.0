<?php


namespace App\Repositories;


use App\ATMInfo;

class AtmInfoRepository extends BaseRepository
{
    protected function model()
    {
        return ATMInfo::class;
    }

    protected function query()
    {
        return ATMInfo::query();
    }
}
