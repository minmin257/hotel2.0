<?php
    namespace App\Repositories;

    use App\Subject;

    class SubjectRepository extends BaseRepository
    {

        protected function model()
        {
            return Subject::class;
        }

        protected function query()
        {
            return Subject::query();
        }
    }
