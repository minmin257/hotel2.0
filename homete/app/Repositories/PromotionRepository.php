<?php


namespace App\Repositories;


use App\Promotion;

class PromotionRepository extends BaseRepository
{

    protected function model()
    {
        return Promotion::class;
    }

    protected function query()
    {
        return Promotion::query();
    }
}
