<?php
    namespace App\Repositories;

    use App\Order;

    class OrderRepository extends BaseRepository
    {

        protected function model()
        {
            return Order::class;
        }

        protected function query()
        {
            return Order::query();
        }
    }
