<?php


namespace App\Repositories;


use App\ExtraShop;

class ExtraShopRepository extends BaseRepository
{

    protected function model()
    {
        return ExtraShop::class;
    }

    protected function query()
    {
        return ExtraShop::query();
    }
}
