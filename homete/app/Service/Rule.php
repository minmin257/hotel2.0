<?php


namespace App\Service;


use App\Repositories\RuleRepository;

class Rule
{
    private $rule;
    public function __construct(RuleRepository $rule)
    {
        $this->rule = $rule;
    }

    public function get()
    {
        return $this->rule->first();
    }

    public function update($data)
    {
        $keys = [
            'id','DefaultExtraPeoplePrice','DefaultExtraBedPrice'
        ];
        $data = $data->only($keys);
        $this->rule->update($data['id'],collect($data)->except(['id'])->toArray());
    }
}
