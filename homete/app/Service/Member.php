<?php


namespace App\Service;


use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

use Tymon\JWTAuth\Facades\JWTAuth;

class Member
{
    public function __construct()
    {

    }



    private function guard()
    {
        return auth('member');
    }

    protected function respondWithToken($token):array
    {
        $user = $this->guard()->user();
        $Collection = new Collection();
        $Collection->put('Email',$user->email);
        $Collection->put('Name',$user->Name);
        $Collection->put('Sex',$user->Sex);
        $Collection->put('IdNumber',$user->IdNumber);
        $Collection->put('Phone',$user->Phone);
        $Collection->put('Area',$user->Area);

        return [
            'access_token' => $token,
            'Member'=> $Collection,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ];
    }

    public function Login($credentials):array {
        $credential = $credentials->only('email', 'password');
        if ($token = $this->guard()->attempt($credential)) {
            return $this->respondWithToken($token);
        }
        else{
            throw new \Exception('登入失敗，帳號密碼有誤');
        }
    }
    public function LoginById($id){
        $member = \App\Member::find($id);
        $token = $this->guard()->login($member);
        return $this->respondWithToken($token);
    }
    public function Create($credentials){
        $credential = $credentials->only('email','password','Name','Phone','Sex','IdNumber','Area');
        if($Member = \App\Member::Create($credential)){
            return $Member;
        }
        else{
            throw new \Exception('創建新用戶失敗');
        }
    }

    public function UpdateResetPassword($id,$password = null){
        $member = \App\Member::find($id);
        if($password){
            $member->update([
               'password' => bcrypt($password)
            ]);
            return $password;
        }
        else{
            //亂碼產生
            $password = $this->random_str(6);
            $member->update([
                'password' => bcrypt($password)
            ]);
            return $password;
        }
    }

    public function UpdateBasic($credentials){
        try {
            if(!$this->guard()->user()){
                throw new \Exception('尚未登入');
            }
            $member = tap($this->guard()->user())->update($credentials->only([
                'Name','Sex','Phone','Area'
            ]));
            $token = $this->guard()->login($member);
            return $this->respondWithToken($token);
        }
        catch(\Exception $e){
            return $e;
        }
    }

    public function UpdatePassword($credentials){
        try {
            if(!$this->guard()->user()){
                throw new \Exception('尚未登入');
            }
            $member = tap($this->guard()->user())->update([
                'password' => bcrypt($credentials['password'])
            ]);
            $token = $this->guard()->login($member);
            return $this->respondWithToken($token);
        }
        catch(\Exception $e){
            return $e;
        }
    }

    public function UpdateEmail($credentials){
        try {
            if(!$this->guard()->user()){
                throw new \Exception('尚未登入');
            }
            if(\App\Member::where('email',$credentials['email'])->where('id','<>',$this->guard()->user()->id)->first()){
                throw new \Exception('信箱已註冊，無法重複');
            }
            $member = tap($this->guard()->user())->update([
                'email' => $credentials['email']
            ]);
            $token = $this->guard()->login($member);
            return $this->respondWithToken($token);
        }
        catch(\Exception $e){
            return $e;
        }
    }

    public function OrderRecords()
    {
        if($user = $this->guard()->user()){
            return $user->OrderRecords;
        }
        throw new \Exception('尚未登入');
    }


    private function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }



    //後台管理用
    public function GetAll()
    {
        return \App\Member::all();
    }

    public function paginate($num,$Keyword = null)
    {
        $Keyword ? $Response = \App\Member::where('email','Like','%'.$Keyword.'%')
                                ->orwhere('Name','Like','%'.$Keyword.'%')
                                ->orwhere('Phone','Like','%'.$Keyword.'%')->paginate($num) : $Response = \App\Member::paginate($num);
        return $Response;
    }

    public function updateMembers($dataArray)
    {
        $keys = [
            'id','State','Note'
        ];
        $dataArray = collect($dataArray)->map(function ($data) use ($keys){
            return collect($data)->only($keys);
        });
        foreach ($dataArray as $data)
        {
            \App\Member::where('id',$data['id'])->first()->update(collect($data)->except(['id'])->toArray());
        }
    }

    public function GetMemberById($id)
    {
        $Member = \App\Member::findOrFail($id);
        return $Member;
    }

    public function updateMember($data)
    {
        $keys = [
            'id','Name','email','password','IdNumber','Phone','Sex','Area','State','Note'
        ];
        $data = collect($data)->only($keys);
        $Member = \App\Member::findOrFail($data['id']);
        $Member->update(collect($data)->except(['id','password'])->toArray());
        if($data['password']){
            $Member->update([
                'password' => bcrypt($data['password'])
            ]);
        }
        return $Member;
    }

    public function DeleteMember($data)
    {
        $keys = [
            'id'
        ];
        $data = collect($data)->only($keys);
        $Member = \App\Member::findOrFail($data['id']);
        $Member->delete();
    }
}
