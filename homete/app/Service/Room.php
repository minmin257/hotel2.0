<?php


namespace App\Service;


use App\Repositories\LicenseRepository;
use App\Repositories\RoomRepository;

class Room
{
    private $room,$license;
    public function __construct(RoomRepository $room,LicenseRepository $license)
    {
        $this->room = $room;
        $this->license = $license;
    }

    public function GetAll()
    {
        return $this->room->getquery()->with('Promotions')->get();
    }

    public function readById($id)
    {
        return $this->room->readById($id);
    }

    public function getExtra($id)
    {
        return $this->room->getquery()->with(['ExtraShops','ExtraCars'])->where('id',$id)->first();
    }

    public function create($data)
    {
        $keys = [
            'Name','PeopleNumber','MaxExtraPeopleNumber','DefaultPrice','DefaultNumber','MainPicture','Html'
        ];
        $keys = $this->MacroEarlyBird($keys);
        $data = $data->only($keys);
        return $this->room->create($data);
    }

    public function update($data){
        $keys = [
            'id','Name','PeopleNumber','MaxExtraPeopleNumber','DefaultPrice','DefaultNumber','MainPicture','Html'
        ];
        $keys = $this->MacroEarlyBird($keys);
        $data = $data->only($keys);
        $this->room->update($data['id'],collect($data)->except(['id'])->toArray());
    }
    public function updateRooms($dataArray)
    {
        $keys = [
            'id','Name','PeopleNumber','DefaultPrice','DefaultNumber'
        ];
        $keys = $this->MacroEarlyBird($keys);

        $dataArray = collect($dataArray)->map(function ($data) use ($keys){
            return collect($data)->only($keys);
        });
        foreach ($dataArray as $data)
        {
            $this->room->update($data['id'],collect($data)->except(['id'])->toArray());
        }
    }

    public function updateExtra($dataArray)
    {
        $this->room->getquery()->where('id',$dataArray->room_id)->first()->HasExtraShops()->delete();
        $this->room->getquery()->where('id',$dataArray->room_id)->first()->HasExtraCars()->delete();

        foreach ($dataArray->cars as $car){
            $this->room->getquery()->where('id',$dataArray->room_id)->first()->HasExtraCars()->Create([
                'extra_car_id' => $car
            ]);
        }
        foreach ($dataArray->shops as $shop){
            $this->room->getquery()->where('id',$dataArray->room_id)->first()->HasExtraShops()->Create([
                'extra_shop_id' => $shop
            ]);
        }

    }

    public function delete($id){
        $this->room->delete($id);
    }

    private function MacroEarlyBird($array)
    {
        if($this->license->check('早鳥折扣'))
        {
            array_push($array,'DefaultAllowEarlyBird');
            array_push($array,'DefaultEarlyBirdPrice');
            array_push($array,'DefaultEarlyBirdDays');
        }
        return $array;
    }
}
