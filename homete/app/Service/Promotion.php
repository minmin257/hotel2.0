<?php


namespace App\Service;


use App\Repositories\LicenseRepository;
use App\Repositories\PromotionRepository;

class Promotion
{
    private $promotion,$license;
    public function __construct(PromotionRepository $promotion,LicenseRepository $license)
    {
        $this->promotion = $promotion;
        $this->license = $license;
    }

    public function GetAll()
    {
        return $this->promotion->getquery()->orderBy('room_id')->orderBy('id')->with('Room')->get();
    }

    public function Getfilters():array
    {
        $Response = [];
        foreach(\App\Room::all() as $room){
            $Response[] = [
                'Room' => ['id' => $room->id, 'Name' => $room->Name],
                'Promotion' => ['id' => null, 'Name' => '基本方案']
            ];
            foreach ($room->Promotions as $promotion){
                $Response[] = [
                    'Room' => ['id' => $room->id, 'Name' => $room->Name],
                    'Promotion' => ['id' => $promotion->id, 'Name' => $promotion->Name]
                ];
            }
        }
        return $Response;
    }

    public function readById($id)
    {
        return $this->promotion->getquery()->with('Room')->find($id);
    }

    public function paginate($num)
    {

        return $this->promotion->getquery()->orderBy('room_id')->orderBy('id')->with('Room')->paginate($num);
    }

    public function update($data){
        $keys = [
            'id','Name','State','DefaultPrice','room_id'
        ];
        $keys = $this->MacroEarlyBird($keys);
        $data = $data->only($keys);
        $this->promotion->update($data['id'],collect($data)->except(['id'])->toArray());
    }

    public function updatePromotions($dataArray)
    {
        $keys = [
            'id','Name','State','DefaultPrice'
        ];
        $keys = $this->MacroEarlyBird($keys);
        $dataArray = collect($dataArray)->map(function ($data) use ($keys){
            return collect($data)->only($keys);
        });
        foreach ($dataArray as $data)
        {
            $this->promotion->update($data['id'],collect($data)->except(['id'])->toArray());
        }
    }

    public function create($data)
    {
        $keys = [
            'room_id','Name','State','DefaultPrice'
        ];
        $keys = $this->MacroEarlyBird($keys);
        $data = $data->only($keys);
        return $this->promotion->create($data);
    }

    public function delete($id){
        $this->promotion->delete($id);
    }

    private function MacroEarlyBird($array)
    {
        if($this->license->check('早鳥折扣'))
        {
            array_push($array,'DefaultAllowEarlyBird');
            array_push($array,'DefaultEarlyBirdPrice');
            array_push($array,'DefaultEarlyBirdDays');
        }
        return $array;
    }

}
