<?php


namespace App\Service;


use App\DailyPromotion;
use App\OrderDetailBooking;
use App\OrderDetailExtraCar;
use App\OrderDetailExtraShop;
use App\Promotion;
use App\DailyRoom;
use App\Repositories\LicenseRepository;
use App\Room;
use App\OrderDetail;
use App\RoomHasExtraCar;
use App\RoomHasExtraShop;
use App\Rule;
use App\ExtraShop;
use App\ExtraCar;
use App\DiscountCode;
use Carbon\CarbonPeriod;
use Illuminate\Container\Container;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class Order
{
    private $lists,$discount,$rules;
    protected  $ValidDiscountCode;
    public function __construct(Collection $lists,Collection $discount)
    {
        $this->lists = $lists;
        $this->discount = $discount;
        $this->rules = Rule::first();
    }

    private function CalcEarlyBirdPrice($date, $collect): float
    {
        if ($collect->AllowEarlyBird) {
            if (\Carbon\Carbon::parse($date)->diffInDays(\Carbon\Carbon::today()->startOfDay()) >= $collect->EarlyBirdDays) {
                return $collect->EarlyBirdPrice;
            }
            return $collect->Price;
        }
        return $collect->Price;
    }

    public function Valid()
    {
        try {
            $Total = 0;
            foreach ($this->lists as $list)
            {
                //檢查入住人數許可
                $totalPeople = $list['adults'] + ($list['childrens'] * $this->rules->DefaultChildrenTransRate) + ($list['babys'] * $this->rules->DefaultBabyTransRate);
                $Room = Room::whereRaw('PeopleNumber + MaxExtraPeopleNumber >=' . $totalPeople)->where('id',$list['room'])->firstOrFail();

                //檢查房間有空
                $periods = CarbonPeriod::create(\Carbon\Carbon::parse($list['startDate']), \Carbon\Carbon::parse($list['endDate'])->add(-1, 'days')->format('Y-m-d'));
                $ck_daily_rooms = true;
                foreach ($periods as $date) {
                    if (!$DailyRoom = DailyRoom::where('room_id', $Room->id)->where('Date', $date->format('Y-m-d'))->first()) {
                        $ck_daily_rooms = false;
                    } else {
                        $count = OrderDetailBooking::where('daily_room_id', $DailyRoom->id)->get()->count();
                        if (!($DailyRoom->Number > $count)) {
                            $ck_daily_rooms = false;
                        }
                    }
                }
                if($ck_daily_rooms){
                    $license = new LicenseRepository(new Container());
                    if($list['promotion'] != NULL)
                    {
                        //活動方案
                        $Promotion = Promotion::find($list['promotion']);
                        foreach ($periods as $date) {
                            $DailyRoom = DailyRoom::where('room_id', $Room->id)->where('Date', $date->format('Y-m-d'))->first();
                            if ($DailyPromotion = DailyPromotion::where('daily_room_id', $DailyRoom->id)->where('promotion_id', $Promotion->id)->first()) {
                                if ($license->check('早鳥折扣')) {
                                    $Total += $this->CalcEarlyBirdPrice($date, $DailyPromotion);
                                } else {
                                    $Total += $DailyPromotion->Price;
                                }
                            } //沒有就原價
                            else {
                                if ($license->check('早鳥折扣')) {
                                    $Total += $this->CalcEarlyBirdPrice($date, $DailyRoom);
                                } else {
                                    $Total += $DailyRoom->Price;
                                }
                            }
                        }
                    }
                    else
                    {
                        //基本方案
                        foreach ($periods as $date) {
                            $DailyRoom = DailyRoom::where('room_id', $Room->id)->where('Date', $date->format('Y-m-d'))->first();
                            if ($license->check('早鳥折扣')) {
                                $Total += $this->CalcEarlyBirdPrice($date, $DailyRoom);
                            } else {
                                $Total += $DailyRoom->Price;
                            }
                        }
                    }

                    //檢驗加購方式
                    if(($list['adults'] + $list['childrens']) > $Room->PeopleNumber)
                    {
                        if($list['Extra'])
                        {
                            //加床
                            $Total += $this->rules->DefaultExtraBedPrice * 1 * (\Carbon\Carbon::parse($list['endDate'])->diffInDays(\Carbon\Carbon::parse($list['startDate'])));
                            if($list['adults'] + $list['childrens'] - $Room->PeopleNumber - 1 > 0)
                            {
                                $Total += ($list['adults'] + $list['childrens'] - $Room->PeopleNumber - 1) * $this->rules->DefaultExtraPeoplePrice * (\Carbon\Carbon::parse($list['endDate'])->diffInDays(\Carbon\Carbon::parse($list['startDate'])));
                            }
                        }
                        else
                        {
                            //加人
                            $Total += ($list['adults'] + $list['childrens'] - $Room->PeopleNumber) * $this->rules->DefaultExtraPeoplePrice * (\Carbon\Carbon::parse($list['endDate'])->diffInDays(\Carbon\Carbon::parse($list['startDate'])));
                        }
                    }

                    //檢驗加購商品
                    foreach ($list['ExtraShops'] as $ExtraShop) {
                        if(!RoomHasExtraShop::where('room_id',$Room->id)->where('extra_shop_id',$ExtraShop['id'])->first())
                        {
                            throw new \Exception('訂房失敗，該房型不適用此加購方案');
                        }
                        else if($ExtraShop['Nights'] > \Carbon\Carbon::parse($list['endDate'])->diffInDays(\Carbon\Carbon::parse($list['startDate'])))
                        {
                            throw new \Exception('訂房失敗，加購商品 夜數超過上限');
                        }
                        else if($ExtraShop['Numbers'] > ($list['adults'] + $list['childrens']))
                        {
                            throw new \Exception('訂房失敗，加購商品 數量超過上限');
                        }
                        else
                        {
                            $Shop = ExtraShop::where('State',1)->where('id',$ExtraShop['id'])->firstOrFail();
                            $Total += $Shop->Price * $ExtraShop['Numbers'] * $ExtraShop['Nights'];
                        }
                    }

                    //檢驗加購搭乘
                    foreach ($list['ExtraCars'] as $ExtraCar) {
                        $Car = ExtraCar::where('State',1)->where('id',$ExtraCar['id'])->firstOrFail();
                        if(!RoomHasExtraCar::where('room_id',$Room->id)->where('extra_car_id',$ExtraCar['id'])->first())
                        {
                            throw new \Exception('訂房失敗，該房型不適用此接駁方案');
                        }
                        else if($ExtraCar['Numbers'] > $Car->Max)
                        {
                            throw new \Exception('訂房失敗，加購接駁 數量超過上限');
                        }
                        else
                        {
                            $ExtraCar['type'] == '1' ? $Total+= $ExtraCar['Numbers'] * $Car->Price : $Total+= $ExtraCar['Numbers'] * $Car->Price2;
                        }

                    }

                }
                else{
                    throw new \Exception('訂房失敗，請重新確認區間內是否仍有空房');
                }

            }

            //檢查折扣
            if($this->discount['Code'] != NULL)
            {
                $DiscountCode = DiscountCode::where('Code',$this->discount['Code'])->where('State',1)->where('Remains','>',0)
                    ->where('Allow_Start','<=',\Carbon\Carbon::now()->format('Y-m-d h:i:s'))
                    ->where('Allow_End','>=',\Carbon\Carbon::now()->format('Y-m-d h:i:s'))->first();
                if($DiscountCode)
                {
                    $checker = false;

                    foreach ($this->lists as $list)
                    {
                      if(\Carbon\Carbon::parse($list['startDate']) <= \Carbon\Carbon::parse($DiscountCode->Use_Start)
                          &&  \Carbon\Carbon::parse($list['endDate']) < \Carbon\Carbon::parse($DiscountCode->Use_End)
                          &&  \Carbon\Carbon::parse($list['endDate']) >= \Carbon\Carbon::parse($DiscountCode->Use_Start))
                      {
                          $checker = true;
                      }
                      else if(\Carbon\Carbon::parse($list['startDate']) >= \Carbon\Carbon::parse($DiscountCode->Use_Start)
                          &&  \Carbon\Carbon::parse($list['startDate']) < \Carbon\Carbon::parse($DiscountCode->Use_End)
                          &&  \Carbon\Carbon::parse($list['endDate']) >= \Carbon\Carbon::parse($DiscountCode->Use_End))
                      {
                          $checker = true;
                      }
                      else if(\Carbon\Carbon::parse($list['startDate']) < \Carbon\Carbon::parse($DiscountCode->Use_Start)
                          &&  \Carbon\Carbon::parse($list['endDate']) > \Carbon\Carbon::parse($DiscountCode->Use_End))
                      {
                          $checker = true;
                      }
                      else if(\Carbon\Carbon::parse($list['startDate']) > \Carbon\Carbon::parse($DiscountCode->Use_Start)
                          &&  \Carbon\Carbon::parse($list['endDate']) < \Carbon\Carbon::parse($DiscountCode->Use_End))
                      {
                          $checker = true;
                      }
                    }
                    if($checker)
                    {
                        $Total -= $DiscountCode->Discount;
                        $this->ValidDiscountCode = $DiscountCode;
                    }
                }
            }

            return $Total;
        }
        catch (\Exception $e)
        {
            throw $e;
        }
    }

    public function Create($Member = null,Collection $Booking,Collection $Guest,$TaxNumber = null,$Note = null)
    {
        \DB::beginTransaction();
        try {
            $TotalPrice = $this->Valid();
            $license = new LicenseRepository(new Container());

            $Order = \App\Order::Create([
                'Num' => sha1(md5(uniqid(mt_rand(), true))),
                'member_id' => $Member,
                'Booking_Email' => $Booking['Email'],
                'Booking_Name' => $Booking['Name'],
                'Booking_Sex' => $Booking['Sex'],
                'Booking_IdNumber' => $Booking['IdNumber'],
                'Booking_Phone' => $Booking['Phone'],
                'Booking_Area' => $Booking['Area'],
                'Guest_Email' => $Guest['Email'],
                'Guest_Name' => $Guest['Name'],
                'Guest_Sex' => $Guest['Sex'],
                'Guest_IdNumber' => $Guest['IdNumber'],
                'Guest_Phone' => $Guest['Phone'],
                'Guest_Area' => $Guest['Area'],
                'Code' => $this->ValidDiscountCode ? $this->ValidDiscountCode->Code : null,
                'CodeName' => $this->ValidDiscountCode ? $this->ValidDiscountCode->Name : null,
                'Discount' => $this->ValidDiscountCode ? $this->ValidDiscountCode->Discount : null,
                'TotalPrice' => $TotalPrice,
                'Note' => $Note,
                'TaxNumber' => $TaxNumber,
            ]);
            $this->ValidDiscountCode ? $this->ValidDiscountCode->decrement('Remains',1) : '';
            $Rule = Rule::first();
            foreach ($this->lists as $list)
            {
                $Price = 0;
                $Extra = null;
                $ExtraBed = 0;
                $ExtraPeople = 0;
                $ExtraBedPrice = $Rule->DefaultExtraBedPrice;
                $ExtraPeoplePrice = $Rule->DefaultExtraPeoplePrice;

                $Room = Room::find($list['room']);
                $periods = CarbonPeriod::create(\Carbon\Carbon::parse($list['startDate']), \Carbon\Carbon::parse($list['endDate'])->add(-1, 'days')->format('Y-m-d'));


                //Calc 房型活動基本價格
                if($list['promotion'])
                {
                    $Promotion = Promotion::find($list['promotion']);
                    foreach ($periods as $date) {
                        $DailyRoom = DailyRoom::where('room_id', $Room->id)->where('Date', $date->format('Y-m-d'))->first();
                        //Checker Full
                        $Total = $DailyRoom->Number ?? 0;
                        $Cancel = 0;
                        $Done = 0;
                        $UnCheck = 0;

                        //全部當日訂購名單
                        $OrderDetailBookings = $DailyRoom->OrderDetailBookings;
                        foreach ($OrderDetailBookings as $OrderDetailBooking)
                        {
                            $TempOrder = $OrderDetailBooking->Order;
                            if($TempOrder->Cancel)
                            {
                                $Cancel += 1;
                            }
                            else if($TempOrder->Paid)
                            {
                                $Done += 1;
                            }
                            else
                            {
                                $UnCheck += 1;
                            }
                        }
                        if($Total <= ($Done+$UnCheck))
                        {
                            throw  new \Exception('訂房失敗，此房型剩餘數量不足');
                        }

                        if ($DailyPromotion = DailyPromotion::where('daily_room_id', $DailyRoom->id)->where('promotion_id', $Promotion->id)->first()) {
                            if ($license->check('早鳥折扣')) {
                                $Price += $this->CalcEarlyBirdPrice($date, $DailyPromotion);
                            } else {
                                $Price += $DailyPromotion->Price;
                            }
                        } //沒有就原價
                        else {
                            if ($license->check('早鳥折扣')) {
                                $Price += $this->CalcEarlyBirdPrice($date, $DailyRoom);
                            } else {
                                $Price += $DailyRoom->Price;
                            }
                        }
                    }
                }
                else
                {
                    foreach ($periods as $date) {
                        $DailyRoom = DailyRoom::where('room_id', $Room->id)->where('Date', $date->format('Y-m-d'))->first();
                        //Checker Full
                        $Total = $DailyRoom->Number ?? 0;
                        $Cancel = 0;
                        $Done = 0;
                        $UnCheck = 0;

                        //全部當日訂購名單
                        $OrderDetailBookings = $DailyRoom->OrderDetailBookings;
                        foreach ($OrderDetailBookings as $OrderDetailBooking)
                        {
                            $TempOrder = $OrderDetailBooking->Order;
                            if($TempOrder->Cancel)
                            {
                                $Cancel += 1;
                            }
                            else if($TempOrder->Paid)
                            {
                                $Done += 1;
                            }
                            else
                            {
                                $UnCheck += 1;
                            }
                        }
                        if($Total <= ($Done+$UnCheck))
                        {
                            throw  new \Exception('訂房失敗，此房型剩餘數量不足');
                        }

                        if ($license->check('早鳥折扣')) {
                            $Price += $this->CalcEarlyBirdPrice($date, $DailyRoom);
                        } else {
                            $Price += $DailyRoom->Price;
                        }
                    }
                }

                //Calc 加購
                if(($list['adults'] + $list['childrens']) > $Room->PeopleNumber)
                {
                    $Extra = $list['Extra'];
                    if($list['Extra'])
                    {
                        //加床
                        $ExtraBed = 1;
                        if($list['adults'] + $list['childrens'] - $Room->PeopleNumber - 1 > 0)
                        {
                            $ExtraPeople = $list['adults'] + $list['childrens'] - $Room->PeopleNumber - 1;
                        }
                    }
                    else
                    {
                        //加人
                        $ExtraPeople = $list['adults'] + $list['childrens'];
                    }
                }

                $OrderDetail = OrderDetail::Create([
                    'order_id' => $Order->id,
                    'StartDate' => $list['startDate'],
                    'EndDate' => $list['endDate'],
                    'room_id' => $Room->id,
                    'promotion_id' => $list['promotion'],
                    'Price' => $Price,
                    'Adult' => $list['adults'],
                    'Child' => $list['childrens'],
                    'Baby' => $list['babys'],
                    'Extra' => $Extra,
                    'ExtraBed' => $ExtraBed,
                    'ExtraPeople' => $ExtraPeople,
                    'ExtraBedPrice' => $ExtraBedPrice,
                    'ExtraPeoplePrice' => $ExtraPeoplePrice,
                ]);

                foreach ($list['ExtraShops'] as $ExtraShop) {
                    $Shop = ExtraShop::where('State',1)->where('id',$ExtraShop['id'])->firstOrFail();
                    OrderDetailExtraShop::Create([
                        'order_detail_id' => $OrderDetail->id,
                        'extra_shop_id' => $Shop->id,
                        'Name' => $Shop->Name,
                        'Numbers' => $ExtraShop['Numbers'],
                        'Nights' => $ExtraShop['Nights'],
                        'Price' => $Shop->Price
                    ]);
                }

                foreach ($list['ExtraCars'] as $ExtraCar) {
                    $Car = ExtraCar::where('State',1)->where('id',$ExtraCar['id'])->firstOrFail();
                    $ExtraCar['type'] == '1' ? $Price = $Car->Price : $Price = $Car->Price2;
                    OrderDetailExtraCar::Create([
                       'order_detail_id' => $OrderDetail->id,
                       'extra_car_id' => $Car->id,
                       'Name' => $Car->Name,
                       'Numbers' => $ExtraCar['Numbers'],
                        'Price' => $Price,
                        'type' => $ExtraCar['type'],
                        'arrvdt' => $ExtraCar['arrvdt'] ? \Carbon\Carbon::parse($ExtraCar['arrvdt'])->format('Y-m-d h:i:s') : null,
                        'pickdt' => $ExtraCar['pickdt'] ? \Carbon\Carbon::parse($ExtraCar['pickdt'])->format('Y-m-d h:i:s') : null
                    ]);
                }

                //Create DailyRoom紀錄
                foreach ($periods as $date) {
                    $DailyRoom = DailyRoom::where('room_id', $Room->id)->where('Date', $date->format('Y-m-d'))->first();
                    OrderDetailBooking::Create([
                        'order_id' => $Order->id,
                        'order_detail_id' => $OrderDetail->id,
                        'daily_room_id' => $DailyRoom->id
                    ]);
                }
            }


            \DB::commit();
            return $Order;
        }
        catch (\Exception $e)
        {
            \DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }
}
