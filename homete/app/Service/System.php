<?php


namespace App\Service;


use App\Repositories\SystemRepository;

class System
{
    private $system;
    public function __construct(SystemRepository $system)
    {
        $this->system = $system;
    }

    public function get():array
    {
        return $this->system->first()->only(['SiteName','Src','MetaDescription','MetaKeyword','Addr','Tel','Email','Maintain','Message']);
    }

    public function update($data)
    {
        $keys = [
            'SiteName','Src','MetaDescription','MetaKeyword','Addr','Tel','Email','Maintain','Message'
        ];
        $data = $data->only($keys);
        return $this->system->update(1,$data);
    }
}
