<?php


namespace App\Service;
use App\OrderDetail;
use App\OrderDetailBooking;
use App\Repositories\LicenseRepository;
use App\RoomHasTag;
use App\Rule;
use App\Room;
use App\DailyPromotion;
use App\DailyRoom;
use App\Promotion;
use App\Tag;
use Carbon\CarbonPeriod;

class RoomSearching
{
    private $CheckIn,$Checkout,$adults,$childrens,$babys,$rules,$license;
    public function __construct($CheckIn,$Checkout,$adults = 1,$childrens = 0,$babys = 0)
    {
        $this->CheckIn = $CheckIn;
        $this->Checkout = $Checkout;
        $this->adults = $adults;
        $this->childrens = $childrens;
        $this->babys = $babys;
        $this->rules = Rule::first();
    }

    public function ResponseData(LicenseRepository $license):array
    {
        //第一步 計算人數
        $total = $this->adults + ($this->childrens * $this->rules->DefaultChildrenTransRate) + ($this->babys * $this->rules->DefaultBabyTransRate);

        //找出推薦房型
        $rooms = Room::whereRaw('PeopleNumber + MaxExtraPeopleNumber >=' . $total)->get();

        //排除 未設定基本檔 已訂間數大於基本間數
        //如果有設好 就會去看優惠有沒可用
        $periods = CarbonPeriod::create(\Carbon\Carbon::parse($this->CheckIn), \Carbon\Carbon::parse($this->Checkout)->add(-1, 'days')->format('Y-m-d'));
        $RoomData = [];

        //valid room from ck_daily_room
        $valid_rooms = [];

        foreach ($rooms as $room) {
            $ck_daily_rooms = true;
            //剩餘間數
            $numbers = $room->DefaultNumber ?? 0;
            foreach ($periods as $date) {
                if (!$DailyRoom = DailyRoom::where('room_id', $room->id)->where('Date', $date->format('Y-m-d'))->first()) {
                    $ck_daily_rooms = false;
                } else {
                    $count = OrderDetailBooking::where('daily_room_id', $DailyRoom->id)->get()->count();
                    $numbers = $numbers - $count;
                    if (!($DailyRoom->Number > $count)) {
                        $ck_daily_rooms = false;
                    }
                }
            }

            if ($ck_daily_rooms) {
                $valid_rooms[] = $room->id;
                //有設定每日
                $Promotions = Promotion::where('room_id', $room->id)->where('State', 1)->get();
                $PromotionsData = [];
                foreach ($Promotions as $Promotion) {
                    //每個活動進去推算 總共多少錢
                    $Total = 0;
                    $Errors = [];
                    foreach ($periods as $date) {
                        $DailyRoom = DailyRoom::where('room_id', $room->id)->where('Date', $date->format('Y-m-d'))->first();
                        if ($DailyPromotion = DailyPromotion::where('daily_room_id', $DailyRoom->id)->where('promotion_id', $Promotion->id)->first()) {
                            if ($license->check('早鳥折扣')) {
                                $Total += $this->CalcEarlyBirdPrice($date, $DailyPromotion);
                            } else {
                                $Total += $DailyPromotion->Price;
                            }
                        } //沒有就原價
                        else {
                            if ($license->check('早鳥折扣')) {
                                $Total += $this->CalcEarlyBirdPrice($date, $DailyRoom);
                            } else {
                                $Total += $DailyRoom->Price;
                            }


                            $Errors[] = $DailyRoom->Date;
                        }
                    }
                    $PromotionsData[] = [
                        'id' => $Promotion->id,
                        'Name' => $Promotion->Name,
                        'Note' => $Promotion->Note,
                        'Total' => $Total,
                        'Errors' => $Errors
                    ];
                }

                $Basic_Total = 0;
                foreach ($periods as $date) {
                    $DailyRoom = DailyRoom::where('room_id', $room->id)->where('Date', $date->format('Y-m-d'))->first();

                    if ($license->check('早鳥折扣')) {
                        $Basic_Total += $this->CalcEarlyBirdPrice($date, $DailyRoom);
                    } else {
                        $Basic_Total += $DailyRoom->Price;
                    }
                }

                //基本價格
                $PromotionsData[] = [
                    'id' => '',
                    'Name' => '基本方案',
                    'Note' => null,
                    'Total' => $Basic_Total,
                    'Errors' => []
                ];


                $RoomData[] = [
                    'room' => $room->only(['id','Name','PeopleNumber','Ping','MaxExtraPeopleNumber','MainPicture','Html']),
                    'numbers' => $numbers,
                    'Promotions' => $PromotionsData,
                    'Tags' => $room->Tags()->get(['Name','IconSrc'])->toArray()
                ];
            }

        }

        $tag_id = RoomHasTag::whereIn('room_id',$valid_rooms)->pluck('tag_id')->unique();
        $Tags = Tag::whereIn('id',$tag_id)->get(['Name','IconSrc']);
        $ResponseData = [
            'Tags' => $Tags,
            'Rooms' => $RoomData
        ];

        return $ResponseData;

    }

    private function CalcEarlyBirdPrice($date, $collect): float
    {
        if ($collect->AllowEarlyBird) {
            if (\Carbon\Carbon::parse($date)->diffInDays(\Carbon\Carbon::today()->startOfDay()) >= $collect->EarlyBirdDays) {
                return $collect->EarlyBirdPrice;
            }
            return $collect->Price;
        }
        return $collect->Price;
    }
}
