<?php


namespace App\Service;


class OrderMange
{
    //後台管理用
    public function GetAll()
    {
        return \App\Order::all();
    }

    public function paginate($num,$Keyword = null,$PromotionFilter = null,$StateFilter = null,$SortTime = null)
    {
        $SortTime ? $SortTime = 'desc' : $SortTime = 'asc';
        $Keyword ? $Response = \App\Order::where('Num','Like','%'.$Keyword.'%')
            ->orwhere('Booking_Email','Like','%'.$Keyword.'%')
            ->orwhere('Booking_Name','Like','%'.$Keyword.'%')
            ->orwhere('Booking_Phone','Like','%'.$Keyword.'%')
            ->orderBy('id',$SortTime)
            ->with(['OrderDetail.Promotion','OrderDetail.Room'])
            : $Response = \App\Order::orderBy('id',$SortTime)->with(['OrderDetail.Promotion','OrderDetail.Room']);
        if($PromotionFilter){
            $Room = $PromotionFilter['Room']['id'] ?? null;
            $Promotion = $PromotionFilter['Promotion']['id'] ?? null;

            $Room ? $Response = $Response->whereHas('OrderDetail', function ($q) use ($Room){
                $q->where('room_id', '=', $Room);
            }) : null;

            $Promotion ? $Response = $Response->whereHas('OrderDetail', function ($q) use ($Promotion){
                $q->where('promotion_id', '=', $Promotion);
            }) : $Response = $Response->whereHas('OrderDetail', function ($q) use ($Promotion){
                $q->where('promotion_id', '=', null);
            });
        }
        if($StateFilter){
            switch ($StateFilter)
            {
                case 'UnPaid':
                    $Response = $Response->where('Paid',0)->where('Cancel',0);
                    break;
                case 'Paid':
                    $Response = $Response->where('Paid',1)->where('Cancel',0);
                    break;
                case 'Cancel':
                    $Response = $Response->where('Cancel',1);
                    break;
                case 'UnRefund':
                    $Response = $Response->where('Paid',1)->where('Cancel',1)->where('Refund',0);
                    break;
                case 'Refund':
                    $Response = $Response->where('Paid',1)->where('Cancel',1)->where('Refund',1);
                    break;
                default:
                    break;
            }
        }

        return $Response->paginate($num);
    }

    public function GetOrderByNum($Num)
    {
        return \App\Order::where('Num',$Num)->with(['OrderDetail.Promotion','OrderDetail.Room','OrderDetail.Car','OrderDetail.Shop'])->firstOrfail();
    }

    public function UpdateOrder($data)
    {
        $keys = [
            'Num','Booking_Email','Booking_Name','Booking_Sex','Booking_IdNumber','Booking_Phone','Booking_Area','Guest_Email','Guest_Name','Guest_Sex','Guest_IdNumber','Guest_Phone','Guest_Area','payment_id','BackendNote','TaxNumber','Cancel','Paid','Refund'
        ];
        $data = $data->only($keys);
        try {
            $Order = \App\Order::where('Num',$data['Num'])->firstOrfail();
            $Order->update(collect($data)->except(['Num'])->toArray());
            return $Order;
        }
        catch (\Exception $e){
            return $e;
        }
    }
}
