<?php

    namespace App\Presenters;

    use App\Repositories\HomeBannerRepository;
    use App\Repositories\HomeAboutRepository;
    use App\Repositories\HomeNewsRepository;
    use App\Repositories\NewsRepository;
    use App\Repositories\HomeRoomRepository;
    use App\Repositories\RoomRepository;
    use App\Repositories\SystemRepository;
    use App\Repositories\SubjectRepository;
    use App\Repositories\LocalizationRepository;
    use App\Repositories\SocialLinkRepository;


    class HomePresenter
    {
        protected $HomeBanner, $HomeAbout, $HomeNews, $News, $HomeRoom, $Room, $System, $Subject, $Localization;

        public function __construct(SystemRepository $System, SubjectRepository $Subject, RoomRepository $Room, LocalizationRepository $Localization, SocialLinkRepository $SocialLink, HomeBannerRepository $HomeBanner, HomeAboutRepository $HomeAbout, HomeNewsRepository $HomeNews, NewsRepository $News, HomeRoomRepository $HomeRoom)
        {
            $this->System = $System;
            $this->Subject = $Subject;
            $this->Room = $Room;
            $this->Localization = $Localization;
            $this->SocialLink = $SocialLink;
            $this->HomeBanner = $HomeBanner;
            $this->HomeAbout = $HomeAbout;
            $this->HomeNews = $HomeNews;
            $this->News = $News;
            $this->HomeRoom = $HomeRoom;
        }

        public function getSystem()
        {
            return $this->System->first();
        }

        public function getSubject()
        {
            return $this->Subject->get();
        }

        public function getRooms()
        {
            return $this->Room->get();
        }

        public function getLocale()
        {
            return $this->Localization->get();
        }

        public function getSocialLink()
        {
            return $this->SocialLink->getquery()->where('State', 1)->orderBy('Sort', 'desc')->get();
        }

        public function getHomeBanner()
        {
            return $this->HomeBanner->getquery()->where('State', 1)->orderBy('Sort', 'desc')->get();
        }

        public function getHomeAbout()
        {
            return $this->HomeAbout->first();
        }

        public function getHomeNews()
        {
            return $this->HomeNews->first();
        }


        // NewsArea
        public function getNews()
        {
            return $this->News->getquery()->where('State', 1)->orderBy('Sort', 'desc')->take(4)->get();
        }


        // RoomsArea
        public function getHomeRoom()
        {
            return $this->HomeRoom->first();
        }

    }
