import VueRouter from 'vue-router'

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/webmin',
            name: 'Main',
            component: require('./components/children/Main').default,
            children:[
                {
                    meta: {
                        GuestOnly : true
                    },
                    path: 'login',
                    name: 'login',
                    component: require('./components/children/login').default
                },
                {
                    meta: {
                        requiresAuth : true
                    },
                    path: '/',
                    name: 'Auth',
                    component: require('./components/children/logined/Main').default,
                    children:[
                        {
                            path: 'index',
                            name: 'index',
                            component: require('./components/children/logined/index').default,
                        },
                        {
                            path: '/',
                            name: 'Blade',
                            component: require('./components/children/logined/Blade').default,
                            children:[
                                {
                                    path: 'Profile',
                                    name: 'Profile',
                                    component: require('./components/children/logined/children/Profile').default,
                                },
                                {
                                    path: 'Setting',
                                    name: 'Setting',
                                    component: require('./components/children/logined/children/Setting').default,
                                },
                                {
                                    path: 'DailyRoom/:id',
                                    name: 'DailyRoom',
                                    component: require('./components/children/logined/children/DailyRoom').default,
                                },
                                {
                                    path: 'DailyPromotion/:id',
                                    name: 'DailyPromotion',
                                    component: require('./components/children/logined/children/DailyPromotion').default,
                                },
                                {
                                    path: 'RemainRoom/:id',
                                    name: 'RemainRoom',
                                    component: require('./components/children/logined/children/RemainRoom').default,
                                },
                                {
                                    path: 'Rooms',
                                    name: 'Rooms',
                                    component: require('./components/children/logined/children/Rooms').default,
                                },
                                {
                                    path: 'Room/:id',
                                    name: 'Room',
                                    component: require('./components/children/logined/children/Room').default,
                                },
                                {
                                    path: 'Promotions',
                                    name: 'Promotions',
                                    component: require('./components/children/logined/children/Promotions').default,
                                },
                                {
                                    path: 'Promotion/:id',
                                    name: 'Promotion',
                                    component: require('./components/children/logined/children/Promotion').default,
                                },
                                {
                                    path: 'Tags',
                                    name: 'Tags',
                                    component: require('./components/children/logined/children/Tags').default,
                                },
                                {
                                    path: 'Rule',
                                    name: 'Rule',
                                    component: require('./components/children/logined/children/Rule').default,
                                },
                                {
                                    path: 'ExtraShops',
                                    name: 'ExtraShops',
                                    component: require('./components/children/logined/children/ExtraShops').default,
                                },
                                {
                                    path: 'ExtraShop/:id',
                                    name: 'ExtraShop',
                                    component: require('./components/children/logined/children/ExtraShop').default,
                                },
                                {
                                    path: 'ExtraCars',
                                    name: 'ExtraCars',
                                    component: require('./components/children/logined/children/ExtraCars').default,
                                },
                                {
                                    path: 'ExtraCar/:id',
                                    name: 'ExtraCar',
                                    component: require('./components/children/logined/children/ExtraCar').default,
                                },
                                {
                                    path: 'Extra/:id',
                                    name: 'Extra',
                                    component: require('./components/children/logined/children/Extra').default,
                                },

                                //試算用
                                {
                                    path: 'TestCalc',
                                    name: 'TestCalc',
                                    component: require('./components/children/logined/children/TestCalc').default,
                                },


                            ]
                        },
                    ]
                },
            ]
        }
    ]

})
