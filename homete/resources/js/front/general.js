import Axios from 'axios';

export function initialize(store, Routes) {
    if (store.getters.Member) {
        setAuthorization(store.getters.Member.token);
    }

    Routes.beforeEach((to,from,next) => {
        const MemberOnly = to.matched.some(record => record.meta.MemberOnly)
        const FrontGuestOnly = to.matched.some(record => record.meta.FrontGuestOnly)
        const currentMember = store.state.Member;

        if(MemberOnly && !currentMember){
            next({
                name: 'FrontLogin'
            });
        }else if(FrontGuestOnly && currentMember) {
            next({
                name: 'FrontProfile'
            });
        }
        else if(MemberOnly  && currentMember) {
            //檢查 token
            Axios.post('/api/Front/MemberChecker').then(res=>{
                next();
            }).catch(reason => {
                store.commit('Logout')
                next({
                    name: 'FrontLogin'
                });
            })

        }else{
            next();
        }
    });

    Axios.interceptors.response.use(null, (error)=>{
        if(error.response.status == 401){
            store.commit('Logout');
            Swal.fire({
                title:'提示',
                icon:'error',
                text:'時效過期，請重新登入',
                showCloseButton: true,
                confirmButtonColor: '#ba0000',
                confirmButtonText: '重新登入',
            }).then((result) => {
                Routes.push({
                    name:'FrontLogin'
                });
            })
        }
        return Promise.reject(error);
    });

}

export function setAuthorization(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
}
