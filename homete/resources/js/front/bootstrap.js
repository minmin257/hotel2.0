window._ = require('lodash');
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import Vue from 'vue';
import VueRouter from 'vue-router';
import swal from 'sweetalert2';


// window.Vue = Vue;
Vue.use(VueRouter);
window.Swal = swal;


