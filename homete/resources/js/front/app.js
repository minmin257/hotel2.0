

require('./bootstrap');
window.Vue = require('vue');
import MainApp from './components/MainApp.vue'; //主進入元件
import ShoppingCart from './components/ShoppingCart.vue'; //主進入元件
import router from './router.js';
import store from "./storage";
import { initialize } from './general.js';

var VueScrollTo = require('vue-scrollto');
Vue.use(VueScrollTo);
initialize(store,router);
const app = new Vue({
    el: '#app',
    components:{
        MainApp,
        ShoppingCart
    },
    router: router,
    store,
});

