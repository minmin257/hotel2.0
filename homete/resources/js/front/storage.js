import Vue from 'vue'
import Vuex from 'vuex'
import router from "../backend/router";
Vue.use(Vuex)
const ShoppingCart = getShoppingCart();
const SearchingForm = getSearchingForm();
const Member = getLocalMember();

export default new Vuex.Store({

    state:{
        ShoppingCart: ShoppingCart,
        Member: Member,
        SearchingForm:SearchingForm
    },
    actions:{
        LoginSucess(context,val){
            return new Promise((resolve, reject) => {
                context.commit('LoginSucess', val)
                resolve()
            })
        },
    },
    mutations:{
        Add (state, data) {
            state.ShoppingCart.push(Object.assign({},data))

            if(sessionStorage.getItem("SessionData")){
                let temp = JSON.parse(sessionStorage.getItem("SessionData"))
                temp.push(data);
                sessionStorage.setItem("SessionData",JSON.stringify(temp))
            }
            else
            {
                let SessionData = [];
                SessionData.push(data);
                sessionStorage.setItem("SessionData",JSON.stringify(SessionData))
            }
        },

        Remove(state, index){
            state.ShoppingCart.splice(index,1)
            if(sessionStorage.getItem("SessionData")){
                let temp = JSON.parse(sessionStorage.getItem("SessionData"))
                temp.splice(index,1);
                sessionStorage.setItem("SessionData",JSON.stringify(temp))
            }
        },

        Clear(state){
            state.ShoppingCart = []
            if(sessionStorage.getItem("SessionData")){
                sessionStorage.setItem("SessionData",JSON.stringify([]))
            }
        },

        SetSearchingForm(state, data){
            state.SearchingForm = data
            sessionStorage.setItem("SearchingForm",JSON.stringify(data));
        },

        LoginSucess(state,data){
            state.Member = Object.assign({}, data.Member, {token: data.access_token});
            sessionStorage.setItem("Member",JSON.stringify(state.Member));
            axios.defaults.headers.common["Authorization"] = `Bearer ${data.access_token}`;
        },

        Logout(state){
            sessionStorage.removeItem("Member");
            state.Member = null;
        },

        Refresh(state,token){
            state.Member.token = token;
            let Member = JSON.parse(sessionStorage.getItem("Member"));
            Member.token = token;
            sessionStorage.setItem("Member",JSON.stringify(Member));
            axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
        }
    },

    getters:{
        ShoppingCart(state){
            return state.ShoppingCart;
        },
        SearchingForm(state){
            return state.SearchingForm;
        },
        Member(state){
            return state.Member;
        }
    }
})


export function getShoppingCart(){
    const SessionData = sessionStorage.getItem("SessionData");
    if(!SessionData){
        return [];
    }
    return JSON.parse(SessionData)
}

export function getLocalMember(){
    const MemberStr = sessionStorage.getItem("Member");
    if(!MemberStr){
        return null;
    }
    return JSON.parse(MemberStr);
}

export function getSearchingForm(){
    const SearchingFormStr = sessionStorage.getItem("SearchingForm");
    if(!SearchingFormStr){
        return {};
    }
    return JSON.parse(SearchingFormStr);
}
