import Axios from 'axios';

export function initialize(store, Routes) {
    Routes.beforeEach((to,from,next) => {
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
        const GuestOnly = to.matched.some(record => record.meta.GuestOnly)
        const currentUser = store.state.currentUser;

        if(requiresAuth && !currentUser){
            next({
                name: 'login'
            });
        }else if(GuestOnly && currentUser){
            next({
                name: 'index'
            });
        }else{
            next();
        }
    });

    // Axios.interceptors.response.use(null, (error)=>{
    //     if(error.response.status == 401){
    //         store.commit('logout');
    //         // router.push('/login');
    //         Swal.fire({
    //             title:'提示',
    //             icon:'error',
    //             text:'時效過期，請重新登入',
    //             showCloseButton: true,
    //             confirmButtonColor: '#ba0000',
    //             confirmButtonText: '重新登入',
    //         }).then((result) => {
    //             Routes.push('/login');
    //         })
    //     }
    //     return Promise.reject(error);
    // });

    if (store.getters.currentUser) {
        setAuthorization(store.getters.currentUser.token);
    }
}

export function setAuthorization(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
}
