import store from './storage.js';
import router from './router.js';
window.axios = require('axios');

axios.interceptors.response.use(null, (error)=>{
    if(error.response.status == 401){
        // router.push('/login');
        Swal.fire({
            title:'提示',
            icon:'error',
            text:'時效過期，請重新登入',
            showCloseButton: true,
            confirmButtonColor: '#ba0000',
            confirmButtonText: '重新登入',
        }).then((result) => {
            store.commit('Logout');
            router.push({name: 'login'});
        })
    }
    else if(error.response.status == 403)
    {
        error.response.data.errors = {錯誤:['權限不足，請洽管理員']}
    }

    return Promise.reject(error);
});

if (store.getters.currentUser) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${store.getters.currentUser.token}`
}

// Config相關的 api
const ConfigRequest = '/api/Config';

// Setting相關的 api
const SettingRequest = '/api/Setting';

// Jwt 相關的 api
const JwtRequest = '/api/Jwt';

// Room 相關的 api
const RoomRequest = '/api/Room';

// DailyRoom 相關的 api
const DailyRoomRequest = '/api/DailyRoom';

// Promotion 相關的 api
const PromotionRequest = '/api/Promotion';

// Tag 相關的 api
const TagRequest = '/api/Tag';


//DailyPromotion 相關的 api
const DailyPromotionRequest = '/api/DailyPromotion';

//Remain 相關的 api
const RemainRequest = '/api/Remain';

//Rulte 相關的 api
const RuleRequest = '/api/Rule';

//ExtraShop 相關的 api
const ExtraShopRequest = '/api/ExtraShop';

//ExtraCar 相關的 api
const ExtraCarRequest = '/api/ExtraCar';

// 傳入 name 取得 Config
export function GetConfig(name){
    return new Promise((res,rej) =>{
        axios.get(ConfigRequest+"/"+name)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

// 傳入 group 取得 Setting
export function GetSetting(group,name = null) {
    return new Promise((res,rej) =>{
        axios.get(SettingRequest+"/"+group+"/"+name)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

// 傳入 name 取得 Config
export function LoginJwt(credential){
    return new Promise((res,rej) =>{
        axios.post(JwtRequest+"/login",credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export function GetRooms(){
    return new Promise((res,rej) =>{
        axios.get(RoomRequest)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function GetRoom(credential){
    return new Promise((res,rej) =>{
        axios.get(RoomRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function GetRoomExtra(credential){
    return new Promise((res,rej) =>{
        axios.get(RoomRequest+"/Extra",{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateRoomExtra(credential){
    return new Promise((res,rej) =>{
        axios.post(RoomRequest+"/Extra",credential )
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function CreateRoom(credential){
    return new Promise((res,rej) =>{
        axios.post(RoomRequest+'/Create',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function UpdateRooms(credential){
    return new Promise((res,rej) =>{
        axios.post(RoomRequest+'/UpdateRooms',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateRoom(credential){
    return new Promise((res,rej) =>{
        axios.post(RoomRequest+'/Update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function DeleteRoom(credential){
    return new Promise((res,rej) =>{
        axios.delete(RoomRequest+'/Delete', {data:credential})
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function GetDailyRoom(credential){
    return new Promise((res,rej) =>{
        axios.get(DailyRoomRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function UpdateOrCreateDailyRoom(credential){
    return new Promise((res,rej) =>{
        axios.post(DailyRoomRequest+'/UpdateOrCreate',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateOrCreateScopeDailyRoom(credential){
    return new Promise((res,rej) =>{
        axios.post(DailyRoomRequest+'/UpdateOrCreateScope',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function GetPromotions(credential){
    return new Promise((res,rej) =>{
        axios.get(PromotionRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function GetPromotion(credential){
    return new Promise((res,rej) =>{
        axios.get(PromotionRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdatePromotions(credential){
    return new Promise((res,rej) =>{
        axios.post(PromotionRequest+'/UpdatePromotions',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdatePromotion(credential){
    return new Promise((res,rej) =>{
        axios.post(PromotionRequest+'/Update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function DeletePromotion(credential){
    return new Promise((res,rej) =>{
        axios.delete(PromotionRequest+'/Delete', {data:credential})
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function CreatePromotion(credential){
    return new Promise((res,rej) =>{
        axios.post(PromotionRequest+'/Create',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}



export async function GetTags(credential){
    return new Promise((res,rej) =>{
        axios.get(TagRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function CreateTag(credential){
    return new Promise((res,rej) =>{
        axios.post(TagRequest+'/Create',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function DeleteTag(credential){
    return new Promise((res,rej) =>{
        axios.delete(TagRequest+'/Delete', {data:credential})
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateTags(credential){
    return new Promise((res,rej) =>{
        axios.post(TagRequest+'/UpdateTags',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}



export async function GetDailyPromotion(credential){
    return new Promise((res,rej) =>{
        axios.get(DailyPromotionRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function UpdateOrCreateDailyPromotion(credential){
    return new Promise((res,rej) =>{
        axios.post(DailyPromotionRequest+'/UpdateOrCreate',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateOrCreateScopeDailyPromotion(credential){
    return new Promise((res,rej) =>{
        axios.post(DailyPromotionRequest+'/UpdateOrCreateScope',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}



export async function GetRemainRoom(credential){
    return new Promise((res,rej) =>{
        axios.get(RemainRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}




export async function Init() {
    return new Promise((res,rej) =>{
        axios.get('/api/init')
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function Calc(credential) {
    return new Promise((res,rej) =>{
        axios.post('/api/calc',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export function StoreOrder(credential){
    return new Promise((res,rej) =>{
        axios.post('/api/order/store', credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}



export async function GetRules(){
    return new Promise((res,rej) =>{
        axios.get(RuleRequest)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateRules(credential){
    return new Promise((res,rej) =>{
        axios.post(RuleRequest,credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function GetExtraShop(credential){
    return new Promise((res,rej) =>{
        axios.get(ExtraShopRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function GetExtraShops(credential){
    return new Promise((res,rej) =>{
        axios.get(ExtraShopRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateExtraShops(credential){
    return new Promise((res,rej) =>{
        axios.post(ExtraShopRequest+'/UpdateExtraShops',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function UpdateExtraShop(credential){
    return new Promise((res,rej) =>{
        axios.post(ExtraShopRequest+'/Update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function CreateExtraShop(credential){
    return new Promise((res,rej) =>{
        axios.post(ExtraShopRequest+'/Create',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function DeleteExtraShop(credential){
    return new Promise((res,rej) =>{
        axios.delete(ExtraShopRequest+'/Delete', {data:credential})
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}



export async function GetExtraCar(credential){
    return new Promise((res,rej) =>{
        axios.get(ExtraCarRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function GetExtraCars(credential){
    return new Promise((res,rej) =>{
        axios.get(ExtraCarRequest,{ params:credential })
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function UpdateExtraCars(credential){
    return new Promise((res,rej) =>{
        axios.post(ExtraCarRequest+'/UpdateExtraCars',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
export async function UpdateExtraCar(credential){
    return new Promise((res,rej) =>{
        axios.post(ExtraCarRequest+'/Update',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function CreateExtraCar(credential){
    return new Promise((res,rej) =>{
        axios.post(ExtraCarRequest+'/Create',credential)
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}

export async function DeleteExtraCar(credential){
    return new Promise((res,rej) =>{
        axios.delete(ExtraCarRequest+'/Delete', {data:credential})
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}


export async function GetLicenses() {
    return new Promise((res,rej) =>{
        axios.get('/api/Licenses')
            .then(response =>{
                res(response.data);
            })
            .catch(error=>{
                rej(error.response);
            })
    });
}
