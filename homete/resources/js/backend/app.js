import Calendar from "./components/Calendar";

require('./bootstrap');

import MainApp from './components/MainApp.vue'; //主進入元件
import router from './router.js';
import store from './storage.js';
import { initialize } from './general.js';
Vue.component('Calendar', Calendar)

// import $ from 'jquery';

initialize(store,router);
const app = new Vue({
    el: '#app',
    components:{
        MainApp,
    },
    router,
    store,
});

