window._ = require('lodash');
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import swal from 'sweetalert2';
import VueFeather from 'vue-feather';


window.Vue = Vue;
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueFeather);
window.Swal = swal;


