import Vue from 'vue'
import Vuex from 'vuex'
import router from './router.js'
Vue.use(Vuex)

import {getLocalUser} from './helper.js';
const user = getLocalUser();
export default new Vuex.Store({

    state:{
        currentUser: user,
    },
    actions:{
        Login(context,val){
            return new Promise((resolve, reject) => {
                context.commit('Login', val)
                resolve()
            })
        },
    },

    mutations:{
        Login (state, data) {
            state.currentUser = Object.assign({},data.user,{token: data.access_token})
            localStorage.setItem("user",JSON.stringify(state.currentUser));
            axios.defaults.headers.common["Authorization"] = `Bearer ${data.access_token}`;
        },
        Logout (state){
            state.currentUser = null;
            localStorage.removeItem("user");
            router.go({
                name : 'login'
            })
        },
        Refresh(state,token){
            state.currentUser.token = token;
            let user = JSON.parse(sessionStorage.getItem("user"));
            user.token = token;
            sessionStorage.setItem("user",JSON.stringify(user));
            axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
        }
    },

    getters:{
        currentUser(state){
            return state.currentUser;
        },
    }
})
