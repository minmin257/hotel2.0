import Vue from 'vue'
import Vuex from 'vuex'
import router from './router.js'
Vue.use(Vuex)

import {getLocalUser} from './helper.js';
const user = getLocalUser();
export default new Vuex.Store({

    state:{
        currentUser: user,
    },

    mutations:{
        Login (state, data) {
            state.currentUser = Object.assign({},data.user,{token: data.access_token})
            localStorage.setItem("user",JSON.stringify(state.currentUser));
            router.go({
                name : 'index'
            })
        },
        Logout (state){
            state.currentUser = null;
            localStorage.removeItem("user");
            router.go({
                name : 'login'
            })
        }
    },

    getters:{
        currentUser(state){
            return state.currentUser;
        },
    }
})
