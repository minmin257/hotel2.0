@extends('front.layouts.default')
    @section('header')
        @include('front.layouts.header')
    @endsection
    @section('content')
            <main-app></main-app>
    @endsection

    @section('footer')
            @include('front.layouts.ShoppingCartFooter')
    @endsection

@section('js')
{{--    <script type="text/javascript" src="/js/front/vendor.js"></script>--}}
{{--    <script type="text/javascript" src="/js/front/manifest.js"></script>--}}
    <script type="text/javascript" src="/js/front/app.js"></script>
@endsection
