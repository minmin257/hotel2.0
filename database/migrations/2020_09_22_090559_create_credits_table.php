<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 暫無，預留信用卡回傳確認匯款
        Schema::create('credits', function (Blueprint $table) {
            $table->id();
            $table->boolean('Success');
            $table->string('OrderNum');
            $table->string('AMOUNT');
            $table->string('AUTHSTATUS');
            $table->string('AUTHCODE');
            $table->datetime('AUTHTIME');
            $table->string('AUTHMSG');
            $table->string('CARDNO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credits');
    }
}
