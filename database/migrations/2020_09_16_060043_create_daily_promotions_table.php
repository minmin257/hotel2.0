<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailyPromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 加購
        Schema::create('daily_promotions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('daily_room_id');
            $table->foreign('daily_room_id')->references('id')->on('daily_rooms')->cascadeOnDelete();
            $table->unsignedBigInteger('promotion_id');
            $table->foreign('promotion_id')->references('id')->on('promotions')->cascadeOnDelete();
            $table->unsignedDecimal('Price', 6, 2)->comment('活動價');

            $table->boolean('AllowEarlyBird')->default(0)->comment('允許使用早鳥');
            $table->unsignedDecimal('EarlyBirdPrice', 6, 2)->default(0)->comment('早鳥價');
            $table->integer('EarlyBirdDays')->default(0)->comment('早鳥天數');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_promotions');
    }
}
