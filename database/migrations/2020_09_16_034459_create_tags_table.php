<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 加購內容
        Schema::create('tags', function (Blueprint $table) {
            $table->id();
            $table->string('Name')->unique()->comment('標籤名稱');

            //預留
            $table->longText('IconSrc')->nullable()->comment('圖標顯示');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
    }
}
