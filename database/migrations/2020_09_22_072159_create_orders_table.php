<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('Num')->unique()->comment('訂房編號');
            $table->foreignId('member_id')->nullable()->constrained('members')->onDelete('cascade');

            $table->string('Booking_Email')->comment('訂購人Email');
            $table->string('Booking_Name')->comment('訂購人姓名');
            $table->boolean('Booking_Sex')->nullable()->comment('訂購人性別');
            $table->string('Booking_IdNumber')->comment('訂購人身份證字號');
            $table->string('Booking_Phone')->comment('訂購人電話');
            $table->string('Booking_Area')->nullable()->comment('訂購人地址');

            $table->string('Guest_Email')->comment('住房人Email');
            $table->string('Guest_Name')->comment('住房人姓名');
            $table->boolean('Guest_Sex')->nullable()->comment('住房人性別');
            $table->string('Guest_IdNumber')->comment('住房人身份證字號');
            $table->string('Guest_Phone')->comment('住房人電話');
            $table->string('Guest_Area')->nullable()->comment('住房人地址');

            $table->foreignId('payment_id')->nullable()->comment('付款方式')->constrained('payment_methods')->onDelete('cascade');
            $table->string('Code')->nullable()->comment('優惠代碼');
            $table->string('CodeName')->nullable()->comment('優惠名稱');
            $table->integer('Discount')->nullable()->comment('優惠金');
            $table->integer('TotalPrice')->comment('總應付價格');


            $table->longText('Note')->nullable();
            $table->longText('BackendNote')->nullable();
            $table->string('TaxNumber')->nullable()->comment('優惠代碼');


            $table->boolean('Cancel')->default(0);
            $table->boolean('Paid')->default(0)->comment('確認付款');
            $table->boolean('RequestPayment')->default(0)->comment('信用卡請款');
            $table->boolean('Refund')->default(0)->comment('退款');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
