<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExtraCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_cars', function (Blueprint $table) {
            $table->id();
            $table->string('Name');
            $table->longText('Src')->nullable();
            $table->longText('Html')->nullable();

            $table->unsignedInteger('Max')->default(1)->comment('最高可選購量');
            $table->unsignedDecimal('Price', 10, 2)->default(0)->comment('單趟加價');
            $table->unsignedDecimal('Price2', 10, 2)->default(0)->comment('來回加價');

            $table->integer('Sort')->default(0)->comment('順序');
            $table->boolean('State')->default(1)->comment('啟用狀態(0關閉/1顯示)');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_cars');
    }
}
