<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->string('email')->unique();
            $table->string('password')->comment('密碼');
            $table->string('Name');
            $table->boolean('Sex')->nullable();
            $table->string('IdNumber')->unique();
            $table->string('Phone');
            $table->string('Area')->nullable();

            $table->longText('Note')->nullable();
            $table->boolean('State')->default(1)->comment('開啟/關閉');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
