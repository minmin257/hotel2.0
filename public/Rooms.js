(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Rooms"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/HtmlManager.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/HtmlManager.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tinymce_tinymce__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tinymce/tinymce */ "./node_modules/tinymce/tinymce.js");
/* harmony import */ var tinymce_tinymce__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(tinymce_tinymce__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tinymce_tinymce_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @tinymce/tinymce-vue */ "./node_modules/@tinymce/tinymce-vue/lib/es2015/main/ts/index.js");
/* harmony import */ var tinymce_themes_silver_theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tinymce/themes/silver/theme */ "./node_modules/tinymce/themes/silver/theme.js");
/* harmony import */ var tinymce_themes_silver_theme__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tinymce_themes_silver_theme__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var tinymce_plugins_image__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tinymce/plugins/image */ "./node_modules/tinymce/plugins/image/index.js");
/* harmony import */ var tinymce_plugins_image__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_image__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var tinymce_plugins_media__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tinymce/plugins/media */ "./node_modules/tinymce/plugins/media/index.js");
/* harmony import */ var tinymce_plugins_media__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_media__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var tinymce_plugins_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tinymce/plugins/table */ "./node_modules/tinymce/plugins/table/index.js");
/* harmony import */ var tinymce_plugins_table__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_table__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var tinymce_plugins_lists__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tinymce/plugins/lists */ "./node_modules/tinymce/plugins/lists/index.js");
/* harmony import */ var tinymce_plugins_lists__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_lists__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var tinymce_plugins_contextmenu__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tinymce/plugins/contextmenu */ "./node_modules/tinymce/plugins/contextmenu/index.js");
/* harmony import */ var tinymce_plugins_contextmenu__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_contextmenu__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var tinymce_plugins_wordcount__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tinymce/plugins/wordcount */ "./node_modules/tinymce/plugins/wordcount/index.js");
/* harmony import */ var tinymce_plugins_wordcount__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_wordcount__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var tinymce_plugins_colorpicker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tinymce/plugins/colorpicker */ "./node_modules/tinymce/plugins/colorpicker/index.js");
/* harmony import */ var tinymce_plugins_colorpicker__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_colorpicker__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var tinymce_plugins_textcolor__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tinymce/plugins/textcolor */ "./node_modules/tinymce/plugins/textcolor/index.js");
/* harmony import */ var tinymce_plugins_textcolor__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_textcolor__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var tinymce_plugins_advlist__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tinymce/plugins/advlist */ "./node_modules/tinymce/plugins/advlist/index.js");
/* harmony import */ var tinymce_plugins_advlist__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_advlist__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var tinymce_plugins_autolink__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! tinymce/plugins/autolink */ "./node_modules/tinymce/plugins/autolink/index.js");
/* harmony import */ var tinymce_plugins_autolink__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_autolink__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var tinymce_plugins_link__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! tinymce/plugins/link */ "./node_modules/tinymce/plugins/link/index.js");
/* harmony import */ var tinymce_plugins_link__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_link__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var tinymce_plugins_print__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tinymce/plugins/print */ "./node_modules/tinymce/plugins/print/index.js");
/* harmony import */ var tinymce_plugins_print__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_print__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var tinymce_plugins_preview__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! tinymce/plugins/preview */ "./node_modules/tinymce/plugins/preview/index.js");
/* harmony import */ var tinymce_plugins_preview__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_preview__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var tinymce_plugins_hr__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! tinymce/plugins/hr */ "./node_modules/tinymce/plugins/hr/index.js");
/* harmony import */ var tinymce_plugins_hr__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_hr__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var tinymce_plugins_anchor__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! tinymce/plugins/anchor */ "./node_modules/tinymce/plugins/anchor/index.js");
/* harmony import */ var tinymce_plugins_anchor__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_anchor__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var tinymce_plugins_pagebreak__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! tinymce/plugins/pagebreak */ "./node_modules/tinymce/plugins/pagebreak/index.js");
/* harmony import */ var tinymce_plugins_pagebreak__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_pagebreak__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var tinymce_plugins_searchreplace__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! tinymce/plugins/searchreplace */ "./node_modules/tinymce/plugins/searchreplace/index.js");
/* harmony import */ var tinymce_plugins_searchreplace__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_searchreplace__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var tinymce_plugins_visualblocks__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! tinymce/plugins/visualblocks */ "./node_modules/tinymce/plugins/visualblocks/index.js");
/* harmony import */ var tinymce_plugins_visualblocks__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_visualblocks__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var tinymce_plugins_charmap__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! tinymce/plugins/charmap */ "./node_modules/tinymce/plugins/charmap/index.js");
/* harmony import */ var tinymce_plugins_charmap__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_charmap__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var tinymce_plugins_visualchars__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! tinymce/plugins/visualchars */ "./node_modules/tinymce/plugins/visualchars/index.js");
/* harmony import */ var tinymce_plugins_visualchars__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_visualchars__WEBPACK_IMPORTED_MODULE_22__);
/* harmony import */ var tinymce_plugins_code__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! tinymce/plugins/code */ "./node_modules/tinymce/plugins/code/index.js");
/* harmony import */ var tinymce_plugins_code__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_code__WEBPACK_IMPORTED_MODULE_23__);
/* harmony import */ var tinymce_plugins_fullscreen__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! tinymce/plugins/fullscreen */ "./node_modules/tinymce/plugins/fullscreen/index.js");
/* harmony import */ var tinymce_plugins_fullscreen__WEBPACK_IMPORTED_MODULE_24___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_fullscreen__WEBPACK_IMPORTED_MODULE_24__);
/* harmony import */ var tinymce_plugins_insertdatetime__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! tinymce/plugins/insertdatetime */ "./node_modules/tinymce/plugins/insertdatetime/index.js");
/* harmony import */ var tinymce_plugins_insertdatetime__WEBPACK_IMPORTED_MODULE_25___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_insertdatetime__WEBPACK_IMPORTED_MODULE_25__);
/* harmony import */ var tinymce_plugins_nonbreaking__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! tinymce/plugins/nonbreaking */ "./node_modules/tinymce/plugins/nonbreaking/index.js");
/* harmony import */ var tinymce_plugins_nonbreaking__WEBPACK_IMPORTED_MODULE_26___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_nonbreaking__WEBPACK_IMPORTED_MODULE_26__);
/* harmony import */ var tinymce_plugins_save__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! tinymce/plugins/save */ "./node_modules/tinymce/plugins/save/index.js");
/* harmony import */ var tinymce_plugins_save__WEBPACK_IMPORTED_MODULE_27___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_save__WEBPACK_IMPORTED_MODULE_27__);
/* harmony import */ var tinymce_plugins_directionality__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! tinymce/plugins/directionality */ "./node_modules/tinymce/plugins/directionality/index.js");
/* harmony import */ var tinymce_plugins_directionality__WEBPACK_IMPORTED_MODULE_28___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_directionality__WEBPACK_IMPORTED_MODULE_28__);
/* harmony import */ var tinymce_plugins_template__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! tinymce/plugins/template */ "./node_modules/tinymce/plugins/template/index.js");
/* harmony import */ var tinymce_plugins_template__WEBPACK_IMPORTED_MODULE_29___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_template__WEBPACK_IMPORTED_MODULE_29__);
/* harmony import */ var tinymce_plugins_paste__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! tinymce/plugins/paste */ "./node_modules/tinymce/plugins/paste/index.js");
/* harmony import */ var tinymce_plugins_paste__WEBPACK_IMPORTED_MODULE_30___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_paste__WEBPACK_IMPORTED_MODULE_30__);
/* harmony import */ var tinymce_plugins_textpattern__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! tinymce/plugins/textpattern */ "./node_modules/tinymce/plugins/textpattern/index.js");
/* harmony import */ var tinymce_plugins_textpattern__WEBPACK_IMPORTED_MODULE_31___default = /*#__PURE__*/__webpack_require__.n(tinymce_plugins_textpattern__WEBPACK_IMPORTED_MODULE_31__);
//
//
//
//
//
//
//
//
//
//
//
































/* harmony default export */ __webpack_exports__["default"] = ({
  name: "HtmlManager",
  components: {
    Editor: _tinymce_tinymce_vue__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  props: {
    //传入一个value，使组件支持v-model绑定
    value: {
      type: String,
      "default": ''
    },
    disabled: {
      type: Boolean,
      "default": false
    },
    plugins: {
      type: [String, Array],
      "default": 'advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking save table  directionality  template paste  textpattern'
    },
    toolbar: {
      type: [String, Array],
      "default": 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | \
                ullist numlist outdent indent | link image media | \
                textcolor'
    },
    height: {
      type: Number,
      "default": 300
    }
  },
  data: function data() {
    return {
      //初始化配置
      init: {
        language_url: '/tinymce/langs/zh_TW.js',
        language: 'zh_TW',
        height: this.height,
        skin_url: '/tinymce/skins/ui/oxide',
        icons_url: '/tinymce/icons/default/icons.js',
        plugins: this.plugins,
        toolbar: this.toolbar,
        branding: false,
        relative_urls: false,
        menubar: true,
        file_picker_callback: function file_picker_callback(callback, value, meta) {
          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
          var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;
          var type = 'image' === meta.filetype ? 'Images' : 'Files',
              url = '/filemanager?editor=tinymce5&type=' + type;
          tinyMCE.activeEditor.windowManager.openUrl({
            url: url,
            title: 'Filemanager',
            width: x * 0.8,
            height: y * 0.8,
            resizable: "yes",
            close_previous: "no",
            onMessage: function onMessage(api, message) {
              callback(message.content);
            }
          });
        },
        //如需ajax上传可参考https://www.tiny.cloud/docs/configure/file-image-upload/#images_upload_handler
        images_upload_handler: function images_upload_handler(blobInfo, success, failure) {
          var img = 'data:image/jpeg;base64,' + blobInfo.base64();
          success(img);
        }
      },
      myValue: this.value
    };
  },
  mounted: function mounted() {
    tinymce_tinymce__WEBPACK_IMPORTED_MODULE_0___default.a.init({});
  },
  methods: {
    //添加相关的事件，可用的事件参照文档=> https://github.com/tinymce/tinymce-vue => All available events
    //需要什么事件可以自己增加
    onClick: function onClick(e) {
      this.$emit('onClick', e, tinymce_tinymce__WEBPACK_IMPORTED_MODULE_0___default.a);
    },
    //可以添加一些自己的自定义事件，如清空内容
    clear: function clear() {
      this.myValue = '';
    }
  },
  watch: {
    value: function value(newValue) {
      this.myValue = newValue;
    },
    myValue: function myValue(newValue) {
      this.$emit('input', newValue);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HtmlManager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../HtmlManager */ "./resources/js/backend/components/HtmlManager.vue");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../api */ "./resources/js/backend/api.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../helper */ "./resources/js/backend/helper.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Rooms",
  components: {
    HtmlManager: _HtmlManager__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: ['Licenses'],
  data: function data() {
    return {
      form: {
        Title: '房型設置',
        Icon: 'settings'
      },
      Rooms: [],
      newRoom: {
        Name: '',
        PeopleNumber: 0,
        MaxExtraPeopleNumber: 0,
        DefaultPrice: 0,
        DefaultNumber: 0,
        MainPicture: '',
        Html: '',
        DefaultAllowEarlyBird: 0,
        DefaultEarlyBirdPrice: 0,
        DefaultEarlyBirdDays: 0
      },
      showModal: false,
      showModalStyle: {
        display: 'none'
      }
    };
  },
  watch: {
    showModal: function showModal(newValue) {
      if (newValue === true) {
        this.showModalStyle.display = 'block';
        document.getElementsByTagName("body")[0].className = "modal-open";
      } else {
        this.showModalStyle.display = 'none';
        document.getElementsByTagName("body")[0].className = "";
      }
    }
  },
  created: function created() {
    this.$emit('ChildUpdated', this.form);
    this.GetRooms();
  },
  methods: {
    GetRooms: function GetRooms() {
      var _this = this;

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["GetRooms"])().then(function (res) {
        _this.Rooms = res;

        _this.$nextTick(function () {
          feather.replace();
        });
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
      });
    },
    ClearNewRoom: function ClearNewRoom() {
      this.newRoom = {
        Name: '',
        PeopleNumber: 0,
        MaxExtraPeopleNumber: 0,
        DefaultPrice: 0,
        DefaultNumber: 0,
        MainPicture: '',
        Html: '',
        DefaultAllowEarlyBird: 0,
        DefaultEarlyBirdPrice: 0,
        DefaultEarlyBirdDays: 0
      };
    },
    CreateRoom: function CreateRoom() {
      var _this2 = this;

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["CreateRoom"])(this.newRoom).then(function (res) {
        _this2.showModal = !_this2.showModal;

        _this2.GetRooms();

        _this2.ClearNewRoom();
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
      });
    },
    UpdateRooms: function UpdateRooms() {
      var _this3 = this;

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["UpdateRooms"])(this.Rooms).then(function (res) {
        _this3.GetRooms();
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
      });
    },
    EditRoom: function EditRoom(Room) {
      this.$router.push({
        name: 'Room',
        params: {
          id: Room.id
        }
      });
    },
    DeleteRoom: function DeleteRoom(Room) {
      var _this4 = this;

      var credential = {
        room_id: Room.id
      };

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["DeleteRoom"])(credential).then(function (res) {
        _this4.GetRooms();
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
      });
    },
    OpenFileManger: function OpenFileManger(type) {
      var options = {};
      var route_prefix = options && options.prefix ? options.prefix : '/filemanager';
      var userStr = localStorage.getItem("user");

      if (userStr) {
        var token = 'Bearer ' + JSON.parse(userStr)['token'];
        window.open(route_prefix + '?type=' + type + '&token=' + token, 'FileManager', 'width=900,height=600');
      } else {
        window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
      }

      var self = this;

      window.SetUrl = function (items) {
        self.newRoom.MainPicture = items[0].url;
      };
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=style&index=0&id=02ecbddf&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=style&index=0&id=02ecbddf&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.vnis[data-v-02ecbddf]{\n    display: inline-flex;\n    width: 100%;\n}\n.wh-100[data-v-02ecbddf]{\n    width: 100%;\n    height: 100%;\n}\n.modal-enter[data-v-02ecbddf] {\n    top: -10px\n}\n.modal-enter-active[data-v-02ecbddf] {\n    transition: all .5s;\n}\n.modal-enter-to[data-v-02ecbddf]{\n    opacity: 1;\n}\n.modal-leave-active[data-v-02ecbddf] {\n    transition: all .5s;\n    opacity: 0;\n    top: -10px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=style&index=0&id=02ecbddf&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=style&index=0&id=02ecbddf&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Rooms.vue?vue&type=style&index=0&id=02ecbddf&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=style&index=0&id=02ecbddf&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/HtmlManager.vue?vue&type=template&id=0749edc2&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/HtmlManager.vue?vue&type=template&id=0749edc2&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "tinymce-editor" },
    [
      _c("editor", {
        attrs: { init: _vm.init, disabled: _vm.disabled, height: _vm.height },
        on: { onClick: _vm.onClick },
        model: {
          value: _vm.myValue,
          callback: function($$v) {
            _vm.myValue = $$v
          },
          expression: "myValue"
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=template&id=02ecbddf&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=template&id=02ecbddf&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "wh-100" },
    [
      _c("div", { staticClass: "col-md-12 col-12" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 col-md-12 col-lg-4 mb-3" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-main_color mr-2 openModal",
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    _vm.showModal = true
                  }
                }
              },
              [
                _vm._m(0),
                _vm._v("\n                    新增\n                ")
              ]
            ),
            _vm._v(" "),
            _c("span", { staticClass: "store-btn" }, [
              _c("input", {
                staticClass: "btn btn-success",
                attrs: { type: "button", value: " 儲存變更" },
                on: { click: _vm.UpdateRooms }
              }),
              _vm._v(" "),
              _c("i", { staticClass: "fa fa-star" })
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-12 col-12" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "table-responsive" }, [
            _c(
              "table",
              {
                staticClass:
                  "align-middle mb-0 table table-borderless table-striped table-hover font-style"
              },
              [
                _c("thead", [
                  _c(
                    "tr",
                    [
                      _c("th", { staticClass: "text-center " }, [
                        _vm._v("名稱")
                      ]),
                      _vm._v(" "),
                      _c("th", { staticClass: "text-center " }, [
                        _vm._v("人數")
                      ]),
                      _vm._v(" "),
                      _c("th", { staticClass: "text-center " }, [
                        _vm._v("預設基本價")
                      ]),
                      _vm._v(" "),
                      _c("th", { staticClass: "text-center " }, [
                        _vm._v("預設間數")
                      ]),
                      _vm._v(" "),
                      _vm.Licenses.includes("早鳥折扣")
                        ? [
                            _c("th", { staticClass: "text-center " }, [
                              _vm._v("預設早鳥")
                            ]),
                            _vm._v(" "),
                            _c("th", { staticClass: "text-center " }, [
                              _vm._v("預設早鳥價")
                            ]),
                            _vm._v(" "),
                            _c("th", { staticClass: "text-center " }, [
                              _vm._v("預設早鳥天數")
                            ])
                          ]
                        : _vm._e(),
                      _vm._v(" "),
                      _c("th", { staticClass: "text-center " }, [
                        _vm._v("編輯/刪除")
                      ])
                    ],
                    2
                  )
                ]),
                _vm._v(" "),
                _vm.Rooms.length > 0
                  ? _c(
                      "tbody",
                      _vm._l(_vm.Rooms, function(Room, index) {
                        return _c(
                          "tr",
                          [
                            _c(
                              "td",
                              {
                                staticClass: "text-md-center text-sm-left",
                                attrs: { "data-title": "名稱" }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: Room.Name,
                                      expression: "Room.Name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    "aria-describedby": ""
                                  },
                                  domProps: { value: Room.Name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        Room,
                                        "Name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass: "text-md-center text-sm-left",
                                attrs: { "data-title": "人數" }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: Room.PeopleNumber,
                                      expression: "Room.PeopleNumber"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "number",
                                    "aria-describedby": ""
                                  },
                                  domProps: { value: Room.PeopleNumber },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        Room,
                                        "PeopleNumber",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass: "text-md-center text-sm-left",
                                attrs: { "data-title": "預設基本價" }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: Room.DefaultPrice,
                                      expression: "Room.DefaultPrice"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "number",
                                    "aria-describedby": ""
                                  },
                                  domProps: { value: Room.DefaultPrice },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        Room,
                                        "DefaultPrice",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass: "text-md-center text-sm-left",
                                attrs: { "data-title": "預設間數" }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: Room.DefaultNumber,
                                      expression: "Room.DefaultNumber"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "number",
                                    "aria-describedby": ""
                                  },
                                  domProps: { value: Room.DefaultNumber },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        Room,
                                        "DefaultNumber",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _vm.Licenses.includes("早鳥折扣")
                              ? [
                                  _c(
                                    "td",
                                    {
                                      staticClass:
                                        "text-md-center text-sm-left",
                                      attrs: { "data-title": "預設早鳥" }
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass: "toggle-btn",
                                          attrs: { id: "status-toggle-btn" }
                                        },
                                        [
                                          _c("div", { staticClass: "on" }, [
                                            _vm._v("ON")
                                          ]),
                                          _vm._v(" "),
                                          _c("div", { staticClass: "off" }, [
                                            _vm._v("OFF")
                                          ]),
                                          _vm._v(" "),
                                          _c("input", {
                                            directives: [
                                              {
                                                name: "model",
                                                rawName: "v-model",
                                                value:
                                                  Room.DefaultAllowEarlyBird,
                                                expression:
                                                  "Room.DefaultAllowEarlyBird"
                                              }
                                            ],
                                            attrs: {
                                              type: "checkbox",
                                              "true-value": 0,
                                              "false-value": 1
                                            },
                                            domProps: {
                                              checked: Array.isArray(
                                                Room.DefaultAllowEarlyBird
                                              )
                                                ? _vm._i(
                                                    Room.DefaultAllowEarlyBird,
                                                    null
                                                  ) > -1
                                                : _vm._q(
                                                    Room.DefaultAllowEarlyBird,
                                                    0
                                                  )
                                            },
                                            on: {
                                              change: function($event) {
                                                var $$a =
                                                    Room.DefaultAllowEarlyBird,
                                                  $$el = $event.target,
                                                  $$c = $$el.checked ? 0 : 1
                                                if (Array.isArray($$a)) {
                                                  var $$v = null,
                                                    $$i = _vm._i($$a, $$v)
                                                  if ($$el.checked) {
                                                    $$i < 0 &&
                                                      _vm.$set(
                                                        Room,
                                                        "DefaultAllowEarlyBird",
                                                        $$a.concat([$$v])
                                                      )
                                                  } else {
                                                    $$i > -1 &&
                                                      _vm.$set(
                                                        Room,
                                                        "DefaultAllowEarlyBird",
                                                        $$a
                                                          .slice(0, $$i)
                                                          .concat(
                                                            $$a.slice($$i + 1)
                                                          )
                                                      )
                                                  }
                                                } else {
                                                  _vm.$set(
                                                    Room,
                                                    "DefaultAllowEarlyBird",
                                                    $$c
                                                  )
                                                }
                                              }
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("span")
                                        ]
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "td",
                                    {
                                      staticClass:
                                        "text-md-center text-sm-left",
                                      attrs: { "data-title": "預設早鳥價" }
                                    },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: Room.DefaultEarlyBirdPrice,
                                            expression:
                                              "Room.DefaultEarlyBirdPrice"
                                          }
                                        ],
                                        staticClass: "form-control",
                                        attrs: {
                                          type: "number",
                                          "aria-describedby": ""
                                        },
                                        domProps: {
                                          value: Room.DefaultEarlyBirdPrice
                                        },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              Room,
                                              "DefaultEarlyBirdPrice",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "td",
                                    {
                                      staticClass:
                                        "text-md-center text-sm-left",
                                      attrs: { "data-title": "預設早鳥天數" }
                                    },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: Room.DefaultEarlyBirdDays,
                                            expression:
                                              "Room.DefaultEarlyBirdDays"
                                          }
                                        ],
                                        staticClass: "form-control",
                                        attrs: {
                                          type: "number",
                                          "aria-describedby": ""
                                        },
                                        domProps: {
                                          value: Room.DefaultEarlyBirdDays
                                        },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              Room,
                                              "DefaultEarlyBirdDays",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ]
                                  )
                                ]
                              : _vm._e(),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticClass: "text-md-center text-sm-left",
                                attrs: { "data-title": "編輯/刪除" }
                              },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass: "btn-group-sm btn-group",
                                    attrs: { role: "group" }
                                  },
                                  [
                                    _c(
                                      "button",
                                      {
                                        staticClass: "active btn btn-focus",
                                        on: {
                                          click: function($event) {
                                            return _vm.EditRoom(Room)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", {
                                          staticClass: "icon-xs",
                                          attrs: { "data-feather": "edit" }
                                        })
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "button",
                                      {
                                        staticClass: "btn btn-focus",
                                        on: {
                                          click: function($event) {
                                            return _vm.DeleteRoom(Room)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", {
                                          staticClass: "icon-xs",
                                          attrs: { "data-feather": "trash-2" }
                                        })
                                      ]
                                    )
                                  ]
                                )
                              ]
                            )
                          ],
                          2
                        )
                      }),
                      0
                    )
                  : _vm._e()
              ]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("transition", { attrs: { name: "modal" } }, [
        _vm.showModal
          ? _c(
              "div",
              {
                staticClass: "modal fade",
                class: { show: _vm.showModal },
                style: [_vm.showModalStyle],
                attrs: {
                  id: "addModal",
                  tabindex: "-1",
                  role: "dialog",
                  "aria-labelledby": "addModalLabel",
                  "aria-hidden": "true"
                }
              },
              [
                _c(
                  "div",
                  { staticClass: "modal-dialog", attrs: { role: "document" } },
                  [
                    _c("div", { staticClass: "modal-content" }, [
                      _c("div", { staticClass: "modal-header" }, [
                        _c(
                          "h5",
                          {
                            staticClass: "modal-title",
                            attrs: { id: "addModalLabel" }
                          },
                          [_vm._v("新增房型")]
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "close",
                            attrs: { type: "button", "aria-label": "Close" },
                            on: {
                              click: function($event) {
                                _vm.showModal = false
                              }
                            }
                          },
                          [
                            _c("span", { attrs: { "aria-hidden": "true" } }, [
                              _vm._v("×")
                            ])
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "modal-body" },
                        [
                          _c("div", { staticClass: "row form-group" }, [
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("label", { attrs: { for: "" } }, [
                                _vm._v("名稱")
                              ]),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.newRoom.Name,
                                    expression: "newRoom.Name"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { type: "text" },
                                domProps: { value: _vm.newRoom.Name },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.newRoom,
                                      "Name",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row form-group" }, [
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("label", { attrs: { for: "" } }, [
                                _vm._v("人數")
                              ]),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.newRoom.PeopleNumber,
                                    expression: "newRoom.PeopleNumber"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "number",
                                  "aria-describedby": ""
                                },
                                domProps: { value: _vm.newRoom.PeopleNumber },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.newRoom,
                                      "PeopleNumber",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row form-group" }, [
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("label", { attrs: { for: "" } }, [
                                _vm._v("最大加人數")
                              ]),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.newRoom.MaxExtraPeopleNumber,
                                    expression: "newRoom.MaxExtraPeopleNumber"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "number",
                                  "aria-describedby": "",
                                  max: "1",
                                  min: "0"
                                },
                                domProps: {
                                  value: _vm.newRoom.MaxExtraPeopleNumber
                                },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.newRoom,
                                      "MaxExtraPeopleNumber",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row form-group" }, [
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("label", { attrs: { for: "" } }, [
                                _vm._v("預設基本價")
                              ]),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.newRoom.DefaultPrice,
                                    expression: "newRoom.DefaultPrice"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "number",
                                  "aria-describedby": ""
                                },
                                domProps: { value: _vm.newRoom.DefaultPrice },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.newRoom,
                                      "DefaultPrice",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row form-group" }, [
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("label", { attrs: { for: "" } }, [
                                _vm._v("預設間數")
                              ]),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.newRoom.DefaultNumber,
                                    expression: "newRoom.DefaultNumber"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "number",
                                  "aria-describedby": ""
                                },
                                domProps: { value: _vm.newRoom.DefaultNumber },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.newRoom,
                                      "DefaultNumber",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row form-group" }, [
                            _c("div", { staticClass: "col-md-12" }, [
                              _c("label", { attrs: { for: "img" } }, [
                                _vm._v("主要縮圖")
                              ]),
                              _vm._v(" "),
                              _c("img", {
                                staticClass: "img-responsive mb-3 w-100",
                                attrs: {
                                  src: _vm.newRoom.MainPicture,
                                  alt: "",
                                  id: "holder"
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-12" }, [
                              _c(
                                "button",
                                {
                                  staticClass: "form-control btn btn-warning",
                                  attrs: {
                                    type: "button",
                                    id: "lfm",
                                    "data-input": "thumbnail"
                                  },
                                  on: {
                                    click: function($event) {
                                      return _vm.OpenFileManger("image")
                                    }
                                  }
                                },
                                [
                                  _c("i", {
                                    staticClass: "icon-xs",
                                    attrs: { "data-feather": "upload-cloud" }
                                  }),
                                  _vm._v(" 圖片")
                                ]
                              ),
                              _vm._v(" "),
                              _c("input", {
                                attrs: {
                                  type: "hidden",
                                  id: "thumbnail",
                                  name: "filepath"
                                },
                                domProps: { value: _vm.newRoom.MainPicture }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row form-group" }, [
                            _c(
                              "div",
                              { staticClass: "col-md-12" },
                              [
                                _c("label", { attrs: { for: "" } }, [
                                  _vm._v("內文")
                                ]),
                                _vm._v(" "),
                                _vm.newRoom
                                  ? _c("html-manager", {
                                      ref: "editor",
                                      model: {
                                        value: _vm.newRoom.Html,
                                        callback: function($$v) {
                                          _vm.$set(_vm.newRoom, "Html", $$v)
                                        },
                                        expression: "newRoom.Html"
                                      }
                                    })
                                  : _vm._e()
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _vm.Licenses.includes("早鳥折扣")
                            ? [
                                _c("div", { staticClass: "row form-group" }, [
                                  _c("div", { staticClass: "col-md-12" }, [
                                    _c("label", { attrs: { for: "" } }, [
                                      _vm._v("預設早鳥")
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "select",
                                      {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value:
                                              _vm.newRoom.DefaultAllowEarlyBird,
                                            expression:
                                              "newRoom.DefaultAllowEarlyBird"
                                          }
                                        ],
                                        staticClass: "form-control",
                                        on: {
                                          change: function($event) {
                                            var $$selectedVal = Array.prototype.filter
                                              .call(
                                                $event.target.options,
                                                function(o) {
                                                  return o.selected
                                                }
                                              )
                                              .map(function(o) {
                                                var val =
                                                  "_value" in o
                                                    ? o._value
                                                    : o.value
                                                return val
                                              })
                                            _vm.$set(
                                              _vm.newRoom,
                                              "DefaultAllowEarlyBird",
                                              $event.target.multiple
                                                ? $$selectedVal
                                                : $$selectedVal[0]
                                            )
                                          }
                                        }
                                      },
                                      [
                                        _c(
                                          "option",
                                          { attrs: { value: "0" } },
                                          [_vm._v("關閉")]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "option",
                                          { attrs: { value: "1" } },
                                          [_vm._v("啟用")]
                                        )
                                      ]
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "row form-group" }, [
                                  _c("div", { staticClass: "col-md-12" }, [
                                    _c("label", { attrs: { for: "" } }, [
                                      _vm._v("預設早鳥價格")
                                    ]),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value:
                                            _vm.newRoom.DefaultEarlyBirdPrice,
                                          expression:
                                            "newRoom.DefaultEarlyBirdPrice"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        type: "number",
                                        "aria-describedby": ""
                                      },
                                      domProps: {
                                        value: _vm.newRoom.DefaultEarlyBirdPrice
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.newRoom,
                                            "DefaultEarlyBirdPrice",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "row form-group" }, [
                                  _c("div", { staticClass: "col-md-12" }, [
                                    _c("label", { attrs: { for: "" } }, [
                                      _vm._v("預設早鳥天數")
                                    ]),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value:
                                            _vm.newRoom.DefaultEarlyBirdDays,
                                          expression:
                                            "newRoom.DefaultEarlyBirdDays"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        type: "number",
                                        "aria-describedby": ""
                                      },
                                      domProps: {
                                        value: _vm.newRoom.DefaultEarlyBirdDays
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.newRoom,
                                            "DefaultEarlyBirdDays",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ])
                                ])
                              ]
                            : _vm._e()
                        ],
                        2
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "modal-footer position-relative" },
                        [
                          _c("input", {
                            staticClass: "btn btn-success submit-btn",
                            attrs: { type: "button", value: " 確定送出" },
                            on: { click: _vm.CreateRoom }
                          }),
                          _vm._v(" "),
                          _c("i", {
                            staticClass: "uil uil-message mr-1",
                            staticStyle: { right: "92px" }
                          })
                        ]
                      )
                    ])
                  ]
                )
              ]
            )
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("div", {
        staticClass: "modal-backdrop fade",
        class: { show: _vm.showModal },
        style: [_vm.showModalStyle]
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "btn-icon-wrapper pr-2 fw-500" }, [
      _c("i", { staticClass: "fas fa-plus" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/backend/components/HtmlManager.vue":
/*!*********************************************************!*\
  !*** ./resources/js/backend/components/HtmlManager.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HtmlManager_vue_vue_type_template_id_0749edc2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HtmlManager.vue?vue&type=template&id=0749edc2&scoped=true& */ "./resources/js/backend/components/HtmlManager.vue?vue&type=template&id=0749edc2&scoped=true&");
/* harmony import */ var _HtmlManager_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HtmlManager.vue?vue&type=script&lang=js& */ "./resources/js/backend/components/HtmlManager.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _HtmlManager_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HtmlManager_vue_vue_type_template_id_0749edc2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HtmlManager_vue_vue_type_template_id_0749edc2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "0749edc2",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/backend/components/HtmlManager.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/backend/components/HtmlManager.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/backend/components/HtmlManager.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HtmlManager_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./HtmlManager.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/HtmlManager.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HtmlManager_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/backend/components/HtmlManager.vue?vue&type=template&id=0749edc2&scoped=true&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/backend/components/HtmlManager.vue?vue&type=template&id=0749edc2&scoped=true& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HtmlManager_vue_vue_type_template_id_0749edc2_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./HtmlManager.vue?vue&type=template&id=0749edc2&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/HtmlManager.vue?vue&type=template&id=0749edc2&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HtmlManager_vue_vue_type_template_id_0749edc2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HtmlManager_vue_vue_type_template_id_0749edc2_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Rooms.vue":
/*!*****************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Rooms.vue ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Rooms_vue_vue_type_template_id_02ecbddf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Rooms.vue?vue&type=template&id=02ecbddf&scoped=true& */ "./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=template&id=02ecbddf&scoped=true&");
/* harmony import */ var _Rooms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Rooms.vue?vue&type=script&lang=js& */ "./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Rooms_vue_vue_type_style_index_0_id_02ecbddf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Rooms.vue?vue&type=style&index=0&id=02ecbddf&scoped=true&lang=css& */ "./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=style&index=0&id=02ecbddf&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Rooms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Rooms_vue_vue_type_template_id_02ecbddf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Rooms_vue_vue_type_template_id_02ecbddf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "02ecbddf",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/backend/components/children/logined/children/Rooms.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Rooms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Rooms.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Rooms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=style&index=0&id=02ecbddf&scoped=true&lang=css&":
/*!**************************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=style&index=0&id=02ecbddf&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Rooms_vue_vue_type_style_index_0_id_02ecbddf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Rooms.vue?vue&type=style&index=0&id=02ecbddf&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=style&index=0&id=02ecbddf&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Rooms_vue_vue_type_style_index_0_id_02ecbddf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Rooms_vue_vue_type_style_index_0_id_02ecbddf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Rooms_vue_vue_type_style_index_0_id_02ecbddf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Rooms_vue_vue_type_style_index_0_id_02ecbddf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Rooms_vue_vue_type_style_index_0_id_02ecbddf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=template&id=02ecbddf&scoped=true&":
/*!************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=template&id=02ecbddf&scoped=true& ***!
  \************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Rooms_vue_vue_type_template_id_02ecbddf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Rooms.vue?vue&type=template&id=02ecbddf&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/Rooms.vue?vue&type=template&id=02ecbddf&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Rooms_vue_vue_type_template_id_02ecbddf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Rooms_vue_vue_type_template_id_02ecbddf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);