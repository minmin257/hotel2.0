(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["TestCalc"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../api */ "./resources/js/backend/api.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../helper */ "./resources/js/backend/helper.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "TestCalc",
  data: function data() {
    return {
      rules: {},
      form: {
        aduits: 0,
        childs: 0,
        babys: 0,
        start: moment__WEBPACK_IMPORTED_MODULE_0___default()().format('YYYY-MM-DD'),
        end: moment__WEBPACK_IMPORTED_MODULE_0___default()().add(1, 'days').format('YYYY-MM-DD')
      },
      Rooms: [],
      Tags: [],
      filters: [],
      showModal: false,
      showModalStyle: {
        display: 'none'
      },
      Order: {
        name: '陳安安',
        phone: '123456789',
        promotion: '',
        price: '',
        room: ''
      }
    };
  },
  created: function created() {
    this.init();
  },
  watch: {
    form: {
      handler: function handler(newValue) {
        this.rooms(newValue);
      },
      deep: true
    },
    showModal: function showModal(newValue) {
      if (newValue === true) {
        this.showModalStyle.display = 'block';
        document.getElementsByTagName("body")[0].className = "modal-open";
      } else {
        this.showModalStyle.display = 'none';
        document.getElementsByTagName("body")[0].className = "";
      }
    }
  },
  methods: {
    init: function init() {
      var _this = this;

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["Init"])().then(function (res) {
        _this.rules = res['rules'];
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
      });
    },
    rooms: function rooms(credential) {
      var _this2 = this;

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["Calc"])(credential).then(function (res) {
        _this2.Rooms = res.Rooms;
        _this2.Tags = res.Tags;
      })["catch"](function (response) {
        this.Rooms = [];
        this.Tags = [];
      });
    },
    al: function al(Promotion, room) {
      var reallyPeople = parseInt(this.form.aduits) + parseInt(this.form.childs);

      if (reallyPeople > room.room.PeopleNumber) {
        alert('須加購1床' + (reallyPeople - room.room.PeopleNumber - 1) + '人 , 或加購 ' + (reallyPeople - room.room.PeopleNumber) + '人');
      } else {
        if (Promotion.Errors.length === 0) {
          this.Order.promotion = Promotion.id;
          this.Order.price = Promotion.Total;
          this.Order.room = room.room.id;
          this.showModal = true;
        }
      }
    },
    AddFilter: function AddFilter(tag) {
      if (this.filters.includes(tag)) {
        var index = this.filters.indexOf(tag);

        if (index > -1) {
          this.filters.splice(index, 1);
        }
      } else {
        this.filters.push(tag);
      }
    },
    Filter: function Filter(room) {
      var show = true;
      this.filters.forEach(function (tag) {
        if (room.Tags.indexOf(tag) === -1) {
          show = false;
        }
      });
      return show;
    },
    StoreOrder: function StoreOrder() {
      var _this3 = this;

      var credential = {
        promotion_id: this.Order.promotion,
        room_id: this.Order.room,
        adult: this.form.aduits,
        child: this.form.childs,
        start: this.form.start,
        end: this.form.end,
        name: this.Order.name,
        phone: this.Order.phone,
        price: this.Order.price
      };

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["StoreOrder"])(credential).then(function (res) {
        _this3.showModal = !_this3.showModal;
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
      });
    }
  },
  computed: {
    calcPeople: function calcPeople() {
      return parseFloat(this.form.aduits) + parseFloat(parseFloat(this.form.childs) * parseFloat(this.rules.DefaultChildrenTransRate)) + parseFloat(parseFloat(this.form.babys) * parseFloat(this.rules.DefaultBabyTransRate));
    } // Rooms:  function() {
    //     Calc(this.form).then((res) => {
    //         console.log(res)
    //         return res;
    //     }).catch(function (response) {
    //         return [];
    //     });
    // }

  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=style&index=0&id=76dcae70&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=style&index=0&id=76dcae70&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.promotion[data-v-76dcae70]{\n    background-color: #c8cddc;\n}\n.promotion[data-v-76dcae70]:hover{\n    background-color: #EDA024;\n}\n.tag[data-v-76dcae70]{\n    background-color: #c8cddc;\n}\n.tag[data-v-76dcae70]:hover{\n    background-color: #EDA024;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=style&index=0&id=76dcae70&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=style&index=0&id=76dcae70&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./TestCalc.vue?vue&type=style&index=0&id=76dcae70&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=style&index=0&id=76dcae70&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=template&id=76dcae70&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=template&id=76dcae70&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "w100" },
    [
      _c("div", { staticClass: "content" }, [
        _c("h4", [_vm._v("當前計算比例")]),
        _vm._v(" "),
        _c("h3", [
          _vm._v(
            "小孩人數轉換比例 " + _vm._s(_vm.rules.DefaultChildrenTransRate)
          )
        ]),
        _vm._v(" "),
        _c("h3", [
          _vm._v("嬰兒人數轉換比例 " + _vm._s(_vm.rules.DefaultBabyTransRate))
        ]),
        _vm._v(" "),
        _c("h3", [
          _vm._v("加人費用 " + _vm._s(_vm.rules.DefaultExtraPeoplePrice))
        ]),
        _vm._v(" "),
        _c("h3", [_vm._v("加床費用 " + _vm._s(_vm.rules.DefaultExtraBedPrice))])
      ]),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c("div", { staticClass: "search" }, [
        _c("label", { attrs: { for: "" } }, [_vm._v("大人")]),
        _vm._v(" "),
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.form.aduits,
                expression: "form.aduits"
              }
            ],
            on: {
              change: function($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function(o) {
                    return o.selected
                  })
                  .map(function(o) {
                    var val = "_value" in o ? o._value : o.value
                    return val
                  })
                _vm.$set(
                  _vm.form,
                  "aduits",
                  $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                )
              }
            }
          },
          [
            _c("option", { attrs: { value: "0" } }, [_vm._v("0")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "1" } }, [_vm._v("1")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "2" } }, [_vm._v("2")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "3" } }, [_vm._v("3")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "4" } }, [_vm._v("4")])
          ]
        ),
        _vm._v(" "),
        _c("label", { attrs: { for: "" } }, [_vm._v("小孩")]),
        _vm._v(" "),
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.form.childs,
                expression: "form.childs"
              }
            ],
            on: {
              change: function($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function(o) {
                    return o.selected
                  })
                  .map(function(o) {
                    var val = "_value" in o ? o._value : o.value
                    return val
                  })
                _vm.$set(
                  _vm.form,
                  "childs",
                  $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                )
              }
            }
          },
          [
            _c("option", { attrs: { value: "0" } }, [_vm._v("0")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "1" } }, [_vm._v("1")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "2" } }, [_vm._v("2")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "3" } }, [_vm._v("3")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "4" } }, [_vm._v("4")])
          ]
        ),
        _vm._v(" "),
        _c("label", { attrs: { for: "" } }, [_vm._v("嬰兒")]),
        _vm._v(" "),
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.form.babys,
                expression: "form.babys"
              }
            ],
            on: {
              change: function($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function(o) {
                    return o.selected
                  })
                  .map(function(o) {
                    var val = "_value" in o ? o._value : o.value
                    return val
                  })
                _vm.$set(
                  _vm.form,
                  "babys",
                  $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                )
              }
            }
          },
          [
            _c("option", { attrs: { value: "0" } }, [_vm._v("0")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "1" } }, [_vm._v("1")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "2" } }, [_vm._v("2")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "3" } }, [_vm._v("3")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "4" } }, [_vm._v("4")])
          ]
        ),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("label", { attrs: { for: "" } }, [_vm._v("入住")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.form.start,
              expression: "form.start"
            }
          ],
          attrs: { type: "date" },
          domProps: { value: _vm.form.start },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.form, "start", $event.target.value)
            }
          }
        }),
        _vm._v(" "),
        _c("label", { attrs: { for: "" } }, [_vm._v("退房")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.form.end,
              expression: "form.end"
            }
          ],
          attrs: { type: "date" },
          domProps: { value: _vm.form.end },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.form, "end", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "control" },
        [
          _c("h3", [_vm._v("推薦人數計算: " + _vm._s(_vm.calcPeople))]),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "tagsDash",
              staticStyle: { border: "black 1px solid", margin: "10px 0px" }
            },
            [
              _vm._l(_vm.Tags, function(tag, index) {
                return [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.filters,
                        expression: "filters"
                      }
                    ],
                    attrs: { type: "checkbox" },
                    domProps: {
                      value: tag,
                      checked: Array.isArray(_vm.filters)
                        ? _vm._i(_vm.filters, tag) > -1
                        : _vm.filters
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.filters,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = tag,
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.filters = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.filters = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.filters = $$c
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("label", { attrs: { for: "" } }, [_vm._v(_vm._s(tag))])
                ]
              })
            ],
            2
          ),
          _vm._v(" "),
          _vm._l(_vm.Rooms, function(room, index) {
            return _vm.Filter(room)
              ? _c(
                  "div",
                  {
                    key: index,
                    staticClass: "room",
                    staticStyle: {
                      border: "black 1px solid",
                      margin: "10px 0px"
                    }
                  },
                  [
                    _c(
                      "h4",
                      { staticStyle: { "background-color": "#5cb85c" } },
                      [_vm._v("房間名稱: " + _vm._s(room.room.Name))]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "promotions" },
                      _vm._l(room.Promotions, function(Promotion) {
                        return _c(
                          "div",
                          {
                            staticClass: "promotion",
                            staticStyle: {
                              border: "black 1px dashed",
                              margin: "15px 0px"
                            },
                            on: {
                              click: function($event) {
                                return _vm.al(Promotion, room)
                              }
                            }
                          },
                          [
                            _c("h4", [
                              _vm._v("活動:" + _vm._s(Promotion.Name))
                            ]),
                            _vm._v(" "),
                            _c("h4", [
                              _vm._v("試算:" + _vm._s(Promotion.Total))
                            ]),
                            _vm._v(" "),
                            Promotion.Errors.length > 0
                              ? _c(
                                  "span",
                                  [
                                    _vm._v("不適用:\n                        "),
                                    _vm._l(Promotion.Errors, function(Error) {
                                      return _c("h4", [_vm._v(_vm._s(Error))])
                                    })
                                  ],
                                  2
                                )
                              : _vm._e()
                          ]
                        )
                      }),
                      0
                    )
                  ]
                )
              : _vm._e()
          })
        ],
        2
      ),
      _vm._v(" "),
      _c("transition", { attrs: { name: "modal" } }, [
        _vm.showModal
          ? _c(
              "div",
              {
                staticClass: "modal fade",
                class: { show: _vm.showModal },
                style: [_vm.showModalStyle],
                attrs: {
                  id: "addModal",
                  tabindex: "-1",
                  role: "dialog",
                  "aria-labelledby": "addModalLabel",
                  "aria-hidden": "true"
                }
              },
              [
                _c(
                  "div",
                  { staticClass: "modal-dialog", attrs: { role: "document" } },
                  [
                    _c("div", { staticClass: "modal-content" }, [
                      _c("div", { staticClass: "modal-header" }, [
                        _c(
                          "h5",
                          {
                            staticClass: "modal-title",
                            attrs: { id: "addModalLabel" }
                          },
                          [_vm._v("訂房服務\n                        ")]
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "close",
                            attrs: { type: "button", "aria-label": "Close" },
                            on: {
                              click: function($event) {
                                _vm.showModal = false
                              }
                            }
                          },
                          [
                            _c("span", { attrs: { "aria-hidden": "true" } }, [
                              _vm._v("×")
                            ])
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "modal-body" }, [
                        _c("div", { staticClass: "row form-group" }, [
                          _c("div", { staticClass: "col-md-12" }, [
                            _c("label", { attrs: { for: "" } }, [
                              _vm._v("姓名")
                            ]),
                            _vm._v(" "),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.Order.name,
                                  expression: "Order.name"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { type: "text" },
                              domProps: { value: _vm.Order.name },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.Order,
                                    "name",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "row form-group" }, [
                          _c("div", { staticClass: "col-md-12" }, [
                            _c("label", { attrs: { for: "" } }, [
                              _vm._v("行動電話")
                            ]),
                            _vm._v(" "),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.Order.phone,
                                  expression: "Order.phone"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { type: "text" },
                              domProps: { value: _vm.Order.phone },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.Order,
                                    "phone",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "modal-footer position-relative" },
                        [
                          _c("input", {
                            staticClass: "btn btn-success submit-btn",
                            attrs: { type: "button", value: " 確定送出" },
                            on: { click: _vm.StoreOrder }
                          }),
                          _vm._v(" "),
                          _c("i", {
                            staticClass: "uil uil-message mr-1",
                            staticStyle: { right: "92px" }
                          })
                        ]
                      )
                    ])
                  ]
                )
              ]
            )
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("div", {
        staticClass: "modal-backdrop fade",
        class: { show: _vm.showModal },
        style: [_vm.showModalStyle]
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/TestCalc.vue":
/*!********************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/TestCalc.vue ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TestCalc_vue_vue_type_template_id_76dcae70_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TestCalc.vue?vue&type=template&id=76dcae70&scoped=true& */ "./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=template&id=76dcae70&scoped=true&");
/* harmony import */ var _TestCalc_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TestCalc.vue?vue&type=script&lang=js& */ "./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _TestCalc_vue_vue_type_style_index_0_id_76dcae70_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TestCalc.vue?vue&type=style&index=0&id=76dcae70&scoped=true&lang=css& */ "./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=style&index=0&id=76dcae70&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _TestCalc_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TestCalc_vue_vue_type_template_id_76dcae70_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TestCalc_vue_vue_type_template_id_76dcae70_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "76dcae70",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/backend/components/children/logined/children/TestCalc.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TestCalc_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./TestCalc.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TestCalc_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=style&index=0&id=76dcae70&scoped=true&lang=css&":
/*!*****************************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=style&index=0&id=76dcae70&scoped=true&lang=css& ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TestCalc_vue_vue_type_style_index_0_id_76dcae70_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./TestCalc.vue?vue&type=style&index=0&id=76dcae70&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=style&index=0&id=76dcae70&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TestCalc_vue_vue_type_style_index_0_id_76dcae70_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TestCalc_vue_vue_type_style_index_0_id_76dcae70_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TestCalc_vue_vue_type_style_index_0_id_76dcae70_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TestCalc_vue_vue_type_style_index_0_id_76dcae70_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TestCalc_vue_vue_type_style_index_0_id_76dcae70_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=template&id=76dcae70&scoped=true&":
/*!***************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=template&id=76dcae70&scoped=true& ***!
  \***************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TestCalc_vue_vue_type_template_id_76dcae70_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./TestCalc.vue?vue&type=template&id=76dcae70&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/TestCalc.vue?vue&type=template&id=76dcae70&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TestCalc_vue_vue_type_template_id_76dcae70_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TestCalc_vue_vue_type_template_id_76dcae70_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);