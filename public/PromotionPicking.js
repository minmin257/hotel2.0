(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["PromotionPicking"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/PromotionPicking.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/PromotionPicking.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Calendar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Calendar */ "./resources/js/front/components/layouts/Calendar.vue");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../api */ "./resources/js/front/api.js");
/* harmony import */ var _backend_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../backend/helper */ "./resources/js/backend/helper.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "PromotionPicking",
  props: {
    room: {
      required: true
    },
    In: {
      required: true
    },
    Out: {
      required: true
    },
    Adults: {
      required: true
    },
    Childrens: {
      required: true
    },
    Babys: {
      required: true
    }
  },
  created: function created() {
    if (this.room === undefined || this.In === undefined || this.Out === undefined || this.Adults === undefined || this.Childrens === undefined || this.Babys === undefined) {
      this.$router.push({
        name: 'Search'
      });
    }

    this.$emit('updateStep', 1);
  },
  data: function data() {
    return {
      startDate: this.In,
      endDate: this.Out
    };
  },
  components: {
    Calendar: _Calendar__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  computed: {
    Price: function Price() {
      return Math.min.apply(Math, this.room.Promotions.map(function (item) {
        return item.Total;
      }));
    }
  },
  methods: {
    PickedDate: function PickedDate(Room, startDate, endDate) {
      var _this = this;

      this.startDate = startDate;
      this.endDate = endDate;
      var form = {
        Room: Room.id,
        StartDate: startDate,
        EndDate: endDate
      };
      Object(_api__WEBPACK_IMPORTED_MODULE_1__["SearchingByRoom"])(form).then(function (res) {
        _this.room.numbers = res.numbers;
        _this.room.Promotions = res.Promotions; //在日曆選擇後 -> 變更剩餘間數 活動各價格

        _this.$emit('RoomPromotionChange', _this.room);
      })["catch"](function (rep) {
        return Object(_backend_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(rep);
      });
    },
    AddShoppingCart: function AddShoppingCart(Promotion, index) {
      var _this2 = this;

      var credential = {
        startDate: this.startDate,
        endDate: this.endDate,
        room: this.room,
        promotion: Promotion,
        adults: this.Adults,
        childrens: this.Childrens,
        babys: this.Babys
      };
      this.$store.commit('Add', credential); //animate

      this.$refs["btn_".concat(index)][0].classList.add('selected');
      this.$refs["btn_".concat(index)][0].innerHTML = '<i class="fas fa-check" aria-hidden="true"></i>已新增';
      setTimeout(function () {
        _this2.$refs["btn_".concat(index)][0].classList.remove('selected');

        _this2.$refs["btn_".concat(index)][0].innerHTML = '新增';
      }, 1000);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/PromotionPicking.vue?vue&type=template&id=b1dd466e&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/PromotionPicking.vue?vue&type=template&id=b1dd466e&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row warp_p" }, [
    _c("div", { staticClass: "col-md-12 col-sm-12" }, [
      _c("div", { attrs: { id: "hotel_booking_rooms_list" } }, [
        _c("div", { attrs: { id: "hotel_booking_search_results" } }, [
          _c("div", { staticClass: "room-list-item " }, [
            _c("div", { staticClass: "row flex-row" }, [
              _c("div", { staticClass: "col-md-4" }, [
                _c("figure", [
                  _c("a", { attrs: { href: "rooms-details.html" } }, [
                    _c("img", { attrs: { src: this.room.room.MainPicture } })
                  ])
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "col-md-5 d-flex flex-column justify-content-between"
                },
                [
                  _c("div", { staticClass: "room-details" }, [
                    _c("h2", { staticClass: "title" }, [
                      _c("a", { attrs: { href: "rooms-details.html" } }, [
                        _vm._v(_vm._s(this.room.room.Name))
                      ])
                    ]),
                    _vm._v(" "),
                    _c("p", [
                      _vm._v(
                        "為自己預約一場沉澱心靈的純粹之旅，每間 24 坪的套房，盡享靜謐舒適空間"
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "room-services" },
                      _vm._l(this.room.Tags, function(tag) {
                        return _c(
                          "div",
                          {
                            staticClass: "room-service-item",
                            attrs: { tooltip: tag.Name }
                          },
                          [_c("img", { attrs: { src: tag.IconSrc } })]
                        )
                      }),
                      0
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "toggle-room-breakpoint room-more-details",
                      attrs: {
                        href: "#more-details" + this.room.room.id,
                        "data-toggle": "collapse"
                      }
                    },
                    [
                      _vm._v("\n                                    詳細內容 "),
                      _c("img", {
                        attrs: {
                          src:
                            "https://img.icons8.com/fluent-systems-filled/11/858a99/expand-arrow.png",
                          alt: ""
                        }
                      })
                    ]
                  )
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-3" }, [
                _c("div", { staticClass: "room-price-details" }, [
                  _c("div", { staticClass: "room-price-search" }, [
                    _c("div", { staticClass: "room-price" }, [
                      _c("div", {}, [
                        _c("span", { staticClass: "price-number" }, [
                          _c("strong", [_vm._v("NT$ " + _vm._s(_vm.Price))])
                        ]),
                        _vm._v(" "),
                        _c("span", { staticClass: "per-night-text" }, [
                          _vm._v(" 起")
                        ])
                      ])
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "collapse",
                staticStyle: { padding: "0 20px 20px 20px" },
                attrs: { id: "more-details" + this.room.room.id }
              },
              [
                _c(
                  "div",
                  {
                    staticClass: "calendar_availability calendar-first pb-4",
                    attrs: { id: "calendar_first" }
                  },
                  [
                    _vm._m(0),
                    _vm._v(" "),
                    _c("Calendar", {
                      attrs: {
                        In: _vm.startDate,
                        Out: _vm.endDate,
                        Room: this.room.room
                      },
                      on: { PickedDate: _vm.PickedDate }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c("div", {
                  staticClass: "textframeEditor",
                  staticStyle: {
                    "font-size": "14px",
                    "line-height": "25px",
                    "letter-spacing": ".025rem"
                  },
                  domProps: { innerHTML: _vm._s(this.room.room.Html) }
                })
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "DeicountPlanPage font-14" }, [
              _c("table", { staticClass: "table mb-0" }, [
                _vm._m(1),
                _vm._v(" "),
                _vm._m(2),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.room.Promotions, function(Promotion, index) {
                    return _c("tr", { key: Promotion.id }, [
                      _c(
                        "th",
                        { staticClass: "text-center", attrs: { scope: "row" } },
                        [_vm._v(_vm._s(Promotion.Name))]
                      ),
                      _vm._v(" "),
                      _c("td", {}, [
                        _vm._v(_vm._s(Promotion.Note ? Promotion.Note : "-"))
                      ]),
                      _vm._v(" "),
                      _c("td", {}, [_vm._v("NT$ " + _vm._s(Promotion.Total))]),
                      _vm._v(" "),
                      _c("td", { staticClass: "text-center" }, [
                        _c(
                          "button",
                          {
                            ref: "btn_" + index,
                            refInFor: true,
                            staticClass: "button eb-btn AddRooms_cart w-100",
                            attrs: { type: "button" },
                            on: {
                              click: function($event) {
                                return _vm.AddShoppingCart(Promotion, index)
                              }
                            }
                          },
                          [_vm._v("新增")]
                        )
                      ])
                    ])
                  }),
                  0
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12" },
              [
                _c("div", { staticClass: "row mt-5" }, [
                  _c(
                    "div",
                    {
                      staticClass:
                        "col-12 col-push-0 col-xs-6 col-xs-push-6 col-sm-4 col-sm-push-4 col-sm-offset-2 col-md-3 col-md-push-3 col-md-offset-3 mb-10"
                    },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "button eb-btn w-100",
                          attrs: { to: { name: "Initialise" }, title: "" }
                        },
                        [_vm._v("確認訂房")]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "col-12 col-pull-0 col-xs-6 col-xs-pull-6 col-sm-4 col-sm-pull-4 col-md-3 col-md-pull-3 mb-10"
                    },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "button back-step-btn w-100",
                          attrs: { to: { name: "Search" }, title: "" }
                        },
                        [_vm._v("返回")]
                      )
                    ],
                    1
                  )
                ])
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "availability_rooms_tips d-flex justify-content-end mb-4"
      },
      [
        _c("div", { staticClass: "availability_rooms" }, [_vm._v("尚有空房")]),
        _vm._v(" "),
        _c("div", { staticClass: "not_availability_rooms" }, [
          _vm._v("尚無空房")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", { staticClass: "lg-Tab" }, [
      _c("tr", [
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("方案")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("說明")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("方案價格")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", { staticClass: "sm-Tab" }, [
      _c("tr", [
        _c(
          "th",
          { staticClass: "text-center", attrs: { scope: "col", colspan: "4" } },
          [_vm._v("選擇方案")]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/front/components/layouts/PromotionPicking.vue":
/*!********************************************************************!*\
  !*** ./resources/js/front/components/layouts/PromotionPicking.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PromotionPicking_vue_vue_type_template_id_b1dd466e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PromotionPicking.vue?vue&type=template&id=b1dd466e&scoped=true& */ "./resources/js/front/components/layouts/PromotionPicking.vue?vue&type=template&id=b1dd466e&scoped=true&");
/* harmony import */ var _PromotionPicking_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PromotionPicking.vue?vue&type=script&lang=js& */ "./resources/js/front/components/layouts/PromotionPicking.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PromotionPicking_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PromotionPicking_vue_vue_type_template_id_b1dd466e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PromotionPicking_vue_vue_type_template_id_b1dd466e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "b1dd466e",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/front/components/layouts/PromotionPicking.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/front/components/layouts/PromotionPicking.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/PromotionPicking.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PromotionPicking_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PromotionPicking.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/PromotionPicking.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PromotionPicking_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/front/components/layouts/PromotionPicking.vue?vue&type=template&id=b1dd466e&scoped=true&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/PromotionPicking.vue?vue&type=template&id=b1dd466e&scoped=true& ***!
  \***************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PromotionPicking_vue_vue_type_template_id_b1dd466e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PromotionPicking.vue?vue&type=template&id=b1dd466e&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/PromotionPicking.vue?vue&type=template&id=b1dd466e&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PromotionPicking_vue_vue_type_template_id_b1dd466e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PromotionPicking_vue_vue_type_template_id_b1dd466e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);