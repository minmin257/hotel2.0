(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Initialise"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/ExtraCar.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/ExtraCar.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../api */ "./resources/js/front/api.js");
/* harmony import */ var _backend_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../backend/helper */ "./resources/js/backend/helper.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ExtraCar",
  props: {
    index: {
      required: true
    },
    Room: {
      required: true
    },
    propPicked: {
      type: Array
    },
    propShow: {
      "default": false
    }
  },
  data: function data() {
    return {
      List: [],
      Picked: this.propPicked,
      showModal: this.propShow
    };
  },
  watch: {
    propShow: function propShow(val) {
      this.showModal = val;
    },
    List: {
      deep: true,
      handler: function handler(val) {
        var checked = val.filter(function (item) {
          return item.checked === true;
        });
        this.$emit('UpdatePicked', this.index, checked);
      }
    }
  },
  created: function created() {
    this.GetRoomExtraCars();
  },
  methods: {
    GetRoomExtraCars: function GetRoomExtraCars() {
      var _this = this;

      var credential = {
        Room: this.Room.id
      };
      Object(_api__WEBPACK_IMPORTED_MODULE_0__["ExtraCarFromRoom"])(credential).then(function (res) {
        var temp = [];
        res.forEach(function (item) {
          return temp.push({
            Html: item.Html,
            Name: item.Name,
            Price: item.Price,
            Price2: item.Price2,
            Max: item.Max,
            Src: item.Src,
            id: item.id,
            type: "1",
            checked: false,
            Numbers: 0,
            arrvdt: "",
            pickdt: ""
          });
        });
        _this.List = temp;
      })["catch"](function (rep) {
        return Object(_backend_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertErrorMessage"])(rep);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/ExtraShopList.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/ExtraShopList.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../api */ "./resources/js/front/api.js");
/* harmony import */ var _backend_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../backend/helper */ "./resources/js/backend/helper.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "ExtraShopList",
  props: {
    index: {
      required: true
    },
    Room: {
      required: true
    },
    propPicked: {
      type: Array
    },
    propNights: {
      "default": 1
    },
    propAdults: {
      "default": 1
    },
    propChildrens: {
      "default": 0
    },
    propBabys: {
      "default": 0
    },
    propShow: {
      "default": false
    }
  },
  data: function data() {
    return {
      List: [],
      Picked: this.propPicked,
      Nights: this.propNights,
      Adults: this.propAdults,
      Childrens: this.propChildrens,
      Babys: this.propBabys,
      showModal: this.propShow
    };
  },
  watch: {
    propShow: function propShow(val) {
      this.showModal = val;
    },
    List: {
      deep: true,
      handler: function handler(val) {
        var checked = val.filter(function (item) {
          return item.checked === true;
        });
        this.$emit('UpdatePicked', this.index, checked);
      }
    }
  },
  created: function created() {
    this.GetRoomExtraShops();
  },
  methods: {
    GetRoomExtraShops: function GetRoomExtraShops() {
      var _this = this;

      var credential = {
        Room: this.Room.id
      };
      Object(_api__WEBPACK_IMPORTED_MODULE_0__["ExtraShopFromRoom"])(credential).then(function (res) {
        var temp = [];
        res.forEach(function (item) {
          return temp.push({
            Html: item.Html,
            Name: item.Name,
            Price: item.Price,
            Src: item.Src,
            id: item.id,
            checked: false,
            Nights: 0,
            Numbers: 0
          });
        });
        _this.List = temp;
      })["catch"](function (rep) {
        return Object(_backend_helper__WEBPACK_IMPORTED_MODULE_1__["SwalAlertErrorMessage"])(rep);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/Initialise.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/Initialise.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ExtraShopList__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExtraShopList */ "./resources/js/front/components/layouts/ExtraShopList.vue");
/* harmony import */ var _ExtraCar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ExtraCar */ "./resources/js/front/components/layouts/ExtraCar.vue");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../api */ "./resources/js/front/api.js");
/* harmony import */ var _backend_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../backend/helper */ "./resources/js/backend/helper.js");
/* harmony import */ var _ShoppingCart__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../ShoppingCart */ "./resources/js/front/components/ShoppingCart.vue");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_6__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Initialise",
  components: {
    ExtraCar: _ExtraCar__WEBPACK_IMPORTED_MODULE_2__["default"],
    ExtraShopList: _ExtraShopList__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      ShoppingCart: {},
      Rule: {
        DefaultExtraBedPrice: Number,
        DefaultExtraPeoplePrice: Number
      },
      form: {
        DiscountCode: {
          Code: ""
        },
        Booking: {
          Email: "",
          Name: "",
          IdNumber: "",
          Phone: "",
          Sex: "",
          Area: ""
        },
        Guest: {
          Email: "",
          Name: "",
          IdNumber: "",
          Phone: "",
          Sex: "",
          Area: ""
        },
        BookingSameGuest: true,
        BookingSameAuth: true,
        TaxNumber: '',
        Note: ''
      },
      ShowMemberLogin: false,
      LoginForm: {
        Email: '',
        Password: ''
      }
    };
  },
  created: function created() {
    this.init();
  },
  computed: {
    StoreShoppingCart: function StoreShoppingCart() {
      return this.$store.getters.ShoppingCart;
    },
    Member: function Member() {
      var _this = this;

      if (this.$store.getters.Member) {
        axios__WEBPACK_IMPORTED_MODULE_6___default.a.post('/api/Front/MemberChecker').then(function (res) {
          return _this.$store.getters.Member;
        })["catch"](function (reason) {
          _this.$store.commit('Logout');

          return null;
        });
      } else {
        return null;
      }
    }
  },
  watch: {
    'form.BookingSameGuest': function formBookingSameGuest(val) {
      if (!val) {
        this.$nextTick(function () {
          this.$scrollTo('#GuestArea', 1500, {
            offset: -100
          });
        });
      } else {
        this.$nextTick(function () {
          this.$scrollTo('#BookArea', 1500, {
            offset: -100
          });
        });
      }
    },
    'StoreShoppingCart': function StoreShoppingCart(val) {
      var temp = [];
      val.map(function (ob) {
        temp.push({
          ShowExtraShop: false,
          ShowExtraCar: false,
          ExtraCars: [],
          ExtraShops: [],
          adults: ob.adults,
          babys: ob.babys,
          childrens: ob.childrens,
          endDate: ob.endDate,
          startDate: ob.startDate,
          promotion: ob.promotion,
          room: ob.room,
          Extra: "0"
        });
      });
      this.ShoppingCart = temp;
    }
  },
  methods: {
    init: function init() {
      var _this2 = this;

      this.$emit('updateStep', 2);
      var temp = [];
      this.$store.getters.ShoppingCart.map(function (ob) {
        temp.push({
          ShowExtraShop: false,
          ShowExtraCar: false,
          ExtraCars: [],
          ExtraShops: [],
          adults: ob.adults,
          babys: ob.babys,
          childrens: ob.childrens,
          endDate: ob.endDate,
          startDate: ob.startDate,
          promotion: ob.promotion,
          room: ob.room,
          Extra: "0"
        });
      });
      this.ShoppingCart = temp;
      Object(_api__WEBPACK_IMPORTED_MODULE_3__["Rule"])().then(function (res) {
        _this2.Rule.DefaultExtraBedPrice = res.DefaultExtraBedPrice;
        _this2.Rule.DefaultExtraPeoplePrice = res.DefaultExtraPeoplePrice;
      })["catch"](function (rep) {
        return Object(_backend_helper__WEBPACK_IMPORTED_MODULE_4__["SwalAlertErrorMessage"])(rep);
      });

      if (this.Member) {
        //有登入
        this.form.Booking.Email = this.Member.Email;
        this.form.Booking.Name = this.Member.Name;
        this.form.Booking.IdNumber = this.Member.IdNumber;
        this.form.Booking.Phone = this.Member.Phone;
        this.form.Booking.Sex = this.Member.Sex;
        this.form.Booking.Area = this.Member.Area;
      }
    },
    WeekDay: function WeekDay(date) {
      moment__WEBPACK_IMPORTED_MODULE_0___default.a.locale('zh-tw');
      return moment__WEBPACK_IMPORTED_MODULE_0___default()(date).format('dddd');
    },
    nights: function nights(startDate, endDate) {
      var _moment$diff;

      return (_moment$diff = moment__WEBPACK_IMPORTED_MODULE_0___default()(endDate).diff(moment__WEBPACK_IMPORTED_MODULE_0___default()(startDate), 'days')) !== null && _moment$diff !== void 0 ? _moment$diff : 0;
    },
    RemovePicking: function RemovePicking(index) {
      this.$store.commit('Remove', index);
    },
    UpdateExtraShopPicked: function UpdateExtraShopPicked(index, val) {
      this.ShoppingCart[index].ExtraShops = val;
    },
    UpdateExtraCarPicked: function UpdateExtraCarPicked(index, val) {
      this.ShoppingCart[index].ExtraCars = val;
    },
    CheckingDiscountCode: function CheckingDiscountCode() {
      var _this3 = this;

      var credential = {
        DiscountCode: this.form.DiscountCode.Code
      };

      Object(_api__WEBPACK_IMPORTED_MODULE_3__["CheckingDiscountCode"])(credential).then(function (res) {
        // this.form.DiscountCode = res
        var checker = false;

        _this3.ShoppingCart.forEach(function (item) {
          if (moment__WEBPACK_IMPORTED_MODULE_0___default()(item.startDate) <= moment__WEBPACK_IMPORTED_MODULE_0___default()(res.Use_Start) && moment__WEBPACK_IMPORTED_MODULE_0___default()(item.endDate) < moment__WEBPACK_IMPORTED_MODULE_0___default()(res.Use_End) && moment__WEBPACK_IMPORTED_MODULE_0___default()(item.endDate) >= moment__WEBPACK_IMPORTED_MODULE_0___default()(res.Use_Start)) {
            checker = true;
          } else if (moment__WEBPACK_IMPORTED_MODULE_0___default()(item.startDate) >= moment__WEBPACK_IMPORTED_MODULE_0___default()(res.Use_Start) && moment__WEBPACK_IMPORTED_MODULE_0___default()(item.startDate) < moment__WEBPACK_IMPORTED_MODULE_0___default()(res.Use_End) && moment__WEBPACK_IMPORTED_MODULE_0___default()(item.endDate) >= moment__WEBPACK_IMPORTED_MODULE_0___default()(res.Use_End)) {
            checker = true;
          } else if (moment__WEBPACK_IMPORTED_MODULE_0___default()(item.startDate) < moment__WEBPACK_IMPORTED_MODULE_0___default()(res.Use_Start) && moment__WEBPACK_IMPORTED_MODULE_0___default()(item.endDate) > moment__WEBPACK_IMPORTED_MODULE_0___default()(res.Use_End)) {
            checker = true;
          } else if (moment__WEBPACK_IMPORTED_MODULE_0___default()(item.startDate) > moment__WEBPACK_IMPORTED_MODULE_0___default()(res.Use_Start) && moment__WEBPACK_IMPORTED_MODULE_0___default()(item.endDate) < moment__WEBPACK_IMPORTED_MODULE_0___default()(res.Use_End)) {
            checker = true;
          } else {}
        });

        if (checker) {
          _this3.form.DiscountCode = res;
        } else {
          _this3.form.DiscountCode = {
            Code: ''
          };
          var response = {
            status: 422,
            data: {
              errors: {
                message: ['折扣碼不適用此訂房區間']
              }
            }
          };
          Object(_backend_helper__WEBPACK_IMPORTED_MODULE_4__["SwalAlertErrorMessage"])(response);
        }
      })["catch"](function (rep) {
        Object(_backend_helper__WEBPACK_IMPORTED_MODULE_4__["SwalAlertErrorMessage"])(rep);
        _this3.form.DiscountCode = {
          Code: ''
        };
      });
    },
    CalcOrderPrice: function CalcOrderPrice(ob) {
      //基本價
      var price = ob.promotion.Total; //人數超過

      if (ob.adults + ob.childrens > ob.room.room.PeopleNumber) {
        if (ob.Extra === '0') {
          price += this.nights(ob.startDate, ob.endDate) * (ob.adults + ob.childrens - ob.room.room.PeopleNumber) * this.Rule.DefaultExtraPeoplePrice;
        } else {
          if (ob.adults + ob.childrens > ob.room.room.PeopleNumber + 1) {
            price += this.nights(ob.startDate, ob.endDate) * 1 * this.Rule.DefaultExtraBedPrice + this.nights(ob.startDate, ob.endDate) * (ob.adults + ob.childrens - (ob.room.room.PeopleNumber + 1)) * this.Rule.DefaultExtraPeoplePrice;
          } else {
            price += this.nights(ob.startDate, ob.endDate) * 1 * this.Rule.DefaultExtraBedPrice;
          }
        }
      } //加購項目


      ob.ExtraShops.forEach(function (item) {
        price += parseInt(item.Nights) * parseInt(item.Price) * parseInt(item.Numbers);
      });
      ob.ExtraCars.forEach(function (item) {
        item.type === '1' ? price += parseInt(item.Numbers) * parseInt(item.Price) : price += parseInt(item.Numbers) * parseInt(item.Price2);
      });
      return price;
    },
    TotalOrderPrice: function TotalOrderPrice(ShoppingCart) {
      var _this4 = this;

      var Total = 0;
      ShoppingCart.forEach(function (ob) {
        return Total += _this4.CalcOrderPrice(ob);
      });
      this.form.DiscountCode.Discount ? Total -= parseInt(this.form.DiscountCode.Discount) : '';
      return Total;
    },
    CheckingOrder: function CheckingOrder() {
      var _this5 = this;

      var BookingData = [];
      this.ShoppingCart.forEach(function (item) {
        var temp = {};
        temp.Extra = item.Extra;
        temp.ExtraCars = [];
        item.ExtraCars.forEach(function (exC) {
          temp.ExtraCars.push({
            Numbers: exC.Numbers,
            id: exC.id,
            arrvdt: exC.arrvdt,
            pickdt: exC.pickdt,
            type: exC.type
          });
        });
        temp.ExtraShops = [];
        item.ExtraShops.forEach(function (exS) {
          temp.ExtraShops.push({
            Numbers: exS.Numbers,
            id: exS.id,
            Nights: exS.Nights
          });
        });
        temp.adults = item.adults;
        temp.babys = item.babys;
        temp.childrens = item.childrens;
        temp.startDate = item.startDate;
        temp.endDate = item.endDate;
        temp.promotion = item.promotion.id;
        temp.room = item.room.room.id;
        BookingData.push(temp);
      });
      var credential = {
        form: this.form,
        BookingData: BookingData
      };

      Object(_api__WEBPACK_IMPORTED_MODULE_3__["CheckingOrder"])(credential).then(function (res) {
        _this5.$store.commit('Clear');

        if (res.Member) {
          _this5.$store.dispatch('LoginSucess', res.Member).then(function (response) {
            _this5.$router.push({
              name: 'PaymentSelect',
              params: {
                Num: res.Order
              }
            });
          });
        } else {
          _this5.$router.push({
            name: 'PaymentSelect',
            params: {
              Num: res.Order
            }
          });
        }
      })["catch"](function (rep) {
        Object(_backend_helper__WEBPACK_IMPORTED_MODULE_4__["SwalAlertErrorMessage"])(rep);
      });
    },
    Login: function Login() {
      var _this6 = this;

      var credential = {
        email: this.LoginForm.Email,
        password: this.LoginForm.Password
      };

      Object(_api__WEBPACK_IMPORTED_MODULE_3__["Login"])(credential).then(function (res) {
        _this6.$store.dispatch('LoginSucess', res).then(function (response) {
          if (_this6.Member) {
            //有登入
            _this6.form.Booking.Email = _this6.Member.Email;
            _this6.form.Booking.Name = _this6.Member.Name;
            _this6.form.Booking.IdNumber = _this6.Member.IdNumber;
            _this6.form.Booking.Phone = _this6.Member.Phone;
            _this6.form.Booking.Sex = _this6.Member.Sex;
            _this6.form.Booking.Area = _this6.Member.Area;
          }
        });
      })["catch"](function (rep) {
        Object(_backend_helper__WEBPACK_IMPORTED_MODULE_4__["SwalAlertErrorMessage"])(rep);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/ExtraCar.vue?vue&type=template&id=2e2b706f&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/ExtraCar.vue?vue&type=template&id=2e2b706f&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      directives: [
        {
          name: "show",
          rawName: "v-show",
          value: _vm.showModal,
          expression: "showModal"
        }
      ],
      staticClass: "table-responsive"
    },
    [
      _c("table", { staticClass: "table" }, [
        _vm._m(0),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.List, function(ob) {
            return _c("tr", { key: ob.id }, [
              _c("th", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: ob.checked,
                      expression: "ob.checked"
                    }
                  ],
                  attrs: { type: "checkbox", id: "ck" + ob.id },
                  domProps: {
                    checked: Array.isArray(ob.checked)
                      ? _vm._i(ob.checked, null) > -1
                      : ob.checked
                  },
                  on: {
                    change: function($event) {
                      var $$a = ob.checked,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = null,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 && _vm.$set(ob, "checked", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              ob,
                              "checked",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(ob, "checked", $$c)
                      }
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "th",
                { staticClass: "text-center", attrs: { scope: "row" } },
                [
                  _c("img", {
                    staticStyle: {
                      width: "100px",
                      height: "80px",
                      "object-fit": "cover"
                    },
                    attrs: { src: ob.Src, alt: "" }
                  })
                ]
              ),
              _vm._v(" "),
              _c("td", { staticClass: "text-center" }, [
                _vm._v(_vm._s(ob.Name))
              ]),
              _vm._v(" "),
              _c("td", { staticClass: "text-center" }, [
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: ob.type,
                        expression: "ob.type"
                      }
                    ],
                    staticStyle: {
                      flex: "1",
                      "align-self": "center",
                      "margin-top": "auto"
                    },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          ob,
                          "type",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { value: "1" } }, [_vm._v("單趟")]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "2" } }, [_vm._v("來回")])
                  ]
                )
              ]),
              _vm._v(" "),
              _c(
                "td",
                { staticClass: "text-center" },
                [
                  ob.type === "1"
                    ? [_vm._v("NT$ " + _vm._s(Math.ceil(ob.Price)))]
                    : [_vm._v("NT$ " + _vm._s(Math.ceil(ob.Price2)))]
                ],
                2
              ),
              _vm._v(" "),
              _c("td", { staticClass: "text-center" }, [
                _c(
                  "div",
                  {
                    staticClass: "input-group",
                    staticStyle: { display: "inline-flex" }
                  },
                  [
                    _c(
                      "select",
                      {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: ob.Numbers,
                            expression: "ob.Numbers"
                          }
                        ],
                        staticStyle: {
                          flex: "1",
                          "align-self": "center",
                          "margin-top": "auto"
                        },
                        on: {
                          change: function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              ob,
                              "Numbers",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          }
                        }
                      },
                      [
                        _c("option", { attrs: { value: "0" } }, [_vm._v("0")]),
                        _vm._v(" "),
                        _vm._l(ob.Max, function(Number) {
                          return _c(
                            "option",
                            {
                              key: "number" + Number,
                              domProps: { value: Number }
                            },
                            [
                              _vm._v(
                                "\n                            " +
                                  _vm._s(Number) +
                                  "\n                        "
                              )
                            ]
                          )
                        })
                      ],
                      2
                    )
                  ]
                )
              ]),
              _vm._v(" "),
              _c("td", [
                _c("div", { domProps: { innerHTML: _vm._s(ob.Html) } })
              ]),
              _vm._v(" "),
              _c("td", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: ob.arrvdt,
                      expression: "ob.arrvdt"
                    }
                  ],
                  staticStyle: {
                    border: "1px solid #ededed",
                    "box-shadow": "none",
                    padding: "8px"
                  },
                  attrs: { type: "datetime-local" },
                  domProps: { value: ob.arrvdt },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(ob, "arrvdt", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("td", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: ob.pickdt,
                      expression: "ob.pickdt"
                    }
                  ],
                  staticStyle: {
                    border: "1px solid #ededed",
                    "box-shadow": "none",
                    padding: "8px"
                  },
                  attrs: { type: "datetime-local" },
                  domProps: { value: ob.pickdt },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(ob, "pickdt", $event.target.value)
                    }
                  }
                })
              ])
            ])
          }),
          0
        )
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("圖片")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("項目")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("類別")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("單價")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("數量")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("說明")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("抵達日期時間(選填)")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("出發日期時間(選填)")
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/ExtraShopList.vue?vue&type=template&id=53a90f29&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/ExtraShopList.vue?vue&type=template&id=53a90f29&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      directives: [
        {
          name: "show",
          rawName: "v-show",
          value: _vm.showModal,
          expression: "showModal"
        }
      ],
      staticClass: "table-responsive"
    },
    [
      _c("table", { staticClass: "table" }, [
        _vm._m(0),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.List, function(ob) {
            return _c("tr", { key: ob.id }, [
              _c("th", [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: ob.checked,
                      expression: "ob.checked"
                    }
                  ],
                  attrs: { type: "checkbox", id: "ck" + ob.id },
                  domProps: {
                    checked: Array.isArray(ob.checked)
                      ? _vm._i(ob.checked, null) > -1
                      : ob.checked
                  },
                  on: {
                    change: function($event) {
                      var $$a = ob.checked,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = null,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 && _vm.$set(ob, "checked", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              ob,
                              "checked",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(ob, "checked", $$c)
                      }
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "th",
                { staticClass: "text-center", attrs: { scope: "row" } },
                [
                  _c("img", {
                    staticStyle: {
                      width: "100px",
                      height: "80px",
                      "object-fit": "cover"
                    },
                    attrs: { src: ob.Src, alt: "" }
                  })
                ]
              ),
              _vm._v(" "),
              _c("td", { staticClass: "text-center" }, [
                _vm._v(_vm._s(ob.Name))
              ]),
              _vm._v(" "),
              _c("td", { staticClass: "text-center" }, [
                _vm._v("NT$ " + _vm._s(Math.ceil(ob.Price)))
              ]),
              _vm._v(" "),
              _c("td", { staticClass: "text-center" }, [
                _c(
                  "div",
                  {
                    staticClass: "input-group",
                    staticStyle: { display: "inline-flex" }
                  },
                  [
                    _c(
                      "select",
                      {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: ob.Nights,
                            expression: "ob.Nights"
                          }
                        ],
                        staticStyle: {
                          flex: "1",
                          "align-self": "center",
                          "margin-top": "auto"
                        },
                        on: {
                          change: function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              ob,
                              "Nights",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          }
                        }
                      },
                      [
                        _c("option", { attrs: { value: "0" } }, [_vm._v("0")]),
                        _vm._v(" "),
                        _vm._l(_vm.Nights, function(Number) {
                          return _c(
                            "option",
                            {
                              key: "nights" + Number,
                              domProps: { value: Number }
                            },
                            [
                              _vm._v(
                                "\n                                " +
                                  _vm._s(Number) +
                                  " 天\n                            "
                              )
                            ]
                          )
                        })
                      ],
                      2
                    ),
                    _vm._v(" "),
                    _c(
                      "span",
                      { staticStyle: { flex: "1", "align-self": "center" } },
                      [_vm._v(" x ")]
                    ),
                    _vm._v(" "),
                    _c(
                      "select",
                      {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: ob.Numbers,
                            expression: "ob.Numbers"
                          }
                        ],
                        staticStyle: {
                          flex: "1",
                          "align-self": "center",
                          "margin-top": "auto"
                        },
                        on: {
                          change: function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              ob,
                              "Numbers",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          }
                        }
                      },
                      [
                        _c("option", { attrs: { value: "0" } }, [_vm._v("0")]),
                        _vm._v(" "),
                        _vm._l(_vm.Adults + _vm.Childrens, function(Number) {
                          return _c(
                            "option",
                            {
                              key: "number" + Number,
                              domProps: { value: Number }
                            },
                            [
                              _vm._v(
                                "\n                                " +
                                  _vm._s(Number) +
                                  "\n                            "
                              )
                            ]
                          )
                        })
                      ],
                      2
                    )
                  ]
                )
              ]),
              _vm._v(" "),
              _c("td", [
                _c("div", { domProps: { innerHTML: _vm._s(ob.Html) } })
              ])
            ])
          }),
          0
        )
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("圖片")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("項目")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("單價")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("數量")
        ]),
        _vm._v(" "),
        _c("th", { staticClass: "text-center", attrs: { scope: "col" } }, [
          _vm._v("說明")
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/Initialise.vue?vue&type=template&id=6e257f42&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/Initialise.vue?vue&type=template&id=6e257f42&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row warp_p" }, [
    _c("div", { staticClass: "col-md-12" }, [
      _c("section", { staticClass: "card mb-5 pb-5" }, [
        _c("h5", { staticClass: "card-header text-center" }, [
          _vm._v("\n                    訂單內容\n                ")
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "card-body" },
          [
            _vm._l(_vm.ShoppingCart, function(ob, index) {
              return _c(
                "div",
                { key: index, staticClass: "row reservation-item pb-4" },
                [
                  _c("div", { staticClass: "col-md-9 reservation-item-info" }, [
                    _c(
                      "h3",
                      { staticClass: "reservation-item-info-title mb-3" },
                      [
                        _c(
                          "span",
                          {
                            staticClass: "reservation-item-info-title-roomname "
                          },
                          [
                            _vm._v(
                              _vm._s(ob.room.room.Name) +
                                " | " +
                                _vm._s(ob.promotion.Name)
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "row reservation-item-info-body" },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              "col-md-5 col-sm-6 col-12 reservation-item-info-body-photo"
                          },
                          [
                            _c("img", {
                              staticClass: "w-100 mw-100 mb-5",
                              attrs: { src: ob.room.room.MainPicture }
                            })
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass:
                              "col-md-7 col-sm-6 col-12 pt-1 reservation-item-info-body-content"
                          },
                          [
                            _c(
                              "span",
                              { staticClass: "d-block font-w-600 b-b-sold-1" },
                              [
                                _vm._v(
                                  "\n                                        人數\n                                    "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "span",
                              { staticClass: "d-block" },
                              [
                                ob.adults > 0
                                  ? [
                                      _vm._v(
                                        "\n                                            成人" +
                                          _vm._s(ob.adults) +
                                          "位 |\n                                        "
                                      )
                                    ]
                                  : _vm._e(),
                                _vm._v(" "),
                                ob.childrens > 0
                                  ? [
                                      _vm._v(
                                        "\n                                            小孩" +
                                          _vm._s(ob.childrens) +
                                          "位 |\n                                        "
                                      )
                                    ]
                                  : _vm._e(),
                                _vm._v(" "),
                                ob.babys > 0
                                  ? [
                                      _vm._v(
                                        "\n                                            兒童" +
                                          _vm._s(ob.babys) +
                                          "位\n                                        "
                                      )
                                    ]
                                  : _vm._e()
                              ],
                              2
                            ),
                            _vm._v(" "),
                            _c("br"),
                            _vm._v(" "),
                            _c(
                              "span",
                              { staticClass: "d-block font-w-600 b-b-sold-1" },
                              [
                                _vm._v(
                                  "\n                                        入住日期/退房日期\n                                    "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("span", { staticClass: "d-block" }, [
                              _vm._v(
                                "\n                                       " +
                                  _vm._s(ob.startDate) +
                                  " " +
                                  _vm._s(_vm.WeekDay(ob.startDate)) +
                                  " ~ " +
                                  _vm._s(ob.endDate) +
                                  " " +
                                  _vm._s(_vm.WeekDay(ob.endDate)) +
                                  " (" +
                                  _vm._s(_vm.nights(ob.startDate, ob.endDate)) +
                                  "晚)\n                                    "
                              )
                            ]),
                            _vm._v(" "),
                            _c("br"),
                            _vm._v(" "),
                            ob.adults + ob.childrens > ob.room.room.PeopleNumber
                              ? _c(
                                  "span",
                                  {
                                    staticClass: "d-block font-w-600 b-b-sold-1"
                                  },
                                  [
                                    _vm._v(
                                      "\n                                        基本必選項目\n                                        "
                                    ),
                                    _c(
                                      "span",
                                      {
                                        attrs: {
                                          tooltip:
                                            "依據您選擇的房型第" +
                                            (ob.room.room.PeopleNumber + 1) +
                                            "位入住起,需加購備品或床位。下列選項必須擇一"
                                        }
                                      },
                                      [
                                        _c("img", {
                                          staticClass: "pl-2",
                                          attrs: {
                                            src:
                                              "https://img.icons8.com/fluent-systems-regular/18/606060/help.png"
                                          }
                                        })
                                      ]
                                    )
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            ob.adults + ob.childrens > ob.room.room.PeopleNumber
                              ? _c(
                                  "span",
                                  {
                                    staticClass:
                                      "d-block text-danger font-w-600 row"
                                  },
                                  [
                                    _c(
                                      "th",
                                      {
                                        staticClass:
                                          "text-center  col-xs-12 pl-0",
                                        attrs: { scope: "row" }
                                      },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "co-12 pl-0" },
                                          [
                                            _c(
                                              "label",
                                              {
                                                staticClass:
                                                  "radio font-w-500 m-0 pb-2",
                                                staticStyle: {
                                                  "white-space": "nowrap"
                                                },
                                                attrs: { for: "PRE" + index }
                                              },
                                              [
                                                _c("input", {
                                                  directives: [
                                                    {
                                                      name: "model",
                                                      rawName: "v-model",
                                                      value: ob.Extra,
                                                      expression: "ob.Extra"
                                                    }
                                                  ],
                                                  attrs: {
                                                    checked: "",
                                                    id: "PRE" + index,
                                                    type: "radio",
                                                    value: "0"
                                                  },
                                                  domProps: {
                                                    checked: _vm._q(
                                                      ob.Extra,
                                                      "0"
                                                    )
                                                  },
                                                  on: {
                                                    change: function($event) {
                                                      return _vm.$set(
                                                        ob,
                                                        "Extra",
                                                        "0"
                                                      )
                                                    }
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("span", {
                                                  staticStyle: {
                                                    "margin-right":
                                                      "1rem !important",
                                                    "padding-right":
                                                      "1rem !important"
                                                  }
                                                }),
                                                _vm._v(
                                                  "加備品 x " +
                                                    _vm._s(
                                                      _vm.nights(
                                                        ob.startDate,
                                                        ob.endDate
                                                      )
                                                    ) +
                                                    " 晚 x " +
                                                    _vm._s(
                                                      ob.adults +
                                                        ob.childrens -
                                                        ob.room.room
                                                          .PeopleNumber
                                                    ) +
                                                    "\n                                                        "
                                                ),
                                                _c(
                                                  "strong",
                                                  { staticClass: "pl-2" },
                                                  [
                                                    _vm._v(
                                                      "NT$ " +
                                                        _vm._s(
                                                          _vm.nights(
                                                            ob.startDate,
                                                            ob.endDate
                                                          ) *
                                                            (ob.adults +
                                                              ob.childrens -
                                                              ob.room.room
                                                                .PeopleNumber) *
                                                            _vm.Rule
                                                              .DefaultExtraPeoplePrice
                                                        )
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "th",
                                      {
                                        staticClass:
                                          "text-center  col-xs-12 pl-0",
                                        attrs: { scope: "row" }
                                      },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "col-12 pl-0" },
                                          [
                                            ob.room.room.MaxExtraPeopleNumber >
                                            0
                                              ? _c(
                                                  "label",
                                                  {
                                                    staticClass:
                                                      "radio font-w-500 m-0 pb-2",
                                                    staticStyle: {
                                                      "white-space": "nowrap"
                                                    },
                                                    attrs: {
                                                      for: "RoomWithPRE" + index
                                                    }
                                                  },
                                                  [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: ob.Extra,
                                                          expression: "ob.Extra"
                                                        }
                                                      ],
                                                      attrs: {
                                                        id:
                                                          "RoomWithPRE" + index,
                                                        type: "radio",
                                                        value: "1"
                                                      },
                                                      domProps: {
                                                        checked: _vm._q(
                                                          ob.Extra,
                                                          "1"
                                                        )
                                                      },
                                                      on: {
                                                        change: function(
                                                          $event
                                                        ) {
                                                          return _vm.$set(
                                                            ob,
                                                            "Extra",
                                                            "1"
                                                          )
                                                        }
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c("span", {
                                                      staticStyle: {
                                                        "margin-right":
                                                          "1rem !important",
                                                        "padding-right":
                                                          "1rem !important"
                                                      }
                                                    }),
                                                    _vm._v(
                                                      "\n                                                    加床 x " +
                                                        _vm._s(
                                                          _vm.nights(
                                                            ob.startDate,
                                                            ob.endDate
                                                          )
                                                        ) +
                                                        " 晚 x 1\n                                                    "
                                                    ),
                                                    ob.adults + ob.childrens >
                                                    ob.room.room.PeopleNumber +
                                                      1
                                                      ? [
                                                          _vm._v(
                                                            "\n                                                        + 加備品 x " +
                                                              _vm._s(
                                                                _vm.nights(
                                                                  ob.startDate,
                                                                  ob.endDate
                                                                )
                                                              ) +
                                                              " 晚 x " +
                                                              _vm._s(
                                                                ob.adults +
                                                                  ob.childrens -
                                                                  (ob.room.room
                                                                    .PeopleNumber +
                                                                    1)
                                                              ) +
                                                              "\n                                                    "
                                                          )
                                                        ]
                                                      : _vm._e(),
                                                    _vm._v(" "),
                                                    ob.adults + ob.childrens >
                                                    ob.room.room.PeopleNumber +
                                                      1
                                                      ? _c(
                                                          "strong",
                                                          {
                                                            staticClass: "pl-2"
                                                          },
                                                          [
                                                            _vm._v(
                                                              "\n                                                        NT$ " +
                                                                _vm._s(
                                                                  _vm.nights(
                                                                    ob.startDate,
                                                                    ob.endDate
                                                                  ) *
                                                                    1 *
                                                                    _vm.Rule
                                                                      .DefaultExtraBedPrice +
                                                                    _vm.nights(
                                                                      ob.startDate,
                                                                      ob.endDate
                                                                    ) *
                                                                      (ob.adults +
                                                                        ob.childrens -
                                                                        (ob.room
                                                                          .room
                                                                          .PeopleNumber +
                                                                          1)) *
                                                                      _vm.Rule
                                                                        .DefaultExtraPeoplePrice
                                                                ) +
                                                                "\n                                                    "
                                                            )
                                                          ]
                                                        )
                                                      : _c(
                                                          "strong",
                                                          {
                                                            staticClass: "pl-2"
                                                          },
                                                          [
                                                            _vm._v(
                                                              "\n                                                        NT$ " +
                                                                _vm._s(
                                                                  _vm.nights(
                                                                    ob.startDate,
                                                                    ob.endDate
                                                                  ) *
                                                                    1 *
                                                                    _vm.Rule
                                                                      .DefaultExtraBedPrice
                                                                ) +
                                                                "\n                                                    "
                                                            )
                                                          ]
                                                        )
                                                  ],
                                                  2
                                                )
                                              : _vm._e()
                                          ]
                                        )
                                      ]
                                    )
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _c("br")
                          ]
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-3 text-center" }, [
                    _c(
                      "div",
                      {
                        staticClass:
                          "position-relative reservation-item-info-price pt-4"
                      },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              "mb-md-3 mb-0 mr-lg-0 mr-md-4 mr-sm-4 mr-4"
                          },
                          [
                            _c("strong", { staticClass: "price-number" }, [
                              _vm._v("NT$ " + _vm._s(_vm.CalcOrderPrice(ob)))
                            ]),
                            _vm._v(" "),
                            _c(
                              "span",
                              {
                                staticClass: "ml-1",
                                on: {
                                  click: function($event) {
                                    return _vm.RemovePicking(index)
                                  }
                                }
                              },
                              [
                                _c("img", {
                                  staticClass: "pl-2",
                                  staticStyle: { "padding-bottom": "8px" },
                                  attrs: {
                                    src:
                                      "https://img.icons8.com/fluent-systems-regular/18/606060/delete.png"
                                  }
                                })
                              ]
                            )
                          ]
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-md-12" },
                    [
                      _c(
                        "span",
                        { staticClass: "d-block font-w-600 b-b-sold-1" },
                        [
                          _vm._v(
                            "\n                                        加購項目\n                                        "
                          ),
                          _c(
                            "span",
                            {
                              on: {
                                click: function($event) {
                                  ob.ShowExtraShop = !ob.ShowExtraShop
                                }
                              }
                            },
                            [
                              _c("img", {
                                staticClass: "pl-2",
                                staticStyle: { "padding-bottom": "5px" },
                                attrs: {
                                  src: !ob.ShowExtraShop
                                    ? "https://img.icons8.com/fluent-systems-regular/18/606060/plus.png"
                                    : "https://img.icons8.com/fluent-systems-regular/18/606060/minus.png"
                                }
                              })
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _vm._l(ob.ExtraShops, function(ExtraShop, index) {
                        return !ob.ShowExtraShop &&
                          ExtraShop.Nights > 0 &&
                          ExtraShop.Numbers > 0
                          ? _c(
                              "span",
                              {
                                key: "exs" + index,
                                staticClass: "d-block text-danger font-w-600"
                              },
                              [
                                _vm._v(
                                  "\n                                " +
                                    _vm._s(ExtraShop.Name) +
                                    " NT$ " +
                                    _vm._s(Math.ceil(ExtraShop.Price)) +
                                    " x " +
                                    _vm._s(ExtraShop.Nights) +
                                    "(天)  x " +
                                    _vm._s(ExtraShop.Numbers) +
                                    "\n                            "
                                )
                              ]
                            )
                          : _vm._e()
                      }),
                      _vm._v(" "),
                      _c("ExtraShopList", {
                        attrs: {
                          index: index,
                          Room: ob.room.room,
                          propShow: ob.ShowExtraShop,
                          propNights: _vm.nights(ob.startDate, ob.endDate),
                          propAdults: ob.adults,
                          propChildrens: ob.childrens,
                          propBabys: ob.babys
                        },
                        on: { UpdatePicked: _vm.UpdateExtraShopPicked }
                      }),
                      _vm._v(" "),
                      _c("br"),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "d-block font-w-600 b-b-sold-1" },
                        [
                          _vm._v(
                            "\n                                加購接駁\n                                "
                          ),
                          _c(
                            "span",
                            {
                              on: {
                                click: function($event) {
                                  ob.ShowExtraCar = !ob.ShowExtraCar
                                }
                              }
                            },
                            [
                              _c("img", {
                                staticClass: "pl-2",
                                staticStyle: { "padding-bottom": "5px" },
                                attrs: {
                                  src: !ob.ShowExtraCar
                                    ? "https://img.icons8.com/fluent-systems-regular/18/606060/plus.png"
                                    : "https://img.icons8.com/fluent-systems-regular/18/606060/minus.png"
                                }
                              })
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _vm._l(ob.ExtraCars, function(ExtraCar, index) {
                        return !ob.ShowExtraCar && ExtraCar.Numbers > 0
                          ? _c(
                              "span",
                              {
                                key: "exc" + index,
                                staticClass: "d-block text-danger font-w-600"
                              },
                              [
                                _vm._v(
                                  "\n                                    " +
                                    _vm._s(ExtraCar.Name) +
                                    " - " +
                                    _vm._s(
                                      ExtraCar.type === "1" ? "單趟" : "來回"
                                    ) +
                                    " NT$ " +
                                    _vm._s(
                                      ExtraCar.type === "1"
                                        ? Math.ceil(ExtraCar.Price)
                                        : Math.ceil(ExtraCar.Price2)
                                    ) +
                                    " x " +
                                    _vm._s(ExtraCar.Numbers) +
                                    "(趟)\n                            "
                                )
                              ]
                            )
                          : _vm._e()
                      }),
                      _vm._v(" "),
                      _c("ExtraCar", {
                        attrs: {
                          index: index,
                          Room: ob.room.room,
                          propShow: ob.ShowExtraCar
                        },
                        on: { UpdatePicked: _vm.UpdateExtraCarPicked }
                      })
                    ],
                    2
                  )
                ]
              )
            }),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-12 DiscountBlock" }, [
                _c("div", { staticClass: "reservation-item-discount" }, [
                  _c("div", { staticClass: "input-group d-flex pt-2" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form.DiscountCode.Code,
                          expression: "form.DiscountCode.Code"
                        }
                      ],
                      staticClass: "form-control col",
                      attrs: {
                        id: "DiscountCode",
                        name: "DiscountCode",
                        placeholder: "請輸入優惠碼"
                      },
                      domProps: { value: _vm.form.DiscountCode.Code },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.form.DiscountCode,
                            "Code",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn discountcode-btn",
                        attrs: { type: "button" },
                        on: { click: _vm.CheckingDiscountCode }
                      },
                      [_vm._v("使用")]
                    )
                  ]),
                  _vm._v(" "),
                  _vm.form.DiscountCode.Discount !== undefined
                    ? _c(
                        "span",
                        {
                          staticClass:
                            "DiscountCodeMessage text-danger d-block mt-2",
                          attrs: { id: "DiscountCodeMessage" }
                        },
                        [
                          _vm._v(
                            _vm._s(_vm.form.DiscountCode.Name) +
                              " -NT$ " +
                              _vm._s(_vm.form.DiscountCode.Discount)
                          )
                        ]
                      )
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c(
                  "table",
                  {
                    staticClass:
                      "text-left d-flex flex-column justify-content-center reservation-item-total"
                  },
                  [
                    _c("tbody", [
                      _c("tr", [
                        _c("td", { staticClass: "price-title" }, [
                          _vm._v("訂房總額")
                        ]),
                        _vm._v(" "),
                        _c("td", { staticClass: "price-content ft-currency" }, [
                          _vm._v(
                            "NT$ " +
                              _vm._s(_vm.TotalOrderPrice(_vm.ShoppingCart))
                          )
                        ])
                      ])
                    ])
                  ]
                )
              ])
            ])
          ],
          2
        )
      ]),
      _vm._v(" "),
      _c("section", { staticClass: "card mt-4", attrs: { id: "BookArea" } }, [
        _c("h4", { staticClass: "card-header text-center" }, [
          _vm._v("訂購人資料")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "row" }, [
            !_vm.Member
              ? _c("div", { staticClass: "col-md-12 pt-4" }, [
                  _c("label", { staticClass: "ft-h5 d-block" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.ShowMemberLogin,
                          expression: "ShowMemberLogin"
                        }
                      ],
                      staticClass: "GuestAsContact_checked",
                      attrs: { checked: "", type: "checkbox" },
                      domProps: {
                        checked: Array.isArray(_vm.ShowMemberLogin)
                          ? _vm._i(_vm.ShowMemberLogin, null) > -1
                          : _vm.ShowMemberLogin
                      },
                      on: {
                        change: function($event) {
                          var $$a = _vm.ShowMemberLogin,
                            $$el = $event.target,
                            $$c = $$el.checked ? true : false
                          if (Array.isArray($$a)) {
                            var $$v = null,
                              $$i = _vm._i($$a, $$v)
                            if ($$el.checked) {
                              $$i < 0 &&
                                (_vm.ShowMemberLogin = $$a.concat([$$v]))
                            } else {
                              $$i > -1 &&
                                (_vm.ShowMemberLogin = $$a
                                  .slice(0, $$i)
                                  .concat($$a.slice($$i + 1)))
                            }
                          } else {
                            _vm.ShowMemberLogin = $$c
                          }
                        }
                      }
                    }),
                    _vm._v(
                      "\n                                我是會員\n                            "
                    )
                  ]),
                  _vm._v(" "),
                  _vm.ShowMemberLogin
                    ? _c(
                        "div",
                        {
                          staticClass: "col-12 pb-5 pt-3",
                          staticStyle: { "border-bottom": "1px solid #ededed" }
                        },
                        [
                          _c(
                            "label",
                            {
                              staticClass: "ft-h5 d-block",
                              attrs: { for: "LoginEmail" }
                            },
                            [_vm._v("* Email")]
                          ),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.LoginForm.Email,
                                expression: "LoginForm.Email"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: { id: "LoginEmail", type: "email" },
                            domProps: { value: _vm.LoginForm.Email },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.LoginForm,
                                  "Email",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("br"),
                          _vm._v(" "),
                          _c(
                            "label",
                            {
                              staticClass: "ft-h5 d-block",
                              attrs: { for: "LoginPassword" }
                            },
                            [_vm._v("* Password")]
                          ),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.LoginForm.Password,
                                expression: "LoginForm.Password"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: { id: "LoginPassword", type: "password" },
                            domProps: { value: _vm.LoginForm.Password },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.LoginForm,
                                  "Password",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("br"),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass: "button eb-btn",
                              attrs: { type: "button" },
                              on: { click: _vm.Login }
                            },
                            [_vm._v("Login")]
                          ),
                          _vm._v(" "),
                          _vm._m(0),
                          _vm._v(" "),
                          _vm._m(1)
                        ]
                      )
                    : _vm._e()
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-12 pt-4" }, [
              _c(
                "label",
                {
                  staticClass: "ft-h5 d-block",
                  attrs: { for: "ContactEmail" }
                },
                [_vm._v("* Email")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.Booking.Email,
                    expression: "form.Booking.Email"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  id: "ContactEmail",
                  name: "ContactEmail",
                  type: "email"
                },
                domProps: { value: _vm.form.Booking.Email },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form.Booking, "Email", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm._m(2)
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6 pt-4" }, [
              _c(
                "label",
                { staticClass: "d-block", attrs: { for: "ContactLastName" } },
                [_vm._v("* 姓名")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "input-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.Booking.Name,
                      expression: "form.Booking.Name"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    id: "ContactLastName",
                    name: "ContactLastName",
                    type: "text"
                  },
                  domProps: { value: _vm.form.Booking.Name },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form.Booking, "Name", $event.target.value)
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6 pt-4" }, [
              _c("label", { staticClass: "d-block" }, [_vm._v(" 性別")]),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.Booking.Sex,
                      expression: "form.Booking.Sex"
                    }
                  ],
                  staticClass: "form-control m-0",
                  attrs: { name: "ContactTWState" },
                  on: {
                    change: function($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function(o) {
                          return o.selected
                        })
                        .map(function(o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.$set(
                        _vm.form.Booking,
                        "Sex",
                        $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      )
                    }
                  }
                },
                [
                  _c("option"),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "1" } }, [_vm._v("先生")]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "0" } }, [_vm._v("小姐")])
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6 col-12 pt-4" }, [
              _c(
                "label",
                { staticClass: "d-block", attrs: { for: "ContactIDNumber" } },
                [_vm._v("* 身份證字號")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.Booking.IdNumber,
                    expression: "form.Booking.IdNumber"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  id: "ContactIDNumber",
                  name: "ContactIDNumber",
                  type: "text"
                },
                domProps: { value: _vm.form.Booking.IdNumber },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form.Booking, "IdNumber", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6 col-12 pt-4" }, [
              _c(
                "label",
                { staticClass: "d-block", attrs: { for: "ContactMobile" } },
                [_vm._v("* 電話")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.Booking.Phone,
                    expression: "form.Booking.Phone"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  id: "ContactMobile",
                  name: "ContactMobile",
                  type: "tel"
                },
                domProps: { value: _vm.form.Booking.Phone },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form.Booking, "Phone", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _c("span", { staticClass: "d-block  w-100" })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6 col-12 pt-4" }, [
              _c("label", { staticClass: "d-block" }, [_vm._v(" 地址")]),
              _vm._v(" "),
              _c(
                "select",
                {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.Booking.Area,
                      expression: "form.Booking.Area"
                    }
                  ],
                  staticClass: "form-control m-0",
                  attrs: { name: "ContactTWState" },
                  on: {
                    change: function($event) {
                      var $$selectedVal = Array.prototype.filter
                        .call($event.target.options, function(o) {
                          return o.selected
                        })
                        .map(function(o) {
                          var val = "_value" in o ? o._value : o.value
                          return val
                        })
                      _vm.$set(
                        _vm.form.Booking,
                        "Area",
                        $event.target.multiple
                          ? $$selectedVal
                          : $$selectedVal[0]
                      )
                    }
                  }
                },
                [
                  _c("option"),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "台北市" } }, [
                    _vm._v("台北市")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "新北市" } }, [
                    _vm._v("新北市")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "基隆市" } }, [
                    _vm._v("基隆市")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "桃園市" } }, [
                    _vm._v("桃園市")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "新竹市" } }, [
                    _vm._v("新竹市")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "新竹縣" } }, [
                    _vm._v("新竹縣")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "宜蘭縣" } }, [
                    _vm._v("宜蘭縣")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "花蓮縣" } }, [
                    _vm._v("花蓮縣")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "苗栗縣" } }, [
                    _vm._v("苗栗縣")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "台中市" } }, [
                    _vm._v("台中市")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "彰化縣" } }, [
                    _vm._v("彰化縣")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "南投縣" } }, [
                    _vm._v("南投縣")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "雲林縣" } }, [
                    _vm._v("雲林縣")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "嘉義市" } }, [
                    _vm._v("嘉義市")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "嘉義縣" } }, [
                    _vm._v("嘉義縣")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "台南市" } }, [
                    _vm._v("台南市")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "澎湖縣" } }, [
                    _vm._v("澎湖縣")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "高雄市" } }, [
                    _vm._v("高雄市")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "屏東縣" } }, [
                    _vm._v("屏東縣")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "台東縣" } }, [
                    _vm._v("台東縣")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "金門縣" } }, [
                    _vm._v("金門縣")
                  ]),
                  _vm._v(" "),
                  _c("option", { attrs: { value: "連江縣" } }, [
                    _vm._v("連江縣")
                  ])
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "clearfix hidden-xs" })
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "section",
        {
          directives: [
            {
              name: "show",
              rawName: "v-show",
              value: !_vm.form.BookingSameGuest,
              expression: "!form.BookingSameGuest"
            }
          ],
          staticClass: "card mt-4 pb-5",
          attrs: { id: "GuestArea" }
        },
        [
          _c("h4", { staticClass: "card-header text-center" }, [
            _vm._v("住房人資料")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-12 pt-4" }, [
                _c(
                  "label",
                  {
                    staticClass: "ft-h5 d-block",
                    attrs: { for: "ContactEmail" }
                  },
                  [_vm._v("* Email")]
                ),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.Guest.Email,
                      expression: "form.Guest.Email"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    id: "ContactEmail",
                    name: "ContactEmail",
                    type: "email"
                  },
                  domProps: { value: _vm.form.Guest.Email },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form.Guest, "Email", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm._m(3)
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-6 col-12  pt-4" }, [
                _c(
                  "label",
                  { staticClass: "d-block", attrs: { for: "ContactLastName" } },
                  [_vm._v("* 姓名")]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "input-group" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.Guest.Name,
                        expression: "form.Guest.Name"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: {
                      id: "ContactLastName",
                      name: "ContactLastName",
                      type: "text"
                    },
                    domProps: { value: _vm.form.Guest.Name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form.Guest, "Name", $event.target.value)
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-6 col-12 pt-4" }, [
                _c(
                  "label",
                  { staticClass: "d-block", attrs: { for: "ContactLastName" } },
                  [_vm._v(" 性別")]
                ),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.Guest.Sex,
                        expression: "form.Guest.Sex"
                      }
                    ],
                    staticClass: "form-control m-0",
                    attrs: { id: "ContactTWState", name: "ContactTWState" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.form.Guest,
                          "Sex",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option"),
                    _vm._v(" "),
                    _c("option", [_vm._v("先生")]),
                    _vm._v(" "),
                    _c("option", [_vm._v("小姐")])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-6 col-12 pt-4" }, [
                _c(
                  "label",
                  { staticClass: "d-block", attrs: { for: "ContactIDNumber" } },
                  [_vm._v("* 身份證字號")]
                ),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.Guest.IdNumber,
                      expression: "form.Guest.IdNumber"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    id: "ContactIDNumber",
                    name: "ContactIDNumber",
                    type: "text"
                  },
                  domProps: { value: _vm.form.Guest.IdNumber },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form.Guest, "IdNumber", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-6 col-12 pt-4" }, [
                _c(
                  "label",
                  { staticClass: "d-block", attrs: { for: "ContactMobile" } },
                  [_vm._v("* 電話")]
                ),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.Guest.Phone,
                      expression: "form.Guest.Phone"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    id: "ContactMobile",
                    name: "ContactMobile",
                    type: "tel"
                  },
                  domProps: { value: _vm.form.Guest.Phone },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form.Guest, "Phone", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "d-block  w-100" })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-6 col-12 pt-4" }, [
                _c(
                  "label",
                  { staticClass: "d-block", attrs: { for: "ContactTWState" } },
                  [_vm._v(" 地址")]
                ),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.Guest.Area,
                        expression: "form.Guest.Area"
                      }
                    ],
                    staticClass: "form-control m-0",
                    attrs: { id: "ContactTWState", name: "ContactTWState" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.form.Guest,
                          "Area",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option"),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "台北市" } }, [
                      _vm._v("台北市")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "新北市" } }, [
                      _vm._v("新北市")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "基隆市" } }, [
                      _vm._v("基隆市")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "桃園市" } }, [
                      _vm._v("桃園市")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "新竹市" } }, [
                      _vm._v("新竹市")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "新竹縣" } }, [
                      _vm._v("新竹縣")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "宜蘭縣" } }, [
                      _vm._v("宜蘭縣")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "花蓮縣" } }, [
                      _vm._v("花蓮縣")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "苗栗縣" } }, [
                      _vm._v("苗栗縣")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "台中市" } }, [
                      _vm._v("台中市")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "彰化縣" } }, [
                      _vm._v("彰化縣")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "南投縣" } }, [
                      _vm._v("南投縣")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "雲林縣" } }, [
                      _vm._v("雲林縣")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "嘉義市" } }, [
                      _vm._v("嘉義市")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "嘉義縣" } }, [
                      _vm._v("嘉義縣")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "台南市" } }, [
                      _vm._v("台南市")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "澎湖縣" } }, [
                      _vm._v("澎湖縣")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "高雄市" } }, [
                      _vm._v("高雄市")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "屏東縣" } }, [
                      _vm._v("屏東縣")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "台東縣" } }, [
                      _vm._v("台東縣")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "金門縣" } }, [
                      _vm._v("金門縣")
                    ]),
                    _vm._v(" "),
                    _c("option", { attrs: { value: "連江縣" } }, [
                      _vm._v("連江縣")
                    ])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "clearfix hidden-xs" })
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "row pb-4 pt-0",
          staticStyle: { padding: "20px", "background-color": "#fff" }
        },
        [
          _c("div", { staticClass: "col-md-12 col-sm-12 pt-4" }, [
            _c(
              "label",
              { staticClass: "ft-h5 d-block", attrs: { for: "RegisterNo" } },
              [_vm._v("統一編號")]
            ),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.form.TaxNumber,
                  expression: "form.TaxNumber"
                }
              ],
              staticClass: "form-control",
              attrs: { id: "RegisterNo", name: "RegisterNo", type: "text" },
              domProps: { value: _vm.form.TaxNumber },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.form, "TaxNumber", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _c("small", { staticClass: "d-block pt-2" }, [
              _vm._v(
                "因應財政部新版電子發票格式，即日起公司戶電子發票證明聯一律無需提供「抬頭」。電子發票上將不顯示抬頭欄位，紙本電子發票印有統編即可作為會計憑證。"
              )
            ]),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _c(
              "label",
              {
                staticClass: "ft-h5 d-block",
                staticStyle: { color: "#858a99" },
                attrs: { for: "" }
              },
              [_vm._v("其它備註")]
            ),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.form.Note,
                  expression: "form.Note"
                }
              ],
              staticClass: "form-control w-100 mt-0",
              attrs: { id: "", name: "", rows: "6" },
              domProps: { value: _vm.form.Note },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.form, "Note", $event.target.value)
                }
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-12 col-sm-12 pt-4" }, [
            _c("label", { staticClass: "ft-h5 d-block" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.BookingSameGuest,
                    expression: "form.BookingSameGuest"
                  }
                ],
                staticClass: "GuestAsContact_checked",
                attrs: {
                  checked: "",
                  id: "GuestAsContact",
                  name: "GuestAsContact",
                  type: "checkbox"
                },
                domProps: {
                  checked: Array.isArray(_vm.form.BookingSameGuest)
                    ? _vm._i(_vm.form.BookingSameGuest, null) > -1
                    : _vm.form.BookingSameGuest
                },
                on: {
                  change: function($event) {
                    var $$a = _vm.form.BookingSameGuest,
                      $$el = $event.target,
                      $$c = $$el.checked ? true : false
                    if (Array.isArray($$a)) {
                      var $$v = null,
                        $$i = _vm._i($$a, $$v)
                      if ($$el.checked) {
                        $$i < 0 &&
                          _vm.$set(
                            _vm.form,
                            "BookingSameGuest",
                            $$a.concat([$$v])
                          )
                      } else {
                        $$i > -1 &&
                          _vm.$set(
                            _vm.form,
                            "BookingSameGuest",
                            $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                          )
                      }
                    } else {
                      _vm.$set(_vm.form, "BookingSameGuest", $$c)
                    }
                  }
                }
              }),
              _vm._v(
                "\n                        訂購人與住房人相同\n                    "
              )
            ]),
            _vm._v(" "),
            !_vm.Member
              ? _c("label", { staticClass: "ft-h5 d-block" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.BookingSameAuth,
                        expression: "form.BookingSameAuth"
                      }
                    ],
                    staticClass: "OrdererInfoJoin",
                    attrs: {
                      checked: "",
                      name: "OrdererInfoJoin",
                      type: "checkbox"
                    },
                    domProps: {
                      checked: Array.isArray(_vm.form.BookingSameAuth)
                        ? _vm._i(_vm.form.BookingSameAuth, null) > -1
                        : _vm.form.BookingSameAuth
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.form.BookingSameAuth,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = null,
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 &&
                              _vm.$set(
                                _vm.form,
                                "BookingSameAuth",
                                $$a.concat([$$v])
                              )
                          } else {
                            $$i > -1 &&
                              _vm.$set(
                                _vm.form,
                                "BookingSameAuth",
                                $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                              )
                          }
                        } else {
                          _vm.$set(_vm.form, "BookingSameAuth", $$c)
                        }
                      }
                    }
                  }),
                  _vm._v(
                    "\n                        以訂購人資料同時加入會員\n                        "
                  ),
                  _c("small", { staticClass: "d-block pt-3" }, [
                    _vm._v(
                      "\n                            *加入會員您將可以享有，不定期促銷活動優惠或專屬會員價哦!\n                        "
                    )
                  ])
                ])
              : _vm._e()
          ])
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-12 col-sm-12 text-center" }, [
          _c(
            "button",
            {
              staticClass: "button eb-btn Nextstep",
              attrs: { type: "button" },
              on: { click: _vm.CheckingOrder }
            },
            [_vm._v("確認")]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "or input_group w-100 mb-0" }, [
      _c("p", [_vm._v("or")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "login_social" }, [
      _c("button", { staticClass: "btn eb-btn btn-fb" }, [
        _c("i", { staticClass: "fab fa-facebook-f" }),
        _vm._v(" FACEBOOK\n                                    ")
      ]),
      _vm._v(" "),
      _c("button", { staticClass: "btn eb-btn btn-google" }, [
        _c("i", { staticClass: "fab fa-google" }),
        _vm._v(" GOOGLE\n                                    ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("small", [
      _vm._v(
        "\n                                *建議填寫非入口網站之免費信箱（Yahoo、PChome、Hotmail…等常有擋件機制），以利訂房確認書順利寄發，謝謝。"
      ),
      _c("br"),
      _vm._v(
        "\n                                *請勿使用共用信箱，已確保您的資料安全。\n                            "
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("small", [
      _vm._v(
        "\n                                *建議填寫非入口網站之免費信箱（Yahoo、PChome、Hotmail…等常有擋件機制），以利訂房確認書順利寄發，謝謝。"
      ),
      _c("br"),
      _vm._v(
        "\n                                *請勿使用共用信箱，已確保您的資料安全。\n                            "
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/front/components/layouts/ExtraCar.vue":
/*!************************************************************!*\
  !*** ./resources/js/front/components/layouts/ExtraCar.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ExtraCar_vue_vue_type_template_id_2e2b706f_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExtraCar.vue?vue&type=template&id=2e2b706f&scoped=true& */ "./resources/js/front/components/layouts/ExtraCar.vue?vue&type=template&id=2e2b706f&scoped=true&");
/* harmony import */ var _ExtraCar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExtraCar.vue?vue&type=script&lang=js& */ "./resources/js/front/components/layouts/ExtraCar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ExtraCar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ExtraCar_vue_vue_type_template_id_2e2b706f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ExtraCar_vue_vue_type_template_id_2e2b706f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "2e2b706f",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/front/components/layouts/ExtraCar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/front/components/layouts/ExtraCar.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/ExtraCar.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExtraCar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ExtraCar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/ExtraCar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExtraCar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/front/components/layouts/ExtraCar.vue?vue&type=template&id=2e2b706f&scoped=true&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/ExtraCar.vue?vue&type=template&id=2e2b706f&scoped=true& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExtraCar_vue_vue_type_template_id_2e2b706f_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ExtraCar.vue?vue&type=template&id=2e2b706f&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/ExtraCar.vue?vue&type=template&id=2e2b706f&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExtraCar_vue_vue_type_template_id_2e2b706f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExtraCar_vue_vue_type_template_id_2e2b706f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/front/components/layouts/ExtraShopList.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/front/components/layouts/ExtraShopList.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ExtraShopList_vue_vue_type_template_id_53a90f29_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExtraShopList.vue?vue&type=template&id=53a90f29&scoped=true& */ "./resources/js/front/components/layouts/ExtraShopList.vue?vue&type=template&id=53a90f29&scoped=true&");
/* harmony import */ var _ExtraShopList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExtraShopList.vue?vue&type=script&lang=js& */ "./resources/js/front/components/layouts/ExtraShopList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ExtraShopList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ExtraShopList_vue_vue_type_template_id_53a90f29_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ExtraShopList_vue_vue_type_template_id_53a90f29_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "53a90f29",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/front/components/layouts/ExtraShopList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/front/components/layouts/ExtraShopList.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/ExtraShopList.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExtraShopList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ExtraShopList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/ExtraShopList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExtraShopList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/front/components/layouts/ExtraShopList.vue?vue&type=template&id=53a90f29&scoped=true&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/ExtraShopList.vue?vue&type=template&id=53a90f29&scoped=true& ***!
  \************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExtraShopList_vue_vue_type_template_id_53a90f29_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ExtraShopList.vue?vue&type=template&id=53a90f29&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/ExtraShopList.vue?vue&type=template&id=53a90f29&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExtraShopList_vue_vue_type_template_id_53a90f29_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExtraShopList_vue_vue_type_template_id_53a90f29_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/front/components/layouts/Initialise.vue":
/*!**************************************************************!*\
  !*** ./resources/js/front/components/layouts/Initialise.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Initialise_vue_vue_type_template_id_6e257f42_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Initialise.vue?vue&type=template&id=6e257f42&scoped=true& */ "./resources/js/front/components/layouts/Initialise.vue?vue&type=template&id=6e257f42&scoped=true&");
/* harmony import */ var _Initialise_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Initialise.vue?vue&type=script&lang=js& */ "./resources/js/front/components/layouts/Initialise.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Initialise_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Initialise_vue_vue_type_template_id_6e257f42_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Initialise_vue_vue_type_template_id_6e257f42_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "6e257f42",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/front/components/layouts/Initialise.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/front/components/layouts/Initialise.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/Initialise.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Initialise_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Initialise.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/Initialise.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Initialise_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/front/components/layouts/Initialise.vue?vue&type=template&id=6e257f42&scoped=true&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/Initialise.vue?vue&type=template&id=6e257f42&scoped=true& ***!
  \*********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Initialise_vue_vue_type_template_id_6e257f42_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Initialise.vue?vue&type=template&id=6e257f42&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/Initialise.vue?vue&type=template&id=6e257f42&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Initialise_vue_vue_type_template_id_6e257f42_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Initialise_vue_vue_type_template_id_6e257f42_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);