(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["RemainRoom"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/RemainRoom.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/RemainRoom.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../api */ "./resources/js/backend/api.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../helper */ "./resources/js/backend/helper.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "RemainRoom",
  data: function data() {
    return {
      form: {
        Title: '訂房情況',
        Icon: 'settings'
      },
      Room: {},
      currentMonth: moment__WEBPACK_IMPORTED_MODULE_0___default()().format('YYYY-MM'),
      Events: []
    };
  },
  props: ['Licenses'],
  created: function created() {
    this.$emit('ChildUpdated', this.form);
    this.GetRoom();
    this.GetEvents(this.currentMonth);
  },
  watch: {
    currentMonth: function currentMonth(value) {
      this.GetEvents(value);
    }
  },
  methods: {
    OpenEventModal: function OpenEventModal(event) {},
    ReloadEvents: function ReloadEvents(date) {
      this.currentMonth = date;
    },
    GetRoom: function GetRoom() {
      var _this = this;

      var credential = {
        room_id: this.$route.params.id
      };

      Object(_api__WEBPACK_IMPORTED_MODULE_1__["GetRoom"])(credential).then(function (res) {
        _this.Room = res;
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
      });
    },
    GetEvents: function GetEvents(Month) {
      var _this2 = this;

      var credential = {
        room_id: this.$route.params.id,
        start: moment__WEBPACK_IMPORTED_MODULE_0___default()(Month).startOf('month').format('YYYY-MM-DD'),
        end: moment__WEBPACK_IMPORTED_MODULE_0___default()(Month).endOf('month').format('YYYY-MM-DD')
      };
      Object(_api__WEBPACK_IMPORTED_MODULE_1__["GetRemainRoom"])(credential).then(function (res) {
        _this2.Events = res;
      })["catch"](function (response) {
        Object(_helper__WEBPACK_IMPORTED_MODULE_2__["SwalAlertErrorMessage"])(response);
        this.Events = [];
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/RemainRoom.vue?vue&type=template&id=5a24f590&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/backend/components/children/logined/children/RemainRoom.vue?vue&type=template&id=5a24f590&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "wh-100" }, [
    _c("div", { staticClass: "col-md-12 col-12" }, [
      _c("div", { staticClass: "row mb-0 mb-sm-3 mb-md-3" }, [
        _c(
          "div",
          { staticClass: "col-md-12 col-12" },
          [
            _c("Calendar", {
              attrs: {
                PropEvents: _vm.Events,
                PropCurrentMonth: _vm.currentMonth || undefined
              },
              on: {
                DayClick: _vm.OpenDailyRoomModal,
                EventClick: _vm.OpenEventModal,
                MonthChange: _vm.ReloadEvents
              }
            })
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/RemainRoom.vue":
/*!**********************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/RemainRoom.vue ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RemainRoom_vue_vue_type_template_id_5a24f590_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RemainRoom.vue?vue&type=template&id=5a24f590&scoped=true& */ "./resources/js/backend/components/children/logined/children/RemainRoom.vue?vue&type=template&id=5a24f590&scoped=true&");
/* harmony import */ var _RemainRoom_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RemainRoom.vue?vue&type=script&lang=js& */ "./resources/js/backend/components/children/logined/children/RemainRoom.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RemainRoom_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RemainRoom_vue_vue_type_template_id_5a24f590_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RemainRoom_vue_vue_type_template_id_5a24f590_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "5a24f590",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/backend/components/children/logined/children/RemainRoom.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/RemainRoom.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/RemainRoom.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RemainRoom_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./RemainRoom.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/RemainRoom.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_RemainRoom_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/backend/components/children/logined/children/RemainRoom.vue?vue&type=template&id=5a24f590&scoped=true&":
/*!*****************************************************************************************************************************!*\
  !*** ./resources/js/backend/components/children/logined/children/RemainRoom.vue?vue&type=template&id=5a24f590&scoped=true& ***!
  \*****************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RemainRoom_vue_vue_type_template_id_5a24f590_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./RemainRoom.vue?vue&type=template&id=5a24f590&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/backend/components/children/logined/children/RemainRoom.vue?vue&type=template&id=5a24f590&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RemainRoom_vue_vue_type_template_id_5a24f590_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RemainRoom_vue_vue_type_template_id_5a24f590_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);