(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["FrontMain"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/Main.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/Main.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Process__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Process */ "./resources/js/front/components/layouts/Process.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Main",
  components: {
    Process: _Process__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      step: 1
    };
  },
  created: function created() {},
  methods: {
    updateStep: function updateStep(val) {
      this.step = val;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/Process.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/Process.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "process",
  props: {
    step: {
      "default": 1
    }
  },
  watch: {
    step: function step(val) {
      this.step = val;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/Main.vue?vue&type=template&id=bbcd88f8&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/Main.vue?vue&type=template&id=bbcd88f8&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "main",
    { staticClass: "no-padding", staticStyle: { overflow: "hidden" } },
    [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "container" }, [
        _c(
          "div",
          { staticClass: "BookingPage" },
          [
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                { staticClass: "col-md-12" },
                [_c("process", { attrs: { step: _vm.step } })],
                1
              )
            ]),
            _vm._v(" "),
            _c("router-view", {
              key: this.$route.path,
              on: { updateStep: _vm.updateStep }
            })
          ],
          1
        )
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "BannerBlock",
        staticStyle: { "background-image": "url('#')" }
      },
      [
        _c("div", { staticClass: "BannerTitle text-center" }, [
          _c(
            "h1",
            {
              staticClass: "wow fadeInUp",
              staticStyle: { visibility: "visible" },
              attrs: { "data-wow-delay": ".2s" }
            },
            [_vm._v("BOOKING")]
          ),
          _vm._v(" "),
          _c(
            "p",
            {
              staticClass: "wow fadeInUp",
              staticStyle: { visibility: "visible" },
              attrs: { "data-wow-delay": ".6s" }
            },
            [_vm._v("/")]
          ),
          _vm._v(" "),
          _c(
            "h2",
            {
              staticClass: "wow fadeInUp",
              staticStyle: { visibility: "visible" },
              attrs: { "data-wow-delay": ".8s" }
            },
            [_vm._v("立即訂房")]
          )
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/Process.vue?vue&type=template&id=2e140e18&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/front/components/layouts/Process.vue?vue&type=template&id=2e140e18&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "eb-stepline" }, [
    _c("div", { staticClass: "eb-stepline-steps" }, [
      _c(
        "div",
        {
          staticClass: "eb-stepline-step",
          class: _vm.step >= 1 ? "completed active" : "disabled"
        },
        [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "eb-stepline-dot" }),
          _vm._v(" "),
          _c("div", { staticClass: "text-center bs-wizard-stepnum" }, [
            _vm._v("搜尋房型")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "bs-wizard-info text-center" }, [
            _vm._v("Choose your favorite room")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "eb-stepline-step",
          class: _vm.step >= 2 ? "completed active" : "disabled"
        },
        [
          _vm._m(1),
          _vm._v(" "),
          _c("div", { staticClass: "eb-stepline-dot" }),
          _vm._v(" "),
          _c("div", { staticClass: "text-center bs-wizard-stepnum" }, [
            _vm._v("填寫訂單")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "bs-wizard-info text-center" }, [
            _vm._v("Enter your booking details")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "eb-stepline-step",
          class: _vm.step >= 3 ? "completed active" : "disabled"
        },
        [
          _vm._m(2),
          _vm._v(" "),
          _c("div", { staticClass: "eb-stepline-dot" }),
          _vm._v(" "),
          _c("div", { staticClass: "text-center bs-wizard-stepnum" }, [
            _vm._v("結帳付款")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "bs-wizard-info text-center" }, [
            _vm._v("Use your preferred payment method")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "eb-stepline-step",
          class: _vm.step >= 4 ? "completed active" : "disabled"
        },
        [
          _vm._m(3),
          _vm._v(" "),
          _c("div", { staticClass: "eb-stepline-dot" }),
          _vm._v(" "),
          _c("div", { staticClass: "text-center bs-wizard-stepnum" }, [
            _vm._v("訂房完成")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "bs-wizard-info text-center" }, [
            _vm._v("Booking confirmation")
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "eb-stepline-progress" }, [
      _c("div", { staticClass: "eb-stepline-progress-bar" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "eb-stepline-progress" }, [
      _c("div", { staticClass: "eb-stepline-progress-bar" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "eb-stepline-progress" }, [
      _c("div", { staticClass: "eb-stepline-progress-bar" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "eb-stepline-progress" }, [
      _c("div", { staticClass: "eb-stepline-progress-bar" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/front/components/layouts/Main.vue":
/*!********************************************************!*\
  !*** ./resources/js/front/components/layouts/Main.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Main_vue_vue_type_template_id_bbcd88f8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Main.vue?vue&type=template&id=bbcd88f8&scoped=true& */ "./resources/js/front/components/layouts/Main.vue?vue&type=template&id=bbcd88f8&scoped=true&");
/* harmony import */ var _Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Main.vue?vue&type=script&lang=js& */ "./resources/js/front/components/layouts/Main.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Main_vue_vue_type_template_id_bbcd88f8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Main_vue_vue_type_template_id_bbcd88f8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "bbcd88f8",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/front/components/layouts/Main.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/front/components/layouts/Main.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/front/components/layouts/Main.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/Main.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/front/components/layouts/Main.vue?vue&type=template&id=bbcd88f8&scoped=true&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/Main.vue?vue&type=template&id=bbcd88f8&scoped=true& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_template_id_bbcd88f8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=template&id=bbcd88f8&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/Main.vue?vue&type=template&id=bbcd88f8&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_template_id_bbcd88f8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_template_id_bbcd88f8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/front/components/layouts/Process.vue":
/*!***********************************************************!*\
  !*** ./resources/js/front/components/layouts/Process.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Process_vue_vue_type_template_id_2e140e18_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Process.vue?vue&type=template&id=2e140e18&scoped=true& */ "./resources/js/front/components/layouts/Process.vue?vue&type=template&id=2e140e18&scoped=true&");
/* harmony import */ var _Process_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Process.vue?vue&type=script&lang=js& */ "./resources/js/front/components/layouts/Process.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Process_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Process_vue_vue_type_template_id_2e140e18_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Process_vue_vue_type_template_id_2e140e18_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "2e140e18",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/front/components/layouts/Process.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/front/components/layouts/Process.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/Process.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Process_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Process.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/Process.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Process_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/front/components/layouts/Process.vue?vue&type=template&id=2e140e18&scoped=true&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/front/components/layouts/Process.vue?vue&type=template&id=2e140e18&scoped=true& ***!
  \******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Process_vue_vue_type_template_id_2e140e18_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Process.vue?vue&type=template&id=2e140e18&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/front/components/layouts/Process.vue?vue&type=template&id=2e140e18&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Process_vue_vue_type_template_id_2e140e18_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Process_vue_vue_type_template_id_2e140e18_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);