$(document).ready(function () {
    var key = '4cc0b3264815fc79c9a1b2a6e5efdc2a',
        fTemp, cTemp, myCity, myCountry, icon, desc, unit;
    //Gets location from IP address
    function getLocation() {
        $.getJSON('https://ipapi.co/json/', function(data){
            var lat = data.latitude;
            var long = data.longitude;
            myCity = data.city;
            myCountry = data.country_name;
            getWeather(lat, long);
        });
    }
    //Gets weather from API
    function getWeather(lat, long) {
        var api = 'https://api.darksky.net/forecast/' + key + '/' + lat + ',' + long + '?callback=?';
        $.getJSON(api, function (data) {
            icon = data.currently.icon;
            desc = data.currently.summary;

            fTemp = Math.round(data.currently.temperature);
            cTemp = Math.round((fTemp - 32) * 5 / 9);
            unit = '°C';

            $('#temp').html(cTemp);
            $('#loc').html(myCity + ' - ' + myCountry);
            $('#weatherTxt').html(desc);
            $('#unit').html(unit);

            getImage(icon);
            getIcon(icon);
        });
    }
    //Gets background image
    function getImage(icon) {
        if (icon === 'clear-night'){
            $('#bg-image').css('background-image', 'url(https://static.pexels.com/photos/1683/sky-night-romantic-blue.jpg)');
        } else if (icon === 'partly-cloudy-day'){
            $('#bg-image').css('background-image', 'url(https://cdn.wallpapersafari.com/62/74/a2jd0u.jpg)');
        } else if (icon === 'partly-cloudy-night'){
            $('#bg-image').css('background-image', 'url(http://cdn.wallpapersafari.com/70/90/swaLxc.jpg)');
        } else if (icon === 'cloudy') {
            $('#bg-image').css('background-image', 'url(https://images.pexels.com/photos/158163/clouds-cloudporn-weather-lookup-158163.jpeg?w=940&h=650&auto=compress&cs=tinysrgb)');
        } else if (icon === 'rain') {
            $('#bg-image').css('background-image', 'url(https://images.pexels.com/photos/110874/pexels-photo-110874.jpeg?w=940&h=650&auto=compress&cs=tinysrgb)');
        } else if (icon === 'sleet') {
            $('#bg-image').css('background-image', 'url(https://www.pixelstalk.net/wp-content/uploads/2016/08/Rain-Window-Background.jpg)');
        } else if (icon === 'snow') {
            $('#bg-image').css('background-image', 'url(https://static.pexels.com/photos/58098/pexels-photo-58098.jpeg)');
        } else if (icon ==='wind') {
            $('#bg-image').css('background-image', 'url(https://static.pexels.com/photos/203272/pexels-photo-203272.jpeg)');
        } else if (icon === 'fog') {
            $('#bg-image').css('background-image', 'url(https://static.pexels.com/photos/249074/pexels-photo-249074.jpeg)');
        }
    }
    //Gets the weather icon
    function getIcon(icon) {
        if (icon === 'clear-night') {
            $('.wi').addClass('wi-night-clear');
        } else if (icon === 'partly-cloudy-day') {
            $('.wi').addClass('wi-day-cloudy');
        } else if (icon === 'partly-cloudy-night') {
            $('.wi').addClass('wi-night-alt-partly-cloudy');
        } else if (icon === 'cloudy') {
            $('.wi').addClass('wi-cloudy');
        } else if (icon === 'rain') {
            $('.wi').addClass('wi-rain');
        } else if (icon === 'sleet') {
            $('.wi').addClass('wi-sleet');
        } else if (icon === 'snow') {
            $('.wi').addClass('wi-snow');
        } else if (icon === 'wind') {
            $('.wi').addClass('wi-strong-wind');
        } else if (icon === 'fog') {
            $('.wi').addClass('wi-fog');
        } else {
            $('.wi').addClass('wi-day-sunny');
        }
    }
    getLocation();
    //Buttons functionality
    $('#btn-C').click(function () {
        unit = '°C';
        $('#temp').html(cTemp);
        $('#btn-C').attr('disabled', true);
        $('#btn-F').removeAttr("disabled");
        $('#unit').html(unit);
    });
    $('#btn-F').click(function () {
        unit = '°F';
        $('#temp').html(fTemp);
        $('#btn-F').attr('disabled', true);
        $('#btn-C').removeAttr('disabled');
        $('#unit').html(unit);
    });
    //Gets date
    var date1 = new Date();
    $(".date").append(date1.toDateString());
});
