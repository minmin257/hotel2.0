(function($) {
    "use strict";
    $(window).on('load', function() {
        $("#loading").fadeOut(500);
        $("#text_rotating").Morphext({
            animation: "fadeInDown",
            separator: ",",
            speed: 5000,
            complete: function() {}
        })
    });
    $(document).ready(function() {
        var wow = new WOW({
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: !0,
            live: !0,
            callback: function(box) {}
        });
        wow.init();
        $(window).on("scroll", function() {
            var header = $('header');
            var topmenu = $('.top_menu');
            var windowheight = $(this).scrollTop();
            var menuheight = header.outerHeight();
            var topmenuheight = 0;
            var adminbar = $('#wpadminbar');
            adminbar.css('position', 'fixed');
            if (adminbar.length && adminbar.is(':visible')) {
                header.css('top', adminbar.height())
            }
            if (topmenu.length > 0) {
                var topmenuheight = topmenu.outerHeight()
            }
            var fixedheight = topmenuheight;
            if (header.length > 0) {
                if ((windowheight > fixedheight) && header.hasClass("fixed")) {
                    header.addClass('navbar-fixed-top');
                    if (!header.hasClass("transparent")) {
                        header.next("*").css("margin-top", menuheight)
                    }
                    if (header.hasClass("fixed")) {
                        header.addClass("scroll");
                        header.addClass("nav_bg")
                    }
                    $("header .light").addClass("nodisplay");
                    $("header .dark").removeClass("nodisplay")
                } else {
                    header.removeClass("navbar-fixed-top");
                    if (!header.hasClass("transparent")) {
                        header.next("*").css("margin-top", "0")
                    }
                    if (header.hasClass("fixed")) {
                        header.removeClass("scroll");
                        header.removeClass("nav_bg")
                    }
                    $("header .dark").addClass("nodisplay");
                    $("header .light").removeClass("nodisplay");
                    if (adminbar.length && adminbar.is(':visible')) {
                        header.css('top', topmenuheight)
                    }
                }
            }
        });
        $(function() {
            function toggleNavbarMethod() {
                if ($(window).width() > 992) {
                    $(".dropdown").on({
                        mouseenter: function() {
                            $(this).addClass("open")
                        },
                        mouseleave: function() {
                            $(this).removeClass("open")
                        }
                    });
                    $('.dropdown-toggle').on('click', function(event) {
                        if ($(this).attr("href") == "#") {
                            event.preventDefault()
                        }
                    })
                } else {
                    $('.mobile-dropdown-toggle').off('mouseover').off('mouseout');
                    $('.mobile-dropdown-toggle').on('click', function(event) {
                        $(this).parent().parent().toggleClass("open");
                        $(this).toggleClass("arrow arrow-up");
                        event.preventDefault()
                    })
                }
            }
            toggleNavbarMethod();
            $(window).on("resize", (toggleNavbarMethod));
            $(".navbar-toggle").on("click", function() {
                $(this).toggleClass("active")
            })
        });
        $('.mobile_menu_btn').jPushMenu({
            closeOnClickLink: !1
        });
        if ($('.top_menu').length === 0) {
            $('header').addClass("no-topbar")
        }
        $('#countdown').each(function() {
            var $this = $(this),
                finalDate = $(this).data('countdown');
            $this.countdown(finalDate, function(event) {
                $this.html(event.strftime('<div class="count_box"><div class="inner"><div class="count_number">%D</div><div class="count_text">Days</div></div></div> ' + '<div class="count_box"><div class="inner"><div class="count_number">%H</div><div class="count_text">Hours</div></div></div> ' + '<div class="count_box"><div class="inner"><div class="count_number">%M</div><div class="count_text">Minutes</div></div></div> ' + '<div class="count_box"><div class="inner"><div class="count_number">%S</div><div class="count_text">Seconds</div><div></div>'))
            })
        });
        var options = {
            useEasing: !0,
            useGrouping: !1,
            separator: ',',
            decimal: '.',
            prefix: '',
            suffix: ''
        };
        $('.countup-item').on('inview', function(event, visible) {
            if (visible) {
                $.each($('.number'), function() {
                    var count = $(this).data('count'),
                        numAnim = new CountUp(this, 0, count);
                    numAnim.start()
                });
                $(this).unbind('inview')
            }
        });
        $(function() {
            var $grid = $('.grid').isotope({
                itemSelector: '.g_item'
            });
            $('.grid_filters').on('click', 'a', function(e) {
                e.preventDefault();
                var filterValue = $(this).attr('data-filter');
                $grid.isotope({
                    filter: filterValue
                })
            });
            $('.grid_filters').each(function(i, buttonGroup) {
                var $buttonGroup = $(buttonGroup);
                $buttonGroup.on('click', 'a', function() {
                    $buttonGroup.find('.active').removeClass('active');
                    $(this).addClass('active')
                })
            });
            if ($(".grid").length) {
                $grid.imagesLoaded().progress(function() {
                    $grid.isotope('layout')
                })
            }
        });
        $(".magnific-popup, a[data-rel^='magnific-popup']").magnificPopup({
            type: 'image',
            mainClass: 'mfp-with-zoom',
            zoom: {
                enabled: !0,
                duration: 300,
                easing: 'ease-in-out',
                fixedContentPos: !0,
                opener: function(openerElement) {
                    return openerElement.is('img') ? openerElement : openerElement.find('img')
                }
            },
            retina: {
                ratio: 1,
                replaceSrc: function(item, ratio) {
                    return item.src.replace(/\.\w+$/, function(m) {
                        return '@2x' + m
                    })
                }
            }
        });
        $('.image-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            fixedContentPos: !0,
            gallery: {
                enabled: !0
            },
            removalDelay: 300,
            mainClass: 'mfp-fade',
            retina: {
                ratio: 1,
                replaceSrc: function(item, ratio) {
                    return item.src.replace(/\.\w+$/, function(m) {
                        return '@2x' + m
                    })
                }
            }
        });
        $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 300,
            preloader: !1,
            fixedContentPos: !0,
        });
        $('[data-toggle="popover"]').popover({
            html: !0,
            offset: '0 10px',
            container: 'body',
        });
        $('[data-toggle="tooltip"]').tooltip({
            animated: 'fade',
            container: 'body',
            offset: '0 5px'
        });
        var amountScrolled = 500;
        var back_to_top = $('#back_to_top');
        $(window).on('scroll', function() {
            if ($(window).scrollTop() > amountScrolled) {
                back_to_top.addClass('active')
            } else {
                back_to_top.removeClass('active')
            }
        });
        back_to_top.on('click', function() {
            $('html, body').animate({
                scrollTop: 0
            }, 500);
            return !1
        });
        $('.footer-language-switcher .selected-language').on('click', function() {
            $(this).parent().toggleClass('open')
        });
        $(window).click(function() {
            $('.footer-language-switcher').removeClass('open')
        });
        $('.footer-language-switcher').on('click', function(event) {
            event.stopPropagation()
        })
    })
})(jQuery)






function setREVStartSize(e) {
    //window.requestAnimationFrame(function() {				 
    window.RSIW = window.RSIW === undefined ? window.innerWidth : window.RSIW;
    window.RSIH = window.RSIH === undefined ? window.innerHeight : window.RSIH;
    try {
        var pw = document.getElementById(e.c).parentNode.offsetWidth,
            newh;
        pw = pw === 0 || isNaN(pw) ? window.RSIW : pw;
        e.tabw = e.tabw === undefined ? 0 : parseInt(e.tabw);
        e.thumbw = e.thumbw === undefined ? 0 : parseInt(e.thumbw);
        e.tabh = e.tabh === undefined ? 0 : parseInt(e.tabh);
        e.thumbh = e.thumbh === undefined ? 0 : parseInt(e.thumbh);
        e.tabhide = e.tabhide === undefined ? 0 : parseInt(e.tabhide);
        e.thumbhide = e.thumbhide === undefined ? 0 : parseInt(e.thumbhide);
        e.mh = e.mh === undefined || e.mh == "" || e.mh === "auto" ? 0 : parseInt(e.mh, 0);
        if (e.layout === "fullscreen" || e.l === "fullscreen")
            newh = Math.max(e.mh, window.RSIH);
        else {
            e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
            for (var i in e.rl)
                if (e.gw[i] === undefined || e.gw[i] === 0) e.gw[i] = e.gw[i - 1];
            e.gh = e.el === undefined || e.el === "" || (Array.isArray(e.el) && e.el.length == 0) ? e.gh : e.el;
            e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
            for (var i in e.rl)
                if (e.gh[i] === undefined || e.gh[i] === 0) e.gh[i] = e.gh[i - 1];

            var nl = new Array(e.rl.length),
                ix = 0,
                sl;
            e.tabw = e.tabhide >= pw ? 0 : e.tabw;
            e.thumbw = e.thumbhide >= pw ? 0 : e.thumbw;
            e.tabh = e.tabhide >= pw ? 0 : e.tabh;
            e.thumbh = e.thumbhide >= pw ? 0 : e.thumbh;
            for (var i in e.rl) nl[i] = e.rl[i] < window.RSIW ? 0 : e.rl[i];
            sl = nl[0];
            for (var i in nl)
                if (sl > nl[i] && nl[i] > 0) {
                    sl = nl[i];
                    ix = i;
                }
            var m = pw > (e.gw[ix] + e.tabw + e.thumbw) ? 1 : (pw - (e.tabw + e.thumbw)) / (e.gw[ix]);
            newh = (e.gh[ix] * m) + (e.tabh + e.thumbh);
        }
        if (window.rs_init_css === undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));
        document.getElementById(e.c).height = newh + "px";
        window.rs_init_css.innerHTML += "#" + e.c + "_wrapper { height: " + newh + "px }";
    } catch (e) {
        console.log("Failure at Presize of Slider:" + e)
    }
    //});
};